<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\PhpExecutableFinder;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;


class NCDeployLoader extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nc:deployloader';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Narrow Casting deploy for all active project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $prefix = "";
        $os = "";
        date_default_timezone_set('Europe/Amsterdam');

        if(Str::contains(PHP_OS_FAMILY, 'Windows'))
        {
            $os = "Windows";
        }

        $phpFinder = new PhpExecutableFinder;
        if (!$phpPath = $phpFinder->find()) {
            throw new \Exception('The php executable could not be found, add it to your PATH environment variable and try again');
        }
        $projectPath = base_path();

        $project_unfinished = DB::table("nc_project")
        ->where("status","=","3")
        ->OrderBy("id_project","ASC")
        ->get();

        foreach ($project_unfinished as $project) {
            echo $project->id_project;
            $total_project_deploy = DB::table('nc_project_dtl')
            ->where("id_project", "=", $project->id_project)
            ->count();
            
            $total_project_finish = DB::table('nc_project_dtl')
            ->where("id_project", "=", $project->id_project)
            ->where("isfinish","=","1")
            ->count();
            
            $total_project_notfinish = DB::table('nc_project_dtl')
            ->where("id_project", "=", $project->id_project)
            ->where("isfinish","=","3")
            ->count();

            $total_project_remaining = DB::table('nc_project_dtl')
            ->where("id_project", "=", $project->id_project)
            ->where("isfinish","=","0")            
            ->orwhere("isfinish","=","3")
            ->where("id_project", "=", $project->id_project)
            ->OrderBy("id_project","ASC")
            ->limit("5")
            ->count();

            echo "\nTotal Deployment : ".$total_project_deploy."\n";
            echo "Finished : ".$total_project_finish."\n";
            echo "UnFinished : ".$total_project_notfinish."\n";
            echo "Remaining : ".$total_project_remaining."\n";


            if($total_project_deploy == $total_project_finish)
            {
                /*
                Target deployment is matched finished deployment,
                we must set the Main Project status to completed (1 = Finish)
                */

                echo "This Deployment is Completed\n";
                DB::table('nc_project')
                ->where("id_project", "=", $project->id_project)
                ->update(
                    [
                        "status"   => "1",
                        "proj_fin" => date("Y-m-d H:i:s")
                    ]
                );

            }

            if($total_project_remaining > 0)
            {
                echo "Queue Target : \n";

                $queue_deploy = DB::table('nc_project_dtl')
                ->join("fleets_vehicles", "nc_project_dtl.bus_id", "=", "fleets_vehicles.id_vehicle")
                ->select("nc_project_dtl.bus_id", "fleets_vehicles.ip_vehicle")
                ->where("nc_project_dtl.id_project", "=", $project->id_project)
                ->where("nc_project_dtl.isfinish", "=", '0')
                ->where("nc_project_dtl.isqueue", "=", '1')

                ->orwhere("nc_project_dtl.isfinish", "=", '3')
                ->where("nc_project_dtl.id_project", "=", $project->id_project)
                ->where("nc_project_dtl.isqueue", "!=", '1')
                ->orderBy("nc_project_dtl.id_project","ASC")
                ->limit("5")
                ->get();
               

                if($queue_deploy->count() == 0)
                {
                    //queue is empty, reset queue list which target deployment still not finish
                    DB::table('nc_project_dtl')
                    ->where("id_project", "=", $project->id_project)
                    ->where("isfinish", "!=", "1")
                    ->update([
                        "isqueue" => "0"
                    ]);
                    
                }else{
                    foreach ($queue_deploy as $queue) {
                        //execute the queue, deploy to the target
                        //echo " - ".$queue->bus_id."\n";
                        $exec_params = $project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle." ";
                        
                        // DB::table("nc_project_dtl")
                        // ->where("id_project", "=", $project->id_project)
                        // ->where("bus_id", "=", $queue->bus_id)
                        // ->update(
                        //     [
                        //         "isfinish"  => "2",
                        //         "isqueue"   => "1"
                        //     ]
                        // );

                        echo $exec_params."\n";
                        if($os == "Windows")
                        {
                            echo $phpPath;
                            //exec($phpPath.' -f artisan nc:deployer');
                            $arg1 = escapeshellarg('nc:deployer');
                            $arg2 = escapeshellarg('a');
                            $arg3 = escapeshellarg('b');
                            $arg4 = escapeshellarg('c');
                            $cmd = $phpPath.' "'.base_path().DIRECTORY_SEPARATOR.'artisan"';
                            pclose(popen("start /B ". $cmd." ".$arg1." ".$arg2." ".$arg3." ".$arg4, "w"));
                            pclose(popen("start /B ". $cmd." ".$arg1." ".$arg2." ".$arg3." ".$arg4, "w"));
                            pclose(popen("start /B ". $cmd." ".$arg1." ".$arg2." ".$arg3." ".$arg4, "w"));
                            pclose(popen("start /B ". $cmd." ".$arg1." ".$arg2." ".$arg3." ".$arg4, "w"));
                            pclose(popen("start /B ". $cmd." ".$arg1." ".$arg2." ".$arg3." ".$arg4, "w"));
                            pclose(popen("start /B ". $cmd." ".$arg1." ".$arg2." ".$arg3." ".$arg4, "w"));
                            pclose(popen("start /B ". $cmd." ".$arg1." ".$arg2." ".$arg3." ".$arg4, "w"));
                            pclose(popen("start /B ". $cmd." ".$arg1." ".$arg2." ".$arg3." ".$arg4, "w"));
                            pclose(popen("start /B ". $cmd." ".$arg1." ".$arg2." ".$arg3." ".$arg4, "w"));
                            // echo "windows running deployer\n";
                            // echo base_path()."\n";
                            // echo $phpPath.' "'.base_path().DIRECTORY_SEPARATOR.'artisan" nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle;
                           // $process = new Process([$phpPath.' artisan nc:deployer', base_path()]); 
                           
                            // dispatch(function () use($project,$queue) {
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            //     Artisan::call('nc:deployer '.$project->id_project." ".$queue->bus_id." ".$queue->ip_vehicle);
                            // });
                            //exec('start /b '.getenv("pathloc").' -f phpsecuploadnew.php '.$uploadexec);
                        }else{
                            echo "linux running deployer\n";
                            //exec(getenv("pathloc").' phpsecuploadnew.php '.$uploadexec.' > /dev/null 2>/dev/null &');
                        }
                    }
                    echo "\n";
                }
                
                
                
            }
            echo "\n";



        }
        
        // while (true)
        // {
        //     echo rand();
        //     sleep(2);
        // }

        return 0;
    }
}
