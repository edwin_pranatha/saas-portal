<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class NCDeployer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nc:deployer {project_id} {bus_id} {bus_ip}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy Narrow Casting configuration to given project_id bus_id  bus_ip';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $project_id = $this->argument('project_id');
        $bus_id     = $this->argument('bus_id');
        $bus_ip     = "192.168.87.87";//$this->argument('bus_ip');
        $port       = 22;
        $username   = "root";
        $password   = "toor";
        $prefix     = "";
        $local_dir  = public_path();
        define("DS",DIRECTORY_SEPARATOR);
        $remote_path_asset_config   =   "/opt/mnp/nc/";
        $remote_path_asset_img      =   "/opt/mnp/nc/img/";
        $remote_path_asset_vid      =   "/opt/mnp/nc/media/";
        $local_path_asset_img       =   $local_dir.DS."nc_images";
        $local_path_asset_vid       =   $local_dir.DS."nc_video";
        $local_path_asset_cfg       =   $local_dir.DS."nc_config";
        // $local_path_asset_img       =   $local_dir."engine".DS."plugins".DS."mt.narrowcasting".DS."asset_img".DS;
        // $local_path_asset_vid       =   $local_dir."engine".DS."plugins".DS."mt.narrowcasting".DS."asset_video".DS;
        // $local_path_asset_cfg       =   $local_dir."engine".DS."plugins".DS."mt.narrowcasting".DS."asset_config".DS;


        date_default_timezone_set('Europe/Amsterdam');
        echo "\nRetrieved :".$project_id." ".$bus_id."  ".$bus_ip;

        $config = DB::table('nc_config')->get()->keyBy('cfg')->toArray();
        foreach ($config as $key => $configs) {
            unset($configs->cfg);
        }
        $config = (object) $config;
        $prefix_path = "";
        if($prefix !== "")
        {
            $prefix_path = $prefix.DS;
        }

        $isonline = $this->check_port($bus_ip, $port);
        if($isonline=="ONLINE")
		{
            //check as bus is online
            echo "\nonline!\n";
            $sftp = new \phpseclib\Net\SFTP($bus_ip,$port);
            if (!$sftp->login($username, $password)){
                echo "login failed!";
                DB::table("nc_project_dtl")
                ->where("id_project","=",$project_id)
                ->where("bus_id","=",$bus_id)
                ->update([
                    "isfinish"          => "3",
                    "retry_deploy"      => date("Y-m-d H:i:s"),
                    "lasttry_deploy"    => date("Y-m-d H:i:s")
                ]);
			}else{
                echo "login success!\n";
                $sftp->setTimeout(0);
                if($config->dep_tmp->val == '1' && $sftp->is_dir(DS."opt".DS."mnp".DS."nc".DS) == false)
		        {
		            $sftp->mkdir(DS."tmp".DS."nc");
		            $sftp->mkdir(DS."tmp".DS."nc".DS."img");
		            $sftp->mkdir(DS."tmp".DS."nc".DS."media");
		        }

                //$assets_list = file($local_dir."engine".DS."plugins".DS."mt.narrowcasting".DS."asset_config".$prefix_path.DS.$project_id.DS."asset_list.txt", FILE_IGNORE_NEW_LINES);
                //$assetdat =   str_replace(array($local_dir."engine".DS."plugins".DS."mt.narrowcasting".DS."asset_img".DS.$prefix_path,$local_dir."engine".DS."plugins".DS."mt.narrowcasting".DS."asset_video".DS.$prefix_path.DS),array("",""), $assets_list);
                
                $assets_list =  file(public_path().DS.'nc_config'.DS.$project_id.DS."asset_list.txt", FILE_IGNORE_NEW_LINES);
                $assets_dat =   str_replace(array($local_dir."engine".DS."plugins".DS."mt.narrowcasting".DS."asset_img".DS.$prefix_path,$local_dir."engine".DS."plugins".DS."mt.narrowcasting".DS."asset_video".DS.$prefix_path.DS),array("",""), $assets_list);
                
                $img_to_upload = array();
                $vid_to_upload = array();
                foreach ($assets_dat as $key => $val) {
                    $ext=explode(".",$val);
                    $idx=count($ext)-1;
                    if($ext[$idx]=="jpg" || $ext[$idx]=="png" || $ext[$idx]=="gif" || $ext[$idx]=="bmp"){
                        $img_to_upload[]=$val;
                    }else if($ext[$idx]=="mp4"){
                        $vid_to_upload[]=$val;
                    }
                }

                DB::table("nc_project_dtl")
                ->where("id_project", "=", $project_id)
                ->where("bus_id", "=", $bus_id)
                ->update(
                    [
                        "upload_stat"       =>  "Preparing...",
                        "upload_stage"      =>  "",
                        "upload_percent"    =>  "Uploading 0/".count($assets_dat)
                    ]
                );

                $upload_stage = "";
                $upload_count = 0;
                $debug_out = "";
                $error_count = 0;
                
                $debug_out = $debug_out."====================================================\n";
                $debug_out = $debug_out."||                 CRC INFORMATION                ||\n";
                $debug_out = $debug_out."====================================================\n";
                

                /*
                /* IMAGE PROCESSING
                */
                if(!empty($img_to_upload))
                {
                    $img_error=array();
                    //iterate through assets list for upload
                    for ($i=0; $i < count($img_to_upload); $i++){
                        /*
                        /* BEGIN UPLOAD the asset images
                        */
                        $img_to_upload_now = "";
                        $img_upload_error = "";
                        if($config->dep_exptag->val == "1"){
                            $img_to_upload_now = substr($img_to_upload[$i],9);
                        }else{
                            $img_to_upload_now = $img_to_upload[$i];
                        }

                        DB::table("nc_project_dtl")
                        ->where("id_project", "=", $project_id)
                        ->where("bus_id", "=", $bus_id)
                        ->update(
                            [
                                "upload_stat"       =>  "Uploading ".$img_to_upload_now
                            ]
                        );

                        if(file_exists($local_path_asset_img.$prefix_path.DS.$img_to_upload_now))
                        {
                            $sftp->put($remote_path_asset_img.$img_to_upload_now, $local_path_asset_img.$prefix_path.DS.$img_to_upload_now,1);
                            $img_upload_error = $sftp->getSFTPErrors();
                        }else{
                            $debug_out = $debug_out."[File Missing] ".$img_to_upload_now." is not found in local directory\n";
                        }

                        $upload_stage=$upload_stage.",".$img_to_upload_now;
                        if(mb_substr(trim($upload_stage), 0, 1)==","){
                            $upload_stage=substr(trim($upload_stage), 1);
                        }

                        $upload_count = $upload_count+1;
                        $img_error[$img_to_upload_now] = $img_upload_error;

                        /*
                        /* VERIFY CRC PROCESS
                        /* Double check the uploaded file is correctly match with local file (HASH check)
                        */
                        if(file_exists($local_path_asset_img.$prefix_path.DS.$img_to_upload_now))
                        {
                            $local_file_size = filesize($local_path_asset_img.$prefix_path.DS.$img_to_upload_now);
                            $remote_file_size = $sftp->size($remote_path_asset_img.$img_to_upload_now);
                            if($local_file_size == $remote_file_size)
                            {
                                $debug_out = $debug_out."[CRC Match] ".$img_to_upload_now."\n";
                            }else{
                                $debug_out = $debug_out."[CRC Invalid] ".$img_to_upload_now."\n";
                                $error_count = $error_count+1;
                                //retry upload here
                            }

                        }
                        
                        
                        DB::table("nc_project_dtl")
                        ->where("id_project", "=", $project_id)
                        ->where("bus_id", "=", $bus_id)
                        ->update(
                            [
                                "upload_stage"   =>  $upload_stage,                                
                                "upload_percent" => "Uploading ".$upload_count."/".count($assets_dat)
                            ]
                        );

                    }
                }else{
                    $img_error="no img file";
                }


                /*
                /* VIDEO PROCESSING
                */
                if(!empty($vid_to_upload))
                {
                    $vid_error = array();
                    for ($i=0; $i < count($vid_to_upload); $i++) {
                        /*
                        /* BEGIN UPLOAD the asset videos
                        */
                        $vid_to_upload_now = "";
                        $vid_upload_error = "";

                        if($config->dep_exptag->val == "1"){
                            $vid_to_upload_now = substr($vid_to_upload[$i],9);
                        }else{
                            $vid_to_upload_now = $vid_to_upload[$i];
                        }

                        DB::table("nc_project_dtl")
                        ->where("id_project", "=", $project_id)
                        ->where("bus_id", "=", $bus_id)
                        ->update(
                            [
                                "upload_stat"       =>  "Uploading ".$vid_to_upload_now
                            ]
                        );

                        if(file_exists($local_path_asset_vid.$prefix_path.DS.$vid_to_upload_now))
                        {
                            $sftp->put($remote_path_asset_vid.$vid_to_upload_now, $local_path_asset_vid.$prefix_path.DS.$vid_to_upload_now,1);
                            $vid_upload_error = $sftp->getSFTPErrors();
                        }else{
                            $debug_out = $debug_out."[File Missing] ".$vid_to_upload_now." is not found in local directory\n";
                        }

                        $upload_stage = $upload_stage.",".$vid_to_upload_now;
                        $upload_count = $upload_count+1;
                        $vid_error[$vid_to_upload_now]=$vid_upload_error;

                        /*
                        /* VERIFY CRC PROCESS
                        /* Double check the uploaded file is correctly match with local file (HASH check)
                        */
                        if(file_exists($local_path_asset_vid.$prefix_path.DS.$vid_to_upload_now))
                        {
                            $local_file_size = filesize($local_path_asset_vid.$prefix_path.DS.$vid_to_upload_now);
                            $remote_file_size = $sftp->size($remote_path_asset_vid.$vid_to_upload_now);
                            if($local_file_size == $remote_file_size)
                            {
                                $debug_out = $debug_out."[CRC Match] ".$vid_to_upload_now."\n";
                            }else{
                                $debug_out = $debug_out."[CRC Invalid] ".$vid_to_upload_now."\n";
                                $error_count = $error_count+1;
                                //retry upload here
                            }

                        }

                        DB::table("nc_project_dtl")
                        ->where("id_project", "=", $project_id)
                        ->where("bus_id", "=", $bus_id)
                        ->update(
                            [
                                "upload_stage"   =>  $upload_stage,                                
                                "upload_percent" => "Uploading ".$upload_count."/".count($assets_dat)
                            ]
                        );
                    }
                }else{
                    $vid_error="no video file";
                }
                /*
                /* 
                    BEGIN UPLOAD the asset bus.ini
                */
                $sftp->put($remote_path_asset_config."bus.ini", $local_path_asset_cfg.$prefix.DS.$project_id."/bus.ini",1);
                $local_busini_size = filesize($local_path_asset_cfg.$prefix.DS.$project_id."/bus.ini");
                $remote_busini_size = $sftp->size($remote_path_asset_config."bus.ini");
                if($local_busini_size == $remote_busini_size)
                {
                    $debug_out=$debug_out."[CRC Match] bus.ini \n";
                }else{
                    $debug_out=$debug_out."[CRC Invalid] bus.ini \n";
                    $error_count=$error_count+1;
                }
                $busini_error = $sftp->getSFTPErrors();

                $debug_out = $debug_out."====================================================\n";
                $debug_out = $debug_out."||                 UPLOAD STATUS                  ||\n";
                $debug_out = $debug_out."====================================================\n";
                if(is_array($img_error) && count($img_error)>0)
                {
                    foreach ($img_error as $key => $val) {
                        if(count($val)>0)
                        {
                            foreach ($val as $value) {
                                $debug_out = $debug_out."[NOK] ".$key." ".$value."\n";
                            }
                        }else{
                            $debug_out = $debug_out."[OK] ".$key."\n";
                        }
                    }
                }

                if(is_array($vid_error) && count($vid_error)>0)
		        {
		        	foreach ($vid_error as $key => $val) {
		        		if(count($val)>0)
		        		{
		        			foreach ($val as $value) {
		        				$debug_out = $debug_out."[NOK] ".$key." ".$value."\n";
		        			}
		        		}else{
		        			$debug_out = $debug_out."[OK] ".$key."\n";
		        		}
		        	}
		        }else{
		        	$debug_out = $debug_out."[OK] There is no video assets\n";
		        }

		        if(is_array($busini_error) && count($busini_error)>0)
		        {
		        	foreach ($busini_error as $key => $val) {
		        		if(count($val)>0)
		        		{
		        			$debug_out = $debug_out."[NOK] ".$key." ".$val."\n";
		        		}else{
		        			$debug_out = $debug_out."[OK] ".$key." ".$val."\n";
		        		}
		        	}
		        }else{
		        	$debug_out = $debug_out."[OK] There is no error on bus.ini\n";
		        }

                //Update Project Status of Bus
		        $crc_fix=0;
		        if($error_count>0)
				{
					$crc_status = "error";
					$crc_fix=1;
                    DB::table("nc_project_dtl")
                    ->where("id_project", "=", $project_id)
                    ->where("bus_id", "=", $bus_id)
                    ->update(
                        [
                            "upload_lasterr"  =>  "Upload NOK, CRC Failed",
                            "isfinish"        =>  "3",
                            "retry_deploy"    =>  date("Y-m-d H:i:s"),
                            "lasttry_deploy"  =>  date("Y-m-d H:i:s")
                        ]
                    );
                    
				}else{
					$crc_status = "passed";
					$crc_fix = 0;

                    DB::table("nc_project_dtl")
                    ->where("id_project", "=", $project_id)
                    ->where("bus_id", "=", $bus_id)
                    ->update(
                        [
                            "debug_output"    =>  $debug_out,
                            "error_cnt"       =>  $error_count,
                            "upload_lasterr"  =>  "",
                            "upload_stat"     =>  "Upload OK",
                            "upload_percent"  =>  "Finished [100%]",
                            "isfinish"        =>  "1",
                            "finish_deploy"   =>  date("Y-m-d H:i:s"),
                            "lasttry_deploy"  =>  date("Y-m-d H:i:s")
                        ]
                    );

                    if($config->dep_refresh->val == "1"){
                        $sockbus = @fsockopen($bus_ip, "9695", $errno, $errstr);
			            if($sockbus==false){
                            DB::table("nc_project_dtl")
                            ->where("id_project", "=", $project_id)
                            ->where("bus_id", "=", $bus_id)
                            ->update(
                                [
                                    "upload_stat"     =>  "Upload OK, Refresh NOK",
                                    "upload_lasterr"  =>  "",
                                    "lasttry_deploy"  =>  date("Y-m-d H:i:s")
                                ]
                            );
			            }else{
                            DB::table("nc_project_dtl")
                            ->where("id_project", "=", $project_id)
                            ->where("bus_id", "=", $bus_id)
                            ->update(
                                [
                                    "upload_stat"     =>  "Upload OK, Refresh OK",
                                    "upload_lasterr"  =>  "",
                                    "lasttry_deploy"  =>  date("Y-m-d H:i:s")
                                ]
                            );
			            }
			            if($sockbus){@fclose($sockbus);}else{@fclose($sockbus);}
                    }else{
                        DB::table("nc_project_dtl")
                        ->where("id_project", "=", $project_id)
                        ->where("bus_id", "=", $bus_id)
                        ->update(
                            [
                                "upload_stat"     =>  "Upload OK, Refresh DISABLED",
                                "upload_lasterr"  =>  "",
                                "lasttry_deploy"  =>  date("Y-m-d H:i:s")
                            ]
                        );
		            }
				}

				//Update The Table Active Project
                $check_active_project = DB::table("nc_project_active")
                ->select("bus_id")
                ->where("bus_id", "=", $bus_id)
                ->count();

                $check_project_revision = DB::table("nc_project")
                ->select("ver_project","proj_nm","layout_apply")
                ->where("id_project", "=", $project_id)
                ->first();

		       if($check_active_project > 0){
                   DB::table("nc_project_active")
                   ->where("bus_id", "=", $bus_id)
                   ->update(
                       [
                           "CRC_output"     =>  $debug_out,
                           "CRC_stat"  =>  $crc_status,
                           "crc_check_date"  =>  date("Y-m-d H:i:s"),
                           "CRCfix"  =>  $crc_fix,
                           "bus_id"  =>  $bus_id,
                           "bus_ip"  =>  $bus_ip,
                           "proj_id"  =>  $project_id,
                           "proj_ver"  =>  $check_project_revision->ver_project,
                           "proj_nm"  =>  $check_project_revision->proj_nm,
                           "layout_id"  =>  $check_project_revision->layout_apply,
                           "bus_assets"  =>  implode(",",$assets_dat),
                           "apply_date"  =>  date("Y-m-d H:i:s")
                       ]
                    );
		       }else{
                DB::table("nc_project_active")
                ->where("bus_id", "=", $bus_id)
                ->insert(
                    [
                        "bus_id"     =>  $bus_id,
                        "proj_id"  =>  $project_id,
                        "proj_ver"  => $check_project_revision->ver_project,
                        "proj_nm"  =>  $check_project_revision->proj_nm,
                        "layout_id"  =>  $check_project_revision->layout_apply,
                        "bus_assets"  =>  implode(",",$assets_dat),
                        "remark"  =>  '',
                        "CRC_output"  =>  $debug_out,
                        "CRC_stat"  =>  $crc_status,
                        "crc_check_date"  =>  date("Y-m-d H:i:s"),
                        "CRCfix"  =>  $crc_fix,
                        "apply_date"  =>  date("Y-m-d H:i:s")
                    ]
                 );
		       }


            }

	        echo $debug_out;
            return 0;
        }else{
            echo $bus_id."-".$bus_ip." is offline";
            DB::table("nc_project_dtl")
            ->where("bus_id", "=", $bus_id)
            ->where("id_project", "=", $project_id)
            ->update(
                [
                    "upload_lasterr"     =>  "Upload NOK, Offline",
                    "isfinish"  =>  "3",
                    "retry_deploy"  =>  date("Y-m-d H:i:s"),
                    "lasttry_deploy"  =>  date("Y-m-d H:i:s")
                ]
             );
        }


    }

    private function check_port($bus_ip, $port)
    {
        $server = array("OFFLINE", "ONLINE");
        $fp = @fsockopen($bus_ip, $port, $errno, $errstr, 2);
        if (!$fp) {return $server[0];}else{return $server[1];}
    }
}
