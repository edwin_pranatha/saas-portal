<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \Qirolab\Theme\Theme;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $environment = $this->app->make(\Hyn\Tenancy\Environment::class);
        $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

        //retrict only running from the normal http access
        if ( !app()->runningInConsole() ){

            if($website == null)
            {
                return false;
            }

            $hostname  = app(\Hyn\Tenancy\Environment::class)->hostname();
            Theme::set($hostname->theme);
            $this->loadViewsFrom(Theme::path(), 'theme');
        }else{
            auth()->getProvider()->setModel(\App\Models\System\User::class);
        }

        $environment->tenant($website);
    }
}
