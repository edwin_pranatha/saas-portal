@foreach($items as $item)
  <li @if($item->hasChildren()) class="dropdown" @endif>
  	  @if($item->hasChildren())
      	<a href="#" class="has-arrow waves-effect waves-dark" aria-expanded="false">{!! $item->title !!} </a>
      @else
      	<a href="{!! $item->url() !!}">{!! $item->title !!} </a>
      @endif
      @if($item->hasChildren())
        <ul aria-expanded="false" class="collapse">
              @include('theme::views.backend.menu', ['items' => $item->children()])
        </ul>
      @endif
  </li>
@endforeach