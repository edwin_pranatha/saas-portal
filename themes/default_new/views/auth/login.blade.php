<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    {{-- <link rel="icon" type="image/png" sizes="16x16" href="{{Theme::assets('img/logo_maestronic.png')}}"> --}}
    <title>Maestronic | Login Form</title>
    
    <!-- Scripts -->
    <script src="{{ mix('js/app_backend.js', 'themes/default') }}" defer></script>
    
    <!-- Styles -->
    <link href="{{ mix('css/app_backend.css', 'themes/default') }}" rel="stylesheet">

    {{-- <link href="{{Theme::assets('adminpress/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{Theme::assets('adminpress/css/style.css')}}" rel="stylesheet">
    <link href="{{Theme::assets('adminpress/css/colors/blue.css')}}" id="theme" rel="stylesheet"> 
    Theme::assets('adminpress/assets/images/background/login-register.jpg')url({{ }})
    --}}
</head>
<body>
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <section id="wrapper">
        <div class="login-register" style="background-image:url('/themes/{{Theme::active()}}/images/background/login-register.jpg');">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login') }}">
                        @csrf
                        <h3 class="box-title m-b-20">Sign In</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control  @error('email') is-invalid @enderror" id="email" type="email" placeholder="Email" name="email" value="" required autocomplete="email" autofocus> 
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control @error('password') is-invalid @enderror" id="password" type="password" placeholder="Password" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 font-14">
                                <div class="checkbox checkbox-primary pull-left p-t-0">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup"> Remember Me</label>
                                </div>
                            </div>
                        </div>
                        <div class="text-center form-group m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Login</button>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="text-center col-sm-12">
                                <div>Don't have an account? <a href="register" class="text-info m-l-5"><b>Register</b></a></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    {{-- <script src="{{ mix('js/backend.js', 'themes/default') }}" defer></script> --}}
    {{-- <script src="{{Theme::assets('adminpress/assets/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{Theme::assets('adminpress/assets/plugins/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{Theme::assets('adminpress/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{Theme::assets('adminpress/js/jquery.slimscroll.js')}}"></script>
    <script src="{{Theme::assets('adminpress/js/waves.js')}}"></script>
    <script src="{{Theme::assets('adminpress/js/sidebarmenu.js')}}"></script>
    <script src="{{Theme::assets('adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{Themen::assets('adminpress/assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{Theme::assets('adminpress/js/custom.min.js')}}"></script> --}}
</body>
</html>