const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath(global.public_theme_path);
mix.js(`${__dirname}/js/app_frontend.js`,`./js/app_frontend.js`);
mix.js(`${__dirname}/js/app_backend.js`,`./js/app_backend.js`);
mix.js(`${__dirname}/js/backend/sticky-kit.min.js`,`./js/app_backend.js`);
mix.js(`${__dirname}/js/backend/jquery.slimscroll.js`,`./js/app_backend.js`);
mix.js(`${__dirname}/js/backend/waves.js`,`./js/app_backend.js`);
mix.js(`${__dirname}/js/backend/sidebarmenu.js`,`./js/app_backend.js`);
mix.js(`${__dirname}/js/backend/custom.min.js`,`./js/app_backend.js`);

mix.js(`${__dirname}/js/frontend/scripts.js`, `./js/frontend.js`);


mix.setResourceRoot('../');

mix.options({
    fileLoaderDirs: {
        images: `./images`,
        fonts: `./fonts`
    }
});
mix.sass(`${__dirname}/sass/app_frontend.scss`,`./css/app_frontend.css`);
mix.sass(`${__dirname}/sass/app_backend.scss`,`./css/app_backend.css`);

mix.copyDirectory(`${__dirname}/assets/images/frontend/`, './'+global.public_theme_path+'/images/');
mix.copyDirectory(`${__dirname}/assets/images/backend/`, './'+global.public_theme_path+'/images/');