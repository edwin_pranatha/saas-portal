<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/frontend.min.css', 'themes/default') }}" rel="stylesheet">
</head>
{{-- <body>
    <div id="app">
        @include('layouts.navigation')

        <main class="py-4">
            {{ $slot }}
        </main>
    </div>
</body>
</html> --}}
<body data-spy="scroll" data-target=".fixed-top">
    
    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->
    
    <div id="header">
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-custom fixed-top" id="header">
            <!-- Text Logo - Use this if you don't have a graphic logo -->
            <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Evolo</a> -->

            <!-- Image Logo -->
            <a class="navbar-brand logo-image" href="index.html"><img src="/themes/{{Theme::active()}}/images/logo_maestronic.png" alt="alternative"></a> 
            
            <!-- Mobile Menu Toggle Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-awesome fas fa-bars"></span>
                <span class="navbar-toggler-awesome fas fa-times"></span>
            </button>
            <!-- end of mobile menu toggle button -->

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="ml-auto navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#header">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#about">About</a>
                    </li> 
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#services">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#pricing">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#register">Register</a>
                    </li>        
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
                <!-- <span class="nav-item social-icons">
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x facebook"></i>
                            <i class="fab fa-facebook-f fa-stack-1x"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x twitter"></i>
                            <i class="fab fa-twitter fa-stack-1x"></i>
                        </a>
                    </span>
                </span> -->
            </div>
        </nav> <!-- end of navbar -->
        <!-- end of navigation -->
    </div>

    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="/themes/{{Theme::active()}}/images/bus1.png" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5 class="title-slide">DRIVEN BY INNOVATION</h5>
                    <p class="text-pSlide">Cost-effective information solutions</br>for public transport</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="/themes/{{Theme::active()}}/images/bus2.png" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5 class="title-slide">DRIVEN BY INNOVATION</h5>
                    <p class="text-pSlide">Cost-effective information solutions</br>for public transport</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!-- About -->
    <div id="about" class="basic-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>About</h2>
                    <p class="p-heading p-large">Meat our team of specialized marketers and business developers which will help you research new products and launch them in new emerging markets</p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-4 -->
    <!-- end of about -->

    <!-- Details 1 -->
    <div class="basic-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>Design And Plan Your Business Growth Steps</h2>
                        <p>Use our staff and our expertise to design and plan your business growth strategy. Evolo team is eager to advise you on the best opportunities that you should look into</p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid" src="/themes/{{Theme::active()}}/images/details-1-office-worker.svg" alt="alternative"> 
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of details 1 -->

    
    <!-- Details 2 -->
    <div class="basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid" src="/themes/{{Theme::active()}}/images/details-2-office-team-work.svg" alt="alternative"> 
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>Search For Optimization Wherever Is Possible</h2>
                        <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                <i class="fas fa-check"></i>
                                <div class="media-body">Basically we'll teach you step by step what you need to do</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-check"></i>
                                <div class="media-body">In order to develop your company and reach new heights</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-check"></i>
                                <div class="media-body">Everyone will be pleased from stakeholders to employees</div>
                            </li>
                        </ul>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-2 -->
    <!-- end of details 2 -->

    <!-- Services -->
    <div id="services" class="cards-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Services</h2>
                    <p class="p-heading p-large">We serve small and medium sized companies in all tech related industries with high quality growth services which are presented below</p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-book sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">Predictions Arrivals</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fab fa-digital-ocean sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">RTP NC/Digital Sigance CMS</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-video sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">CCTV Live Video</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-money-check sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">Automatic Fare Collection</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-hands-helping sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">Computer Aided Dispatching</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-car sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">Automatic Vehicle Location</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-bus sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">Bus Planner/Scheduler</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-people-carry sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">Asset Management (SN) Units</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-user-circle sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">Incident Management Centreon</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-users sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">RMA Management</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-procedures sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">SLA Contract Management</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="card padCustom">
                        <i class="fas fa-screwdriver sizeColor-icon"></i>
                        <div class="card-body">
                            <h4 class="card-title card-titleCustom">Bus Driver HR Management</h4>
                            <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <button class="btn-solid-reg page-scroll m-bottom30 show-more" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Show More
                    </button>
                </div>
                <div class="container collapse" id="collapseExample">
                    <div class="well row">
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-plus sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">Bus Commissioning Reporter</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-users-cog sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">Automatic Passengger Counter</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-wifi sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">Bus Wifi Management</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-school sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">AI Machine Learning</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-database sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">Reporting (Power BI)</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-university sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">Knowledge Base Q&A</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-users-cog sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">Customer Issue Portatl (Jira)</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-project-diagram sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">PLM HW/SW Data</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fab fa-product-hunt sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">ERP Production Data SN</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-money-bill sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">ERP Billing/CRM</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fab fa-dashcube sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">Vehicle Configuration Dash</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-exclamation-triangle sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">SOS Emergency MNG</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-screwdriver sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">Driver COMS</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="card padCustom">
                                <i class="fas fa-money-check-alt sizeColor-icon"></i>
                                <div class="card-body">
                                    <h4 class="card-title card-titleCustom">Confort/Economy Monitor</h4>
                                    <p class="text-pCustom">Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <button class="btn-solid-reg page-scroll m-bottom30 hidden-more" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Hidden
                            </button>
                        </div>
                    </div>
                </div>
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-1 -->
    <!-- end of services -->

    <!-- Pricing -->
    <div id="pricing" class="cards-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Multiple Pricing Options</h2>
                    <p class="p-heading p-large">We've prepared pricing plans for all budgets so you can get started right away. They're great for small companies and large organizations</p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">

                    <!-- Card-->
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">STARTER</div>
                            <div class="card-subtitle">Just to see what can be achieved</div>
                            <hr class="cell-divide-hr">
                            <div class="price">
                                <span class="currency">$</span><span class="value">199</span>
                                <div class="frequency">monthly</div>
                            </div>
                            <hr class="cell-divide-hr">
                            <ul class="list-unstyled li-space-lg">
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">Improve Your Email Marketing</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">User And Admin Rights Control</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-times"></i><div class="media-body">List Building And Cleaning</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-times"></i><div class="media-body">Collected Data Management</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-times"></i><div class="media-body">More Planning And Evaluation</div>
                                </li>
                            </ul>
                            <div class="button-wrapper">
                                <a class="btn-solid-reg page-scroll" href="#register">REGISTER</a>
                            </div>
                        </div>
                    </div> <!-- end of card -->
                    <!-- end of card -->

                    <!-- Card-->
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">MEDIUM</div>
                            <div class="card-subtitle">Very appropriate for the short term</div>
                            <hr class="cell-divide-hr">
                            <div class="price">
                                <span class="currency">$</span><span class="value">299</span>
                                <div class="frequency">monthly</div>
                            </div>
                            <hr class="cell-divide-hr">
                            <ul class="list-unstyled li-space-lg">
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">Improve Your Email Marketing</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">User And Admin Rights Control</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">List Building And Cleaning</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">Collected Data Management</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-times"></i><div class="media-body">More Planning And Evaluation</div>
                                </li>
                            </ul>
                            <div class="button-wrapper">
                                <a class="btn-solid-reg page-scroll" href="#register">REGISTER</a>
                            </div>
                        </div>
                    </div> <!-- end of card -->
                    <!-- end of card -->

                    <!-- Card-->
                    <div class="card">
                        <div class="label">
                            <p class="best-value">Best Value</p>
                        </div>
                        <div class="card-body">
                            <div class="card-title">COMPLETE</div>
                            <div class="card-subtitle">Must have for large companies</div>
                            <hr class="cell-divide-hr">
                            <div class="price">
                                <span class="currency">$</span><span class="value">399</span>
                                <div class="frequency">monthly</div>
                            </div>
                            <hr class="cell-divide-hr">
                            <ul class="list-unstyled li-space-lg">
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">Improve Your Email Marketing</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">User And Admin Rights Control</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">List Building And Cleaning</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">Collected Data Management</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i><div class="media-body">More Planning And Evaluation</div>
                                </li>
                            </ul>
                            <div class="button-wrapper">
                                <a class="btn-solid-reg page-scroll" href="#register">REGISTER</a>
                            </div>
                        </div>
                    </div> <!-- end of card -->
                    <!-- end of card -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-2 -->
    <!-- end of pricing -->


    <!-- Request -->
    <div id="register" class="form-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>Fill The Following Form To Request A Meeting</h2>
                        <p>Evolo is one of the easiest and feature packed marketing automation apps in the market. Discover what it can do for your business organization right away.</p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">

                    <!-- Request Form -->
                    <div class="form-container">
                        <form id="requestForm" data-toggle="validator" data-focus="false">
                            <div class="form-group">
                                <input type="text" class="form-control-input" id="rname" name="rname" required>
                                <label class="label-control" for="rname">Tenant Name</label>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control-input" id="remail" name="remail" required>
                                <label class="label-control" for="remail">Admin Username</label>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control-input" id="rphone" name="rphone" required>
                                <label class="label-control" for="rphone">Admin Email</label>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group checkbox">
                                <input type="checkbox" id="rterms" value="Agreed-to-Terms" name="rterms" required>I agree with Evolo's stated <a href="privacy-policy.html">Privacy Policy</a> and <a href="terms-conditions.html">Terms & Conditions</a>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="form-control-submit-button">REQUEST</button>
                            </div>
                            <div class="form-message">
                                <div id="rmsgSubmit" class="hidden text-center h3"></div>
                            </div>
                        </form>
                    </div> <!-- end of form-container -->
                    <!-- end of request form -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form-1 -->
    <!-- end of request -->

    <!-- Contact -->
    <div id="contact" class="form-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Contact Information</h2>
                    <ul class="list-unstyled li-space-lg">
                        <li class="address">Don't hesitate to give us a call or send us a contact form message</li>
                        <li><i class="fas fa-phone"></i><a class="turquoise" href="tel:003024630820">+852 (0) 2180 7223 (Hong Kong)</a></li>
                        <li><i class="fas fa-phone"></i><a class="turquoise" href="tel:003024630820">+1 604 922 1008 (Canada)</a></li>
                        <li><i class="fas fa-phone"></i><a class="turquoise" href="tel:003024630820">+31 (0) 457 112 380 (the Netherlands)</a></li>
                    </ul>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="map-responsive">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d773.2517038393905!2d-123.08514605547104!3d49.316208780814705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x548670381378bce9%3A0xfcf008749c851e6e!2sM%20C%20Squared%20System%20Design%20Grp!5e0!3m2!1sid!2sid!4v1579760777552!5m2!1sid!2sid" allowfullscreen></iframe>
                    </div>
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    
                    <!-- Contact Form -->
                    <form id="contactForm" data-toggle="validator" data-focus="false">
                        <div class="form-group">
                            <input type="text" class="form-control-input" id="cname" required>
                            <label class="label-control" for="cname">Name</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control-input" id="cemail" required>
                            <label class="label-control" for="cemail">Email</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control-textarea" id="cmessage" required></textarea>
                            <label class="label-control" for="cmessage">Your message</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group checkbox">
                            <input type="checkbox" id="cterms" value="Agreed-to-Terms" required>I have read and agree with Evolo's stated <a href="privacy-policy.html">Privacy Policy</a> and <a href="terms-conditions.html">Terms Conditions</a> 
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-control-submit-button">SUBMIT MESSAGE</button>
                        </div>
                        <div class="form-message">
                            <div id="cmsgSubmit" class="hidden text-center h3"></div>
                        </div>
                    </form>
                    <!-- end of contact form -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form-2 -->
    <!-- end of contact -->


    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-col">
                        <h4>Corporate Office</h4>
                        <p>#122 901 West 3rd Street North Vancouver, BC V7P 3P9 Canada</p>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col">
                        <h4>Hong Kong Office</h4>
                        <p>1702 Favor Industrial Centre 2-6 Kin Hong Street, Kwai Chung Hong Kong</p>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-4">
                    <div class="footer-col">
                        <h4>Branch Offices</h4>
                        <p>The Netherlands,Indonesia,China</p>
                    </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of footer -->  
    <!-- end of footer -->


    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © <a href="#">Maestronic</a> {{ date('Y') }}</p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->
    
    <!-- Scripts -->
    <script src="{{ mix('js/frontend.min.js', 'themes/default') }}" defer></script>
    	
    <!-- Scripts -->
    {{-- <script src="{{ mix('js/frontend.js', 'themes/default') }}" defer></script> --}}
    {{-- <script src="{{Theme::assets('frontend/js/jquery.min.js')}}"></script> <!-- jQuery for Bootstrap's JavaScript plugins --> 
    {{-- <script src="{{Theme::assets('frontend/js/popper.min.js')}}"></script> <!-- Popper tooltip library for Bootstrap --> --}}
    {{-- <script src="{{Theme::assets('frontend/js/bootstrap.min.js')}}"></script> <!-- Bootstrap framework --> --}}
    {{-- <script src="{{Theme::assets('frontend/assets/js/pop.js')}}"></script> <!-- Pop framework --> --}}
    {{-- <script src="{{Theme::assets('frontend/assets/js/bootstrap.js')}}"></script> <!-- Bootstrap framework --> --}}
    {{-- <script src="{{Theme::assets('frontend/js/jquery.easing.min.js')}}"></script> <!-- jQuery Easing for smooth scrolling between anchors --> --}}
    {{-- <script src="{{Theme::assets('frontend/js/swiper.min.js')}}"></script> <!-- Swiper for image and text sliders --> --}}
    {{-- <script src="{{Theme::assets('frontend/js/jquery.magnific-popup.js')}}"></script> <!-- Magnific Popup for lightboxes --> --}}
    {{-- <script src="{{Theme::assets('frontend/js/validator.min.js')}}"></script> <!-- Validator.js')}} - Bootstrap plugin that validates forms --> --}}
    {{-- <script src="{{Theme::assets('frontend/js/scripts.js')}}"></script> <!-- Custom scripts --> --}} 
</body>
</html>