@foreach($items as $key => $groups)
{{ $key }}
  @if($key == "Main")
    <li class="nav">
      <a href="/admin">
      <i class="fa fa-home" aria-hidden="true"></i>
      <span>Dashboard</span>
      </a>
    </li>
  @endif
  @foreach($groups as $data)
  <li @if($data['item']->hasChildren()) class="nav-parent {{ $data['item']->active ? "nav-expanded nav-active" : "" }}" @endif>
    @if($data['item']->hasChildren())
      <a>
        <i class="{!! $data['item']->icon !!}" aria-hidden="true"></i> 
        <span>{!! $data['item']->title !!}</span>
      </a>
    @else
      <li class="nav{{ $data['item']->active ? " nav-expanded nav-active" : "" }}">       
        <a href="{!! $data['item']->url() !!}"> 
          <i class="{!! $data['item']->icon !!}" aria-hidden="true"></i>
          <span>{!! $data['item']->title !!}</span>
        </a>
      </li>
    @endif
    @if($data['item']->hasChildren())
      <ul aria-expanded="false" class="nav nav-children">
            @php 
            $child = $data['item']->children()->map(function ($item) {
              return ["group" => '', "item" => $item];
            })->groupBy("group");
            @endphp
            @include('theme::views.backend.menu', ['items' => $child])
      </ul>
    @endif
  </li>
  @endforeach
@endforeach

