<!DOCTYPE html>
<html lang="en" class="fixed js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling no-mobile-device custom-scroll @yield('page_class')">
	{{-- @if(Request::url()==route('timetable.line')||Request::url()==route('timetable.route')||Request::url()==route('timetable.servicejourney')||Request::url()==route('timetable.destinationdisplay')||Request::url()==route('timetable.journeylayover')||Request::url()==route('timetable.stoparea')||Request::url()==route('timetable.routeline')||Request::url()==route('layout.manager.create')||substr(Request::getPathInfo(),0,7)=='/u/gtfs') class="fixed sidebar-left-collapsed" @endif --}}
<head>
	<title>Sistem MTSAAS</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="stylesheet" href="{{ mix('css/backend-up.min.css', 'themes/default') }}" />
	@yield('module_css')
	<link rel="stylesheet" href="{{ mix('css/backend-down.min.css', 'themes/default') }}" />
</head>
<body>
	<section class="body">
		<header class="header">
			<div class="logo-container">
				<a href="../" class="logo">
					{{-- "{{ $urltheme }}/backend/assets/images/maes.gif" --}}
					<img src="/themes/{{Theme::active()}}/images/maes.gif" height="35" alt="Logo" />
				</a>
				<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
					<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				</div>
			</div>

			<div class="header-right">

				<ul class="notifications">
					<li>
						<a href="#" id="notif-add" class="dropdown-toggle notification-icon" data-toggle="dropdown">
							<i class="fa fa-bell"></i>
						</a>

						<div class="dropdown-menu notification-menu">
							<div class="notification-title">
								Notification
							</div>	
							<div class="content">
								{{-- <ul id="content-notif-add">
									<li id="new-notif-add" style="display: none;"></li>
									@php 
									$models = DB::table('log_import_timetable')->orderBy('date_time','desc')->limit(5)->get();  
									foreach($models as $row){
										if($row->status=='Success'){
											@endphp
											<li>
												<a href="{{ route('timetable.import.log') }}" class="clearfix">
													<div class="image">
														<i class="fa fa-bell bg-success"></i>
													</div>
													<span class="title">{{ $row->task_name }}</span>
													<span class="message">{{ $row->status }} {{ $row->date_time }}</span>
												</a>
											</li>
											@php 
										} 
									} 
									@endphp
								</ul> --}}
								<hr/>
								<div class="text-right">
									{{-- <a href="{{ route('timetable.import.log') }}" class="view-more">View All</a> --}}
								</div>
							</div>
						</div>
					</li>
				</ul>

				<span class="separator"></span>

				<div id="userbox" class="userbox">
					<a href="#" data-toggle="dropdown">
						<figure class="profile-picture">
							<img src="{{ auth()->user()->profile_photo_url }}" alt="Profile" class="img-circle" data-lock-picture="{{ auth()->user()->profile_photo_url }}" />
						</figure>
						<div class="profile-info" data-lock-name="{{ Auth::user()->name }}" data-lock-email="{{ Auth::user()->name }}">
							<span class="name">{{ Auth::user()->name }}</span>
							<span class="role">-</span>
						</div>

						<i class="fa custom-caret"></i>
					</a>

					<div class="dropdown-menu">
						<ul class="list-unstyled">
							<li class="divider"></li>
							<li>
								<a role="menuitem" tabindex="-1" href="/personal}"><i class="fa fa-user"></i> My Profile</a>
							</li>
							<li>
								<a role="menuitem" tabindex="-1" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<i class="fa fa-power-off"></i> {{ __('Logout') }}
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
									@csrf
								</form>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</header>

		<div class="inner-wrapper">
			<aside id="sidebar-left" class="sidebar-left noselect">
				<div class="sidebar-header">
					<div class="sidebar-title">
						Navigation
					</div>
					<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
				<div class="nano">
					<div class="nano-content">
						<nav id="menu" class="nav-main" role="navigation">
							<ul class="nav nav-main">
								
								@php 
								$data = $MenuBackend->roots()->map(function ($item) {
									return ["group" => $item->group, "item" => $item];
								})->groupBy("group")->toArray();
								uasort($data, function ($a, $b) {
									if ((strpos($a[0]['group'], 'Main') === false && strpos($b[0]['group'], 'Main') !== false)) {
											return 1;
										} elseif (strpos($a[0]['group'], 'Main') !== false && strpos($b[0]['group'], 'Main') === false) {
											return -1;
										}
								});
								@endphp
								@include('theme::views.backend.menu', ['items' => $data])
							</ul>
						</nav>
					</div>			
				</div>
				
			</aside>

			<section role="main" class="content-body">

				<header class="page-header">
					@yield('page-title')

					<div class="right-wrapper pull-right">
						@yield('breadcrumb')

						<a class="sidebar-right-toggle"></i></a>
					</div>
				</header>

				@yield('content')
			</section>
		</div>
		<div id="cms_ver">
			<a href="/admin/version-history">v1.0.3</a>
		</div>
	</section>
    <script type="text/javascript" src="/js/config.js"></script>
    <script type="text/javascript" src="{{ mix('js/backend-up.min.js', 'themes/default') }}"></script>
	@yield('module_javascript')
    <script type="text/javascript" src="{{ mix('js/backend-down.min.js', 'themes/default') }}"></script>
	{{-- @livewireScripts
	<script src="{{ $urltheme }}/backend/compiled/js/backend-up.min.js"></script>
	@stack('data-table')
	<script src="{{ $urltheme }}/backend/compiled/js/backend-down.min.js"></script>
	<script src="/js/config.js"></script>
	@stack('core-scripts')
	@stack('custom-scripts')
	@stack('data-scripts')
	@stack('data-livescripts') --}}
</body>
</html>