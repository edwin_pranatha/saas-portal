// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $(document).on('click', '.show-more', function(event) {
        $(this).css('display','none');
        $('.hidden-more').css('display','inline-block');
    });
    $(document).on('click', '.hidden-more', function(event) {
        $(this).css('display','none');
        $('.show-more').css('display','inline-block');
    });
});