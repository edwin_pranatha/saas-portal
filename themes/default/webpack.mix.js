const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath(global.public_theme_path);
mix.options({
    processCssUrls: false,
  })
  .sourceMaps();
//   .js(`${__dirname}/js/app_frontend.js`,`./js/app_frontend.js`)
//   .js(`${__dirname}/js/app_backend.js`,`./js/app_backend.js`)
//   .js(`${__dirname}/js/backend/modernizr.js`,`./js/app_backend.js`)
//   .js(`${__dirname}/js/backend/theme.js`,`./js/app_backend.js`)
//   .js(`${__dirname}/js/backend/theme.init.js`,`./js/app_backend.js`);
// mix.js(`${__dirname}/js/frontend/scripts.js`, `./js/frontend.js`);

//Frontend JS
mix.scripts([
    'node_modules/jquery-3.3.0/dist/jquery.min.js',
    'node_modules/bootstrap-4/dist/js/bootstrap.min.js',
    'node_modules/jquery.easing/jquery.easing.min.js',
    'node_modules/swiper-4.4.6/dist/js/swiper.min.js',
    'node_modules/magnific-popup/dist/jquery.magnific-popup.js',
    'node_modules/bootstrap-validator/dist/validator.min.js',
    `${__dirname}/frontend/js/scripts.js`,
    `${__dirname}/frontend/js/add.js`,
], global.public_theme_path +'js/frontend.min.js');

//Auth JS
mix.scripts([
    'node_modules/jquery-3.3.0/dist/jquery.min.js',
    // 'node_modules/popper.js/dist/popper.min.js',
    'node_modules/bootstrap-330/dist/js/bootstrap.min.js',
    'node_modules/jquery-slimscroll/jquery.slimscroll.js',
    `${__dirname}/auth/js/waves.js`,
    `${__dirname}/auth/js/sidebarmenu.js`,
    'node_modules/sticky-kit/dist/sticky-kit.min.js',
    'node_modules/jquery-sparkline/jquery.sparkline.min.js',
    `${__dirname}/auth/js/custom.min.js`,
],  global.public_theme_path + 'js/auth.min.js');

//Backend JS
mix.scripts([
    'node_modules/jquery-3.3.0/dist/jquery.min.js',
    'node_modules/jquery-ui-1.12.1/jquery-ui.min.js',
    'node_modules/jquery.browser/dist/jquery.browser.min.js',
    'node_modules/bootstrap-330/dist/js/bootstrap.min.js',
    'node_modules/nanoscroller/bin/javascripts/jquery.nanoscroller.js',
    'node_modules/jquery-placeholder/jquery.placeholder.js',
],  global.public_theme_path + 'js/backend-up.min.js');

mix.scripts([
    `${__dirname}/backend/js/modernizr.js`,
    `${__dirname}/backend/js/pnotify/pnotify.custom.js`,
    `${__dirname}/backend/js/theme.js`,
    `${__dirname}/backend/js/theme.init.js`,
],  global.public_theme_path + 'js/backend-down.min.js');


//Frontend CSS
// mix.postCss('node_modules/bootstrap-4/dist/css/bootstrap.min.css', global.public_theme_path + 'css/frontend.min.css');
// mix.postCss('node_modules/font-awesome/css/font-awesome.min.css', global.public_theme_path + 'css/frontend.min.css');
// mix.postCss('node_modules/swiper-4.4.6/dist/css/swiper.min.css', global.public_theme_path + 'css/frontend.min.css');
// mix.postCss('node_modules/magnific-popup/dist/magnific-popup.css', global.public_theme_path + 'css/frontend.min.css');
mix.sass(`${__dirname}/frontend/sass/frontend.scss`, global.public_theme_path + 'css/frontend.min.css');
mix.postCss(`${__dirname}/frontend/css/styles.css`, global.public_theme_path + 'css/frontend.min.css');

//Auth CSS
//mix.postCss('node_modules/bootstrap-4/dist/css/bootstrap.min.css', global.public_theme_path + 'css/auth.min.css');
mix.sass(`${__dirname}/auth/sass/auth.scss`, global.public_theme_path + 'css/auth.min.css');
mix.postCss(`${__dirname}/auth/css/style.css`, global.public_theme_path + 'css/auth.min.css');
mix.postCss(`${__dirname}/auth/css/colors/blue.css`, global.public_theme_path + 'css/auth.min.css');


// //Backend CSS
// mix.postCss('node_modules/bootstrap-4/dist/css/bootstrap.min.css', global.public_theme_path + 'css/backend-up.min.css');
// mix.postCss('node_modules/font-awesome/css/font-awesome.min.css', global.public_theme_path + 'css/backend-up.min.css');
mix.sass(`${__dirname}/backend/sass/backend-up.scss`, global.public_theme_path + 'css/backend-up.min.css');


mix.sass(`${__dirname}/backend/sass/backend-down.scss`, global.public_theme_path + 'css/backend-down.min.css');
// mix.postCss('public/themes/maestronic/assets/backend/assets/vendor/pnotify/pnotify.custom.css', global.public_theme_path + 'css/backend-down.min.css');
mix.postCss(`${__dirname}/backend/css/theme.css`, global.public_theme_path + 'css/backend-down.min.css');
mix.postCss(`${__dirname}/backend/css/skins/default.css`, global.public_theme_path + 'css/backend-down.min.css');
mix.postCss(`${__dirname}/backend/css/navigation.css`, global.public_theme_path + 'css/backend-down.min.css');


// mix.sass(`${__dirname}/sass/app_frontend.scss`,`./css/app_frontend.css`);
// mix.sass(`${__dirname}/sass/app_backend.scss`,`./css/app_backend.css`);
// mix.sass(`${__dirname}/sass/app_backend_theme.scss`,`./css/app_backend_theme.css`);


mix.setResourceRoot('../');
mix.options({
    fileLoaderDirs: {
        images: `./images`,
        fonts: `./fonts`
    }
});

// mix.webpackConfig({
//     dependOn: {
//         uniqueItems: false,
//     }
// });


mix.copyDirectory(`${__dirname}/auth/images/`, './'+global.public_theme_path+'/images/');
mix.copyDirectory(`${__dirname}/frontend/images/`, './'+global.public_theme_path+'/images/');
mix.copyDirectory(`${__dirname}/backend/images/`, './'+global.public_theme_path+'/images/');
mix.copyDirectory('node_modules/font-awesome/fonts','./'+global.public_theme_path+'/fonts/');
mix.copyDirectory('node_modules/bootstrap-3/fonts','./'+global.public_theme_path+'/fonts/');
mix.copyDirectory(`node_modules/@fortawesome/fontawesome-free/webfonts/`, './'+global.public_theme_path+'/fonts/');