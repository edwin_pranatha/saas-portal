const mix = require('laravel-mix');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

let theme = process.env.npm_config_theme;
let modules = process.env.npm_config_module;

if(modules !== undefined && modules !== '') {
    console.log("Compiling Module: " + modules);    
    global.module_path = `Modules/`+modules+`/`;
    require(`${__dirname}/Modules/`+modules+`/webpack.mix.js`);
} else {
    if(theme) {
        console.log("Compiling Theme: " + theme);
        global.public_theme_path = "public/themes/"+theme+'/';
        require(`${__dirname}/themes/${theme}/webpack.mix.js`);
    }else {
        console.log("Compiling Theme: default");
        global.public_theme_path = "public/themes/default/";
        require(`${__dirname}/themes/default/webpack.mix.js`);
    }
}
// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css', [
//         require('postcss-import'),
//         require('tailwindcss'),
//     ]);

if (mix.inProduction()) {
    mix.version();
}
