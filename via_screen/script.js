var options = {
  valueNames: [ 'id', 'name', 'age', 'city' ]
};

// Init list
var contactList = new List('contacts', options);

var idField = $('#id-field'),
    nameField = $('#name-field'),
    ageField = $('#age-field'),
    cityField = $('#city-field'),
    addBtn = $('#add-btn'),
    editBtn = $('#edit-btn').hide(),
    removeBtns = $('.remove-item-btn'),
    editBtns = $('.edit-item-btn');

// Sets callbacks to the buttons in the list
refreshCallbacks();

addBtn.click(function() {
  contactList.add({
    id: Math.floor(Math.random()*110000),
    name: nameField.val(),
    age: ageField.val(),
    city: cityField.val()
  });
  clearFields();
  refreshCallbacks();
});

editBtn.click(function() {
  var item = contactList.get('id', idField.val())[0];
  item.values({
    id:idField.val(),
    name: nameField.val(),
    age: ageField.val(),
    city: cityField.val()
  });
  clearFields();
  editBtn.hide();
  addBtn.show();
});

function refreshCallbacks() {
  // Needed to add new buttons to jQuery-extended object
  removeBtns = $(removeBtns.selector);
  editBtns = $(editBtns.selector);
  
  removeBtns.click(function() {
    var itemId = $(this).closest('tr').find('.id').text();
    contactList.remove('id', itemId);
  });
  
  editBtns.click(function() {
    var itemId = $(this).closest('tr').find('.id').text();
    var itemValues = contactList.get('id', itemId)[0].values();
    idField.val(itemValues.id);
    nameField.val(itemValues.name);
    ageField.val(itemValues.age);
    cityField.val(itemValues.city);
    
    editBtn.show();
    addBtn.hide();
  });
}

function clearFields() {
  nameField.val('');
  ageField.val('');
  cityField.val('');
}


function jsoncontains(arr, key, val) {
    for (var i = 0; i < arr.length; i++) {
        if(arr[i][key] === val) return true;
    }
    return false;
}


// Create a client instance
client = new Paho.MQTT.Client("192.168.87.62", 8083, "web_" + parseInt(Math.random() * 100, 10));
//Example client = new Paho.MQTT.Client("m11.cloudmqtt.com", 32903, "web_" + parseInt(Math.random() * 100, 10));

// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;
var options = {
  useSSL: false,
  // userName: "username",
  // password: "password",
  onSuccess:onConnect,
  onFailure:doFail
}

// connect the client
client.connect(options);

// called when the client connects
function onConnect() {
  // Once a connection has been made, make a subscription and send a message.
  toastr.info('You are now connected to the server!');
  console.log("onConnect");
  client.subscribe("/mt/mnpc/via/+/gnss");
  toastr.info('You subcribe the "gnss" topic!');
  /*
  message = new Paho.MQTT.Message("initialize");
  message.destinationName = "alert";
  client.send(message);
  */
}

function doFail(e){
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "0",
    "extendedTimeOut": "0",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  toastr.error(e, 'Error');
  console.log(e);
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    toastr.error(responseObject.errorMessage, 'ConnectionLost');
    console.log("onConnectionLost:"+responseObject.errorMessage);
  }
}

// called when a message arrives
function onMessageArrived(message) {
    toastr.success(message.payloadString, 'Message Arrive')
    console.log(message.payloadString);
    var msg;
    try {
        msg = JSON.parse(message.payloadString);
        if(msg.flags.length>0)
        {
            console.log("type:alert contains (" + msg.flags.length+")");
            if(msg.flags[0][0]=="")
            {
                console.log("timeout found");
            }else{

                var findid = $(".id:contains('"+msg.id+"')");
                if(findid.length>0)
                {
                    console.log("ID already exists updating....");
                    var item = contactList.get('id', msg.id)[0];
                    item.values({
                        id:msg.id,
                        name: msg.id,
                        age: msg.flags,
                        city: msg.ip+"*UPDATED"
                    });
                    /*
                    idField.val(msg.id);
                    nameField.val(msg.id);
                    ageField.val(msg.flags);
                    cityField.val(msg.ip+"*UPDATED");
                    */
                }else{
                    console.log("ID is not exists, inserting....");
                    contactList.add({
                        id: msg.id,
                        name: msg.id,
                        age: msg.flags,
                        city: msg.ip
                      });
                      refreshCallbacks();
                }
            }

           

        }else{
            console.log("type:alert but no alert");
        }
    } catch (e) {
        msg = "notjson";
    }
    console.log(msg);
}