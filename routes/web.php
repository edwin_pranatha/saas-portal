<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Testing\MimeType;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home');
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');


// Route::get('/home', function () {
//     return view('home');
// })->middleware(['auth'])->name('home');


Route::get('/assets/{module}/{type}/{file}', [ function ($module, $type, $file) {
    $module = ucfirst($module);
    $path = base_path("Modules/$module/Resources/assets/compiled/$type/$file");
    if (\File::exists($path)) {
        $mime = MimeType::from($path);
        return response(\File::get($path))
            ->header('Content-Type', $mime);
    }else{
        return response()->json([ "error"=> "Asset not found" ], 404);
    }
}]);


Route::get('/assets/{module}/{any}', [ function ($module) {
    $module = ucfirst($module);
    $path = base_path("Modules/$module/Resources/assets/compiled/" . str_replace(array("assets/",$module."/"), array("","") , request()->path()));
    $path = str_replace("/", DIRECTORY_SEPARATOR, $path);
    $path = str_replace("\\", DIRECTORY_SEPARATOR, $path);
    if (\File::exists($path)) {
        $mime = MimeType::from($path);
        return response(\File::get($path))
            ->header('Content-Type', $mime);
    }else{
        $module = ucfirst($module);
        $path = base_path("Modules/$module/Resources/assets/" . str_replace(array("assets/",$module."/"), array("","") , request()->path()));
        $path = str_replace("/", DIRECTORY_SEPARATOR, $path);
        $path = str_replace("\\", DIRECTORY_SEPARATOR, $path);
        if (\File::exists($path)) {
            $mime = MimeType::from($path);
            return response(\File::get($path))
                ->header('Content-Type', $mime);
        }else{
            return response()->json([ "error"=> "Asset not found" ], 404);
        }
    }

    return response()->json([ "error"=> "Asset not found" ], 404);
}])->where('any','.*');




Route::get('/js/config.js', function () {
    //"http://3.238.190.121"
    $response = '
    window.APP_URL = "'.getenv("APP_URL").'"
    window.APP_PORT = ""
    window.SIRI_URL = ""
    window.SIRI_PORT = ""
    window.GTFS_URL = "'.getenv("GTFS_URL").'",
    window.GTFS_PORT = "'.getenv("GTFS_PORT").'"
    window.GTFS_URL_STATIC = "http://192.168.87.62",
    window.GTFS_PORT_STATIC = "4002"
    window.GTFS_URL_DYNAMIC = "http://192.168.87.62",
    window.GTFS_PORT_DYNAMIC = "4001",
    window.GTFS_MAP_LAT = 29.442859649658203,
    window.GTFS_MAP_LONG = -98.51445770263672,
    window.GTFS_TIMEZONE = "America/Chicago",
    window.GTFS_AGENCY = 1
    ';
    return response($response)->header('Content-Type', 'Application/Javascript');
});

// Route::prefix('admin')->middleware(['theme:maestronic'])->name('admin.')->group(function () {
//    // Route::view('/login', 'auth.login')->name('login');

//     // $limiter = config('fortify.limiters.login');

//     // Route::post('/login', [AuthenticatedSessionController::class, 'store'])
//     //     ->middleware(array_filter([
//     //         'guest:admin',
//     //         $limiter ? 'throttle:' . $limiter : null,
//     //     ]));

//     // Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
//     //     ->middleware('auth:admin')
//     //     ->name('logout');

//     //Route::view('/home', 'home')->middleware('auth:admin')->name('home');
// });


require __DIR__ . '/admin.php';

// require __DIR__.'/auth.php';