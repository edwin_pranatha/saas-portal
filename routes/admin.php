<?php

use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;
use Modules\Core\Http\Controllers\Backend\AdminController;

Route::prefix('admin')->name('admin.')->group(function () {
    Route::view('/login', 'auth.login')->middleware('auth')->name('login');

    $limiter = config('fortify.limiters.login');

    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware(array_filter([
            'guest:user',
            $limiter ? 'throttle:' . $limiter : null,
        ]));

    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth:user')
        ->name('logout');

    Route::get('/dashboard',  [AdminController::class, 'index'])->middleware('auth')->name('dashboard');
    Route::get('/', [AdminController::class, 'index'])->middleware('auth')->name('home');

});