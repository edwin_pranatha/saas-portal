<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class TenantPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'roles-view',
            'roles-create',
            'roles-edit',
            'roles-delete',
        ];

        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission,
                "guard_name" => "web",
                "group" => "User Management",
            ]);
        }

        $permissions = [
            'users-view',
            'users-create',
            'users-edit',
            'users-delete',
        ];

        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission,
                "guard_name" => "web",
                "group" => "User Management",
            ]);
        }

        $permissions = [
            'tenant.users-view',
            'tenant.users-create',
            'tenant.users-edit',
            'tenant.users-delete',

            'tenant.modules-view',
            'tenant.modules-create',
            'tenant.modules-edit',
            'tenant.modules-delete',
        ];

        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission,
                "guard_name" => "web",
                "group" => "Tenant Management",
            ]);
        }

        $role_admin = Role::create(['name' => 'Admin']);
        $role_via = Role::create(['name' => 'Via']);
        $role_canada = Role::create(['name' => 'Canada']);
        $role_connexxion = Role::create(['name' => 'Connexxion']);
        $role_marketing = Role::create(['name' => 'Marketing']);

        $permissions_admin = Permission::pluck('id', 'id')->all();
        $role_admin->syncPermissions($permissions_admin);
        $role_canada->syncPermissions($permissions_admin);
    }
}
