<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Support\Str;
use App\Models\Tenant\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class TenantUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($x = 0; $x <= 5; $x++) {
            $faker = Faker::create();
            $user = User::create([
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
            ]);
            $role = Role::where('name', 'Admin')->first();
            $user->assignRole([$role->id]);
        }
        

        $admin = User::create([
            'name' => "Maestronic",
            'email' => "maestronic@maestronic.com",
            'email_verified_at' => now(),
            'password' => '$2y$10$dLVveoZsqHgb2ssc2cHoy.0oIKNYnKqKi8PhqkgYs4mT8sZwgWJBC',
            'remember_token' => Str::random(10),
        ]);

        $via = User::create([
            'name' => "Via",
            'email' => "via@maestronic.com",
            'email_verified_at' => now(),
            'password' => '$2y$10$dLVveoZsqHgb2ssc2cHoy.0oIKNYnKqKi8PhqkgYs4mT8sZwgWJBC',
            'remember_token' => Str::random(10),
        ]);

        $canada = User::create([
            'name' => "Canada",
            'email' => "canada@maestronic.com",
            'email_verified_at' => now(),
            'password' => '$2y$10$dLVveoZsqHgb2ssc2cHoy.0oIKNYnKqKi8PhqkgYs4mT8sZwgWJBC',
            'remember_token' => Str::random(10),
        ]);

        $connexxion = User::create([
            'name' => "Connexxion",
            'email' => "connexxion@maestronic.com",
            'email_verified_at' => now(),
            'password' => '$2y$10$dLVveoZsqHgb2ssc2cHoy.0oIKNYnKqKi8PhqkgYs4mT8sZwgWJBC',
            'remember_token' => Str::random(10),
        ]);

        $marketing = User::create([
            'name' => "Marketing",
            'email' => "marketing@maestronic.com",
            'email_verified_at' => now(),
            'password' => '$2y$10$dLVveoZsqHgb2ssc2cHoy.0oIKNYnKqKi8PhqkgYs4mT8sZwgWJBC',
            'remember_token' => Str::random(10),
        ]);

        $role_admin = Role::where('name', 'Admin')->first();
        $role_via = Role::where('name', 'Via')->first();
        $role_canada = Role::where('name', 'Canada')->first();
        $role_connexxion = Role::where('name', 'Connexxion')->first();

        $admin->assignRole([$role_admin->id]);
        $via->assignRole([$role_via->id]);
        $canada->assignRole([$role_canada->id]);
        $connexxion->assignRole([$role_connexxion->id]);
        
    }
}
