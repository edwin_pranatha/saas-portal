<?php

namespace Database\Seeders;

// use Faker\Factory as Faker;
// use Illuminate\Support\Str;
// use App\Models\System\User;
use Illuminate\Database\Seeder;

use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;

class SystemPredefinedMaster extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $website = new Website();
        app(WebsiteRepository::class)->create($website);

        $hostname = new Hostname();
        $hostname->fqdn = env('TENANCY_MASTER_HOSTNAME');
        echo "Creating hostname: ".$hostname->fqdn." with UUID : ".$website->uuid."\n";
        if($hostname->fqdn == null)
        {
            echo "FQDN is null\n";
            echo "Please run 'php artisan optimize:clear' from the console!\n";
        }
        app(HostnameRepository::class)->attach($hostname, $website);
    }
}
