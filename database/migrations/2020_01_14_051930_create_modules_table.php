<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('modules', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('uuid', 32)->nullable();
			$table->string('name', 191);
			$table->string('alias', 191)->nullable();
			$table->text('description', 65535)->nullable();
			$table->text('keywords', 65535)->nullable();
			$table->smallInteger('is_active')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('modules');
	}

}
