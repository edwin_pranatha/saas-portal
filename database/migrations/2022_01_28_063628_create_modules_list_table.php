<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules_list', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('name', 191);
            $table->string('alias', 191)->nullable();
            $table->text('description')->nullable();
            $table->string('keywords', 150)->nullable();
            $table->smallInteger('is_listed')->default(0);
            $table->timestamps();
            $table->text('changelog')->nullable();
            $table->integer('feedback_id')->nullable();
            $table->string('version')->nullable();
            $table->integer('stat_installed')->nullable();
            $table->integer('stat_like')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules_list');
    }
}
