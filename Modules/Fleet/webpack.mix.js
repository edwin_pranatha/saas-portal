const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('./').mergeManifest();

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/stops.js'
],  __dirname + '/Resources/assets/compiled/js/stops.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/routes.js'
],  __dirname + '/Resources/assets/compiled/js/routes.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    'node_modules/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js',   
    __dirname + '/Resources/assets/js/vehicles.js'
],  __dirname + '/Resources/assets/compiled/js/vehicles.min.js');

mix.options({
    processCssUrls: false,
});

mix.sass( __dirname + '/Resources/assets/sass/stops.scss', __dirname + '/Resources/assets/compiled/css/stops.min.css');
mix.sass( __dirname + '/Resources/assets/sass/routes.scss', __dirname + '/Resources/assets/compiled/css/routes.min.css');
mix.sass( __dirname + '/Resources/assets/sass/vehicles.scss', __dirname + '/Resources/assets/compiled/css/vehicles.min.css');

if (process.env.NODE_ENV === 'production') {
mix.copy('node_modules/select2-3.5.1/select2.png',  __dirname + '/Resources/assets/compiled/css/select2.png');
mix.copy('node_modules/select2-3.5.1/select2-spinner.gif',  __dirname + '/Resources/assets/compiled/css/select2-spinner.gif');
mix.copy('node_modules/select2-3.5.1/select2x2.png',  __dirname + '/Resources/assets/compiled/css/select2x2.png');
mix.copyDirectory('node_modules/datatables.net-dt/images/',  __dirname + '/Resources/assets/compiled/images/');
}

if (mix.inProduction()) {
    mix.version();
}
