<?php

namespace Modules\Fleet\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class FleetsPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $permissions = [
           'vehicles-list',
           'vehicles-create',
           'vehicles-edit',
           'vehicles-delete',

           'routes-list',
           'routes-create',
           'routes-edit',
           'routes-delete',

           'stops-list',
           'stops-create',
           'stops-edit',
           'stops-delete'
        ];

        foreach ($permissions as $permission) {
          Permission::firstorCreate(
               [
                    "name" => $permission,
                    "guard_name" => "web",
                    "group" => "Fleet"
               ]
          );
        }

        $permissions_active = [
          'vehicles-list',
          'vehicles-create',
          'vehicles-edit',
          'vehicles-delete',
       ];

        $role = Role::findByName('Admin');
        $role->givePermissionTo($permissions_active);
    }
}
