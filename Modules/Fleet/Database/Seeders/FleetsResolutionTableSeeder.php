<?php

namespace Modules\Fleet\Database\Seeders;

use Illuminate\Database\Seeder;

class FleetsResolutionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fleets_resolution')->delete();
        
        \DB::table('fleets_resolution')->insert(array (
            0 => 
            array (
                'id_res' => 'RES001',
                'nm_res' => '1920x540',
                'aspectratio' => '16:4',
            ),
            1 => 
            array (
                'id_res' => 'RES002',
                'nm_res' => '1920x1080',
                'aspectratio' => '16:4',
            ),
            2 => 
            array (
                'id_res' => 'RES003',
                'nm_res' => '1680x1050',
                'aspectratio' => '8:5',
            ),
            3 => 
            array (
                'id_res' => 'RES004',
                'nm_res' => '1280x1024',
                'aspectratio' => '5:4',
            ),
            4 => 
            array (
                'id_res' => 'RES005',
                'nm_res' => '1440x900',
                'aspectratio' => '8:5',
            ),
            5 => 
            array (
                'id_res' => 'RES006',
                'nm_res' => '1152x864',
                'aspectratio' => '4:3',
            ),
            6 => 
            array (
                'id_res' => 'RES007',
                'nm_res' => '1024x768',
                'aspectratio' => '4:3',
            ),
            7 => 
            array (
                'id_res' => 'RES008',
                'nm_res' => '832x624',
                'aspectratio' => '4:3',
            ),
            8 => 
            array (
                'id_res' => 'RES009',
                'nm_res' => '800x600',
                'aspectratio' => '4:3',
            ),
            9 => 
            array (
                'id_res' => 'RES010',
                'nm_res' => '640x480',
                'aspectratio' => '4:3',
            ),
            10 => 
            array (
                'id_res' => 'RES011',
                'nm_res' => '1920x1080i',
                'aspectratio' => '16:9',
            ),
            11 => 
            array (
                'id_res' => 'RES012',
                'nm_res' => '2880x576i',
                'aspectratio' => '16:9',
            ),
            12 => 
            array (
                'id_res' => 'RES013',
                'nm_res' => '1280x720',
                'aspectratio' => '16:9',
            ),
            13 => 
            array (
                'id_res' => 'RES014',
                'nm_res' => '1440x576',
                'aspectratio' => '4:3',
            ),
            14 => 
            array (
                'id_res' => 'RES015',
                'nm_res' => '720x576',
                'aspectratio' => '4:3',
            ),
            15 => 
            array (
                'id_res' => 'RES016',
                'nm_res' => '720x480',
                'aspectratio' => '3:2',
            ),
            16 => 
            array (
                'id_res' => 'RES017',
                'nm_res' => '720x400',
                'aspectratio' => '3:2',
            ),
            17 => 
            array (
                'id_res' => 'RES018',
                'nm_res' => '1366X768',
                'aspectratio' => '4:3',
            ),
        ));
        
        
    }
}