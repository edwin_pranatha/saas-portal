<?php

namespace Modules\Fleet\Database\Seeders;

use Illuminate\Database\Seeder;

class FleetsOperatorTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fleets_operator')->delete();
        
        \DB::table('fleets_operator')->insert(array (
            0 => 
            array (
                'id_operator' => 'OP000',
                'id_class' => 'CLS001',
                'nm_operator' => 'MAESTRONIC',
                'prefix' => NULL,
                'cdn_server' => '192.168.87.43',
                'istwofactor' => NULL,
            ),
            1 => 
            array (
                'id_operator' => 'OP001',
                'id_class' => 'CLS003',
                'nm_operator' => 'CONNEXXION',
                'prefix' => NULL,
                'cdn_server' => '192.168.87.43',
                'istwofactor' => NULL,
            ),
            2 => 
            array (
                'id_operator' => 'OP002',
                'id_class' => 'CLS002',
                'nm_operator' => 'NEXTBUS',
                'prefix' => NULL,
                'cdn_server' => NULL,
                'istwofactor' => NULL,
            ),
        ));
        
        
    }
}