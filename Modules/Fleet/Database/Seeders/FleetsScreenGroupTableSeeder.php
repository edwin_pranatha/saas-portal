<?php

namespace Modules\Fleet\Database\Seeders;

use Illuminate\Database\Seeder;

class FleetsScreenGroupTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fleets_screen_group')->delete();
        
        \DB::table('fleets_screen_group')->insert(array (
            0 => 
            array (
                'id_group' => 'GRP001',
                'nm_group' => 'A',
                'desc_group' => NULL,
                'data_group' => NULL,
            ),
            1 => 
            array (
                'id_group' => 'GRP002',
                'nm_group' => 'B',
                'desc_group' => NULL,
                'data_group' => NULL,
            ),
            2 => 
            array (
                'id_group' => 'GRP003',
                'nm_group' => 'C',
                'desc_group' => NULL,
                'data_group' => NULL,
            ),
            3 => 
            array (
                'id_group' => 'GRP004',
                'nm_group' => 'D',
                'desc_group' => NULL,
                'data_group' => NULL,
            ),
        ));
        
        
    }
}