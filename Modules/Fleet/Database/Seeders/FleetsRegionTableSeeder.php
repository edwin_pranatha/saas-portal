<?php

namespace Modules\Fleet\Database\Seeders;

use Illuminate\Database\Seeder;

class FleetsRegionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fleets_region')->delete();
        
        \DB::table('fleets_region')->insert(array (
            0 => 
            array (
                'id_region' => 'REG001',
                'id_operator' => 'OP001',
                'nm_region' => 'AML',
            ),
            1 => 
            array (
                'id_region' => 'REG002',
                'id_operator' => 'OP001',
                'nm_region' => 'Gv',
            ),
            2 => 
            array (
                'id_region' => 'REG003',
                'id_operator' => 'OP001',
                'nm_region' => 'Hwgo',
            ),
            3 => 
            array (
                'id_region' => 'REG004',
                'id_operator' => 'OP001',
                'nm_region' => 'Ijmond',
            ),
            4 => 
            array (
                'id_region' => 'REG005',
                'id_operator' => 'OP001',
                'nm_region' => 'Ijsselmond',
            ),
            5 => 
            array (
                'id_region' => 'REG006',
                'id_operator' => 'OP001',
                'nm_region' => 'Nhn',
            ),
            6 => 
            array (
                'id_region' => 'REG007',
                'id_operator' => 'OP001',
                'nm_region' => 'NTVK',
            ),
            7 => 
            array (
                'id_region' => 'REG008',
                'id_operator' => 'OP001',
                'nm_region' => 'San',
            ),
            8 => 
            array (
                'id_region' => 'REG009',
                'id_operator' => 'OP001',
                'nm_region' => 'Zaanstad',
            ),
            9 => 
            array (
                'id_region' => 'REG010',
                'id_operator' => 'OP001',
                'nm_region' => 'Zeeland',
            ),
            10 => 
            array (
                'id_region' => 'REG011',
                'id_operator' => 'OP001',
                'nm_region' => 'Zob',
            ),
        ));
        
        
    }
}