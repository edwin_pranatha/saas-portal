<?php

namespace Modules\Fleet\Database\Seeders;

use Illuminate\Database\Seeder;

class FleetsScreenTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fleets_screen')->delete();
        
        \DB::table('fleets_screen')->insert(array (
            0 => 
            array (
                'id_screen' => 'SCR001',
                'nm_screen' => 'TFT19"',
                'type_screen' => 'TFT',
                'inch_screen' => 19,
                'list_resolution' => 'RES001,RES002,RES009,RES013',
            ),
            1 => 
            array (
                'id_screen' => 'SCR002',
                'nm_screen' => 'TFT29"',
                'type_screen' => 'TFT',
                'inch_screen' => 29,
                'list_resolution' => 'RES001,RES002,RES009',
            ),
            2 => 
            array (
                'id_screen' => 'SCR003',
                'nm_screen' => 'LED19"',
                'type_screen' => 'LED',
                'inch_screen' => 19,
                'list_resolution' => 'RES001,RES002,RES009',
            ),
            3 => 
            array (
                'id_screen' => 'SCR004',
                'nm_screen' => 'LED29"',
                'type_screen' => 'LED',
                'inch_screen' => 29,
                'list_resolution' => 'RES001,RES002,RES009',
            ),
            4 => 
            array (
                'id_screen' => 'SRC005',
                'nm_screen' => 'TFT42"',
                'type_screen' => 'TFT',
                'inch_screen' => 42,
                'list_resolution' => 'RES001,RES002,RES009,RES013',
            ),
            5 => 
            array (
                'id_screen' => 'SRC006',
                'nm_screen' => 'LED42"',
                'type_screen' => 'LED',
                'inch_screen' => 42,
                'list_resolution' => 'RES001,RES002,RES009,RES013',
            ),
            6 => 
            array (
                'id_screen' => 'SRC007',
                'nm_screen' => 'TFT15"',
                'type_screen' => 'TFT',
                'inch_screen' => 15,
                'list_resolution' => 'RES001,RES002,RES009,RES013',
            ),
            7 => 
            array (
                'id_screen' => 'SRC008',
                'nm_screen' => 'LED37"',
                'type_screen' => 'LED',
                'inch_screen' => 37,
                'list_resolution' => 'RES001,RES002,RES009,RES013',
            ),
            8 => 
            array (
                'id_screen' => 'SRC009',
                'nm_screen' => 'LED27"',
                'type_screen' => 'LED',
                'inch_screen' => 27,
                'list_resolution' => 'RES001,RES002,RES009,RES013',
            ),
        ));
        
        
    }
}