<?php

namespace Modules\Fleet\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\Fleet\Database\Seeders\FleetsVehiclesTableSeeder;
use Modules\Fleet\Database\Seeders\FleetsVehiclesCatTableSeeder;
use Modules\Fleet\Database\Seeders\FleetsOperatorTableSeeder;
use Modules\Fleet\Database\Seeders\FleetsLinesTableSeeder;
use Modules\Fleet\Database\Seeders\FleetsStopsTableSeeder;
use Modules\Fleet\Database\Seeders\FleetsRegionTableSeeder;

use Modules\Fleet\Database\Seeders\FleetsResolutionTableSeeder;
use Modules\Fleet\Database\Seeders\FleetsScreenTableSeeder;
use Modules\Fleet\Database\Seeders\FleetsScreenGroupTableSeeder;


use Modules\Fleet\Database\Seeders\FleetsPermissionTableSeeder;

class FleetDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        $this->call(FleetsVehiclesTableSeeder::class);
        $this->call(FleetsVehiclesCatTableSeeder::class);
        $this->call(FleetsOperatorTableSeeder::class);
        $this->call(FleetsLinesTableSeeder::class);
        $this->call(FleetsStopsTableSeeder::class);
        $this->call(FleetsRegionTableSeeder::class);
        $this->call(FleetsResolutionTableSeeder::class);
        $this->call(FleetsScreenTableSeeder::class);
        $this->call(FleetsScreenGroupTableSeeder::class);
        $this->call(FleetsPermissionTableSeeder::class);
    }
}
