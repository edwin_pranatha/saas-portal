<?php

namespace Modules\Fleet\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FleetsVehiclesCatTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        

        DB::table('fleets_vehicles_cat')->delete();
        
        DB::table('fleets_vehicles_cat')->insert(array (
            0 => 
            array (
                'id_cat' => 'VC001',
                'cat_nm' => 'Bus',
            ),
            1 => 
            array (
                'id_cat' => 'VC002',
                'cat_nm' => 'Mini Bus',
            ),
            2 => 
            array (
                'id_cat' => 'VC003',
                'cat_nm' => 'Electric Bus',
            ),
        ));
        
        
    }
}