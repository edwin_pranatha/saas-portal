<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FleetsCreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets_vehicles', function (Blueprint $table) {
            $table->string('id_vehicle', 20)->primary();
            $table->string('ip_vehicle', 30)->nullable();
            $table->string('cat_vehicle', 7)->nullable();
            $table->string('id_operator', 7)->nullable();
            $table->string('id_region', 7)->nullable();
            $table->integer('is_wifi')->nullable();
            $table->integer('is_beacon')->nullable();
            $table->integer('is_cctv')->nullable();
            $table->string('screens_installed', 200)->nullable();
            $table->string('res_installed', 200)->nullable();
            $table->timestamp('comm_start')->nullable();
            $table->timestamp('comm_end')->nullable();
            $table->integer('operation_hour')->nullable();
            $table->string('cms_address', 150)->nullable();
            $table->string('maintenance_address', 200)->nullable();
            $table->string('mon_group_id', 200)->nullable();
            $table->binary('vec_group_id')->nullable();
            $table->timestamp('date')->nullable();
            $table->timestamp('lastchange')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets_vehicles');
    }
}
