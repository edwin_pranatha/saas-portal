<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FleetsCreateScreenGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets_screen_group', function (Blueprint $table) {
            $table->string('id_group', 7)->nullable();
            $table->string('nm_group', 25)->nullable();
            $table->string('desc_group', 100)->nullable();
            $table->string('data_group', 250)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets_screen_group');
    }
}
