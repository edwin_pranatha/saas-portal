<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FleetsCreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets_vehicles', function (Blueprint $table) {
            $table->integer('no_vehicle')->nullable();
            $table->string('id_vehicle')->nullable();
            $table->string('ip_vehicle_old')->nullable();
            $table->string('ip_vehicle')->nullable();
            $table->string('cat_vehicle')->nullable();
            $table->string('id_operator')->nullable();
            $table->string('id_region')->nullable();
            $table->string('is_wifi')->nullable();
            $table->string('is_beacon')->nullable();
            $table->string('is_cctv')->nullable();
            $table->string('screens_installed')->nullable();
            $table->string('res_installed')->nullable();
            $table->string('comm_start')->nullable();
            $table->string('comm_end')->nullable();
            $table->string('operation_hour')->nullable();
            $table->string('cms_address')->nullable();
            $table->string('maintenance_address')->nullable();
            $table->string('mon_group_id')->nullable();
            $table->string('vec_group_id')->nullable();
            $table->string('date')->nullable();
            $table->string('lastchange')->nullable();
        });
        \DB::statement("CREATE SEQUENCE no_vehicle_seq;");
        \DB::statement("ALTER TABLE fleets_vehicles ALTER COLUMN no_vehicle SET DEFAULT NEXTVAL('no_vehicle_seq');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DROP SEQUENCE no_vehicle_seq;");
        Schema::dropIfExists('fleets_vehicles');

    }
}
