<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FleetsCreateStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets_stops', function (Blueprint $table) {
            $table->integer('id_stop')->nullable();
            $table->smallInteger('direction_stop')->nullable();
            $table->integer('idx_stop')->nullable();
            $table->integer('id_line')->nullable();
            $table->string('num_line', 15)->nullable();
            $table->string('stop_code', 10)->nullable();
            $table->string('nm_stop', 150)->nullable();
            $table->string('addr_stop', 200)->nullable();
            $table->string('coordinate_stop', 150)->nullable();
            $table->string('special_stop', 200)->nullable();
            $table->string('poi_stop', 200)->nullable();
            $table->string('interconn_line', 200)->nullable();
            $table->string('cms_group', 200)->nullable();
            $table->string('cms_address', 200)->nullable();
            $table->integer('is_tts')->nullable();
            $table->integer('is_advsound')->nullable();
            $table->integer('is_cctv')->nullable();
            $table->string('tts_language', 10)->nullable();
            $table->string('screen_installed', 200)->nullable();
            $table->string('res_installed', 200)->nullable();
            $table->timestamp('comm_start')->nullable();
            $table->timestamp('comm_end')->nullable();
            $table->integer('operation_hour')->nullable();
            $table->string('default_layout', 200)->nullable();
            $table->timestamp('date')->nullable();
            $table->timestamp('lastchange')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets_stops');
    }
}
