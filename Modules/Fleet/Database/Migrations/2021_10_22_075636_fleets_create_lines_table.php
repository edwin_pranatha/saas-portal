<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FleetsCreateLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets_lines', function (Blueprint $table) {
            $table->string('num_line', 15)->nullable();
            $table->string('id_line', 10)->nullable();
            $table->string('short_nm_line', 200)->nullable();
            $table->string('id_region', 10)->nullable();
            $table->string('rev', 10)->nullable();
            $table->string('color', 7)->nullable();
            $table->string('textcolor', 7)->nullable();
            $table->string('symbol', 20)->nullable();
            $table->integer('delaytime')->nullable();
            $table->string('pickgroup', 200)->nullable();
            $table->string('stop_link', 250)->nullable();
            $table->integer('ishidden')->nullable();
            $table->timestamp('date')->nullable();
            $table->timestamp('lastchange')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets_lines');
    }
}
