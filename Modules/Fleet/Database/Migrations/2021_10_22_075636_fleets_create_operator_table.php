<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FleetsCreateOperatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets_operator', function (Blueprint $table) {
            $table->string('id_operator', 7)->nullable();
            $table->string('id_class', 10)->nullable();
            $table->string('nm_operator', 100)->nullable();
            $table->binary('prefix')->nullable();
            $table->string('cdn_server', 33)->nullable();
            $table->integer('istwofactor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets_operator');
    }
}
