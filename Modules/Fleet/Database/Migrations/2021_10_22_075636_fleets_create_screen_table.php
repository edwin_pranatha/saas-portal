<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FleetsCreateScreenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets_screen', function (Blueprint $table) {
            $table->string('id_screen', 10)->nullable();
            $table->string('nm_screen', 25)->nullable();
            $table->string('type_screen')->nullable();
            $table->integer('inch_screen')->nullable();
            $table->string('list_resolution', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets_screen');
    }
}
