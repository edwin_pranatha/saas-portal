@extends('theme::views.backend.app')

@section('page-title')
<h2>Vehicle Fleet Info</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Vehicle</span></li>
</ol>
@endsection


@section('module_css')
    <link rel="stylesheet" href="/assets/Fleet/css/vehicles.min.css" />
@endsection

@section('content')
{{-- 
@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('fleet:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('fleet:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('fleet:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('fleet:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('fleet:assets/vendor/font-awesome/css/font-awesome.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('fleet:css/bootstrap-datetimepicker.css') }}" media="screen">
<link rel="stylesheet" href="{{ Module::asset('fleet:datatable/buttons.dataTables.min.css') }}" >
<link rel="stylesheet" href="{{ Module::asset('fleet:css/vehicles.css') }}" >
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
@endpush --}}

<div class="row">
    @can('vehicles-create')
    <div class="col-md-6 col-lg-12 col-xl-6">
        <a class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary getVehicle" data-url="{{ route('admin.fleet.vehicles.data.fleet') }}" href="#modalFull">Add Vehicle</a>
    </div>
    @endcan

    <div id="modalFull" class="modal-block modal-block-lg mfp-hide" style="max-width: 1400px;">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Vehicle Fleet Info</h2>
            </header>
            <div class="panel-body">
                <div class="alert alert-danger print-error-msg" style="display: none;">
                    <ul></ul>
                </div>
                <div id="pleaseWaitFormVehicle" style="background-color: #000000a3;width: 98%;height: 480px;position: absolute;z-index: 2500;text-align: center;font-size: 30px;color: #FFF;padding-top: 230px;display: none;">Please Wait...</div>

                <div class="col-md-6">
                    <form class="form-horizontal" id="formVehiclesData" method="get" style="border-right: 2px solid #e6e6e6;">
                        <div id="idVehicles"></div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Vehicle ID</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control id_vehicle" id="inputDefault" name="id_vehicle">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Vehicle IP</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control ip_vehicle" id="inputDefault" name="ip_vehicle">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Vehicle Type</label>
                            <div class="col-md-6">
                                <select class="form-control cat_vehicle" name="cat_vehicle">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">PTO Name</label>
                            <div class="col-md-6">
                                <select class="form-control id_operator" name="id_operator" data-url="{{ route('admin.fleet.vehicles.data.region') }}">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Region</label>
                            <div class="col-md-6">
                                <select class="form-control id_region" name="id_region">
                                    <option>- Select Region -</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">WiFi Hotspots</label>
                            <div class="col-md-6">
                                <select class="form-control is_wifi" name="is_wifi">
                                    <option>- Availability -</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Beacon</label>
                            <div class="col-md-6">
                                <select class="form-control is_beacon" name="is_beacon">
                                    <option>- Availability -</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Commisioning Date</label>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <div class="controls input-append date form_datetime" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                        <input size="16" type="text" class="form-control input-sm comm_start" name="comm_start" id="comm_start" value="" style="width: 88%;display: inline-block;">
                                        <span class="add-on"><i class="icon-remove fa fa-trash"></i></span>
                                        <span class="add-on"><i class="icon-th fa fa-th"></i></span>
                                    </div>
                                    <input type="hidden" id="dtp_input1" class="comm_start" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Service End</label>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <div class="controls input-append date form_datetime" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                        <input size="16" type="text" class="form-control input-sm comm_end" name="comm_end" id="comm_end" value="" style="width: 88%;display: inline-block;">
                                        <span class="add-on"><i class="icon-remove fa fa-trash"></i></span>
                                        <span class="add-on"><i class="icon-th fa fa-th"></i></span>
                                    </div>
                                    <input type="hidden" id="dtp_input1" class="comm_end" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">CCTV, Passenger Count</label>
                            <div class="col-md-6">
                                <select class="form-control is_cctv" name="is_cctv">
                                    <option>- Availability -</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Operational Hours</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control operation_hour" id="inputDefault" name="operation_hour">
                            </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="col-md-3 control-label" for="inputDefault">Screen Group</label>
                            <div class="col-md-6">
                                <select class="form-control mon_group_id" name="mon_group_id">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label" for="inputDefault" style="text-align: center;">Monitor/Player Type</label>
                            <table class="table table-bordered table-striped mb-none" id="TableMonPlay">
                                <thead>
                                    <tr>
                                        <th><button type="button" class="btn btn-primary" id="addRowMoPlay">+</button></th>
                                        <th>Type</th>
                                        <th>Inch</th>
                                        <th>Aspect</th>
                                        <th>Resolution</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <select class="form-control type" name="type[0]">
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control inch" name="inch[0]">
                                                <option>- No Data -</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control aspect" name="aspect[0]">
                                                <option value="">- No Data -</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control resolution" name="resolution[0]">
                                                <option value="">- No Data -</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="button" class="btn btn-default" id="closeVehicleData">Close</button>
                        <button type="button" class="btn btn-primary" id="saveVehiclesData" data-url="{{ route('admin.fleet.vehicles.store') }}">Save Changes</button>
                        <button type="button" class="btn btn-primary" id="editVehiclesData" style="display: none;">Save Changes</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>

    <div id="modalCenterIcon" class="modal-block modal-block-primary mfp-hide">
        <section class="panel">
            <div class="panel-body text-center">
                <div class="modal-wrapper">
                    <div class="modal-icon center">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <h4>Are you sure?</h4>
                        <p>Are you sure that you want to delete this Vehicle <span id="idVehiclesDel"></span>?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary delVehicle">Yes</button>
                        <button class="btn btn-default modal-dismiss">No</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>

    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>
                        <h2 class="panel-title">Vehicle</h2>
                    </header>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped mb-none" id="dataTableRowVehicle">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Bus ID</th>
                                    <th>Bus Type</th>
                                    <th>Region</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
 </div>
 {{--
@push('data-table')
<script src="{{ Module::asset('fleet:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('fleet:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('fleet:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('fleet:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('fleet:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('fleet:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('fleet:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('fleet:js/bootstrap-datetimepicker.js') }}" charset="UTF-8"></script>
<script src="{{ Module::asset('fleet:js/locales/bootstrap-datetimepicker.fr.js') }}" charset="UTF-8"></script>
<script src="{{ Module::asset('fleet:compiled/vehicles.min.js') }}"></script>
@endpush --}}

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Fleet/js/vehicles.min.js"></script>
@endsection
