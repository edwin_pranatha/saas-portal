@extends('theme::views.backend.app')

@section('page-title')
<h2>Bus Line Info</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Routes</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Fleet/css/routes.min.css" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('fleet:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('fleet:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('fleet:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('fleet:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('fleet:datatable/buttons.dataTables.min.css') }}" >
@endpush --}}

<div class="row">
    @can('routes-create')
    <div class="col-md-6 col-lg-12 col-xl-6">
        <a class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary" href="#modalFull">Add Routes</a>
    </div>
    @endcan

    <div id="modalFull" class="modal-block modal-block-lg mfp-hide" style="max-width: 1400px;">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Add New Routes</h2>
            </header>
            <div class="panel-body">
                <div class="alert alert-danger print-error-msg" style="display: none;">
                    <ul></ul>
                </div>
                <div id="pleaseWaitFormRoutes" style="background-color: #000000a3;width: 98%;height: 190px;position: absolute;z-index: 2500;text-align: center;font-size: 30px;color: #FFF;padding-top: 80px;display: none;">Please Wait...</div>
                <form class="form-horizontal" id="formRoutesData" method="get">
                    <div id="idRoutes"></div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Routes Code</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control num_line" name="num_line">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Routes ID</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control id_line" name="id_line">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Routes Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control short_nm_line" name="short_nm_line">
                        </div>
                    </div>
                </form>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="button" class="btn btn-default" id="closeRoutesData">Close</button>
                        <button type="button" class="btn btn-primary" id="saveRoutesData" data-url="{{ route('admin.fleet.routes.store') }}">Save Changes</button>
                        <button type="button" class="btn btn-primary" id="editRoutesData" style="display: none;">Save Changes</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>

    <div id="modalCenterIcon" class="modal-block modal-block-primary mfp-hide">
        <section class="panel">
            <div class="panel-body text-center">
                <div class="modal-wrapper">
                    <div class="modal-icon center">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <h4>Are you sure?</h4>
                        <p>Are you sure that you want to delete this Routes Code <span id="idRoutesDel"></span>?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary delRoutes">Yes</button>
                        <button class="btn btn-default modal-dismiss">No</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>

    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>
                        <h2 class="panel-title">Vehicle</h2>
                    </header>
                    <input type="hidden" name="urlinfo" data-url="{{ route('admin.fleet.routes.index') }}">
                    <div class="panel-body">
                        <table class="table table-bordered table-striped mb-none" id="dataTableRowRoutes">
                            <thead>
                                <tr>
                                    <th>Routes Code</th>
                                    <th>Routes Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('fleet:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('fleet:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('fleet:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('fleet:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('fleet:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('fleet:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('fleet:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('fleet:compiled/routes.min.js') }}"></script>
@endpush --}}


@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Fleet/js/routes.min.js"></script>
@endsection
