$(document).ready(function(){ 
    var norowplay = 2;
    var norowarray = 1;

    $('.modal-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,   
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $('.modal-sizes').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,   
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $(document).on('click', '.modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    $('select.populate').select2();

    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function(item) {
                return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
            }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, 
            opener: function(element) {
                return element.find('img');
            }
        }       
    });
       
    var tableRowVehicle = $('#dataTableRowVehicle').dataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': '/admin/infovision/vehicles',
            'type': 'POST',
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        'columns': [
            { data: 'DT_RowIndex', "width" : "30px" },
            { data: 'id_vehicle', "width" : "80px" },
            { data: 'cat_vehicle', "width" : "80px" },
            { data: 'id_region', "width" : "150px" },
            { data: 'action', "width" : "150px" },
        ],
        'fnDrawCallback': function () {
            $('.modal-sizes').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,   
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });

            $(".editVehicle").click(function() {
                var dataUrl = $('.getVehicle').attr('data-url');
                var id = $(this).attr('id');
                $('#editVehiclesData').css('display','inline-block');
                $('#saveVehiclesData').css('display','none');
                $('.form_datetime').datetimepicker({
                    format: "yyyy-mm-dd hh:ii:ss",
                    autoclose: true,
                    todayBtn: true,
                    defaultDate: getNowDate(),
                    fontAwesome: 'font-awesome'
                });
                getDataDefaultVehicle(dataUrl, id);
            });

            $(".getPopupDelVehicle").click(function() {
                var id = $(this).attr('id');
                var split = id.split('-');
                $('#idVehiclesDel').text(split[1]);
                $('.delVehicle').attr('id', split[0]);
            });
        }
    });

    $(".delVehicle").click(function(e) {
        var id = $(this).attr('id');
        var iplocaladdress = window.location.href;
        $.ajax({
            type: "POST",
            url: iplocaladdress + '/' + id + '/delete',
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(result){
                var busid = result.success;
                e.preventDefault();
                $.magnificPopup.close();
                tableRowVehicle.fnDraw(false);
                new PNotify({
                    title: 'Remove Success',
                    text: 'Vehicle ' + busid + ' has been successfully deleted',
                    type: 'success'
                });
            }
        });
    });

    $("#addRowMoPlay").click(function() {
        var html = '<tr>';
            html += '<td>' + norowplay + '</td>';
            html += '<td><select class="form-control type" name="type[' + norowarray + ']"><option value="">- Select -</option></select></td>';
            html += '<td><select class="form-control inch" name="inch[' + norowarray + ']"><option value="">- No Data -</option></select></td>';
            html += '<td><select class="form-control aspect" name="aspect[' + norowarray + ']"><option value="">- No Data -</option></select></td>';
            html += '<td><select class="form-control resolution" name="resolution[' + norowarray + ']"><option value="">- No Data -</option></select></td>';
            html += '</tr>'
        $('#TableMonPlay > tbody:first').append(html);
        getDataScreen(norowarray);
        norowplay++;
        norowarray++;
    });

    $(document).on('click', '#closeVehicleData', function (e) {
        $('#formVehiclesData')[0].reset();
        $('#idVehicles').html('');
        generateRowTableVehicle();
        $('#editVehiclesData').css('display','none');
        $('#saveVehiclesData').css('display','inline-block');
        norowarray = 1;
        norowplay = 2;
        e.preventDefault();
        $.magnificPopup.close();
    });

    function generateRowTableVehicle(){
        $('#TableMonPlay > tbody').html('');
        var html = '<tr>';
            html += '<td>1</td>';
            html += '<td><select class="form-control type" name="type[0]"><option value="">- Select -</option></select></td>';
            html += '<td><select class="form-control inch" name="inch[0]"><option value="">- No Data -</option></select></td>';
            html += '<td><select class="form-control aspect" name="aspect[0]"><option value="">- No Data -</option></select></td>';
            html += '<td><select class="form-control resolution" name="resolution[0]"><option value="">- No Data -</option></select></td>';
            html += '</tr>'
        $('#TableMonPlay > tbody:first').append(html);
    }

    $("#saveVehiclesData").click(function(e) {
        $('#pleaseWaitFormVehicle').css('display','block');
        $(this).text('Loading...');
        $(this).attr('disabled','disabled');
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','none');
        var $form = $('input,select');
        var formData = $form.serializeArray();
        $.ajax({
            url: $('#formVehiclesData').attr('data-url'),
            data: formData,                     
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function(data){
                $('#pleaseWaitFormVehicle').css('display','none');
                $('#saveVehiclesData').text('Save Changes');
                $('#saveVehiclesData').removeAttr('disabled');
                if($.isEmptyObject(data.error)){
                    norowarray = 1;
                    norowplay = 2;
                    $('#formVehiclesData')[0].reset();
                    generateRowTableVehicle();
                    e.preventDefault();
                    $.magnificPopup.close();
                    tableRowVehicle.fnDraw(false);
                }else{
                    printErrorMsg(data.error);
                }
            }
        });
    });

    $("#editVehiclesData").click(function(e) {
        $('#pleaseWaitFormVehicle').css('display','block');
        $(this).text('Loading...');
        $(this).attr('disabled','disabled');
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','none');
        var iplocaladdress = window.location.href;
        var $form = $('input,select');
        var formData = $form.serializeArray();
        var id = $('.id_vehicle').val();
        console.log(id);
        $.ajax({
            url: iplocaladdress +'/'+ id,
            data: formData,                     
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function(data){
                $('#pleaseWaitFormVehicle').css('display','none');
                $('#editVehiclesData').text('Save Changes');
                $('#editVehiclesData').removeAttr('disabled');
                if($.isEmptyObject(data.error)){
                    norowarray = 1;
                    norowplay = 2;
                    $('#formVehiclesData')[0].reset();
                    $('#idVehicles').html('');
                    generateRowTableVehicle();
                    $('#editVehiclesData').css('display','none');
                    $('#saveVehiclesData').css('display','inline-block');
                    e.preventDefault();
                    $.magnificPopup.close();
                    tableRowVehicle.fnDraw(false);
                }else{
                    printErrorMsg(data.error);
                }
            }
        });
    });

    $(".getVehicle").click(function() {
        var urlData = $(this).attr('data-url');
        $('.form_datetime').datetimepicker({
            format: "yyyy-mm-dd hh:ii:ss",
            autoclose: true,
            todayBtn: true,
            defaultDate: getNowDate(),
            fontAwesome: 'font-awesome'
        });
        getDataDefaultVehicle(urlData, null);
    });

    function getDataDefaultVehicle(urlData, id){
        $.ajax({
            type: "GET",
            url: urlData,
            cache: false,
            success: function(result){
                var models = result['models'];

                var operator = models['operator'];
                $('.id_operator').html('<option>- Select PTO -</option>');
                for (var i = 0; i < operator.length; i++) {
                    $('.id_operator').append('<option value="' + operator[i]['id_operator'] + '">' + operator[i]['nm_operator'] + '</option>');
                }

                var screengroup = models['screen_group'];
                $('.mon_group_id').html('<option>- Select Screen -</option>');
                for (var i = 0; i < screengroup.length; i++) {
                    $('.mon_group_id').append('<option value="' + screengroup[i]['id_group'] + '">' + screengroup[i]['nm_group'] + '</option>');
                }

                var vehiclecat = models['vehicle_cat'];
                $('.cat_vehicle').html('<option>- Select Type -</option>');
                for (var i = 0; i < vehiclecat.length; i++) {
                    $('.cat_vehicle').append('<option value="' + vehiclecat[i]['id_cat'] + '">' + vehiclecat[i]['cat_nm'] + '</option>');
                }

                $('.id_region').html('<option>- Select Region -</option>');

                getDataScreen(0);

                if(id!==null){
                    editDataForm(id);
                }         
            }
        });
    }

    function editDataForm(id){
        var iplocaladdress = window.location.href;
        var operator_temp = '';
        $.ajax({
            type: "GET",
            url: iplocaladdress + '/' + id + '/edit',
            cache: false,
            success: function(result){
                var models = result['models'];
                jQuery.each(models, function(i, val) {
                    $('.' + i).val(val);
                    if(i=='id_operator'){
                        operator_temp = val;
                    }else if(i=='id_region'){
                        var dataUrl = $('.id_operator').attr('data-url');
                        getValRegion(operator_temp, dataUrl, val);
                    }
                });
                var detail = result['detail'];
                $('#TableMonPlay > tbody').html('');
                norowplay = 1;
                norowarray = 0;
                for (var i = 0; i < detail.length; i++) {
                    var html = '<tr id="tr-' + i + '">';
                    html += '<td>' + norowplay + '</td>';
                    html += '<td><select class="form-control type" name="type[' + norowarray + ']">option value="">- Select -</option><option value="TFT">TFT</option><option value="LED">LED</option></select></td>';
                    html += '<td><select class="form-control inch" name="inch[' + norowarray + ']"><option value="">- No Data -</option><option value="19">19</option><option value="27">27</option><option value="29">29</option><option value="37">37</option><option value="42">42</option></select></td>';
                    html += '<td><select class="form-control aspect" name="aspect[' + norowarray + ']"><option value="">- No Data -</option><option value="16">16:4</option><option value="4">4:3</option></select></td>';
                    html += '<td><select class="form-control resolution" name="resolution[' + norowarray + ']"><option value="">- No Data -</option><option value="1152x864">1152x864</option><option value="1024x768">1024x768</option><option value="832x624">832x624</option><option value="800x600">800x600</option><option value="640x480">640x480</option><option value="1440x576">1440x576</option><option value="720x576">720x576</option><option value="1366x768">1366x768</option></select></td>';
                    html += '</tr>';
                    $('#TableMonPlay > tbody:first').append(html);
                    $('#tr-' + i + ' .type').val(detail[i]['type']);
                    $('#tr-' + i + ' .inch').val(detail[i]['inch']);
                    $('#tr-' + i + ' .aspect').val(detail[i]['aspect']);
                    $('#tr-' + i + ' .resolution').val(detail[i]['resolution']);
                    norowarray++;
                    norowplay++;
                }
            }
        });
    }

    function getDataScreen(a){
        $.ajax({
            type: "GET",
            url: "/admin/infovision/vehicles/data/screen",
            cache: false,
            success: function(result){
                var models = result['models'];
                $("[name='type[" + a + "]']").html('<option>- Select -</option>');
                for (var i = 0; i < models.length; i++) {
                    $("[name='type[" + a + "]']").append('<option value="' + models[i]['type_screen'] + '">' + models[i]['type_screen'] + '</option>');
                }
            }
        });
    }

    $(".id_operator").change(function() {
        var values = $(this).val();
        var urlData = $(this).attr('data-url');
        getValRegion(values, urlData, null);
    });

    function getValRegion(values, urlData, idregion){
        $.ajax({
            type: "GET",
            url: urlData,
            data: {
                id: values
            },
            cache: false,
            success: function(result){
                var models = result['models'];
                $('.id_region').html('<option>- Select Region -</option>');
                for (var i = 0; i < models.length; i++) {
                    $('.id_region').append('<option value="' + models[i]['id_region'] + '">' + models[i]['nm_region'] + '</option>');
                }
                if(idregion!==null){
                    $('.id_region').val(idregion);   
                }
            }
        });
    }

    $(document).on("change", ".type", function () {
        var values = $(this).val();
        var name = $(this).attr('name');
        getDataInch(values, name);
    });

    function getDataInch(values, name){
        var split = name.split('type');
        $.ajax({
            type: "GET",
            url: "/admin/infovision/vehicles/data/inch",
            data: {
                id: values
            },
            cache: false,
            success: function(result){
                var models = result['models'];
                $("[name='inch" + split[1] + "']").html('<option>- No Data -</option>');
                for (var i = 0; i < models.length; i++) {
                    $("[name='inch" + split[1] + "']").append('<option value="' + models[i]['inch_screen'] + '">' + models[i]['inch_screen'] + '</option>');
                }
            }
        });
    }

    $(document).on("change", ".inch", function () {
        var values = $(this).val();
        var name = $(this).attr('name');
        getDataAspect(values, name);
    });

    function getDataAspect(values, name){
        var split = name.split('inch');
        var typeval = $("[name='type" + split[1] + "']").val();
        $.ajax({
            type: "GET",
            url: "/admin/infovision/vehicles/data/aspect",
            data: {
                id: values,
                type: typeval
            },
            cache: false,
            success: function(result){
                var models = result['models'];
                $("[name='aspect" + split[1] + "']").html('<option>- No Data -</option>');
                for (var i = 0; i < models.length; i++) {
                    $("[name='aspect" + split[1] + "']").append('<option value="' + models[i]['aspectratio'] + '">' + models[i]['aspectratio'] + '</option>');
                }
            }
        });
    }

    $(document).on("change", ".aspect", function () {
        var values = $(this).val();
        var name = $(this).attr('name');
        getDataResolution(values, name);
    });

    function getDataResolution(values, name){
        var split = name.split('aspect');
        $.ajax({
            type: "GET",
            url: "/admin/infovision/vehicles/data/resolution",
            data: {
                id: values,
            },
            cache: false,
            success: function(result){
                var models = result['models'];
                $("[name='resolution" + split[1] + "']").html('<option>- No Data -</option>');
                for (var i = 0; i < models.length; i++) {
                    $("[name='resolution" + split[1] + "']").append('<option value="' + models[i]['nm_res'] + '">' + models[i]['nm_res'] + '</option>');
                }
            }
        });
    }
});