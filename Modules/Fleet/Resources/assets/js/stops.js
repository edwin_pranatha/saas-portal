        $(document).ready(function(){
            $('.modal-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,   
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });
        
            $('.modal-sizes').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,   
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });
        
            $(document).on('click', '.modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
        
            $('select.populate').select2();
        
            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: false,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: true,
                    titleSrc: function(item) {
                        return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
                    }
                },
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, 
                    opener: function(element) {
                        return element.find('img');
                    }
                }       
            });
            
            var tableRowStops = $('#dataTableRowStops').dataTable({
                'processing': true,
                'serverSide': true,
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'id_stop', "width" : "80px" },
                    { data: 'direction_stop', "width" : "150px" },
                    { data: 'num_line', "width" : "150px" },
                    { data: 'nm_stop', "width" : "150px" },
                    { data: 'action', "width" : "150px" },
                ],
                'fnDrawCallback': function () {
                    $('.modal-sizes').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,   
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in',
                        modal: true
                    });

                    $(".editStops").click(function() {
                        showListLine();
                        $('#editStopsData').css('display','inline-block');
                        $('#saveStopsData').css('display','none');
                        var id = $(this).attr('id');
                        var iplocaladdress = window.location.href;
                        $.ajax({
                            type: "GET",
                            url: iplocaladdress + '/' + id + '/edit',
                            cache: false,
                            success: function(result){
                                var models = result['models'];
                                var stoplinedata = '';
                                jQuery.each(models, function(i, val) {
                                    $('.' + i).val(val);
                                    if(i=='id_stop'){
                                        $('#idStops').html('<input type="hidden" value="' + val + '">');
                                    }else if(i=='id_line'){
                                        stoplinedata = val;
                                    }else if(i=='num_line'){
                                        stoplinedata = val + '-' + stoplinedata;
                                        $('.stop_line').val(stoplinedata);
                                    }else if(i=='coordinate_stop'){
                                        if(val!==null){
                                            var split = val.split(',');
                                            $('.lat').val(split[0]);
                                            $('.long').val(split[1]);
                                        }
                                    }
                                });
                            }
                        });
                    });

                    $(".getPopupDelStops").click(function() {
                        var id = $(this).attr('id');
                        var split = id.split('-');
                        $('#idStopsDel').text(split[1]);
                        $('.delStops').attr('id', split[0]);
                    });
                }
            });

            $(".delStops").click(function(e) {
                var id = $(this).attr('id');
                var iplocaladdress = window.location.href;
                $.ajax({
                    type: "POST",
                    url: iplocaladdress + '/' + id + '/delete',
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(result){
                        var busstopid = result.success;
                        e.preventDefault();
                        $.magnificPopup.close();
                        tableRowStops.fnDraw(false);
                        new PNotify({
                            title: 'Remove Success',
                            text: 'Stop ID ' + busstopid + ' has been successfully deleted',
                            type: 'success'
                        });
                    }
                });
            });

            $("#showPopBusStop").click(function(e) {
                showListLine();
            });

            function showListLine(){
                var iplocaladdress = window.location.href;
                $.ajax({
                    type: "GET",
                    url: iplocaladdress + '/show/line',
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(result){
                        var data = result.models;
                        $('.stop_line').html('');
                        $('.stop_line').append('<option value="">- Select Line Code -</option>');
                        for (var i = 0; i < data.length; i++) {
                            $('.stop_line').append('<option value="' + data[i].num_line + '-' + data[i].id_line + '">' + data[i].short_nm_line + '</option>');
                        }
                    }
                });
            }

            $(document).on('click', '#closeStopsData', function (e) {
                $('#formStopsData')[0].reset();
                $('#idStops').html('');
                $('#editStopsData').css('display','none');
                $('#saveStopsData').css('display','inline-block');
                e.preventDefault();
                $.magnificPopup.close();
            });

            $("#saveStopsData").click(function(e) {
                $('#pleaseWaitFormStops').css('display','block');
                $(this).text('Loading...');
                $(this).attr('disabled','disabled');
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").css('display','none');
                var $form = $('input,select');
                var formData = $form.serializeArray();
                $.ajax({
                    url: $(this).attr('data-url'),
                    data: formData,                     
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data){
                        $('#pleaseWaitFormStops').css('display','none');
                        $('#saveStopsData').text('Save Changes');
                        $('#saveStopsData').removeAttr('disabled');
                        if($.isEmptyObject(data.error)){
                            $('#formStopsData')[0].reset();
                            e.preventDefault();
                            $.magnificPopup.close();
                            tableRowStops.fnDraw(false);
                        }else{
                            printErrorMsg(data.error);
                        }
                    }
                });
            });

            $("#editStopsData").click(function(e) {
                $('#pleaseWaitFormStops').css('display','block');
                $(this).text('Loading...');
                $(this).attr('disabled','disabled');
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").css('display','none');
                var iplocaladdress = window.location.href;
                var $form = $('input,select');
                var formData = $form.serializeArray();
                var id = $('#idStops input').val();
                $.ajax({
                    url: iplocaladdress +'/'+ id,
                    data: formData,                     
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data){
                        $('#pleaseWaitFormStops').css('display','none');
                        $('#editStopsData').text('Save Changes');
                        $('#editStopsData').removeAttr('disabled');
                        if($.isEmptyObject(data.error)){
                            $('#formStopsData')[0].reset();
                            $('#editStopsData').css('display','none');
                            $('#saveStopsData').css('display','inline-block');
                            e.preventDefault();
                            $.magnificPopup.close();
                            tableRowStops.fnDraw(false);
                        }else{
                            printErrorMsg(data.error);
                        }
                    }
                });
            });
        });