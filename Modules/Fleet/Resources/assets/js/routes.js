        $(document).ready(function(){
            $('.modal-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,   
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });
        
            $('.modal-sizes').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,   
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });
        
            $(document).on('click', '.modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
        
            $('select.populate').select2();
        
            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: false,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: true,
                    titleSrc: function(item) {
                        return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
                    }
                },
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, 
                    opener: function(element) {
                        return element.find('img');
                    }
                }       
            });
            
            var tableRowRoutes = $('#dataTableRowRoutes').dataTable({
                'processing': true,
                'serverSide': true,
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'num_line', "width" : "80px" },
                    { data: 'id_line', "width" : "150px" },
                    { data: 'action', "width" : "150px" },
                ],
                'fnDrawCallback': function () {
                    $('.modal-sizes').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,   
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in',
                        modal: true
                    });

                    $(".editRoutes").click(function() {
                        $('#editRoutesData').css('display','inline-block');
                        $('#saveRoutesData').css('display','none');
                        var id = $(this).attr('id');
                        var iplocaladdress = window.location.href;
                        $.ajax({
                            type: "GET",
                            url: iplocaladdress + '/' + id + '/edit',
                            cache: false,
                            success: function(result){
                                var models = result['models'];
                                jQuery.each(models, function(i, val) {
                                    $('.' + i).val(val);
                                    if(i=='num_line'){
                                        $('#idRoutes').html('<input type="hidden" value="' + val + '">');
                                    }
                                });
                            }
                        });
                    });

                    $(".getPopupDelRoutes").click(function() {
                        var id = $(this).attr('id');
                        var split = id.split('-');
                        $('#idRoutesDel').text(split[1]);
                        $('.delRoutes').attr('id', split[0]);
                    });
                }
            });

            $(".delRoutes").click(function(e) {
                var id = $(this).attr('id');
                var iplocaladdress = window.location.href;
                $.ajax({
                    type: "POST",
                    url: iplocaladdress + '/' + id + '/delete',
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(result){
                        var routescode = result.success;
                        e.preventDefault();
                        $.magnificPopup.close();
                        tableRowRoutes.fnDraw(false);
                        new PNotify({
                            title: 'Remove Success',
                            text: 'Routes Code ' + routescode + ' has been successfully deleted',
                            type: 'success'
                        });
                    }
                });
            });

            $(document).on('click', '#closeRoutesData', function (e) {
                $('#formRoutesData')[0].reset();
                $('#idRoutes').html('');
                $('#editRoutesData').css('display','none');
                $('#saveRoutesData').css('display','inline-block');
                e.preventDefault();
                $.magnificPopup.close();
            });

            $("#saveRoutesData").click(function(e) {
                $('#pleaseWaitFormRoutes').css('display','block');
                $(this).text('Loading...');
                $(this).attr('disabled','disabled');
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").css('display','none');
                var $form = $('input,select');
                var formData = $form.serializeArray();
                $.ajax({
                    url: $(this).attr('data-url'),
                    data: formData,                     
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data){
                        $('#pleaseWaitFormRoutes').css('display','none');
                        $('#saveRoutesData').text('Save Changes');
                        $('#saveRoutesData').removeAttr('disabled');
                        if($.isEmptyObject(data.error)){
                            $('#formRoutesData')[0].reset();
                            e.preventDefault();
                            $.magnificPopup.close();
                            tableRowRoutes.fnDraw(false);
                        }else{
                            printErrorMsg(data.error);
                        }
                    }
                });
            });

            $("#editRoutesData").click(function(e) {
                $('#pleaseWaitFormRoutes').css('display','block');
                $(this).text('Loading...');
                $(this).attr('disabled','disabled');
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").css('display','none');
                var iplocaladdress = window.location.href;
                var $form = $('input,select');
                var formData = $form.serializeArray();
                var id = $('#idRoutes input').val();
                $.ajax({
                    url: iplocaladdress +'/'+ id,
                    data: formData,                     
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data){
                        $('#pleaseWaitFormRoutes').css('display','none');
                        $('#editRoutesData').text('Save Changes');
                        $('#editRoutesData').removeAttr('disabled');
                        if($.isEmptyObject(data.error)){
                            $('#formRoutesData')[0].reset();
                            $('#editRoutesData').css('display','none');
                            $('#saveRoutesData').css('display','inline-block');
                            e.preventDefault();
                            $.magnificPopup.close();
                            tableRowRoutes.fnDraw(false);
                        }else{
                            printErrorMsg(data.error);
                        }
                    }
                });
            });
        });