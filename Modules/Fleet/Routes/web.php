<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/admin/infovision')->middleware(['auth'])->name('admin.fleet.')->group(function() {
    //Vehicles
    Route::get('vehicles/data/fleet', 'Backend\FleetController@getDataFleet')->name('vehicles.data.fleet');
    Route::get('vehicles/data/region', 'Backend\FleetController@getDataRegion')->name('vehicles.data.region');
    Route::get('vehicles/data/screen', 'Backend\FleetController@getDataScreen')->name('vehicles.data.screen');
    Route::get('vehicles/data/inch', 'Backend\FleetController@getDataInch')->name('vehicles.data.inch');
    Route::get('vehicles/data/aspect', 'Backend\FleetController@getDataAspect')->name('vehicles.data.aspect');
    Route::get('vehicles/data/resolution', 'Backend\FleetController@getDataResolution')->name('vehicles.data.resolution');

    Route::get('vehicles', 'Backend\FleetController@index')->name('vehicles.index');
    Route::post('vehicles', 'Backend\FleetController@index')->name('vehicles.index');
    Route::post('vehicles/store', 'Backend\FleetController@store')->name('vehicles.store');
    Route::get('vehicles/{id}/edit', 'Backend\FleetController@edit')->name('vehicles.edit');
    Route::post('vehicles/{id}', 'Backend\FleetController@update')->name('vehicles.update');
    Route::post('vehicles/{id}/delete', 'Backend\FleetController@destroy')->name('vehicles.delete');

    //Routes
    Route::get('routes', 'Backend\RoutesController@index')->name('routes.index');
    Route::post('routes', 'Backend\RoutesController@index')->name('routes.index');
    Route::post('routes/store', 'Backend\RoutesController@store')->name('routes.store');
    Route::get('routes/{id}/edit', 'Backend\RoutesController@edit')->name('routes.edit');
    Route::post('routes/{id}', 'Backend\RoutesController@update')->name('routes.update');
    Route::post('routes/{id}/delete', 'Backend\RoutesController@destroy')->name('routes.delete');

    //Stops
    Route::get('stops', 'Backend\StopsController@index')->name('stops.index');
    Route::post('stops', 'Backend\StopsController@index')->name('stops.index');
    Route::post('stops/store', 'Backend\StopsController@store')->name('stops.store');
    Route::get('stops/{id}/edit', 'Backend\StopsController@edit')->name('stops.edit');
    Route::post('stops/{id}', 'Backend\StopsController@update')->name('stops.update');
    Route::post('stops/{id}/delete', 'Backend\StopsController@destroy')->name('stops.delete');
    Route::get('stops/show/line', 'Backend\StopsController@showLine')->name('stops.show.line');
    Route::get('stops/show/stop/{id}', 'Backend\StopsController@showStop')->name('stops.show.stops');
});

// Route::prefix('/admin/fleet')->middleware(['auth'])->name('admin.')->group(function() {
    
//     // Route::get('/routes', 'RoutesController@index')->name("fleet.routes");
//     // Route::get('/stops', 'StopsController@index')->name("fleet.stops");
    
//     Route::get('/routes', 'Backend\FleetController@index')->name("fleet.routes");
//     Route::get('/stops', 'Backend\StopsController@index')->name("fleet.stops");

//     Route::get('/vehicles', 'Backend\FleetController@index')->name("fleet.vehicles");
//     Route::post('/vehicles', 'Backend\FleetController@index')->name('fleet.vehicles');
//     Route::post('/vehicles/store', 'Backend\FleetController@store')->name('fleet.vehicles.store');
//     Route::get('/vehicles/{id}/edit', 'Backend\FleetController@edit')->name('fleet.vehicles.edit');
//     Route::post('/vehicles/{id}', 'Backend\FleetController@update')->name('fleet.vehicles.update');
//     Route::post('/vehicles/{id}/delete', 'Backend\FleetController@destroy')->name('fleet.vehicles.delete');

// });

// Route::prefix('fleet')->group(function() {
//     Route::get('/', 'FleetController@index');
// });
