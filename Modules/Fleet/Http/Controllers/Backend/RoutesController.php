<?php

namespace Modules\Fleet\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Fleet\Entities\Lines;
use Validator;
use Response;
use Auth;

class RoutesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $models = Lines::orderBy('num_line','asc')->get();
            return datatables()->of($models)
                ->addColumn('action', function ($row) {
                    $html = '';
                    if(Auth::user()->can('routes-edit')==1){
                        $html = '<a href="#modalFull" class="mb-xs mt-xs mr-xs modal-sizes btn btn-sm btn-primary editRoutes" id="'.$row->num_line.'">Edit</a> ';
                    }
                    if(Auth::user()->can('routes-delete')==1){
                        $html .= '<a href="#modalCenterIcon" class="mb-xs mt-xs modal-sizes mr-xs btn btn-sm btn-danger getPopupDelRoutes" id="'.$row->num_line.'-'.$row->id_line.'">Delete</a>';
                    }
                    return $html;
                })->addIndexColumn()->toJson();
        }

        return view('fleet::routes.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('fleet::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'num_line'          => 'required',
            'id_line'           => 'required',
            'short_nm_line'     => 'required'
        ]);

        if($validator->passes()){
            $models                     = New Lines;
            $models->num_line           = $request->num_line;
            $models->id_line            = $request->id_line;
            $models->short_nm_line      = $request->short_nm_line;
            $models->date               = date("Y-m-d H:i:s");
            $models->lastchange         = date("Y-m-d H:i:s");
            $models->save();

            return response()->json(['success' => 'success']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('fleet::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $models = Lines::where('num_line', $id)->first();
        $data = array();
        $data['models'] = $models;
        return Response::json($data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'num_line'          => 'required',
            'id_line'           => 'required',
            'short_nm_line'     => 'required'
        ]);

        if($validator->passes()){
            $models                     = Lines::where('num_line', $id)->first();
            $models->num_line           = $request->num_line;
            $models->id_line            = $request->id_line;
            $models->short_nm_line      = $request->short_nm_line;
            $models->date               = date("Y-m-d H:i:s");
            $models->lastchange         = date("Y-m-d H:i:s");
            $models->save();

            return response()->json(['success' => 'success']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $models = Lines::where('num_line', $id)->first();;
        $routescode = $models->routes_code;
        $delete = Lines::where('num_line', $id)->delete();
        return response()->json(['success' => $routescode]);
    }
}
