<?php

namespace Modules\Fleet\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Fleet\Entities\Stops;
use Modules\Fleet\Entities\Lines;
use Validator;
use Response;
use Auth;

class StopsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $models = Stops::orderBy('id_stop','asc')->get();
            return datatables()->of($models)
                ->addColumn('action', function ($row) {
                    $html = '';
                    if(Auth::user()->can('stops-edit')==1){
                        $html = '<a href="#modalFull" class="mb-xs mt-xs mr-xs modal-sizes btn btn-sm btn-primary editStops" id="'.$row->id_stop.'">Edit</a> ';
                    }
                    if(Auth::user()->can('stops-delete')==1){
                        $html .= '<a href="#modalCenterIcon" class="mb-xs mt-xs modal-sizes mr-xs btn btn-sm btn-danger getPopupDelStops" id="'.$row->id_stop.'-'.$row->id_stop.'">Delete</a>';
                    }
                    return $html;
                })->addIndexColumn()->toJson();
        }

        return view('fleet::stops.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('fleet::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_stop'           => 'required',
            'direction_stop'    => 'required',
            'addr_stop'         => 'required',
            'lat'               => 'required',
            'long'              => 'required',
            'nm_stop'           => 'required',
            'stop_line'         => 'required'
        ]);

        if($validator->passes()){
            $exp = explode('-', $request->stop_line);
            $numline = $exp[0];
            $idline = $exp[1];

            $models                     = New Stops;
            $models->id_stop            = $request->id_stop;
            $models->direction_stop     = $request->direction_stop;
            $models->addr_stop          = $request->addr_stop;
            $models->coordinate_stop    = $request->lat.','.$request->long;
            $models->nm_stop            = $request->nm_stop;
            $models->id_line            = $idline;
            $models->num_line           = $numline;
            $models->save();

            return response()->json(['success' => 'success']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('fleet::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $models = Stops::where('id_stop', $id)->first();
        $data = array();
        $data['models'] = $models;
        return Response::json($data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'id_stop'           => 'required',
            'direction_stop'    => 'required',
            'addr_stop'         => 'required',
            'lat'               => 'required',
            'long'              => 'required',
            'nm_stop'           => 'required',
            'stop_line'         => 'required'
        ]);

        if($validator->passes()){
            $exp = explode('-', $request->stop_line);
            $numline = $exp[0];
            $idline = $exp[1];

            $models                     = Stops::where('id_stop', $id)->first();
            $models->id_stop            = $request->id_stop;
            $models->direction_stop     = $request->direction_stop;
            $models->addr_stop          = $request->addr_stop;
            $models->coordinate_stop    = $request->lat.','.$request->long;
            $models->nm_stop            = $request->nm_stop;
            $models->id_line            = $idline;
            $models->num_line           = $numline;
            $models->save();

            return response()->json(['success' => 'success']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $models = Stops::where('id_stop', $id)->first();
        $busstopid = $models->id_stop;
        $delete = Stops::where('id_stop', $id)->delete();
        return response()->json(['success' => $busstopid]);
    }

    public function showLine()
    {
        $models = Lines::all();

        return response()->json(['models' => $models]);   
    }

    public function showStop($id)
    {
        $exp = explode(',', $id);
        $models = Stops::join('fleets_lines', 'fleets_lines.num_line', '=', 'fleet_stops.num_line')->whereIn('fleets_lines.id_line', $exp)->orderBy('fleets_lines.id_line','asc')->orderBy('fleet_stops.direction_stop','asc')->select('fleet_stops.*','fleets_lines.id_line as idlinenew')->get();

        return response()->json(['models' => $models]);  
    }
}
