<?php

namespace Modules\Fleet\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Fleet\Entities\Vehicles;
use Validator;
use Response;
use Auth;
use DB;

class FleetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $models = Vehicles::orderBy('id_vehicle','asc')->get();
            return datatables()->of($models)
                ->addColumn('action', function ($row) {
                    $html = '';
                    if(Auth::user()->can('vehicles-edit')==1){
                        $html = '<a href="#modalFull" class="mb-xs mt-xs mr-xs modal-sizes btn btn-sm btn-primary editVehicle" id="'.$row->id_vehicle.'">Edit</a> ';
                    }
                    if(Auth::user()->can('vehicles-delete')==1){
                        $html .= '<a href="#modalCenterIcon" class="mb-xs mt-xs modal-sizes mr-xs btn btn-sm btn-danger getPopupDelVehicle" id="'.$row->id_vehicle.'">Delete</a>';
                    }
                    return $html;
                })->addIndexColumn()->toJson();
        }

        return view('fleet::vehicles.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('fleet::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_vehicle'        => 'required',
            'ip_vehicle'        => 'required'
        ]);

        if($validator->passes()){
            $data = '';
            if($request->type != null){
                for($i = 0; $i < count($request->type); $i++){
                    $data .= 'type:'.$request->type[$i].',inch:'.$request->inch[$i].',aspect:'.$request->aspect[$i].',resolution:'.$request->resolution[$i];
                    if($i < count($request->type)-1){
                        $data .= '|';
                    }
                }
            }

            $max = Vehicles::max('no_vehicle');

            $nomor = (int) $max + 1;

            $models                     = New Vehicles;
            $models->no_vehicle         = $nomor;
            $models->id_vehicle         = $request->id_vehicle;
            $models->ip_vehicle         = $request->ip_vehicle;
            $models->cat_vehicle        = $request->cat_vehicle;
            $models->id_operator        = $request->id_operator;
            $models->id_region          = $request->id_region;
            $models->is_wifi            = $request->is_wifi;
            $models->is_beacon          = $request->is_beacon;
            $models->comm_start         = $request->comm_start;
            $models->comm_end           = $request->comm_end;
            $models->is_cctv            = $request->is_cctv;
            $models->operation_hour     = $request->operation_hour;
            $models->mon_group_id       = $request->mon_group_id;
            $models->screens_installed  = $data;
            $models->date               = date("Y-m-d H:i:s");
            $models->lastchange         = date("Y-m-d H:i:s");
            $models->save();

            return response()->json(['success' => 'success']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('fleet::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $models = Vehicles::where('id_vehicle', $id)->first();
        $detail = [];
        if(strpos($models->screens_installed, ":") !== false){
            $exp = explode('|', $models->screens_installed);
            for($i = 0; $i < count($exp); $i++){
                $exp1 = explode(',', $exp[$i]);
                for($a = 0; $a < count($exp1); $a++){
                    $split = explode(':', $exp1[$a]);
                    $detail[$i][$split[0]] = $split[1];
                }
            }
        }
        $data = array();
        $data['models'] = $models;
        $data['detail'] = $detail;
        return Response::json($data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'id_vehicle'        => 'required',
            'ip_vehicle'        => 'required'
        ]);

        if($validator->passes()){
            $data = '';
            if($request->type != null){
                for($i = 0; $i < count($request->type); $i++){
                    $data .= 'type:'.$request->type[$i].',inch:'.$request->inch[$i].',aspect:'.$request->aspect[$i].',resolution:'.$request->resolution[$i];
                    if($i < count($request->type)-1){
                        $data .= '|';
                    }
                }
            }
            
            $models                     = Vehicles::where('no_vehicle', $id)->first();
            $models->id_vehicle         = $request->id_vehicle;
            $models->ip_vehicle         = $request->ip_vehicle;
            $models->cat_vehicle        = $request->cat_vehicle;
            $models->id_operator        = $request->id_operator;
            $models->id_region          = $request->id_region;
            $models->is_wifi            = $request->is_wifi;
            $models->is_beacon          = $request->is_beacon;
            $models->comm_start         = $request->comm_start;
            $models->comm_end           = $request->comm_end;
            $models->is_cctv            = $request->is_cctv;
            $models->operation_hour     = $request->operation_hour;
            $models->mon_group_id       = $request->mon_group_id;
            $models->screens_installed  = $data;
            $models->date               = date("Y-m-d H:i:s");
            $models->lastchange         = date("Y-m-d H:i:s");
            $models->save();

            return response()->json(['success' => 'success']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $models = Vehicles::where('id_vehicle', $id)->first();
        $busid = $models->id_vehicle;
        $delete = Vehicles::where('id_vehicle', $id)->delete();
        return response()->json(['success' => $busid]);
    }

    public function getDataFleet()
    {
        $operator = DB::table('fleets_operator')->get();
        $screen_group = DB::table('fleets_screen_group')->get();
        $vehicle_cat = DB::table('fleets_vehicles_cat')->get();
        $models = [];
        $models['operator'] = $operator;
        $models['screen_group'] = $screen_group;
        $models['vehicle_cat'] = $vehicle_cat;

        return response()->json(['models' => $models]);
    }

    public function getDataRegion(Request $request)
    {
        $id = $request->id;
        $models = DB::table('fleets_region')->where('id_operator', $id)->get();

        return response()->json(['models' => $models]);
    }

    public function getDataScreen()
    {
        $models = DB::table('fleets_screen')->select('type_screen')->distinct()->orderBy('type_screen', 'asc')->get();

        return response()->json(['models' => $models]);
    }

    public function getDataInch(Request $request)
    {
        $id = $request->id;

        $models = DB::table('fleets_screen')->where('type_screen', $id)->get();

        return response()->json(['models' => $models]);
    }

    public function getDataAspect(Request $request)
    {
        $id = (int) $request->id;
        $type = $request->type;

        $data = DB::table('fleets_screen')->where('type_screen', $type)->where('inch_screen', $id)->value('list_resolution');
        $exp = explode(',', $data);
        $models = DB::table('fleets_resolution')->whereIn('id_res', $exp)->get();

        return response()->json(['models' => $models]);   
    }

    public function getDataResolution(Request $request)
    {
        $id = $request->id;

        $models = DB::table('fleets_resolution')->where('aspectratio', $id)->get();

        return response()->json(['models' => $models]);
    }
}
