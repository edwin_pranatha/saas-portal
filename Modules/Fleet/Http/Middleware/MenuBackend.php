<?php

namespace Modules\Fleet\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Lavary\Menu\Facade as Menu;
use Illuminate\Support\Str;
use Auth;
class MenuBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Menu::make('MenuBackend', function ($menu) {

            // $menu->add('Fleet', '#')->data([
            //     'icon' => 'fa fa-bus-alt',
            //     'permissions' => 'vehicles-list|routes-list|stops-list',
            //     'active_match' => ['admin/fleet*'],
            //     //'group' => 'Fleet',
            //     'order' => 1
            // ]);
            // $menu->infoVision->add('Vehicles', 'admin/infovision/vehicles')->data([
            //     'permissions' => 'vehicles-list',
            //     'active_match' => 'admin/infovision/vehicles*',
            //     'order' => 1
            //     //'group' => 'Fleet',
            // ]);
            // $menu->infoVision->add('Routes', 'admin/fleet/routes')->data([
            //     'permissions' => 'routes-list',
            //     'active_match' => 'admin/fleet/routes*',
            //     //'group' => 'Fleet',
            // ]);
            // $menu->infoVision->add('Stops', 'admin/fleet/stops')->data([
            //     'permissions' => 'stops-list',
            //     'active_match' => 'admin/fleet/stops*',
            //     //'group' => 'Fleet',
            // ]);
        })->filter(function ($item) use ($request) {

            if($item->data('permissions')!=null){
                $permissions = explode('|', $item->data('permissions'));
                if(!Auth::user()->hasAnyPermission($permissions)){
                    return false;
                }
            }

            if ($request->url() === rtrim($item->url(), '#/')) {
                $item->active();
            }
            
            if ($item->active_match) {
                $matches = is_array($item->active_match) ? $item->active_match : [$item->active_match];                
                foreach ($matches as $pattern) {
                    if (Str::is($pattern, $request->path())) {
                        $item->activate();
                    }
                }
            }
            return true;
        })->sortBy('order');
        return $next($request);
    }
}