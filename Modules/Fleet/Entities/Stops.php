<?php

namespace Modules\Fleet\Entities;

use Illuminate\Database\Eloquent\Model;

class Stops extends Model
{
    protected $table = 'fleets_stops';
    public $incrementing = false;
    protected $primaryKey = 'id_stop';
    public $timestamps = false;
}
