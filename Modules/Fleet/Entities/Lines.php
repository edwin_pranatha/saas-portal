<?php

namespace Modules\Fleet\Entities;

use Illuminate\Database\Eloquent\Model;

class Lines extends Model
{
    protected $table = 'fleets_lines';
    public $incrementing = false;
    protected $primaryKey = 'num_line';
    public $timestamps = false;
}
