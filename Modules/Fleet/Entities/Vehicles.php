<?php

namespace Modules\Fleet\Entities;

use Illuminate\Database\Eloquent\Model;

class Vehicles extends Model
{
    protected $table = 'fleets_vehicles';
    public $incrementing = false;
    protected $primaryKey = 'no_vehicle';
    public $timestamps = false;
}
