<?php

namespace Modules\Siri\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Lavary\Menu\Facade as Menu;
use Illuminate\Support\Str;

class MenuBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Menu::make('MenuBackend', function ($menu) {
            $menu->add('SIRI Preview', 'admin/siri/preview')->data([
                'icon' => 'fa fa-car',
                'permissions' => 'siri.view|roles.view|permissions.view',
                'active_match' => ['admin/siri*'],
                'group' => 'SIRI',
                'order' => 3
            ]);
        })->filter(function ($item) use ($request) {
            if ($request->url() === rtrim($item->url(), '#/')) {
                $item->active();
            }
            
            if ($item->active_match) {
                $matches = is_array($item->active_match) ? $item->active_match : [$item->active_match];                
                foreach ($matches as $pattern) {
                    if (Str::is($pattern, $request->path())) {
                        $item->activate();
                    }
                }
            }
            return true;
        })->sortBy('order');
        return $next($request);
    }
}