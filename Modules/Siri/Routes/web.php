<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/admin/siri')->middleware(['auth'])->name('admin.')->group(function() {
    Route::get('/data', 'SiriController@index')->name('siri.data');
    Route::get('/preview', 'SiriController@previewIndex')->name('siri.preview');
});