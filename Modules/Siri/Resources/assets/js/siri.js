            $(document).ready(function(){  
                $('#dataTableRowSiriData').dataTable();
                    
                function send(){
                    $.ajax({
                        url: ipaddressSiri + "/api/siri/dummy/vehiclemonitor?agency=CXX&vehicle=5247&format=json",
                        crossDomain: true,
                        success:function(data)
                        {
                            const siri_data = data.Siri.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall;
                            var siri_content = "";
                            var siri_content_size = 5;
                            var journey_head_date = new Date(data.Siri.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.MonitoredCall.ExpectedArrivalTime);
                            $("#journey_destination_head").html(data.Siri.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.MonitoredCall.StopPointName[0].value)
                            $("#journey_destination_head_time").html(time_format(journey_head_date));
                            if (siri_data.length > 0)
                            {
                                siri_data.forEach(el => {
                                    //console.log(el)
                                    //console.log(el.StopPointName[0].value);
                                    var onward_date = new Date(el.AimedArrivalTime);
                                    siri_content += '<div class="row"> <div class="col-sm" style="float: left;width: 50%;padding-left: 10px;"> <h1 class="text-left" style="font-size: 20px;padding: 0px 0px;margin: 5px;">'+ el.StopPointName[0].value +'</h1> </div><div class="col-sm" style="float: left;width: 50%;"><h1 class="text-right" style="font-size: 20px;padding-right: 10px;margin: 5px;">'+ time_format(onward_date) +'</h1></div> </div>'
                                    //'<div class="row"><div class="col-sm"> <span class="text-left">'+ el.StopPointName[0].value +'</span></div><div class="col-sm"><span class="text-right">'+ time_format(onward_date) +'</span></div></div>'
                                    //$("#journey-content").append('<div class="row"><div class="col-sm"> <h1 class="text-left">'+ el.StopPointName[0].value +'</h1></div><div class="col-sm"><h1 class="text-right">'+ time_format(onward_date) +'</h1></div></div>');
                                });
                                let journey_foot = siri_data.slice(-1);
                                var journey_foot_date = new Date(journey_foot[0].ExpectedArrivalTime);
                                $("#journey_destination_foot").html(journey_foot[0].StopPointName[0].value);
                                $("#journey_destination_foot_time").html(time_format(journey_foot_date));
                            }else{
                                siri_content = "No Route Information Available";
                            }
                            $("#journey-content").html(siri_content);
                            console.log(data.Siri.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall);
                            setTimeout(function(){
                                send();
                            }, 1000);
                        }
                    });
                }
                send();
            });