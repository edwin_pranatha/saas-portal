@extends('theme::views.backend.app')

@section('page-title')
<h2>Siri Preview</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Siri Preview</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Siri/css/siri.min.css" media="screen" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:datatable/buttons.dataTables.min.css') }}" >
<link rel="stylesheet" href="{{ Module::asset('siri:sass/siri.min.css') }}">
@endpush --}}

<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>
                        <h2 class="panel-title">Siri Preview</h2>
                    </header>
                    <div class="panel-body" style="padding-bottom: 410px;">
                        <img src="/assets/Siri/images/prev_maestronic19new.png" style="position: absolute;left: 30%;">
                        <div class="container-fluid" style="">
                            <div class="container" style="width: 628px;position: absolute;left: 31%;padding: 0px 20px;">
                                <div class="row justify-content-end" style="padding: 0px 1px;margin-top: 20px;">
                                    <div class="text-rightcol-2 bg-info" style="width: 60px;float: right;">
                                        <h1 class="font-weight-bolder" style="font-size: 17px;padding: 2px 10px;margin-bottom: 0px;margin-top: 0px;color: #000;font-weight: bold;">19:36</h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <h1 class="text-left font-weight-bolder" id="journey_destination_head" style="font-size: 20px;padding: 0px 1px;margin: 0px;font-weight: bold;">Eerstvolgende haltes</h1>                
                                    </div>
                                </div>
                                <div class="row bg-success">
                                    <div class="col-sm" style="float: left;width: 50%;">
                                        <h1 class="text-left font-weight-bolder" style="font-size: 20px;padding: 1px 0px;margin: 6px 2px;font-weight: bold;color: #000;">Halte</h1>                
                                    </div>
                                    <div class="col-sm" style="float: left;width: 50%;">
                                        <h1 class="text-right font-weight-bolder" style="font-size: 20px;padding: 1px 0px;margin: 6px 2px;font-weight: bold;color: #000;">Aankomst</h1>
                                    </div>
                                </div>
                                <div id="journey-content" class="row" style="height: 208px;overflow:hidden;">
                                    <!-- <div class="row">
                                        <div class="col-sm">
                                            <h1 class="text-left">Amstelstation</h1>                
                                        </div>
                                        <div class="col-sm">
                                            <h1 class="text-right">19:36</h1>
                                        </div>
                                    </div> -->
                                </div>

                                <div id="journey-footer">
                                    <div class="text-white bg-secondary row" style="background-color: #999;">
                                        <div class="col-sm">
                                            <h1 class="text-left" style="font-size: 20px;padding: 0px 0px;margin: 5px;">Aankomst eindbestemming:</h1>                
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm" style="float: left;width: 50%;">
                                            <h1 class="text-left" style="font-size: 20px;padding: 0px 0px;margin: 5px;">Hilversum via Blaricum</h1>                
                                        </div>
                                        <div class="col-sm" style="float: left;width: 50%;">
                                            <h1 class="text-right" style="font-size: 20px;padding: 0px 0px;margin: 5px;">21:01</h1>
                                        </div>
                                    </div>
                                </div>               
                            </div>          
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('core:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('core:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('core:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('core:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('siri:compiled/siri.min.js') }}"></script>
@endpush --}}

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Siri/js/siri.min.js"></script>
@endsection