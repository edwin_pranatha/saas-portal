<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcGeofence extends Model
{
    protected $table = 'nc_geofences';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'id_geofence';
}
