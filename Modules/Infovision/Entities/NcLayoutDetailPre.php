<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcLayoutDetailPre extends Model
{
    protected $table = 'nc_layout_dtl_pre';
    public $incrementing = false;
    public $timestamps = false;
}
