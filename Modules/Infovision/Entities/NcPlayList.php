<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcPlayList extends Model
{
    protected $table = 'nc_playlist';
    public $incrementing = false;
}
