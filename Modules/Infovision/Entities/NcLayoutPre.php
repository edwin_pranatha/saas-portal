<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcLayoutPre extends Model
{
    protected $table = 'nc_layout_pre';
    public $incrementing = false;
    protected $primaryKey = 'id_layout';
}
