<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcContentUpload extends Model
{
    protected $table = 'nc_content_upload';
}
