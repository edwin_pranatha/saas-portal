<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcNoticeDetail extends Model
{
    protected $table = 'nc_notice_dtl';
    public $incrementing = false;
    public $timestamps = false;
}
