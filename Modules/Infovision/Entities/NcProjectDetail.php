<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcProjectDetail extends Model
{
    protected $table = 'nc_project_dtl';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'id_project';
}
