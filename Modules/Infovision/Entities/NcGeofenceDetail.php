<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcGeofenceDetail extends Model
{
    protected $table = 'nc_geofences_dtl';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'id_geofence';
}
