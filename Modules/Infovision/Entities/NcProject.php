<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcProject extends Model
{
    protected $table = 'nc_project';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'id_project';
}
