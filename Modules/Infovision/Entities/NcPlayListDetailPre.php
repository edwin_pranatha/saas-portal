<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcPlayListDetailPre extends Model
{
    protected $table = 'nc_playlist_dtl_pre';
    public $incrementing = false;
    public $timestamps = false;
}
