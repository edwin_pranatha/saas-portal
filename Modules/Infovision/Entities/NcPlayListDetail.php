<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcPlayListDetail extends Model
{
    protected $table = 'nc_playlist_dtl';
    public $incrementing = false;
    public $timestamps = false;
}
