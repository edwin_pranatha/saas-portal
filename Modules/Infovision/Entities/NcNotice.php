<?php

namespace Modules\Infovision\Entities;

use Illuminate\Database\Eloquent\Model;

class NcNotice extends Model
{
    protected $table = 'nc_notice';
    public $incrementing = false;
    public $timestamps = false;
}
