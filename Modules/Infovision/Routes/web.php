<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Storage;

Route::prefix('/admin/infovision')->middleware(['auth'])->name('admin.infovision.')->group(function() {
    //Campaign
    Route::resource('geofence', 'Backend\GeofenceController');
    Route::post('geofence/list', 'Backend\GeofenceController@index')->name('geofence.list');
    Route::get('geofence/{id}/show', 'Backend\GeofenceController@show')->name('geofence.view');
    Route::get('geofence/polygon/export', 'Backend\GeofenceController@exportPolygon')->name('geofence.polygon.export');
    Route::get('geofence/test/{id}', 'Backend\GeofenceController@showMapGeofence')->name('geofence.map.show');
    Route::get('geofence/check/test', 'Backend\GeofenceController@checkPolygon')->name('geofence.check');
    Route::get('geofence/polygon/info', 'Backend\GeofenceController@infoPolygon')->name('geofence.polygon.info');

    //Content Upload
    Route::get('content-upload', 'Backend\ContentuploadController@index')->name('content.upload.view');
    Route::post('content-upload', 'Backend\ContentuploadController@index')->name('content.upload.index');
    Route::post('content-upload/show', 'Backend\ContentuploadController@show')->name('content.upload.show');
    Route::delete('content-upload/{id}/delete', 'Backend\ContentuploadController@destroy')->name('content.upload.delete');

    //Layout Manager
    Route::get('layout', 'Backend\LayoutmanagerController@index')->name('layout.manager.index');
    Route::get('layout-create', 'Backend\LayoutmanagerController@create')->name('layout.manager.create');
    Route::get('layout-create/list/geofence', 'Backend\LayoutmanagerController@getShowGeofence')->name('layout.manager.list.geofence');
    Route::get('layout-create/info/geofence', 'Backend\LayoutmanagerController@getShowDetailGeofence')->name('layout.manager.info.geofence');
    
    //Campaign
    Route::get('campaign', 'Backend\CampaignsController@index')->name('campaign.manager.view');
    Route::post('campaign', 'Backend\CampaignsController@index')->name('campaign.manager.index');
    Route::get('campaign/edit/show', 'Backend\CampaignsController@editCampaign')->name('campaign.edit.show');
    Route::post('campaign/store', 'Backend\CampaignsController@store')->name('campaign.store');
    Route::post('campaign/updateCampaign', 'Backend\CampaignsController@updateCampaign')->name('campaign.updateCampaign');
    Route::post('campaign/show/playlist', 'Backend\CampaignsController@showPlayList')->name('campaign.show.playlist');
    Route::post('campaign/show/playlist/detail', 'Backend\CampaignsController@showPlayListDetail')->name('campaign.show.playlist.detail');
    Route::post('campaign/playlist', 'Backend\CampaignsController@savePlayList')->name('campaign.save.playlist');
    Route::post('campaign/layout', 'Backend\CampaignsController@saveLayout')->name('campaign.save.layout');

    Route::post('campaign/clone/layout', 'Backend\CampaignsController@cloneLayoutMain')->name('campaign.clone.layout');
    Route::post('campaign/clone/layout/content', 'Backend\CampaignsController@cloneLayouContenttMain')->name('campaign.clone.content.layout');
    Route::post('campaign/rename/layout', 'Backend\CampaignsController@renameLayoutMain')->name('campaign.rename.layout');
    Route::post('campaign/delete/layout', 'Backend\CampaignsController@deleteLayoutMain')->name('campaign.delete.layout');

    Route::get('campaign/show/image/first', 'Backend\CampaignsController@getImageFirst')->name('campaign.images.first');
    Route::get('campaign/show/layout/manager', 'Backend\CampaignsController@showLayoutManager')->name('campaign.show.layout.manager');
    Route::get('campaign/prev/layout/manager', 'Backend\CampaignsController@prevLayoutManager')->name('campaign.prev.layout.manager');
    Route::get('campaign/vehicles/list', 'Backend\CampaignsController@showVehicle')->name('campaign.vehicles.list');
    Route::post('campaign/generate/config', 'Backend\CampaignsController@generateConfig')->name('campaign.generate.config');
    Route::get('campaign/info/show', 'Backend\CampaignsController@infoCampaign')->name('campaign.infoCamapaign');
    Route::post('campaign/stopCamapaign', 'Backend\CampaignsController@stopCamapaign')->name('campaign.stopCamapaign');
    Route::post('campaign/notice/show', 'Backend\CampaignsController@infoNoticeCampaign')->name('campaign.infoCamapaign.notice');
    Route::post('campaign/notice/save', 'Backend\CampaignsController@saveNoticeCampaign')->name('campaign.saveCamapaign.notice');
    Route::post('campaign/deleteCampaign', 'Backend\CampaignsController@deleteCampaign')->name('campaign.deleteCampaign');
    Route::get('campaign/status', 'Backend\CampaignsController@checkStatusCampaignAll')->name('campaign.checkStatusCampaignAll');


    Route::get('/image/{filename}', function ($filename)
    {
        $path = Storage::disk('tenant')->path("nc_images/{$filename}");
        $mime = \Illuminate\Http\Testing\MimeType::from($path);
        if($filename == "none")
        {
            $path = base_path("Modules/Infovision/Resources/assets/images/noimage.png");
            $mime = \Illuminate\Http\Testing\MimeType::from($path);
        }
        return response(\File::get($path))
            ->header('Content-Type', $mime);
    });

    Route::get('/image/thumbnail/{filename}', function ($filename)
    {
        $path = Storage::disk('tenant')->path("nc_images/thumbnail/{$filename}");
        $mime = \Illuminate\Http\Testing\MimeType::from($path);
        if($filename == "none")
        {
            $path = base_path("Modules/Infovision/Resources/assets/images/noimage.png");
            $mime = \Illuminate\Http\Testing\MimeType::from($path);
        }
        return response(\File::get($path))
            ->header('Content-Type', $mime);
    });

    Route::get('/video/{filename}', function ($filename)
    {
        $path = Storage::disk('tenant')->path("nc_videos/{$filename}");
        $mime = \Illuminate\Http\Testing\MimeType::from($path);
        if($filename == "none")
        {
            $path = base_path("Modules/Infovision/Resources/assets/images/noimage.png");
            $mime = \Illuminate\Http\Testing\MimeType::from($path);
        }
        return response(\File::get($path))
            ->header('Content-Type', $mime);
    });

    Route::get('/video/thumbnail/{filename}', function ($filename)
    {
        $path = Storage::disk('tenant')->path("nc_videos/thumbnail/{$filename}");
        $mime = \Illuminate\Http\Testing\MimeType::from($path);
        if($filename == "none")
        {
            $path = base_path("Modules/Infovision/Resources/assets/images/noimage.png");
            $mime = \Illuminate\Http\Testing\MimeType::from($path);
        }
        return response(\File::get($path))
            ->header('Content-Type', $mime);
    });
    
    Route::get('/layout/{filename}', function ($filename)
    {
        $path = Storage::disk('tenant')->path("nc_layouts/{$filename}");
        $mime = \Illuminate\Http\Testing\MimeType::from($path);
        return response(\File::get($path))
            ->header('Content-Type', $mime);
    });


});
