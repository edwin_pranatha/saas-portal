@extends('theme::views.backend.app')

@section('page-title')
<h2>Content Uploading</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Content Upload</span></li>
</ol>
@endsection


@section('module_css')
    <link rel="stylesheet" href="/assets/Infovision/css/contentupload.min.css" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:sass/contentupload.min.css') }}">
<link rel="stylesheet" href="{{ Module::asset('campaigns:blueimp/css/blueimp-gallery.min.css') }}">
<link rel="stylesheet" href="{{ Module::asset('campaigns:blueimp/css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ Module::asset('campaigns:blueimp/css/jquery.fileupload-ui.css') }}">
<noscript><link rel="stylesheet" href="{{ Module::asset('campaigns:blueimp/css/jquery.fileupload-noscript.css') }}"></noscript>
<noscript><link rel="stylesheet" href="{{ Module::asset('campaigns:blueimp/css/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endpush --}}

<div class="row">
    @can('content-upload-create')
    <div class="col-md-6 col-lg-12 col-xl-6">
        <a class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary" id="upload_media" href="#modalFull">Upload Media</a>
    </div>
    @endcan

    <div id="modalFull" class="modal-block modal-block-lg mfp-hide" style="max-width: 1400px;">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                </div>
                <h2 class="panel-title">Content Upload</h2>
            </header>
            <div class="panel-body">
                <form id="fileupload" action="https://jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                    <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"
                        /></noscript>

                        <div class="row fileupload-buttonbar">
                            <div class="col-lg-7">
                                @can('content-upload-create')
                                <span class="btn btn-success fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Add files...</span>
                                    <input type="file" name="files[]" multiple />
                                </span>
                                @endcan
                                <button type="submit" class="btn btn-primary start">
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Start upload</span>
                                </button>
                                <button type="reset" class="btn btn-warning cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Cancel upload</span>
                                </button>
                                @can('content-upload-delete')
                                <button type="button" class="btn btn-danger delete">
                                    <i class="glyphicon glyphicon-trash"></i>
                                    <span>Delete selected</span>
                                </button>
                                @endcan
                                <input type="checkbox" class="toggle" />
                                <span class="fileupload-process"></span>
                            </div>
                            <div class="col-lg-5 fileupload-progress fade">
                                <div
                                class="progress progress-striped active"
                                role="progressbar"
                                aria-valuemin="0"
                                aria-valuemax="100"
                                >
                                <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                            </div>
                            <div class="progress-extended">&nbsp;</div>
                        </div>
                    </div>

                    <table role="presentation" class="table table-striped">
                        <tbody class="files"></tbody>
                    </table>
                </form>
                <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" aria-label="image gallery" aria-modal="true" role="dialog" data-filter=":even">
                    <div class="slides" aria-live="polite"></div>
                    <h3 class="title"></h3>
                    <a class="prev" aria-controls="blueimp-gallery" aria-label="previous slide" aria-keyshortcuts="ArrowLeft"></a>
                    <a class="next" aria-controls="blueimp-gallery" aria-label="next slide" aria-keyshortcuts="ArrowRight"></a>
                    <a class="close" aria-controls="blueimp-gallery" aria-label="close" aria-keyshortcuts="Escape"></a>
                    <a class="play-pause" aria-controls="blueimp-gallery" aria-label="play slideshow" aria-keyshortcuts="Space" aria-pressed="false" role="button"></a>
                    <ol class="indicator"></ol>
                </div>

                <script id="template-upload" type="text/x-tmpl">
                    {% for (var i=0, file; file=o.files[i]; i++) { %}
                        <tr class="template-upload fade{%=o.options.loadImageFileTypes.test(file.type)?' image':''%}">
                            <td>
                                <span class="preview"></span>
                            </td>
                            <td>
                                <p class="name">{%=file.name%}</p>
                                <strong class="error text-danger"></strong>
                            </td>
                            <td>
                                <p class="size">Processing...</p>
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                            </td>
                            <td>
                                {% if (!o.options.autoUpload && o.options.edit && o.options.loadImageFileTypes.test(file.type)) { %}
                                <button class="btn btn-success edit" data-index="{%=i%}" disabled>
                                    <i class="glyphicon glyphicon-edit"></i>
                                    <span>Edit</span>
                                </button>
                                {% } %}
                                {% if (!i && !o.options.autoUpload) { %}
                                <button class="btn btn-primary start" disabled>
                                    <i class="glyphicon glyphicon-upload"></i>
                                    <span>Start</span>
                                </button>
                                {% } %}
                                {% if (!i) { %}
                                <button class="btn btn-warning cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Cancel</span>
                                </button>
                                {% } %}
                            </td>
                        </tr>
                    {% } %}
                </script>

                <script id="template-download" type="text/x-tmpl">
                {% for (var i=0, file; file=o.files[i]; i++) { %}
                    <tr class="template-download fade{%=file.thumbnailUrl?' image':''%}">
                        <td>
                            <span class="preview">
                            {% if (file.thumbnailUrl) { %}
                                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" {% if (file.type=='mp4') { %} style="border: none;padding: 0px;margin: 0px;max-width: 100%;max-height: none;width: 80px;height: 57px;" {% } %}></a>
                            {% } %}
                            </span>
                        </td>
                        <td>
                            <p class="name">
                            {% if (file.url) { %}
                                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                            {% } else { %}
                                <span>{%=file.name%}</span>
                            {% } %}
                            </p>
                            {% if (file.error) { %}
                                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                            {% } %}
                        </td>
                        <td>
                            <span class="size">{%=o.formatFileSize(file.size)%}</span>
                        </td>
                        <td>
                            @can('content-upload-delete')
                                {% if (file.deleteUrl) { %}
                                    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Delete</span>
                                    </button>
                                    <input type="checkbox" name="delete" value="1" class="toggle">
                                {% } else { %}
                                    <button class="btn btn-warning cancel">
                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                        <span>Cancel</span>
                                    </button>
                                {% } %}
                            @endcan
                        </td>
                    </tr>
                {% } %}
                </script>                                 
            </div>
        </section>
    </div>

    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>
                        <h2 class="panel-title">Server Media Library</h2>
                    </header>
                    <input type="hidden" name="urlinfo" data-url="{{ route('admin.infovision.content.upload.show') }}">
                    <div class="panel-body">
                        <div class="tabs tabs-vertical tabs-left">
                            <ul class="nav nav-tabs col-sm-2 col-xs-3">
                                <li class="active">
                                    <a href="#popular11" data-toggle="tab">JPG</a>
                                </li>
                                <li>
                                    <a href="#recent11" data-toggle="tab">Movie</a>
                                </li>
                            </ul>
                            <div class="tab-content" style="height: 570px;overflow: auto;display: block;">
                                <div id="loadContentUpload" style="background-color: #000000a1;width: 79%;height: 550px;position: absolute;z-index: 999;display: none;font-size: 20px;color: #FFF;text-align: center;padding-top: 280px;">Please Wait...</div>
                                <div id="popular11" class="tab-pane active zoom-gallery"></div>
                                <div id="recent11" class="tab-pane zoom-video"></div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('campaigns:compiled/contentupload.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/tmpl.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/load-image.all.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/canvas-to-blob.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/jquery.blueimp-gallery.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/jquery.fileupload.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/jquery.fileupload-process.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/jquery.fileupload-image.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/jquery.fileupload-audio.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/jquery.fileupload-video.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/jquery.fileupload-validate.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/jquery.fileupload-ui.js') }}"></script>
<script src="{{ Module::asset('campaigns:blueimp/js/demo.js') }}"></script>
@endpush --}}

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Infovision/js/contentupload.min.js"></script>
@endsection
