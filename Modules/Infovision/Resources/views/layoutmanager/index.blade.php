@extends('theme::views.backend.app')

@section('page-title')
<h2>Layout Manager</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Layout Manager</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Infovision/css/layoutmanager.min.css">
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/owl-carousel/owl.carousel.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/owl-carousel/owl.theme.css') }}" />
<link rel='stylesheet' href="{{ Module::asset('campaigns:css/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ Module::asset('campaigns:dist/jquery.contextMenu.min.css') }}">
<link rel="stylesheet" href="{{ Module::asset('campaigns:sass/layoutmanager.min.css') }}">
@endpush --}}

<div class="row">
    @can('layout-create')
    <div class="col-md-6 col-lg-12 col-xl-6">
        <a href="{{ route('admin.infovision.layout.manager.create') }}" class="mb-xs mt-xs mr-xs btn btn-primary">Create Layout</a>
    </div>
    @endcan

    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>
                        <h2 class="panel-title">Layout Manager</h2>
                    </header>
                    <div class="panel-body" id="showLayoutManager">
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<div id="modalDisplayScreen" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
    <section class="panel" id="panelScreen" style="display: block;margin: 0 auto;">
        <button type="button" class="btn-danger prevClose modal-dismiss"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <div id="screenbus" class="screenbus_29">
            <div id="canvaspreview" class="row" style="position: relative;"></div>
        </div>
    </section>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')

<script src="{{ Module::asset('campaigns:js/fabric.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:dist/jquery.contextMenu.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:dist/jquery.ui.position.js') }}"></script>
<script src="{{ Module::asset('campaigns:js/sweetalert2.all.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/owl-carousel/owl.carousel.js') }}"></script>
<script src="{{ Module::asset('campaigns:compiled/layoutmanager.min.js') }}"></script>
@endpush --}}

@if(Auth::user()->can('layout-edit')==1)
<script type="text/javascript">var accesseditlayout = 1;</script>
@else
<script type="text/javascript">var accesseditlayout = 0;</script>
@endif
<script type="text/javascript">var accessdeletelayout = 0;</script>
@can('layout-delete')
<script type="text/javascript">accessdeletelayout = 1;</script>
@endcan

@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Infovision/js/layoutmanager.min.js"></script>
@endsection
