@extends('theme::views.backend.app')

@section('page-title')
<h2>Layout Create</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Layout Create</span></li>
</ol>
@endsection

@section('page_class')
sidebar-left-collapsed
@endsection

@section('module_css')
<link rel="stylesheet" href="/assets/Infovision/css/layoutmanager.min.css">
@endsection

@section('content')
<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<a class="mb-xs mt-xs mr-xs modal-with-move-anim btn btn-default modalLayoutCreate" href="#modalAnim"
					style="display: none;">Open with fade-slide animation</a>
				<nav class="navbar navbar-inverse navbar-static-top" style="background-color: #34495e;z-index:100;">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
							data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<input type="hidden" id="idlayout" name="idlayout">
					<input type="hidden" id="nmlayout" name="nmlayout">
					<input type="hidden" id="idbg" name="idbg">
					<input type="hidden" id="urlparam" name="urlparam" value="">
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
									aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus-circle"></i> Add
									Widget</a>
								<div class="dropdown-menu dropdown-sizeMenu">
									<h3 class="position-title-dropdown">Add Widget</h3>
									<form class="accordion" id="accordionExample">
										<div class="rowPanelPosition">
											<div class="row">
												<div class="col-md-12">
													<div class="toggle" data-plugin-toggle
														data-plugin-options='{ "isAccordion": true }'>
														<section class="toggle active">
															<label>Single Widget</label>
															<div class="toggle-content">
																<div
																	class="isotope-item document col-xs-6 col-md-4 pad0">
																	<div class="panel-groupWidget">
																		<div class="thumb-preview text-center">
																			<a class="thumb-image"
																				href="javascript:void(0)"
																				id="addImageScreen">
																				<i
																					class="fa fa-image position-icon-widget"></i>
																			</a>
																		</div>
																		<div class="text-center">
																			<small
																				class="size-text-widget">Image</small>
																		</div>
																	</div>
																</div>
																<div
																	class="isotope-item document col-xs-6 col-md-4 pad0">
																	<div class="panel-groupWidget">
																		<div class="thumb-preview text-center">
																			<a class="thumb-image"
																				href="javascript:void(0)"
																				id="addVideoScreen">
																				<i
																					class="fa fa-film position-icon-widget"></i>
																			</a>
																		</div>
																		<div class="text-center">
																			<small
																				class="size-text-widget">Video</small>
																		</div>
																	</div>
																</div>
																<div
																	class="isotope-item document col-xs-6 col-md-4 pad0">
																	<div class="panel-groupWidget">
																		<div class="thumb-preview text-center">
																			<a class="thumb-image"
																				href="javascript:void(0)"
																				id="addTextScreen">
																				<i
																					class="fa fa-bars position-icon-widget"></i>
																			</a>
																		</div>
																		<div class="text-center">
																			<small class="size-text-widget">Text</small>
																		</div>
																	</div>
																</div>
																<div
																	class="isotope-item document col-xs-6 col-md-4 pad0">
																	<div class="panel-groupWidget">
																		<div class="thumb-preview text-center">
																			<a class="thumb-image"
																				href="javascript:void(0)"
																				id="addTickerScreen">
																				<i
																					class="fa fa-list-ul position-icon-widget"></i>
																			</a>
																		</div>
																		<div class="text-center">
																			<small class="size-text-widget">Ticker
																				Text</small>
																		</div>
																	</div>
																</div>
																<div
																	class="isotope-item document col-xs-6 col-md-4 pad0">
																	<div class="panel-groupWidget">
																		<div class="thumb-preview text-center">
																			<a class="thumb-image"
																				href="javascript:void(0)"
																				id="addUrlScreen">
																				<i
																					class="fa fa-newspaper position-icon-widget"></i>
																			</a>
																		</div>
																		<div class="text-center">
																			<small class="size-text-widget">Web
																				Page</small>
																		</div>
																	</div>
																</div>
															</div>
														</section>
														<section class="toggle pad-top5">
															<label>Multi Widget</label>
															<div class="toggle-content">
																<div
																	class="isotope-item document col-xs-6 col-md-4 pad0">
																	<div class="panel-groupWidget">
																		<div class="thumb-preview text-center">
																			<a class="thumb-image"
																				href="javascript:void(0)"
																				id="addSlideScreen">
																				<i class="fa fa-images position-icon-widget"></i>
																			</a>
																		</div>
																		<div class="text-center">
																			<small class="size-text-widget">Multi
																				Slides</small>
																		</div>
																	</div>
																</div>
															</div>
														</section>

														<section class="toggle pad-top5">
															<label>VIA</label>
															<div class="toggle-content">
																<div class="isotope-item document col-xs-6 col-md-4 pad0">
																	<div class="panel-groupWidget">
																		<div class="thumb-preview text-center">
																			<a class="thumb-image"
																				href="javascript:void(0)"
																				id="addVIAheader">
																				<i class="fa fa-images position-icon-widget"></i>
																			</a>
																		</div>
																		<div class="text-center">
																			<small class="size-text-widget">Header</small>
																		</div>
																	</div>
																</div>

																<div class="isotope-item document col-xs-6 col-md-4 pad0">
																	<div class="panel-groupWidget">
																		<div class="thumb-preview text-center">
																			<a class="thumb-image"
																				href="javascript:void(0)"
																				id="addVIArouteinfo">
																				<i class="fa fa-images position-icon-widget"></i>
																			</a>
																		</div>
																		<div class="text-center">
																			<small class="size-text-widget">Route Info</small>
																		</div>
																	</div>
																</div>

															</div>
														</section>

													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</li>
							<li><a href="javascript:void(0)" id="saveLayout"
									data-url="{{ route('admin.infovision.campaign.save.layout') }}"><i
										class="fa fa-save"></i> Save Layout</a></li>
							<li><a href="#modalDisplayScreen" id="prevDisplayScreen" class="modal-sizes"><i
										class="fa fa-eye"></i> Preview Layout</a></li>
							<li><a href="#modalConfigScreen" class="modal-sizes layoutConfig"><i
										class="fa fa-wrench"></i> Configure</a></li>
							<li><a href="#modalLayoutPreset" class="modal-sizes"><i class="fa fa-window-restore"></i>
									Preset Layout</a></li>
						</ul>
					</div>
				</nav>

				<div id="canvas-container" class="panelCanvas">
					<canvas id="paper" width="800" height="600"></canvas>
				</div>

				<div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span
										class="sr-only">Close</span></button>
							</div>
							<h2 class="panel-title" style="text-align: center;">Please Select Screen Type</h2>
						</header>
						<div class="panel-body">
							<div class="pad-left-right">
								<div id="1366x768" class="isotope-item document col-xs-6 col-md-6 h-imgRes pad-h250">
									<div class="thumb-preview">
										<a class="thumb-image" href="javascript:void(0)">
											<img src="/assets/Infovision/assets/images/19inch_new.png"
												class="img-responsive img-size19" alt="Project">
										</a>
									</div>
									<div class="text-center">
										<small class="size-text-widget">TFT 19" inch</small></br>
										<small class="size-text-widget">Resolution : 1366x768</small>
									</div>
								</div>
								<div id="1920x540" class="isotope-item document col-xs-6 col-md-6 h-imgRes pad-h250">
									<div class="thumb-preview mar-top40">
										<a class="thumb-image" href="javascript:void(0)">
											<img src="/assets/Infovision/assets/images/29inch.png"
												class="img-responsive" alt="Project">
										</a>
									</div>
									<div class="center-top20">
										<small class="size-text-widget">Ultra Wide 29" inch</small></br>
										<small class="size-text-widget">Resolution : 1920x540</small>
									</div>
								</div>
								<div id="1024x768" class="isotope-item document col-xs-6 col-md-6 h-imgRes pad-h250">
									<div class="thumb-preview">
										<a class="thumb-image" href="javascript:void(0)">
											<img src="/assets/Infovision/assets/images/19inch_new.png"
												class="img-responsive img-size19" alt="Project">
										</a>
									</div>
									<div class="text-center">
										<small class="size-text-widget">TFT 19" inch</small></br>
										<small class="size-text-widget">Resolution : 1024x768</small>
									</div>
								</div>
								<div id="1920x1080" class="isotope-item document col-xs-6 col-md-6 h-imgRes pad-h250">
									<div class="thumb-preview mar-top40">
										<a class="thumb-image" href="javascript:void(0)">
											<img src="/assets/Infovision/assets/images/29inch.png"
												class="img-responsive" alt="Project">
										</a>
									</div>
									<div class="center-top20">
										<small class="size-text-widget">Screen 29" inch</small></br>
										<small class="size-text-widget">Resolution : 1920x1080</small>
									</div>
								</div>
							</div>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="btn btn-primary" id="chooseScreen">Select
										Screen</button>
								</div>
							</div>
						</footer>
					</section>
				</div>

				<div id="modalAddImage" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span
										class="sr-only">Close</span></button>
							</div>
							<h2 class="panel-title" style="text-align: center;">Please Select Image File</h2>
						</header>
						<div class="panel-body">
							<h5>Media Library</h5>
							<div class="tabs tabs-vertical tabs-left">
								<ul class="nav nav-tabs col-sm-3 col-xs-5">
									<li class="active">
										<a href="#showimagelayout" data-toggle="tab">Picture</a>
									</li>
									<li class="listMovie" style="display: none;">
										<a href="#showmovielayout" data-toggle="tab">Movie</a>
									</li>
								</ul>
								<div class="tab-content">
									<div id="showimagelayout" class="tab-pane active"></div>
									<div id="showmovielayout" class="tab-pane listMovie" style="display: none;"></div>
								</div>
							</div>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="btn btn-primary" id="selectImageLayout">Select
										Image</button>
									<button type="button" class="btn btn-primary" id="selectTextBgImage"
										style="display: none;">Select Image</button>
									<button type="button" class="btn btn-primary" id="selectMedia"
										style="display: none;">Select Media</button>
								</div>
							</div>
						</footer>
					</section>
				</div>

				<div id="modalAddVideo" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span
										class="sr-only">Close</span></button>
							</div>
							<h2 class="panel-title text-center">Please Select Video File</h2>
						</header>
						<div class="panel-body">
							<h5>Media Library</h5>
							<div class="tabs tabs-vertical tabs-left">
								<ul class="nav nav-tabs col-sm-3 col-xs-5">
									<li class="active">
										<a href="#showvideolayout" data-toggle="tab">Movie</a>
									</li>
								</ul>
								<div class="tab-content">
									<div id="showvideolayout" class="tab-pane active"></div>
								</div>
							</div>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="btn btn-primary" id="selectVideoLayout">Select
										Video</button>
								</div>
							</div>
						</footer>
					</section>
				</div>

				<div id="modalAddMedia" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span
										class="sr-only">Close</span></button>
							</div>
							<h2 class="panel-title" style="text-align: center;">Please Select Media File</h2>
						</header>
						<div class="panel-body">
							<h5>Media Library</h5>
							<div class="tabs tabs-vertical tabs-left">
								<ul class="nav nav-tabs tab-media-object col-sm-3 col-xs-5">
									<li class="active" id="picture">
										<a href="#showimagelayout" data-toggle="tab">Picture</a>
									</li>
									<li id="movie">
										<a href="#showmovielayout" data-toggle="tab">Movie</a>
									</li>
									<li id="text">
										<a href="#showtextlayout" data-toggle="tab">Text</a>
									</li>
									<li id="url">
										<a href="#showurllayout" data-toggle="tab">URL</a>
									</li>
									<li id="api">
										<a href="#showapilayout" data-toggle="tab">API</a>
									</li>
								</ul>
								<div class="tab-content">
									<div id="showimagelayout" class="tab-pane active"></div>
									<div id="showmovielayout" class="tab-pane"></div>
									<div id="showtextlayout" class="tab-pane">
										<div>
											<div class="input-group dis-width150">
												<span class="input-group-addon btn-success"><i
														class="fa fa-align-center"></i></span>
												<select class="form-control mb-md" id="arrowText"
													style="margin-bottom: 0px !important;">
													<option value="Top Left">Top Left</option>
													<option value="Top">Top</option>
													<option value="Top Right">Top Right</option>
													<option value="Left">Left</option>
													<option value="Center">Center</option>
													<option value="Right">Right</option>
													<option value="Bottom Left">Bottom Left</option>
													<option value="Bottom">Bottom</option>
													<option value="Bottom Right">Bottom Right</option>
												</select>
											</div>
											<div class="input-group color dis-width150" data-color="rgb(255, 146, 180)"
												data-color-format="rgb" data-plugin-colorpicker>
												<span class="input-group-addon"><i></i></span>
												<input type="text" class="form-control" id="colorBgText">
											</div>
											<a href="#modalAddImage" id="textBgImg"
												class="btn btn-sm btn-primary modal-sizes mar-top-28"><i
													class="fa fa-image"></i> <span id="mediaFileImg"></span></a>
										</div>
										<div id="textAreaSum"></div>
									</div>
									<div id="showurllayout" class="tab-pane">
										<input type="text" class="form-control" id="urlMediaObject"
											placeholder="Input your URL here">
									</div>
									<div id="showapilayout" class="tab-pane">
										<div class="row">
											<div class="isotope-item document col-sm-6 col-md-4 col-lg-3">
												<div class="thumbnail">
													<div class="thumb-preview">
														<a class="thumb-image modal-sizes" href="#modalAddPro">
															<img src="/assets/Infovision/assets/images/api_rss.png"
																class="img-responsive" alt="Project">
														</a>
													</div>
													<h5 class="mg-title text-semibold text-center">api_rss</h5>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="btn btn-default modal-dismiss">Cancel</button>
									<button type="button" class="btn btn-primary" id="selectMediaObject">Select
										Media</button>
								</div>
							</div>
						</footer>
					</section>
				</div>

				<div id="modalAddPro" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span
										class="sr-only">Close</span></button>
							</div>
							<h2 class="panel-title text-center">Additional Properties</h2>
						</header>
						<div class="panel-body">
							<form class="form-horizontal" id="formRoutesData" method="get">
								<div class="form-group">
									<label class="col-md-3 control-label" for="inputDefault">RSS Source</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="rss_source"
											value="https://www.nu.nl/rss">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="inputDefault">RSS Category</label>
									<div class="col-md-6">
										<select class="form-control" id="rss_category">
											<option value="">- General -</option>
											<option value="algemeen">Algemeen</option>
											<option value="algemeen">Algemeen</option>
											<option value="binnenland">Binnenland</option>
											<option value="politiek">Politiek</option>
											<option value="economie">Economie</option>
											<option value="geldzaken">Geldzaken</option>
											<option value="ondernemen">Ondernemen</option>
											<option value="beurs">Beurs</option>
											<option value="sport">Sport</option>
											<option value="tech">Tech</option>
											<option value="internet">Internet</option>
											<option value="gadgets">Gadgets</option>
											<option value="games">Games</option>
											<option value="entertainment">Entertainment</option>
											<option value="achterklap">Achterklap</option>
											<option value="film">Film</option>
											<option value="muziek">Muziek</option>
											<option value="boek">Boek</option>
											<option value="media">Media</option>
											<option value="cultuur-overig">Cultuur Overig</option>
											<option value="overig">Overig</option>
											<option value="opmerkelijk">Opmerkelijk</option>
											<option value="wetenschap">Wetenschap</option>
											<option value="gezondheid">Gezondheid</option>
											<option value="lifestyle">Lifestyle</option>
											<option value="auto">Auto</option>
											<option value="regio">Regio</option>
											<option value="amsterdam">Amsterdam</option>
											<option value="groningen">Groningen</option>
											<option value="rotterdam">Rotterdam</option>
											<option value="den-haag">Den Haag</option>
											<option value="salland">Salland</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="inputDefault">Stylesheet</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="rss_stylesheet"
											value="*{font-size:19px;color: #000000; background-color: #FFFFFF; border:2px; border-color: #000000;}">
									</div>
								</div>
							</form>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="btn btn-primary" id="selectAddPro">Select</button>
								</div>
							</div>
						</footer>
					</section>
				</div>

				<div id="modalDisplayScreen" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel" id="panelScreen" style="display: block;margin: 0 auto;">
						<button type="button" class="btn-danger prevClose modal-dismiss"><span
								aria-hidden="true">×</span><span class="sr-only">Close</span></button>
						<div id="screenbus" class="screenbus_29">
							<div id="canvaspreview" class="row" style="position: relative;"></div>
						</div>
					</section>
				</div>

				<div id="modalConfigScreen" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span
										class="sr-only">Close</span></button>
							</div>
							<h2 class="panel-title text-center">Main Layout Configuration</h2>
						</header>
						<div class="panel-body">
							<form class="form-horizontal" id="formRoutesData" method="get">
								<div class="form-group">
									<label class="col-md-3 control-label">Main BgColor</label>
									<div class="col-md-6">
										<div class="input-group color" data-color="rgb(255, 146, 180)"
											data-color-format="rgb" data-plugin-colorpicker>
											<span class="input-group-addon"><i></i></span>
											<input type="text" class="form-control" id="mainbc">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="inputDefault">Fallback Method</label>
									<div class="col-md-6">
										<div class="radio">
											<a href="#modalAddImage" id="actionFallbackMethod" class="modal-sizes">
												<label>
													<input type="radio" name="fallbackmt" id="optionsRadiosFM1"
														value="Use Media" checked="">
													Use Media : <span id="textSelectFM">[Video] Zaanseschans.mp4</span>
												</label>
											</a>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="fallbackmt" id="optionsRadiosFM2"
													value="Clone Screen">
												Clone Screen
											</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="inputDefault">Fallback TTL
										Refresh</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="fallbackttl" value="10"
											style="width: 50px;">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="inputDefault">Screen Alignment</label>
									<div class="col-md-6">
										<select class="form-control mb-md" id="screenalign">
											<option value="Right">Right</option>
											<option value="Left">Left</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="inputDefault">Screen OffsetX</label>
									<div class="col-md-6">
										<select class="form-control mb-md" id="screenoffX">
											<option value="0">0</option>
											<option value="800">800</option>
											<option value="1024">1024</option>
											<option value="1366">1366</option>
											<option value="1920">1920</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="inputDefault">Current Screen Size
										</br><span id="sizeScreen"></span></label>
									<div class="col-md-6">
										<a href="#modalAnim" id="resizeScreen"
											class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary">Resize Screen</a>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="inputDefault">Use PFT</label>
									<div class="col-md-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="usepft" value="Use PFT" checked="">

											</label>
										</div>
									</div>
								</div>
							</form>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="btn btn-primary" id="saveLayoutConfig">Save</button>
								</div>
							</div>
						</footer>
					</section>
				</div>

				<div id="modalLayoutPreset" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span
										class="sr-only">Close</span></button>
							</div>
							<h2 class="panel-title text-center">Please Select Layout Preset</h2>
						</header>
						<div class="panel-body">
							<div class="row">
								<div class="isotope-item document col-sm-6 col-md-4">
									<div class="thumbnail h-layout-preset" id="add-pre-1-1">
										<div class="thumb-preview">
											<a class="thumb-image" href="javascript:void(0)">
												<img src="/assets/Infovision/assets/images/preset/preset_1-1.jpg"
													class="img-responsive" alt="Project">
											</a>
										</div>
									</div>
								</div>
								<div class="isotope-item document col-sm-6 col-md-4">
									<div class="thumbnail h-layout-preset" id="add-pre-1-1_v">
										<div class="thumb-preview">
											<a class="thumb-image" href="javascript:void(0)">
												<img src="/assets/Infovision/assets/images/preset/preset_1-1_v.jpg"
													class="img-responsive" alt="Project">
											</a>
										</div>
									</div>
								</div>
								<div class="isotope-item document col-sm-6 col-md-4">
									<div class="thumbnail h-layout-preset" id="preset_1-2-1_v">
										<div class="thumb-preview">
											<a class="thumb-image" href="javascript:void(0)">
												<img src="/assets/Infovision/assets/images/preset/preset_1-2-1_v.jpg"
													class="img-responsive" alt="Project">
											</a>
										</div>
									</div>
								</div>
								<div class="isotope-item document col-sm-6 col-md-4">
									<div class="thumbnail h-layout-preset" id="preset_1-3">
										<div class="thumb-preview">
											<a class="thumb-image" href="javascript:void(0)">
												<img src="/assets/Infovision/assets/images/preset/preset_1-3.jpg"
													class="img-responsive" alt="Project">
											</a>
										</div>
									</div>
								</div>
								<div class="isotope-item document col-sm-6 col-md-4">
									<div class="thumbnail h-layout-preset" id="preset_2-2">
										<div class="thumb-preview">
											<a class="thumb-image" href="javascript:void(0)">
												<img src="/assets/Infovision/assets/images/preset/preset_2-2.jpg"
													class="img-responsive" alt="Project">
											</a>
										</div>
									</div>
								</div>
								<div class="isotope-item document col-sm-6 col-md-4">
									<div class="thumbnail h-layout-preset" id="add-pre-1-1_v">
										<div class="thumb-preview">
											<a class="thumb-image" href="javascript:void(0)">
												<img src="/assets/Infovision/assets/images/preset/preset_1-1_cxx.jpg"
													class="img-responsive" alt="Project">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<footer class="panel-footer"></footer>
					</section>
				</div>

				<div id="addMedia" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span
										class="sr-only">Close</span></button>
							</div>
							<h2 class="panel-title text-center">Configuration Object Properties</h2>
						</header>
						<div class="panel-body">
							<form class="form-horizontal" id="formObjProData" method="get">
								<div class="form-group">
									<label class="col-md-3 control-label">Pick Media File</label>
									<div class="col-md-6">
										<a href="#modalAddMedia" id="picksourceobject"
											class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary">Pick Source</a> Source
										<span id="textpicksourceobject">Selected None</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">TTL</label>
									<div class="col-md-6">
										<input type="text" class="form-control" id="ttlobject">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Date</label>
									<div class="col-md-6">
										<a href="javascript:void(0)" id="day-0" class="fullweek date-object">Full
											Week</a>
										<a href="javascript:void(0)" id="day-7" class="date-object">Sun</a>
										<a href="javascript:void(0)" id="day-1" class="date-object">Mon</a>
										<a href="javascript:void(0)" id="day-2" class="date-object">Tue</a>
										<a href="javascript:void(0)" id="day-3" class="date-object">Wed</a>
										<a href="javascript:void(0)" id="day-4" class="date-object">Thu</a>
										<a href="javascript:void(0)" id="day-5" class="date-object">Fri</a>
										<a href="javascript:void(0)" id="day-6" class="date-object">Sat</a>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Date Start</label>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
											<input type="text" id="datestartobject" class="form-control datepicker">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Date End</label>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
											<input type="text" id="dateendobject" class="form-control datepicker">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Time Start</label>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-clock"></i>
											</span>
											<input type="text" id="timestartobject" data-plugin-timepicker
												class="form-control" data-plugin-options='{ "showMeridian": false }'>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Time End</label>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-clock"></i>
											</span>
											<input type="text" id="timeendobject" data-plugin-timepicker
												class="form-control" data-plugin-options='{ "showMeridian": false }'>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Priority</label>
									<div class="col-md-6">
										<select class="form-control mb-md" id="priorityobject">
											<option value="">- Priority -</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Video Time</label>
									<div class="col-md-6">
										<select class="form-control mb-md" id="videotimeobject">
											<option value="">- Follow Video -</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Location Trigger</label>
									<div class="col-md-6">
										<select class="form-control mb-md" id="locationtriggerobject">
											<option value="">- Use Trigger -</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
									</div>
								</div>
								<div class="form-group" id="typeLoc" style="display: none;">
									<label class="col-md-3 control-label">Type Location</label>
									<div class="col-md-6">
										<label class="checkbox-inline" style="padding-left: 0px;">
											<input type="radio" id="inlineCheckbox1" name="optionsRadiosLocation"
												value="1"> By Line
										</label>
										<label class="checkbox-inline">
											<input type="radio" id="inlineCheckbox2" name="optionsRadiosLocation"
												value="2"> By Geofence
										</label>
									</div>
								</div>
								<div class="form-group" id="Geo" style="display: none;">
									<label class="col-md-3 control-label">Geofence</label>
									<div class="col-md-6">
										<select multiple="multiple" id="geofence-title">
										</select>
										<div id="map" class="geofencer-map"
											style="display: none; width: 422px; height: 200px; min-width: 100%; min-height:100%; margin-top: 20px;">
										</div>
									</div>
								</div>
								<div class="form-group" id="lineOn" style="display: none;">
									<label class="col-md-3 control-label">Line</label>
									<div class="col-md-6">
										<select multiple data-plugin-selectTwo class="form-control populate"
											id="lineobject">
										</select>
										<label class="checkbox-inline" style="padding-left: 0px;">
											<input type="radio" id="inlineCheckbox1" name="optionsRadiosLine" value="1">
											Whole Line
										</label>
										<label class="checkbox-inline">
											<input type="radio" id="inlineCheckbox2" name="optionsRadiosLine" value="2">
											Certain Stop Only
										</label>
									</div>
								</div>
								<div class="form-group" id="stopOn" style="display: none;">
									<label class="col-md-3 control-label">Stop</label>
									<div class="col-md-6 stopobj">
										<select multiple="multiple" id="stopobject">
										</select>
									</div>
								</div>
							</form>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="btn btn-default modal-dismiss"
										id="cancelConfigObjectPro">Cancel</button>
									<button type="button" class="btn btn-primary" id="saveConfigObjectPro">Save</button>
									<button type="button" class="btn btn-primary" id="editConfigObjectPro" data-value=""
										style="display: none;">Save</button>
									<button type="button" class="btn btn-primary" id="simFillObjectPro">Simulate
										Fill</button>
								</div>
							</div>
						</footer>
					</section>
				</div>

				<div id="loadPlaylist" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
					<section class="panel" style="margin-bottom: 150px !important;">
						<header class="panel-heading">
							<div class="panel-actions">
								<button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span
										class="sr-only">Close</span></button>
							</div>
							<h2 class="panel-title text-center">Please Select Playlist To Load</h2>
						</header>
						<div class="panel-body">
							<input type="hidden" id="idplaylistchoose">
							<div class="col-md-4">
								<section class="panel">
									<div class="panel-body">
										<div id="slideshow-playlist"></div>
										<h4>Plalist Detail</h4>
										<h5>Name : <span id="namepl"></span></h5>
										<h5>Description : <span id="descpl"></span></h5>
										<h5>Total Content : <span id="tcpl"></span></h5>
										<h5>Created By : <span id="cbpl"></span></h5>
										<h5>Created on : <span id="copl"></span></h5>
										<h5>Content Data : </h5>
										<table class="table table-bordered table-hover mb-none"
											id="tabel-detail-playlist">
										</table>
									</div>
								</section>
							</div>
							<div class="col-md-8">
								<table class="table table-bordered table-striped mb-none" id="datatable-playlist">
									<thead>
										<tr>
											<th>No</th>
											<th>Name Playlist</th>
											<th>Desc Playlist</th>
											<th class="hidden-phone">Action</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-md-12 text-right">
									<button type="button" class="btn btn-primary" id="loadPlaylistMain">Load
										Playlist</button>
								</div>
							</div>
						</footer>
					</section>
				</div>

			</div>
		</div>
	</div>
</div>

@if(Auth::user()->can('layout-edit')==1)
	<script type="text/javascript">
		var accesseditlayout = 1;
	</script>
@else
	<script type="text/javascript">
		var accesseditlayout = 0;
	</script>
@endif
<script type="text/javascript">
	var accessdeletelayout = 0;
</script>
@can('layout-delete')
	<script type="text/javascript">
		accessdeletelayout = 1;
	</script>
@endcan

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Infovision/js/layoutmanager.min.js"></script>
@endsection

@endsection