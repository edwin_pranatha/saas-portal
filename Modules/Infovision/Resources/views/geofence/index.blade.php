@extends('theme::views.backend.app')

@section('page-title')
<h2>Geofence List</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Geofence</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Infovision/css/geofence.min.css" media="screen" />
@endsection

@section('content')

{{-- <!-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:datatable/buttons.dataTables.min.css') }}" >

<script src="{{ Module::asset('campaigns:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('campaigns:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('campaigns:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('campaigns:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />
@endpush --> --}}

<div class="row">
    @can('geofence-create')
    <div class="col-md-6 col-lg-12 col-xl-6">
        <a class="mb-xs mt-xs mr-xs btn btn-primary" href="{{ route('admin.infovision.geofence.create') }}">Add Geofence</a>
    </div>
    @endcan

    <div id="modalDelGeofence" class="modal-block modal-block-primary mfp-hide">
        <section class="panel">
            <div class="text-center panel-body">
                <div class="modal-wrapper">
                    <div class="modal-icon center">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <input type="hidden" id="urlinfoGeofence">
                        <h4>Are you sure?</h4>
                        <p>Are you sure that you want to delete this Geofence <span id="idGeofenceDel"></span>?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="text-right col-md-12">
                        <button class="btn btn-primary delGeofence">Yes</button>
                        <button class="btn btn-default modal-dismiss">No</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>

    <div id="modalInfoMap" class="modal-block modal-block-lg mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                </div>
                <h2 class="panel-title">Geofence : <span id="labelGeofence"></span></h2>
            </header>
            <div class="panel-body">
                <div id="map" class="geofencer-map" style="width: 800px; height: 600px; min-width: 100%; min-height:100%"></div>
            </div>
        </section>
    </div>

    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>
                        <h2 class="panel-title">Geofence</h2>
                    </header>
                    <input type="hidden" name="urlinfo" data-url="{{ route('admin.infovision.geofence.list') }}">
                    <div class="panel-body">
                        @if(!empty(Request::get('status')))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            Data Geofence saved successfully
                        </div>
                        @endif
                        <table class="table table-bordered table-striped mb-none" id="dataTableRowGeofence">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Infovision/js/geofence.min.js"></script>
@endsection
