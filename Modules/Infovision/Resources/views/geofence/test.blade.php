@extends('theme::views.backend.app')

@section('page-title')
<h2>Geofence Add</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Geofence</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<script src="{{ Module::asset('campaigns:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('campaigns:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('campaigns:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('campaigns:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:sass/geofence-add.min.css') }}" />
@endpush

<div class="row">
    <div>
        <div class="sidebar">
            <div class="alert alert-danger print-error-msg" style="display: none;">
                <ul></ul>
            </div>
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Name Geofence</label>
                    <input type="hidden" class="form-control" id="idGeofence" value="{{ $id }}">
                    <input type="text" class="form-control" id="nameGeofence" placeholder="Name Geofence...">
                </div>
                <button type="button" class="btn btn-primary" id="saveGeofence" data-url="{{ route('geofence.index') }}">Save</button>
            </form>
            <hr/>
            <div>
                <a href="javascript:void(0)" class="btn" id="new-polygon">New Polygon</a> |
                <a href="javascript:void(0)" class="btn" id="allow-dragging">Disable Dragging</a> |
                <a href="javascript:void(0)" class="btn" id="clear-all">Delete All</a>
            </div>
            <hr/>
            <ul class="coords"></ul>    
        </div>

        <div id="map" class="geofencer-map" style="width: 800px; height: 600px; min-width: 100%; min-height:100%"></div>
    </div>
</div>
@push('data-table')
<script src="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('campaigns:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('campaigns:js/geofence-add-test.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('campaigns:leaflet/bower_components/jsts/lib/javascript.util.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('campaigns:leaflet/bower_components/jsts/lib/jsts.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('campaigns:leaflet/dist/leaflet.geofencer.js') }}"></script>
@endpush

@endsection