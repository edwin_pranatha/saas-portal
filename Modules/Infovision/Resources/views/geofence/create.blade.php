@extends('theme::views.backend.app')

@section('page-title')
<h2>Geofence Add</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Geofence</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Infovision/css/geofence-add.min.css" media="screen" />
@endsection

@section('content')
{{-- 
@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<script src="{{ Module::asset('campaigns:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('campaigns:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('campaigns:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('campaigns:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:leaflet/fullscreen/leaflet.fullscreen.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:sass/geofence-add.min.css') }}" />
@endpush --}}
<div class="row">
    <div>
        <section class="panel sidebar">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>
                <h2 class="panel-title">Menu</h2>
            </header>
            <div class="panel-body">
                <div class="alert alert-danger print-error-msg" style="display: none;">
                    <ul></ul>
                </div>
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name Geofence</label>
                        <input type="hidden" class="form-control" id="idGeofence" value="{{ $id }}">
                        <input type="text" class="form-control" id="nameGeofence" placeholder="Name Geofence...">
                    </div>
                    <button type="button" class="btn btn-primary" id="saveGeofence" data-url="{{ route('admin.infovision.geofence.index') }}">Save</button>
                </form>
                <hr/>
                <div>
                    <a href="javascript:void(0)" class="btn" id="new-polygon">New Polygon</a> |
                    <a href="javascript:void(0)" class="btn" id="allow-dragging">Disable Dragging</a> |
                    <a href="javascript:void(0)" class="btn" id="clear-all">Delete All</a>
                </div>
                <hr/>
                <ul class="coords"></ul>
            </div>
        </section>

        <div id="map" class="geofencer-map" style="width: 800px; height: 720px; min-width: 100%; min-height:100%"></div>
    </div>

    <div id="modalEditLabel" class="modal-block modal-block-lg mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Edit Label Polygon</h2>
            </header>
            <div class="panel-body">
                <div class="alert alert-danger print-error-msg" style="display: none;">
                    <ul></ul>
                </div>
                <form class="form-horizontal" id="formLabelData" method="get">
                    <input type="hidden" class="form-control idPolygon" name="idPolygon">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Label</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control labelPolygon" name="labelPolygon">
                        </div>
                    </div>
                </form>
            </div>
            {{-- <footer class="panel-footer">
                <div class="row">
                    <div class="text-right col-md-12">
                        <button type="button" class="btn btn-default modal-dismiss">Close</button>
                        <button type="button" class="btn btn-primary" id="saveLabelPolygonData" data-url="{{ route('admin.infovision.routes.store') }}">Save Changes</button>
                    </div>
                </div>
            </footer> --}}
        </section>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('campaigns:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('campaigns:compiled/geofence-add.min.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('campaigns:leaflet/bower_components/jsts/lib/javascript.util.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('campaigns:leaflet/bower_components/jsts/lib/jsts.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('campaigns:leaflet/dist/leaflet.geofencer.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('campaigns:leaflet/fullscreen/Leaflet.fullscreen.min.js') }}"></script>
@endpush --}}

@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Infovision/js/geofence-add.min.js"></script>
@endsection
