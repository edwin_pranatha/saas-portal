@extends('theme::views.backend.app')

@section('page-title')
<h2>Campaign List</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Campaign</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Infovision/css/campaign.min.css" media="screen" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('campaigns:css/bootstrap-datetimepicker.css') }}" media="screen">
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/owl-carousel/owl.carousel.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/owl-carousel/owl.theme.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/summernote/summernote.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/summernote/summernote-bs3.css') }}" />
<link rel='stylesheet' href="{{ Module::asset('campaigns:css/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('campaigns:datatable/buttons.dataTables.min.css') }}" >
<link rel="stylesheet" href="{{ Module::asset('campaigns:sass/campaign.min.css') }}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
@endpush --}}

<div class="row">
    @can('campaign-create')
    <div class="col-md-6 col-lg-12 col-xl-6">
        <a class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary" id="newCam" href="#modalFull">New Campaign</a>
    </div>
    @endcan

    <div id="modalFull" class="modal-block modal-block-lg mfp-hide" style="max-width: 1400px;">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button type="button" class="close modal-dismiss" id="closeCampaign"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                </div>
                <h2 class="panel-title" id="title-campaign">Create Campaign</h2>
            </header>
            <div class="panel-body">
                <section class="panel form-wizard" id="w1">
                    <div class="bg-progress" style="display: none;">
                        <img src="/assets/Infovision/assets/images/loading.gif" class="bg-iconProgress">
                    </div>
                    <div class="panel-body panel-body-nopadding">
                        <div class="wizard-tabs">
                            <ul class="wizard-steps">
                                <li class="active">
                                    <a href="#w1-account" data-toggle="tab" id="camPre" class="text-center">
                                        <span class="badge hidden-xs">1</span>
                                        Campaign Preference
                                    </a>
                                </li>
                                <li>
                                    <a href="#w1-profile" data-toggle="tab" class="text-center">
                                        <span class="badge hidden-xs">2</span>
                                        Layout - Bus Screen
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <form class="form-horizontal" id="formCampaigns" novalidate="novalidate">
                            <div class="tab-content">
                                <div id="w1-account" class="tab-pane active">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label" for="w1-username">Campaign Name</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control input-sm" name="nm_project" id="nm_project" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label" for="w1-username">Campaign Start</label>
                                        <div class="col-lg-2">
                                            <div class="control-group">
                                                <div class="controls input-append date form_datetime" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1" style="display: inline-flex;">
                                                    <input size="16" type="text" class="form-control input-sm" name="startdate_project" id="startdate_project" value="" style="width: 96%;display: inline;" readonly required>
                                                    <span class="add-on" style="margin-top: 5px;margin-left: 5px;"><i class="icon-remove fa fa-trash"></i></span>
                                                    <span class="add-on" style="margin-top: 5px;margin-left: 5px;"><i class="icon-th fa fa-th"></i></span>
                                                </div>
                                                <input type="hidden" id="dtp_input1" class="startdate_project" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label" for="w1-username">Campaign End</label>
                                        <div class="col-lg-2">
                                            <div class="control-group">
                                                <div class="controls input-append date form_datetime" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1" style="display: inline-flex;">
                                                    <input size="16" type="text" class="form-control input-sm" name="enddate_project" id="enddate_project" value="" style="width: 96%;display: inline;" readonly required>
                                                    <span class="add-on" style="margin-top: 5px;margin-left: 5px;"><i class="icon-remove fa fa-trash"></i></span>
                                                    <span class="add-on" style="margin-top: 5px;margin-left: 5px;"><i class="icon-th fa fa-th"></i></span>
                                                </div>
                                                <input type="hidden" id="dtp_input1" class="enddate_project" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label" for="w1-username">Add Buses</label>
                                        <div class="col-lg-8">
                                            <select multiple="multiple" id="showAddBuses" required> </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label" for="w1-username">Campaign Type</label>
                                        <div class="col-lg-3">
                                            <select class="form-control" id="type_project" name="type_project" required>
                                                <option value="" selected>- Select Campaign Type -</option>
                                                <option value="Bus Screen">Bus Screen</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="w1-profile" class="tab-pane">
                                    <input type="hidden" id="status-wizard" value="0">
                                    <input type="hidden" id="idprojectCam">
                                    <a href="#modelChooseLayout" id="PopUpLayout" class="modal-sizes" style="display: none;">CLick Layout</a>
                                    <h5>Layout - <span id="defDD">(Default DD bus Dimension : 1366x768)</span></h5>
                                    <p id="parag-new">Override default layout? <select class="form-control w-initial" id="overRideLayout" required><option value="No">No</option><option value="Yes">Yes</option></select></p>
                                    <p id="parag-edit" style="display: none;"><a href="#" target="_blank" id="editLayoutCampaign" class="mb-xs mt-xs mr-xs btn btn-primary">Edit Layout</a></p>
                                    <div class="isotope-item document m-width50">
                                        <div class="thumbnail bg-imgScreen">
                                            <h5 class="mg-title center-wBold" id="title-layout">LY0001</h5>
                                            <div class="thumb-preview">
                                                <a class="thumb-image" href="javascript:void(0)">
                                                    <img id="img-screen-layout" src="/admin/infovision/layout/LY0001.png" class="img-responsive" alt="Project">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="">
                        <ul class="pager">
                            <li class="previous disabled">
                                <a><i class="fa fa-angle-left"></i> Previous</a>
                            </li>
                            <li class="hidden finish pull-right">
                                <a id="submitCampaign">Submit</a>
                            </li>
                            <li class="next">
                                <a id="nextCampaign">Next <i class="fa fa-angle-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </section>
    </div>

    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                            <a href="#" class="fa fa-times"></a>
                        </div>
                        <h2 class="panel-title">Campaign</h2>
                    </header>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped mb-none" id="dataTableRowCampaignManager" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Campaign ID</th>
                                    <th>Version</th>
                                    <th>Campaign Name</th>
                                    <th>Manager ID</th>
                                    <th>Campaign Start</th>
                                    <th>Campaign Stop</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div id="modelChooseLayout" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button type="button" class="close modal-dismiss"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                </div>
                <h2 class="panel-title" style="text-align: center;">Please Select Override Layout</h2>
            </header>
            <div class="panel-body" id="showLayoutManagerPopUp">

            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="text-right col-md-12">
                        <button type="button" class="btn btn-default modal-dismiss" id="cancelOverRide">Cancel</button>
                        <button type="button" class="btn btn-primary" id="selectLayoutPopUp">Select Layout</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>

    <div id="modelInfoLayout" class="zoom-anim-dialog modal-block modal-block-lg mfp-hide" style="max-width: 1047px;">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button type="button" class="close modal-dismiss closeNotice"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                </div>
                <h2 class="panel-title" style="text-align: center;">Campaign Information</h2>
            </header>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active">
                                <a href="#campaign-summary" id="camSummary" data-toggle="tab" class="text-center">Campaign Summary</a>
                            </li>
                            <li>
                                <a href="#deploy-summary" data-toggle="tab" class="text-center">Deploy Summary</a>
                            </li>
                            <li>
                                <a href="#screen-preview" data-toggle="tab" class="text-center">Screen Preview</a>
                            </li>
                            <li>
                                <a href="#send-notice" data-toggle="tab" class="text-center">Send Notice</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="campaign-summary" class="tab-pane active">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="inputDefault">Project Name :</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="project_nameInfo" id="project_nameInfo" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="inputDefault">Project Type :</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="project_typeInfo" id="project_typeInfo" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="inputDefault">Project Layout :</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="project_layoutInfo" id="project_layoutInfo" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="inputDefault">Project Apply To :</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="project_applyToInfo" id="project_applyToInfo" disabled></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="inputDefault">Project Playlist :</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="project_playlistInfo" id="project_playlistInfo" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="inputDefault">Campaign Start :</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="camp_startInfo" id="camp_startInfo" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="inputDefault">Campaign End :</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="camp_endInfo" id="camp_endInfo" disabled>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="deploy-summary" class="tab-pane">
                                <table class="table table-noborder">
                                    <tbody>
                                        <tr>
                                            <td><b>Status</b></td>
                                            <td>: <span id="statusInfo"></span></td>
                                            <td><b>Target</b></td>
                                            <td>: <span id="targetInfo"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>Time Started</b></td>
                                            <td>: <span id="time_startedInfo"></span></td>
                                            <td><b>Succesdfully</b></td>
                                            <td>: <span id="successfullInfo"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>Time End</b></td>
                                            <td>: <span id="time_endInfo"></span></td>
                                            <td><b>Failed</b></td>
                                            <td>: <span id="failedInfo"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>Time Interrupt</b></td>
                                            <td>: <span id="time_interInfo"></span></td>
                                            <td><b>In Progress</b></td>
                                            <td>: <span id="inprogressInfo"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-bordered table-striped mb-none" id="dataTableRowInfoCamp">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Bus</th>
                                            <th>Status Message</th>
                                            <th>Progress</th>
                                            <th>Last Try</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div id="screen-preview" class="tab-pane">
                                <section class="panel" id="panelScreen" style="display: block;margin: 0 auto;">
                                    <div id="screenbus" class="screenbus_29">
                                        <div id="canvaspreview" class="row" style="position: relative;"></div>
                                    </div>
                                </section>
                            </div>
                            <div id="send-notice" class="tab-pane">
                                <form class="form-horizontal" style="margin-bottom: 20px;">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="inputDefault">Select Buses</label>
                                        <div class="col-md-7">
                                            <select multiple="multiple" id="showAddBusesInfo">
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-primary" id="saveNoticeCampaign">Send Notice</button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="inputDefault">Set Duration</label>
                                        <div class="col-md-7">
                                            <select class="form-control" id="set_duration_notice">
                                                <option value="">- Method -</option>
                                                <option value="Fixed Duration">Fixed Duration</option>
                                                <option value="End Date Duration">End Date Duration</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12" style="margin-bottom: 10px;">
                                            <div class="input-group" style="width: 130px;float: left;margin-right: 10px;">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-font"></i>
                                                </span>
                                                <select class="form-control" id="fontNotice">
                                                    <option value="Serif">Serif</option>
                                                    <option value="Sans">Sans</option>
                                                    <option value="Courier">Courier</option>
                                                    <option value="Comic Sans MS">Comic Sans MS</option>
                                                    <option value="Lucida Grande">Lucida Grande</option>
                                                    <option value="Verdana">Verdana</option>
                                                </select>
                                            </div>
                                            <div class="input-group" style="width: 130px;float: left;margin-right: 10px;">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-align-justify"></i>
                                                </span>
                                                <select class="form-control" id="positionNotice">
                                                    <option value="Top">Top</option>
                                                    <option value="Bottom">Bottom</option>
                                                </select>
                                            </div>
                                            <div class="input-group" style="width: 100px;float: left;margin-right: 10px;">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-text-height"></i>
                                                </span>
                                                <select class="form-control" id="sizeNotice">
                                                    <option value="35">35</option>
                                                    <option value="40">40</option>
                                                    <option value="50">50</option>
                                                </select>
                                            </div>
                                            <div class="input-group color colorpicker-element" data-color="rgb(255, 146, 180)" data-color-format="rgb" data-plugin-colorpicker="" style="width: 170px;float: left;margin-right: 10px;">
                                                <span class="input-group-addon"><i style="background-color: rgb(153, 35, 72);"></i></span>
                                                <input type="text" class="form-control" id="colorNotice">
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="areaEditorInfo">
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered table-striped mb-none" id="dataTableRowInfoNotice">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Notice Target</th>
                                            <th>Notice Message</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="text-right col-md-12">
                        <button type="button" class="btn btn-default modal-dismiss closeNotice">Close</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('campaigns:assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/owl-carousel/owl.carousel.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/summernote/summernote.js') }}"></script>
<script src="{{ Module::asset('campaigns:js/sweetalert2.all.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/javascripts/forms/examples.wizard.js') }}"></script>
<script src="{{ Module::asset('campaigns:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('campaigns:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('campaigns:js/bootstrap-datetimepicker.js') }}" charset="UTF-8"></script>
<script src="{{ Module::asset('campaigns:js/locales/bootstrap-datetimepicker.fr.js') }}" charset="UTF-8"></script>
<script src="{{ Module::asset('campaigns:js/campaign.js') }}"></script>
@endpush --}}

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Infovision/js/campaign.min.js"></script>
@endsection
