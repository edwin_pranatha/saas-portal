function bootstrapTabControl(){
    var i, items = $('.nav-link'), pane = $('.tab-pane');

    $('.nexttab').on('click', function(){
        for(i = 0; i < items.length; i++){
            if($(items[i]).hasClass('active') == true){
                break;
            }
        }
        if(i < items.length - 1){

            $(items[i]).removeClass('active');
            $(items[i+1]).addClass('active');
            $(items[i+1]).focus();

            $(pane[i]).removeClass('active');
            $(pane[i+1]).addClass('active');
        }
        $('#div1').animate( { scrollLeft: '+=50' }, 1000);
    });

    $('.prevtab').on('click', function(){
        for(i = 0; i < items.length; i++){
            if($(items[i]).hasClass('active') == true){
                break;
            }
        }
        if(i != 0){

            $(items[i]).removeClass('active');
            $(items[i-1]).addClass('active');
            $(items[i-1]).focus();

            $(pane[i]).removeClass('active');
            $(pane[i-1]).addClass('active');
        }
        $('#div1').animate( { scrollLeft: '-=50' }, 1000);
    });
}


bootstrapTabControl();

$(document).ready(function(){   
            var setSlideImage = '';
            var nomorslide = 0;

            $('.modal-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,   
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });
        
            $('.modal-sizes').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,   
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });
        
            $(document).on('click', '.modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
        
            $('select.populate').select2();
        
            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: false,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: true,
                    titleSrc: function(item) {
                        return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
                    }
                },
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, 
                    opener: function(element) {
                        return element.find('img');
                    }
                }       
            });

            $("#newCam").click(function() {
                $('.bg-progress').css('display','block');
                $('.form_datetime').datetimepicker({
                    format: "yyyy-mm-dd hh:ii",
                    autoclose: true,
                    todayBtn: true,
                    defaultDate: getNowDate(),
                    fontAwesome: 'font-awesome'
                });
                $('#title-campaign').text('Create Campaign');
                getVehicleList(0);
            });

            function resetFormCam(){
                $('#formCampaigns')[0].reset();
                $('#status-wizard').val('0');
                $('#idprojectCam').val('');
                $('#defDD').text('(Default DD bus Dimension : 1366x768)');
                $('#title-layout').text('Default DD bus');
                $('#img-screen-layout').attr('src', '/admin/infovision/layout/LY0001.png');
                $('#parag-new').css('display', 'block');
                $('#parag-edit').css('display', 'none');
                $('#camPre').click();
            }

            function getVehicleList(no){
                $.ajax({
                    type: "GET",
                    url: "/admin/infovision/campaign/vehicles/list",
                    cache: false,
                    success: function(result){
                        var data = result.models;
                        $('#showAddBuses').empty();
                        var html = '';
                        var statusobj = '';
                        for (var i = 0; i < data.length; i++) {
                            if(statusobj==''){
                                html = '<optgroup label="' + data[i].id_region + '">';
                            }else if(statusobj!=data[i].region){
                                html += '<optgroup label="' + data[i].id_region + '">';
                            }
                            statusobj = data[i].region;
                            if(statusobj==data[i].region){
                                html += '<option class="' + data[i].id_region + '-1" value="' + data[i].id_vehicle + '">' + data[i].id_vehicle + '</option>';
                            }
                        }

                        $('#showAddBuses').append(html);
                        $('#showAddBuses').multiselect({
                            enableClickableOptGroups: true,
                            enableCollapsibleOptGroups: true,
                            enableFiltering: true,
                            includeSelectAllOption: true,
                            templates: {
                                liGroup: '<li><a href="javascript:void(0);" style="background-color: #ccc;"><input type="checkbox" class="groupStop" value="" style="margin-left: 20px;"><label class="multiselect-group" style="padding-left: 7px;font-weight: bold;"></label></a></li>'
                            }
                        });
                        $('#showAddBuses').multiselect('rebuild');
                        if(no==0){
                            $('.bg-progress').css('display','none');
                        }
                    }
                });
            }

            $("#overRideLayout").change(function() {
                var values = $(this).val();
                if(values=='Yes'){
                    $('#PopUpLayout').click();
                    getShowLayoutModels();
                }
            });

            function getShowLayoutModels(){
                $.ajax({
                    type: "GET",
                    url: "/admin/infovision/campaign/show/layout/manager",
                    cache: false,
                    success: function(result){
                        var models = result.models;
                        var html = '';
                        for (var i = 0; i < models.length; i++) {
                            var bgid = models[i].bg_layout;
                            if(bgid=='BG001'){
                                var screen = '1366x768';
                            }else if(bgid=='BG002'){
                                var screen = '1920x540';
                            }else if(bgid=='BG003'){
                                var screen = '1024x768';
                            }else if(bgid=='BG004'){
                                var screen = '1920x1080';
                            }
                            html += '<div class="isotope-item document col-xs-6 col-md-3">';
                            html += '<div class="thumbnail h-layoutmanager choose-layout" id="' + models[i].id_layout + '|' + screen + '">';
                            html += '<div class="thumb-preview">';
                            html += '<a class="thumb-image" href="javascript:void(0)">';
                            html += '<img src="/admin/infovision/layout/' + models[i].pic_layout + '" class="img-responsive" alt="Project">';
                            html += '</a>';
                            html += '</div>';
                            html += '<h5 class="mg-title text-semibold">' + models[i].nm_layout + '</h5>';
                            html += '<small>By: ' + models[i].user + '</small></br>';
                            html += '<small>On: ' + models[i].created_at.replace('T',' ').replace('.000000Z','') + '</small></br>';
                            html += '<small>Dimension: ' + screen + '</small>';
                            html += '</div>';
                            html += '</div>';
                            $('#showLayoutManagerPopUp').html(html);
                        }

                        $(".choose-layout").click(function() {
                            $('.choose-layout').removeClass('selectLayout');
                            $(this).addClass('selectLayout');
                        });
                    }
                });
            }

            $("#selectLayoutPopUp").click(function() {
                var values = $('.selectLayout').attr('id');
                var split = values.split('|');
                $('#defDD').text(split[0] + ' Dimension : ' + split[1]);
                $('#title-layout').text(split[0]);
                $('#img-screen-layout').attr('src', '/admin/infovision/layout/' + split[0] + '.png');
                $('#newCam').click();
            });
            
            var arrayCamp = [];

            $("#submitCampaign").click(function(e) {
                var statusWizard = $('#status-wizard').val();
                if(statusWizard=='0'){
                    $.ajax({
                        type: "POST",
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "/admin/infovision/campaign/store",
                        data: {
                            nmproject: arrayCamp['nmproject'],
                            startdate: arrayCamp['startdate'],
                            enddate: arrayCamp['enddate'],
                            busid: arrayCamp['busid'],
                            typeproject: arrayCamp['typeproject'],
                            idlayout: $('#title-layout').text()
                        },
                        cache: false,
                        success: function(result){
                            swal("Campaign Submited!", "You will find you campaign in the campaign overview page, from where you can deploy it to your selection of buses", "success");
                            e.preventDefault();
                            $.magnificPopup.close();
                            tableRowCampaigns.fnDraw(false);
                            resetFormCam();
                        }
                    });
                }else{
                    $.ajax({
                        type: "POST",
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "/admin/infovision/campaign/updateCampaign",
                        data: {
                            idproject: $('#idprojectCam').val(),
                            nmproject: arrayCamp['nmproject'],
                            startdate: arrayCamp['startdate'],
                            enddate: arrayCamp['enddate'],
                            busid: arrayCamp['busid'],
                            typeproject: arrayCamp['typeproject'],
                            idlayout: $('#title-layout').text()
                        },
                        cache: false,
                        success: function(result){
                            swal("Campaign Submited!", "You will find you campaign in the campaign overview page, from where you can deploy it to your selection of buses", "success");
                            $('#status-wizard').val('0');
                            e.preventDefault();
                            $.magnificPopup.close();
                            tableRowCampaigns.fnDraw(false);
                            resetFormCam();
                        }
                    });
                }
            });

            $("#closeCampaign").click(function() {
                $('#status-wizard').val('0');
                resetFormCam();
            });

            var tableRowCampaigns = $('#dataTableRowCampaignManager').dataTable({
                'processing': true,
                'serverSide': true,
                'ajax': {
                    'url':"/admin/infovision/campaign",
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columnDefs': [{
                    "targets": 8,
                    "render": function (data, type, row) {
                        var parser = new DOMParser();
                        var doc = parser.parseFromString(data, 'text/html');
                        var checkbox = doc.body.firstChild.data;
                        return checkbox;
                    }
                }],
                'columns': [
                    { data: 'DT_RowIndex', "width" : "40px" },
                    { data: 'id_project', "width" : "80px" },
                    { data: 'ver_project', "width" : "80px" },
                    { data: 'proj_nm', "width" : "80px" },
                    { data: 'mngr_id', "width" : "80px" },
                    { data: 'proj_start', "width" : "150px" },
                    { data: 'proj_end', "width" : "150px" },
                    { data: 'type_apply', "width" : "80px" },
                    { data: 'statusdeploy', "width" : "150px"},
                    { data: 'proj_gen', "width" : "150px" },
                    { data: 'action', "width" : "190px" },
                ],
                'fnDrawCallback': function () {
                    $('.modal-sizes').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,   
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in',
                        modal: true
                    });

                    $('.form_datetime').datetimepicker({
                        format: "yyyy-mm-dd hh:ii",
                        autoclose: true,
                        todayBtn: true,
                        defaultDate: getNowDate()
                    });

/*                    getVehicleList();*/

                    $(".editCampaign").click(function() {
                        $('.bg-progress').css('display','block');
                        var values = $(this).attr('data-value');
                        $('#title-campaign').text('Edit Campaign');
                        $('#status-wizard').val('1');
                        $('#camPre').click();
                        getVehicleList(1);
                        getEditCampaign(values);
                    });

                    $(".deployCampaign").click(function() {
                        var startnow = $(this).closest('tr').find('td').eq(8);
                        var idproject = $(this).attr('data-value');
                        var txtmsg = "Are you sure to deploy this campaign?";
                        swal({
                            title: "Deploy Confirmation",
                            text: txtmsg,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#337ab7",
                            confirmButtonText: "Yes, Deploy!"
                        }).then(result => {
                            if (result.value) {
                                startnow.html('Processing <div class="progress progress-striped light active m-md"><div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div></div>');
                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                                    },
                                    url: "/admin/infovision/campaign/generate/config",
                                    data: {
                                        idproject: idproject,
                                    },
                                    cache: false,
                                    success: function(result){
                                        tableRowCampaigns.fnDraw(false);
                                    }
                                });
                                return false;
                            }
                        });
                    });

                    $(".infoCampaign").click(function() {
                        $('#areaEditorInfo').html('<div class="summernote" id="infoEditor" data-plugin-summernote data-plugin-options="{ "height": 180, "codemirror": { "theme": "ambiance" } }"">Start typing...</div>');
                        $('#infoEditor').summernote({
                            toolbar: [
                                ['style', ['bold', 'italic', 'clear']],
                                ['fontsize', ['fontsize']],
                                ['color', ['color']]
                            ]
                        });
                        $('#camSummary').click();
                        var values = $(this).attr('data-value');
                        getInfoCampaign(values);
                    });

                    $(".stopCampaign").click(function() {
                        var values = $(this).attr('data-value');
                        swal({
                            title: "Stop Confirmation",
                            text: "are you sure want to stop this campaign?, campaign upload queue will be stopped",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#337ab7",
                            confirmButtonText: "Yes, Deploy!"
                        }).then(result => {
                            if (result.value) {
                                var data = $(this).closest('tr').find('td').eq(8);
                                getStopCampaign(values, data);
                            }
                        });
                    });

                    $(".delCampaign").click(function() {
                        var values = $(this).attr('data-value').split('|');
                        var idproject = values[0];
                        var idlayout = values[1];
                        remCampaign(idproject, idlayout);
                    });
                }
            });

            function remCampaign(idproject, idlayout){
                swal({
                    title: "Delete Confirmation", 
                    text: "are you sure want to delete this campaign?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#337ab7",
                    confirmButtonText: "Yes, Delete!",
                }).then(result => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            headers: {
                                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "/admin/infovision/campaign/deleteCampaign",
                            data: {
                                idproject: idproject,
                                idlayout: idlayout
                            },
                            cache: false,
                            success: function(result){
                                tableRowCampaigns.fnDraw(false);
                            }
                        });
                    }
                });
            }

            function getStopCampaign(values, data){
                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/admin/infovision/campaign/stopCamapaign",
                    data: {
                        id_project: values
                    },
                    cache: false,
                    success: function(result){
                        data.html('Stopped');
                    }
                });
            }

            function getEditCampaign(values){
                $.ajax({
                    type: "GET",
                    url: "/admin/infovision/campaign/edit/show",
                    data: {
                        id: values
                    },
                    cache: false,
                    success: function(result){
                        var data = result.models;
                        var detail = result.detail;
                        $('#nm_project').val(data.proj_nm);
                        $('#startdate_project').val(data.proj_start);
                        $('#enddate_project').val(data.proj_end);
                        var split = data.group_apply.split(',');
                        $('#showAddBuses').val(split);
                        $("#showAddBuses").multiselect("refresh");
                        if(data.type_apply=='Bus'){
                            var typedata = 'Bus Screen';
                            $('#type_project').val(typedata);
                        }else{
                            var typedata = '';
                        }
                        $('#title-layout').val(data.layout_apply);
                        var bgid = detail.bg_layout;
                        if(bgid=='BG001'){
                            var screen = '1366x768';
                        }else if(bgid=='BG002'){
                            var screen = '1920x540';
                        }else if(bgid=='BG003'){
                            var screen = '1024x768';
                        }else if(bgid=='BG004'){
                            var screen = '1920x1080';
                        }
                        $('#idprojectCam').val(data.id_project);
                        $('#defDD').text(detail.nm_layout + ' Dimension : ' + screen);
                        $('#title-layout').text(detail.id_layout);
                        $('#img-screen-layout').attr('src', '/admin/infovision/layout/' + detail.pic_layout);
                        $('#parag-new').css('display', 'none');
                        $('#parag-edit').css('display', 'block');
                        $('#editLayoutCampaign').attr('href', '/admin/infovision/layout-create?id=' + detail.id_layout);
                        $('.bg-progress').css('display','none');
                    }
                });
            }

            $("#nextCampaign").click(function() {
                arrayCamp['nmproject'] = $('#nm_project').val();
                arrayCamp['startdate'] = $('#startdate_project').val();
                arrayCamp['enddate'] = $('#enddate_project').val();
                arrayCamp['busid'] = $('#showAddBuses').val();
                arrayCamp['typeproject'] = $('#type_project').val();
            });

            $("#cancelOverRide").click(function() {
                $('#newCam').click();
            });

            $(window).on('storage', function (e) {
                var storageEvent = e.originalEvent;
                if(storageEvent.key == 'detectMyChange'){
                    swal("Layout Saved!", "Your layout campaign has been saved!", "success");
                    var data = localStorage.getItem('newlayoutver');
                    $('#title-layout').text(data);
                    $('#img-screen-layout').attr('src', '/admin/infovision/layout/' + data + '.png');
                }
            });

            $("#saveNoticeCampaign").click(function() {
                var target = $('#showAddBusesInfo').val();
                var duration = $('#set_duration_notice').val();
                var text = $('.note-editable').html();
                var font = $('#fontNotice').val();
                var position = $('#positionNotice').val();
                var size = $('#sizeNotice').val();
                var color = $('#colorNotice').val(); 
                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/admin/infovision/campaign/notice/save",
                    data: {
                        target: target,
                        duration: duration,
                        text: text,
                        font: font,
                        position: position,
                        size: size,
                        color: color
                    },
                    cache: false,
                    success: function(result){
                        $('#showAddBusesInfo').val('').multiselect('refresh');
                        $('#set_duration_notice').val('');
                        $('.note-editable').html('');
                        $('#colorNotice').val(''); 
                        tableRowNotice.fnDraw(false);
                    }
                });
            });

            var tableRowNotice = '';

            function getInfoCampaign(values){
                $.ajax({
                    type: "GET",
                    url: "/admin/infovision/campaign/info/show",
                    data: {
                        idproject: values
                    },
                    cache: false,
                    success: function(result){
                        var models = result.models;
                        var lay = result.lay;
                        var laydet = result.laydet;
                        var playlist = result.playlist;
                        var detail = result.detail;
                        $('#project_nameInfo').val(models.proj_nm);
                        $('#project_typeInfo').val(models.type_apply);
                        $('#project_layoutInfo').val('[' + lay.id_layout + '] - ' + lay.nm_layout);
                        $('#project_applyToInfo').val(models.group_apply);
                        $('#project_playlistInfo').val(models.assetlist);
                        $('#camp_startInfo').val(models.proj_start);
                        $('#camp_endInfo').val(models.proj_end);
                        if(models.status==''||models.status==null){
                            var status = 'Ready';
                        }else if(models.status=='2'){
                            var status = 'Update Ready';
                        }else if(models.status=='3'){
                            var status = 'Process';
                        }else if(models.status=='4'){
                            var status = 'Stop';
                        }else if(models.status=='1'){
                            var status = 'Finish';
                        }
                        $('#statusInfo').text(status);
                        $('#time_startedInfo').text(models.proj_fin);
                        if(models.status=='4'){
                            $('#time_endInfo').text(models.proj_stop);
                            $('#time_interInfo').text('');
                        }else{
                            $('#time_endInfo').text('');
                            $('#time_interInfo').text(models.proj_stop);
                        }
                        $('#targetInfo').text(result.target);
                        $('#successfullInfo').text(result.successfull);
                        $('#failedInfo').text(result.failed);
                        $('#inprogressInfo').text(result.inprogress);
                        var no = 1;
                        $('#dataTableRowInfoCamp > tbody').empty();
                        for(i = 0; i < detail.length; i++){
                            if(detail[i].upload_percent=='100'){
                                var progress = 'Finished';
                            }else{
                                var progress = detail[i].upload_percent;
                            }
                            var html = '<tr>';
                                html += '<td>' + no + '</td>';
                                html += '<td>' + detail[i].bus_id + '</td>';
                                html += '<td>' + detail[i].upload_lasterr + '</td>';
                                html += '<td>' + progress + '</td>';
                                html += '<td>' + detail[i].lasttry_deploy + '</td>';
                                html += '<td><a href="#" class="btn btn-danger refreshBuild" data-value="' + detail[i].id_project + '"><i class="fa fa-refresh"></i></a></td>';
                                html += '</tr>';
                                $('#dataTableRowInfoCamp > tbody').append(html);
                            no++;
                        }
                        $('#dataTableRowInfoCamp').dataTable();

                        prevLayoutManager(lay, laydet, playlist);

                        showVehicleInfoCamp();

                        $(".refreshBuild").click(function() {
                            var valuesdata = $(this).attr('data-value');
                            swal({
                                title: "Redeploy Confirmation",
                                text: "Do you want to redeploy campaign for this bus?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#337ab7",
                                confirmButtonText: "Yes, Sure!"
                            }).then(result => {
                                if(result.value){
                                    refreshBuildProject(valuesdata);
                                }
                            });
                            return false;
                        });

                        $("#dataTableRowInfoNotice").dataTable().fnDestroy();
                        tableRowNotice = $('#dataTableRowInfoNotice').dataTable({
                            'processing': true,
                            'serverSide': true,
                            'ajax': {
                                'url': "/admin/infovision/campaign/notice/show",
                                'type': 'POST',
                                'headers': {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            },
                            'columns': [
                                { data: 'DT_RowIndex', "width" : "40px" },
                                { data: 'target_notice', "width" : "200px" },
                                { data: 'msg_notice', "width" : "200px" },
                                { data: 'stat_notice', "width" : "200px" },
                                { data: 'date_start', "width" : "200px" },
                            ],
                            'fnDrawCallback': function () {

                            }
                        });
                    }
                });
            }

            function prevLayoutManager(models,detail,playlist){
                var models = models;
                var detail = detail;
                var playlist = playlist;
                $("#canvaspreview").html("");
                var bglayout = 'rgb(0, 0, 0)';
                var targetshow = 'screenbus';
                var bgid = models.bg_layout;
                if(bgid=='BG001'){
                    var width = 1366;
                    var height = 768;
                }else if(bgid=='BG002'){
                    var width = 1920;
                    var height = 540;
                }else if(bgid=='BG003'){
                    var width = 1024;
                    var height = 768;
                }else if(bgid=='BG004'){
                    var width = 1920;
                    var height = 1080;
                }
                if(width==1920 && height==540){
                    $("#panelScreen").css('width',"955px");
                    $("#panelScreen").css('height',"");
                    $("#panelScreen").css('background-color',bglayout);
                    $("#panelScreen").css('border-radius',"45px");
                    $("#screenbus").removeClass().addClass("screen_maes_29");
                }else if(width==1920 && height==1080){
                    $("#panelScreen").css('width',"955px");
                    $("#panelScreen").css('height',"395px");
                    $("#panelScreen").css('background-color',bglayout);
                    $("#panelScreen").css('border-radius',"45px");
                    $("#screenbus").removeClass().addClass("screen_maes_29new");
                }else if(width==1366 && height==768){
                    $("#panelScreen").css('width',"658px");
                    $("#panelScreen").css('height',"");
                    $("#panelScreen").css('background-color',bglayout);
                    $("#screenbus").removeClass().addClass("screen_maes_19new");
                }else if(width==1024 && height==768){
                    $("#panelScreen").css('width',"529px");
                    $("#panelScreen").css('height',"400px");
                    $("#panelScreen").css('border-radius',"45px");
                    $("#panelScreen").css('background-color',bglayout);
                    $("#screenbus").removeClass().addClass("screen_maes_19_1024-768");
                }
                var sclfactor=0.46;
                var spcfactorw=1;
                var spcfactorh=1;
                var spchplus=1;
                if(width==1920 && height==1080){
                    spcfactorh=0.60;
                }
                var objcontent = "";
                for (var i = 0; i < detail.length; i++) {
                    objcontent = "";
                    if(detail[i].objtype=='labeledRect'){
                        objcontent = "<div class='obdata'><img src='/themes/maestronic/assets/backend/img/infovision_cnxx.png' width='" + ((detail[i].width*sclfactor)*spcfactorw)+ "px' height='" + ((detail[i].height*sclfactor)*spcfactorh)+ "px'></div>";
                    }else if(detail[i].objtype=='image'){
                        objcontent = "<div class='objdata'><img src='/admin/infovision/image/"+detail[i].src_val+"' width='" + ((detail[i].width*sclfactor)*spcfactorw)+ "px' height='" + (((detail[i].height*sclfactor)*spcfactorh)*spchplus)+ "px'></div>";
                    }else if(detail[i].objtype=='text'){
                        objcontent = "<div style='height:100%;display:block;background-color:white;vertical-align:middle;'>" + detail[i].src_val + "</div>";
                    }else if(detail[i].objtype=='ticker'){
                        objcontent = "<marquee style='height:100%;display:block;background-color:white;vertical-align:middle;'>"+detail[i].src_val.replace(/\\"/g, '"')+"</marquee>";
                    }else if(detail[i].objtype=='url'){
                        objcontent = '<iframe height="100%" width="100%" src="'+detail[i].src_val+'" /></iframe>';
                    }else if(detail[i].objtype=='slide'){
                        objcontent = "<div class='slidedata'>";
                        for (var a = 0; a < playlist.length; a++) {
                            objcontent += "<img id='slide-" + a + "' class='group-slide' src='/admin/infovision/image/" + playlist[a].src_val + "' style='background-color: rgb(0, 0, 0); position: absolute; top: 0px; left: 0px; display: block; z-index: 4; opacity: 1; width: " + ((detail[i].width*sclfactor)*spcfactorw) + "px; height: " + ((detail[i].height*sclfactor)*spcfactorh) + "px;'>";
                        }
                        objcontent += "</div>";
                        slideShowAuto(playlist.length);
                    }
                    $("#canvaspreview").append('<span style="position: absolute; top: '+(detail[i].top*sclfactor)+'px; left: '+(detail[i].left*sclfactor)+'px; display: block; background: '+detail[i].fill+'; width: '+((detail[i].width*sclfactor)*spcfactorw)+'px; height: '+((detail[i].height*sclfactor)*spcfactorh)+'px;">'
                        +objcontent+
                        '</span>');
                }
            }

            function slideShowAuto(length){
                if(nomorslide >= length){
                    nomorslide = 0;
                }
                $('.group-slide').css('opacity', '0');
                $('.group-slide').css('transition', 'opacity 0.3s, visibility 0.3s');
                $('#slide-' + nomorslide).css('opacity', '1');
                $('#slide-' + nomorslide).css('transition', 'opacity 0.3s, visibility 0.3s');
                nomorslide++;
                setSlideImage = setTimeout(function () {
                    slideShowAuto(length);
                }, 5000);
            }

            function refreshBuildProject(valuesdata){
                $.ajax({
                    type: "GET",
                    url: "/admin/infovision/campaign/info/show",
                    data: {
                        idproject: valuesdata
                    },
                    cache: false,
                    success: function(result){
                        var detail = result.detail;
                        var no = 1;
                        $('#dataTableRowInfoCamp > tbody').html('');
                        for(i = 0; i < detail.length; i++){
                            if(detail[i].upload_percent=='100'){
                                var progress = 'Finished';
                            }else{
                                var progress = detail[i].upload_percent;
                            }
                            var html = '<tr>';
                                html += '<td>' + no + '</td>';
                                html += '<td>' + detail[i].bus_id + '</td>';
                                html += '<td>' + detail[i].upload_lasterr + '</td>';
                                html += '<td>' + progress + '</td>';
                                html += '<td>' + detail[i].lasttry_deploy + '</td>';
                                html += '<td><a href="#" class="btn btn-danger refreshBuild" data-value="' + detail[i].id_project + '"><i class="fa fa-refresh"></i></a></td>';
                                html += '</tr>';
                            $('#dataTableRowInfoCamp').append(html);
                            no++;
                        }
                        $('#dataTableRowInfoCamp').dataTable();

                        $(".refreshBuild").click(function() {
                            var valuesdata = $(this).attr('data-value');
                            swal({
                                title: "Redeploy Confirmation",
                                text: "Do you want to redeploy campaign for this bus?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#337ab7",
                                confirmButtonText: "Yes, Sure!"
                            }).then(result => {
                                if(result.value){
                                    refreshBuildProject(valuesdata);
                                }
                            });
                            return false;
                        });
                    }
                });
            }

            function showVehicleInfoCamp(){
                $.ajax({
                    type: "GET",
                    url: "/admin/infovision/campaign/vehicles/list",
                    cache: false,
                    success: function(result){
                        var data = result.models;
                        $('#showAddBusesInfo').empty();
                        var html = '';
                        var statusobj = '';
                        for (var i = 0; i < data.length; i++) {
                            if(statusobj==''){
                                html = '<optgroup label="' + data[i].id_region + '">';
                            }else if(statusobj!=data[i].id_region){
                                html += '<optgroup label="' + data[i].id_region + '">';
                            }else if(statusobj==data[i].id_region){
                                html += '<option class="' + data[i].id_region + '-1" value="' + data[i].id_vehicle + '">' + data[i].id_vehicle + '</option>';
                            }
                            statusobj = data[i].id_region;
                        }

                        $('#showAddBusesInfo').append(html);
                        $('#showAddBusesInfo').multiselect({
                            enableClickableOptGroups: true,
                            enableCollapsibleOptGroups: true,
                            enableFiltering: true,
                            includeSelectAllOption: true,
                            templates: {
                                liGroup: '<li><a href="javascript:void(0);" style="background-color: #ccc;"><input type="checkbox" class="groupStop" value="" style="margin-left: 20px;"><label class="multiselect-group" style="padding-left: 7px;font-weight: bold;"></label></a></li>'
                            }
                        });
                        $('#showAddBusesInfo').multiselect('rebuild');
                        $('.color').colorpicker();
                    }
                });
            }

            $(".closeNotice").click(function() {
                clearTimeout(setSlideImage);
            });

            var statusProject = [];

            function checkStatusProject(){
                setInterval(function(){
                    $.ajax({
                        type: "GET",
                        url: "/admin/infovision/campaign/status",
                        cache: false,
                        success: function(result){
                            var models = result.models
                            for (var i = 0; i < models.length; i++) {
                                if(statusProject[models[i].id_project] == undefined){
                                    statusProject[models[i].id_project] = "";
                                }
                                if(models[i].status=='1' && statusProject[models[i].id_project]=='3'){
                                    $('#progress-' + models[i].id_project).html('<span style="color: green;">Deployed<span>');
                                    var notice = new PNotify({
                                        title: 'Notification',
                                        text: 'This project ' + models[i].id_project + ' status is complete',
                                        type: 'success'
                                    });
                                }
                                statusProject[models[i].id_project] = models[i].status;
                                $('#progress-bar-' + models[i].id_project).css('width', models[i].sum + '%');
                                $('#progress-bar-' + models[i].id_project).attr('aria-valuenow', models[i].sum);
                                $('#progress-bar-' + models[i].id_project).text(models[i].sum + '%');
                            }
                        }
                    });
                }, 120000);   
            }

            checkStatusProject();
        });