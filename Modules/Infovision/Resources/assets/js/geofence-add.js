$(document).ready(function(){        
    var upLabelPolygon = [];
    var upObjLabel = new Object();        
    var idGeofence = $('#idGeofence').val();

    $("#saveGeofence").click(function(e) {
        var form_data = new FormData();
        var name = $('#nameGeofence').val();
        form_data.append('idGeofence', idGeofence);
        form_data.append('name', name);
        var data = new Array();
        var id = $('.getDataOriPolygon').text().split(/\s+/);
        for (let i = 0; i < id.length; i++) { 
            if(id[i]!==''){
                form_data.append('data[' + id[i] + '][polygon]', $('li.' + id[i]).text().split(/\s+/).join('|') + ';');
                if(upObjLabel[id[i]]!=='undefined'){
                    form_data.append('data[' + id[i] + '][label]', upObjLabel[id[i]]);
                }else{
                    form_data.append('data[' + id[i] + '][label]', '');
                }
            }
        }
        $.ajax({
            url: $(this).attr('data-url'),
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                var data = JSON.parse(data);
                if($.isEmptyObject(data.error)){
                    window.location.href = '/admin/infovision/geofence?status=success';
                }else{
                    printErrorMsg(data.error);
                }
            }
        });
    });

    var map        = null;
    var polygon    = null;
    var draggable  = true;

    if(idGeofence==''||idGeofence==undefined){
        var totalLatLng = [];
        initialize();
    }else{
        var text = '';
        var totalLatLng = [];
        $.ajax({
            type: "GET",
            url: "/admin/infovision/geofence/" + idGeofence + "/show",
            cache: false,
            success: function(result){
                var json = result;
                var models = json['models'];
                var detail = json['detail'];
                $('#nameGeofence').val(models['name']);
                for(let a = 0; a < detail.length; a++){
                    text = '';
                    var split1fixs = detail[a]['latlong'].split('|');
                    for(let i = 0; i < split1fixs.length; i++){
                        var split2fixs = split1fixs[i].split(',');
                        text += '(' + split2fixs[0] + ', ' + split2fixs[1] + ')|'
                    }
                    totalLatLng.push(text);
                    if(detail[a]['label']!=='' && detail[a]['label']!==null && detail[a]['label']!=='undefined'){
                        upLabelPolygon.push(detail[a]['name_polygon']);
                        upObjLabel[detail[a]['name_polygon']] = detail[a]['label'];
                    }
                }
                initialize();
            }
        });
    }            
    
    function initialize() {
        map     = new GeofencingMap('map').map;
        polygon = new MultiPolygon(map, $('#polygon-name').val());

        if(totalLatLng.length > 0){
            for(var a = 0; a < totalLatLng.length; a++){
                var coords = new Array();
                var splitLatLng = totalLatLng[a].split("|");
                for(var i = 0; i < splitLatLng.length; i++){
                    var latlng = splitLatLng[i].trim().substring(1, splitLatLng[i].length-1).split(",");
                    if(latlng.length > 1){
                       coords.push(L.latLng(latlng[0], latlng[1]));
                    }
                    if(a==0 && i==0){
                        map.setView([latlng[0], latlng[1]], 6);
                    }
                }
                polygon.addPolygon(coords, null, null, true)
                polygon.setCreatePolygonsCallback(updateDetails);
                polygon.setAllowDragging(draggable)
                polygon.setEditable(true);
            }
        }else{
            var coords = new Array();
            polygon.addPolygon(coords, null, null, true)
            polygon.setCreatePolygonsCallback(updateDetails);
            polygon.setAllowDragging(draggable)
            polygon.setEditable(true);
        }
        
        // Update coordinates displayed on 'C' press
        updateCoords();
        /*$(window).keyup(function(e){
            if(e.keyCode == 67){
                updateCoords();
            }
        });*/

        $('#allow-dragging').click(function(){
            draggable = !draggable;
            polygon.setAllowDragging(draggable);
            if(draggable){
                $(this).html('Disable Dragging')
            }
            else{
                $(this).html('Enable Dragging')

            }
        });

        $('#new-polygon').click(function(){
            polygon.createNewPolygon();
            $('.geofencer-map').css('display', '');
        });

        $('#clear-all').click(function(){
            $('.coords').html('');
            polygon.deleteAllPolygons();
        });

        $('#update-polygon').click(function(){
            polygon.setName($('#polygon-name').val());
            polygon.panToPolygon();
        });

        map.on('dragend',function(e){
            $('.fa-caret-down').click();
        });

        map.on('fullscreenchange', function () {
            if (map.isFullscreen()) {
                setTimeout(function() {
                    var check = $('.geofencer-map').css('cursor');
                    if(check=='' || check=='grab'){
                        $('.geofencer-map').css('display', 'contents');
                    }
                    $('.header').css('display', 'none');
                    $('.sidebar-left').css('display', 'none');
                    $('.page-header').css('display', 'none');
                }, 500);
            } else {
                $('.geofencer-map').css('display', '');
                $('.header').css('display', '');
                $('.sidebar-left').css('display', '');
                $('.page-header').css('display', '');
            }
        });
    }

    function updateDetails(p){
        updateCoords();
    }

    function updateCoords(e){
        var polys = polygon.getPolygons();
        var multi_coords = polygon.getPolygonCoordinates();
        $('.coords').empty();
        var nomulticoords = 1;
        var sum = multi_coords.length;
        for (var j in multi_coords){
            if(nomulticoords==sum){
                var name = $('<h3 class="getDataPolygon ' + polys[j].name + '"><span class="getDataOriPolygon">' + polys[j].name + '</span><a href="javascript:void(0)" id="' + polys[j].name + '">' + polys[j].name + '</a> <a href="#modalEditLabel" class="modal-sizes showLabelPolygon" data-a="' + polys[j].name + '" style="margin-left: 10px;"><i class="fa fa-pencil"></i></a></h3>');
            }else{
                var name = $('<h3 class="getDataPolygon ' + polys[j].name + '"><span class="getDataOriPolygon">' + polys[j].name + ' </span><a href="javascript:void(0)" id="' + polys[j].name + '">' + polys[j].name + '</a> <a href="#modalEditLabel" class="modal-sizes showLabelPolygon" data-a="' + polys[j].name + '" style="margin-left: 10px;"><i class="fa fa-pencil"></i></a></h3>');
            }
            if(polys[j].selfIntersects()){
                name.css('color', 'red')
            }

            $('.coords').append(name);
            var coords = multi_coords[j];
            var nocoords = 1;
            var sumcoords = coords.length; 
            for (var i in coords){
                if(nocoords==sumcoords){
                    var c = $('<li class="' + polys[j].name + '">').html(coords[i].lat + ", " + coords[i].lng);
                }else{
                    var c = $('<li class="' + polys[j].name + '">').html(coords[i].lat + ", " + coords[i].lng + '  ');
                }
                $('.coords').append(c);
                nocoords++;
            }
            $('.coords').append('<hr class="' + polys[j].name + '">');
            nomulticoords++;
        }
        $('.modal-sizes').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,   
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            modal: true
        });

        $('.showLabelPolygon').click(function(){
            var values = $(this).attr('data-a');
            var text = $('#' + values).html();
            $('.idPolygon').val(values);
            $('.labelPolygon').val(text);
        });

        if(upLabelPolygon.length > 0){
            for(var i = 0; i < upLabelPolygon.length; i++){
                var element = upLabelPolygon[i];
                $('#' + element).html(upObjLabel[element]);
            }
        }
    }

    $("#saveLabelPolygonData").click(function(e) {
        var idPolygon = $('.idPolygon').val();
        var values = $('.labelPolygon').val();
        upLabelPolygon.push(idPolygon);
        upObjLabel[idPolygon] = values;
        e.preventDefault();
        $.magnificPopup.close();
        $('#' + idPolygon).html(values);
    });
});