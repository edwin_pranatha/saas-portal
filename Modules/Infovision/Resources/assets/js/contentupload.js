/*
 * jQuery File Upload Demo
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $ */
$(document).ready(function () {
  $('.modal-with-zoom-anim').magnificPopup({
    type: 'inline',
    fixedContentPos: false,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-zoom-in',
    modal: true
  });

  $('.modal-sizes').magnificPopup({
    type: 'inline',
    fixedContentPos: false,
    fixedBgPos: true,
    overflowY: 'auto',
    closeBtnInside: true,
    preloader: false,
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-zoom-in',
    modal: true
  });

  $(document).on('click', '.modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
  });

  $('select.populate').select2();

  $('.zoom-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    closeOnContentClick: false,
    closeBtnInside: false,
    mainClass: 'mfp-with-zoom mfp-img-mobile',
    image: {
      verticalFit: true,
      titleSrc: function (item) {
        return item.el.attr('title') + ' &middot;';
      }
    },
    gallery: {
      enabled: true
    },
    zoom: {
      enabled: true,
      duration: 300,
      opener: function (element) {
        return element.find('img');
      }
    }
  });

  $('.zoom-video').magnificPopup({
    delegate: 'a',
    type: 'iframe',
    closeOnContentClick: false,
    closeBtnInside: false,
    mainClass: 'mfp-with-zoom mfp-img-mobile',
    image: {
      verticalFit: true,
      titleSrc: function (item) {
        return item.el.attr('title') + ' &middot;';
      }
    },
    gallery: {
      enabled: true
    },
    zoom: {
      enabled: true,
      duration: 300,
      opener: function (element) {
        return element.find('data-media');
      }
    }
  });



  function showListContentUpload() {
    $('#loadContentUpload').css('display', 'inline-block');
    var urlCurrent = window.location.origin;
    $.ajax({
      type: "POST",
      url: $('[name=urlinfo]').attr('data-url'),
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
      },
      cache: false,
      success: function (result) {
        $('#loadContentUpload').css('display', 'none');
        var json = JSON.parse(result);
        var statusImg = 0;
        var statusVid = 0;
        var url_thumbnail = '';
        var url_media = '';
        for (var i = 0; i < json.length; i++) {
          if (json[i]['type'] !== 'mp4') {
            url_thumbnail = urlCurrent + '/admin/infovision/image/thumbnail/' + json[i]['name'];
            url_media = urlCurrent + '/admin/infovision/image/' + json[i]['name'];
          } else {
            var file_name = json[i]['name'];
              var video_ext = ['mp4','mov','m4v','avi','wmv','flv','3gp','mkv'];
              video_ext.forEach(x => {
                file_name = file_name.replace(x, 'jpg');
              });
            url_thumbnail = urlCurrent + '/admin/infovision/video/thumbnail/' + file_name;
            url_media = urlCurrent + '/admin/infovision/video/' + json[i]['name'];
          }
          var html = '<div class="isotope-item document col-xs-6 col-custom-01">';
          html += '<div class="thumbnail">';
          html += '<div class="thumb-preview">';
          if (json[i]['type'] !== 'mp4') {
            html += '<a class="thumb-image" href="' + url_media + '" title="' + json[i]['name'] + '">';
            html += '<img src="' + url_thumbnail + '" data-media="' + url_media + '" class="img-responsive" alt="Project" style="height: 68px;width: 116px;">';
            html += '</a>';
          } else {
            html += '<a class="thumb-video" href="' + url_media + '" title="' + json[i]['name'] + '">';
            html += '<img src="' + url_thumbnail + '" data-media="' + url_media + '" class="img-responsive" alt="Project" style="width: 118px;height: 74px;">';
            html += '</a>';
          }
          html += '</div>';
          html += '<h5 class="mg-title text-semibold">' + json[i]['name'] + '</h5>';
          html += '</div>';
          html += '</div>';
          if (json[i]['type'] !== 'mp4') {
            $('#popular11').append(html);
            statusImg = 1;
          } else {
            $('#recent11').append(html);
            statusVid = 1;
          }
        }
        if (statusImg === 0) {
          $('#popular11').append('<p>No data images</p>');
        }
        if (statusVid === 0) {
          $('#recent11').append('<p>No data video</p>');
        }
      }
    });
  }

  showListContentUpload();

});

$(function () {
  'use strict';

  // Initialize the jQuery File Upload widget:
  $('#fileupload').fileupload({
    // Uncomment the following to send cross-domain cookies:
    //xhrFields: {withCredentials: true},
    url: 'content-upload',
    headers: {
      'X-CSRF-TOKEN': $('#token').val()
    }
  });

  // Enable iframe cross-domain access via redirect option:
  $('#fileupload').fileupload(
    'option',
    'redirect',
    window.location.href.replace(/\/[^/]*$/, '/cors/result.html?%s')
  );

  if (window.location.hostname === 'blueimp.github.io') {
    // Demo settings:
    $('#fileupload').fileupload('option', {
      url: '//jquery-file-upload.appspot.com/',
      // Enable image resizing, except for Android and Opera,
      // which actually support image resizing, but fail to
      // send Blob objects via XHR requests:
      disableImageResize: /Android(?!.*Chrome)|Opera/.test(
        window.navigator.userAgent
      ),
      maxFileSize: 999000,
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
    });
    // Upload server status check for browsers with CORS support:
    if ($.support.cors) {
      $.ajax({
        url: '//jquery-file-upload.appspot.com/',
        type: 'HEAD'
      }).fail(function () {
        $('<div class="alert alert-danger"></div>')
          .text('Upload server currently unavailable - ' + new Date())
          .appendTo('#fileupload');
      });
    }
  } else {
    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    var loc = window.location;
    if (loc.pathname == '/admin/infovision/content-upload') {
      $.ajax({
          // Uncomment the following to send cross-domain cookies:
          //xhrFields: {withCredentials: true},
          url: $('#fileupload').fileupload('option', 'url'),
          dataType: 'json',
          context: $('#fileupload')[0]
        })
        .always(function () {
          $(this).removeClass('fileupload-processing');
        })
        .done(function (result) {
          $(this)
            .fileupload('option', 'done')
            // eslint-disable-next-line new-cap
            .call(this, $.Event('done'), {
              result: result
            });
        });
    }
  }
});
