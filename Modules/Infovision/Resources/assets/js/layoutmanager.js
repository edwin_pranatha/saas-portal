$(document).on('change', '.groupStop', function () {
    if ($(this).prop('checked') == true) {
        var values = $(this).next('.multiselect-group');
        values.next('input').prop('checked', true);
        values.next('input').next('span').next('input').prop('checked', true);
    } else {
        var values = $(this).next('.multiselect-group');
        values.next('input').prop('checked', false);
        values.next('input').next('span').next('input').prop('checked', false);
    }
});

$(document).on('change', "[value='multiselect-all']", function () {
    if ($(this).prop('checked') == true) {
        $('.groupStop').prop('checked', true);
        var values = $('.groupStop').next('.multiselect-group');
        values.next('input').prop('checked', true);
        values.next('input').next('span').next('input').prop('checked', true);
    } else {
        $('.groupStop').prop('checked', false);
        var values = $('.groupStop').next('.multiselect-group');
        values.next('input').prop('checked', false);
        values.next('input').next('span').next('input').prop('checked', false);
    }
});


$(document).ready(function () {
    var openconfiglayout = 0;
    var countobj = 0;
    var setSlideImage = '';
    var nomorslide = 0;
    var loc = window.location;
    var splitsearch = loc.search.split('=');
    var panelpoint = "";
    var align = "";
    var sCanvas = "";
    var oCanvas = "";
    var widnm = "";
    var srcval = "";
    var canvas = "";

    $('#timestartobject').timepicker({ showMeridian:false, minuteStep: 1 }).on('changeTime.timepicker', function (e) {
        var hours = e.time.hours,
            min = e.time.minutes;
        if (hours < 10) {
            hours = '0' + hours;
        }
        if (min < 10) {
            min = '0' + min;
        }
        $('#timestartobject').val(hours + ':' + min);
    });

    $('#timeendobject').timepicker( { showMeridian:false, minuteStep: 1 }).on('changeTime.timepicker', function (e) {
        var hours = e.time.hours,
            min = e.time.minutes;
        if (hours < 10) {
            hours = '0' + hours;
        }
        if (min < 10) {
            min = '0' + min;
        }
        $('#timeendobject').val(hours + ':' + min);
    });


    $('.modal-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $('.modal-sizes').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $(document).on('click', '.modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    $('select.populate').select2();

    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function (item) {
                return item.el.attr('title') + ' &middot; <a class="image-source-link" href="' + item.el.attr('data-source') + '" target="_blank">image source</a>';
            }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            opener: function (element) {
                return element.find('img');
            }
        }
    });

    var ctx = "";
    if (loc.pathname == '/admin/infovision/layout-create') {
        canvas = new fabric.Canvas('paper', {
            isDrawingMode: false,
            preserveObjectStacking: true
        });
        canvas.uniScaleTransform = true; //unlocks scale ratio //snapscaling //snapscale
        fabric.Object.prototype.transparentCorners = false; //baru
        ctx = canvas.getSelectionContext();
    } else {
        canvas = "";
    }


    /*canvas move*/
    var aligningLineOffset = 5,
        aligningLineMargin = 4,
        aligningLineWidth = 1,
        aligningLineColor = 'rgb(255,0,188)',
        viewportTransform,
        zoom = 1;

    var grid = 10;
    var scrollleft = 0;
    var scrolltop = 0;

    var thisobjleft = 0;
    var thisobjtop = 0;
    var thisobjwidth = 0;
    var thisobjheight = 0;

    function drawVerticalLine(coords) {
        drawLine(
            coords.x + 0.5,
            coords.y1 > coords.y2 ? coords.y2 : coords.y1,
            coords.x + 0.5,
            coords.y2 > coords.y1 ? coords.y2 : coords.y1);
    }

    function drawHorizontalLine(coords) {
        drawLine(
            coords.x1 > coords.x2 ? coords.x2 : coords.x1,
            coords.y + 0.5,
            coords.x2 > coords.x1 ? coords.x2 : coords.x1,
            coords.y + 0.5);
    }

    function drawLine(x1, y1, x2, y2) {
        ctx.save();
        ctx.lineWidth = aligningLineWidth;
        ctx.strokeStyle = aligningLineColor;
        ctx.beginPath();
        ctx.moveTo(((x1 + viewportTransform[4]) * zoom), ((y1 + viewportTransform[5]) * zoom));
        ctx.lineTo(((x2 + viewportTransform[4]) * zoom), ((y2 + viewportTransform[5]) * zoom));
        ctx.stroke();
        ctx.restore();
    }

    function isInRange(value1, value2) {
        value1 = Math.round(value1);
        value2 = Math.round(value2);
        for (var i = value1 - aligningLineMargin, len = value1 + aligningLineMargin; i <= len; i++) {
            if (i === value2) {
                return true;
            }
        }
        return false;
    }
    var verticalLines = [],
        horizontalLines = [];



    var LabeledRect = fabric.util.createClass(fabric.Rect, {
        type: 'labeledRect',
        initialize: function (options) {
            options || (options = {});
            this.callSuper('initialize', options);
            this.set('label', options.label || ''),
                this.set('getWidth', options.getWidth || ''),
                this.set('getHeight', options.getHeight || ''),
                this.set('getLeft', options.getLeft || ''),
                this.set('getTop', options.getTop || ''),
                this.set('getType', options.getType || '');
        },

        toObject: function () {
            return fabric.util.object.extend(this.callSuper('toObject'), {
                label: this.get('label'),
                getWidth: this.get('getWidth'),
                getHeight: this.get('getHeight'),
                getLeft: this.get('getLeft'),
                getTop: this.get('getTop'),
                getType: this.get('getType')
            });
        },

        _render: function (ctx) {
            this.callSuper('_render', ctx);
            ctx.font = '12px "Open Sans", Arial, sans-serif';
            ctx.fillStyle = '#FFF';
            ctx.fillText(this.label, -this.width / 2 + 180, -this.height / 2 + 20);
            ctx.fillText(this.getWidth, -this.width / 2 + 180, -this.height / 2 + 40);
            ctx.fillText(this.getHeight, -this.width / 2 + 180, -this.height / 2 + 60);
            ctx.fillText(this.getLeft, -this.width / 2 + 180, -this.height / 2 + 80);
            ctx.fillText(this.getTop, -this.width / 2 + 180, -this.height / 2 + 100);
            ctx.fillText(this.getType, -this.width / 2 + 180, -this.height / 2 + 120);
        }
    });

    $('.modal-with-move-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    if (loc.search == '') {
        $('.modalLayoutCreate').click();
    } else {
        if (loc.pathname == '/admin/infovision/layout-create') {
            showEditCanvas();
        }
    }

    function showEditCanvas() {
        var id = splitsearch[1];
        $.ajax({
            type: "GET",
            url: "/admin/infovision/campaign/prev/layout/manager",
            data: {
                id: id
            },
            cache: false,
            success: function (result) {
                var data = result;
                resultShowEditCanvas(data);
            }
        });
    }

    function resultShowEditCanvas(data) {
        countobj = 0;
        var models = data.models;
        var detail = data.detail;
        var playlist = data.playlist;
        var bgid = models.bg_layout;
        if (bgid == 'BG001') {
            var width = 1366;
            var height = 768;
        } else if (bgid == 'BG002') {
            var width = 1920;
            var height = 540;
        } else if (bgid == 'BG003') {
            var width = 1024;
            var height = 768;
        } else if (bgid == 'BG004') {
            var width = 1920;
            var height = 1080;
        }
        $('#idbg').val(bgid);
        $('#idlayout').val(models.id_layout);
        $('#nmlayout').val(models.nm_layout);
        $('#urlparam').val(splitsearch[1]);

        var screen = width + 'x' + height;
        var imgScreen = '/assets/Infovision/assets/images/canvas_' + screen + '.png';
        canvas.setDimensions({
            width: width,
            height: height
        });
        canvas.setBackgroundImage(
            imgScreen,
            function () {
                canvas.renderAll();
            }, {
                width: canvas.width,
                height: canvas.height,
                originX: 'left',
                originY: 'top'
            }
        );

        for (var i = 0; i < detail.length; i++) {
            if (detail[i].objtype == 'labeledRect') {
                var labeledRect = new LabeledRect({
                    width: parseInt(detail[i].width),
                    height: parseInt(detail[i].height),
                    left: parseInt(detail[i].left),
                    top: parseInt(detail[i].top),
                    objtype: detail[i].objtype,
                    objstyle: '',
                    name: detail[i].name,
                    label: 'Name : ' + detail[i].name,
                    src_val: 'none',
                    src_type: detail[i].objtype,
                    getWidth: 'Width : ' + detail[i].width,
                    getHeight: 'Height : ' + detail[i].height,
                    getLeft: 'Left : 0',
                    getTop: 'Top : 0',
                    getType: 'Type : api_route_cnxx',
                    fill: '#7c7779',
                    evented: 1,
                    selectable: 0,
                    hasBorders: 1,
                    hasControls: 1,
                    lockScalingFlip: true,
                    hasRotatingPoint: false,
                    objstyle: '',
                    plylstisdb: '0',
                    plylstid: '',
                    plylstnm: '',
                    plylstdata: ''
                });

                canvas.add(labeledRect);
                countobj++;
            } else {
                var bgcolor = 'rgb(' + r() + "," + r() + "," + r() + ')';
                var rectangle = new fabric.Rect({
                    width: parseInt(detail[i].width),
                    height: parseInt(detail[i].height),
                    left: parseInt(detail[i].left),
                    top: parseInt(detail[i].top),
                    name: detail[i].name,
                    objtype: detail[i].objtype,
                    objstyle: '',
                    fill: detail[i].fill,
                    src_val: detail[i].src_val,
                    src_type: detail[i].objtype,
                    zIndex: countobj,
                    evented: detail[i].evented,
                    selectable: detail[i].selectable,
                    hasBorders: detail[i].hasBorders,
                    hasControls: detail[i].hasControls,
                    lockScalingFlip: true,
                    hasRotatingPoint: false,
                    objstyle: '',
                    plylstisdb: '0',
                    plylstid: '',
                    plylstnm: '',
                    plylstdata: ''
                });

                $('<span class="objtxt">').attr({
                    name: detail[i].name,
                    value: detail[i].name,
                    id: detail[i].name,
                    style: 'position: absolute;pointer-events: none;'
                }).appendTo("#canvas-container");

                var btn = document.getElementById(detail[i].name);
                btn.innerHTML = 'Name : ' + detail[i].name + '<br>Width : ' + detail[i].width + '<br>Height : ' + detail[i].height + '<br>Left : ' + detail[i].left + '<br>Top : ' + detail[i].top + '<br>Type : ' + detail[i].objtype;
                btn.style.left = (parseInt(detail[i].left) + 130) + 'px';
                btn.style.top = parseInt(detail[i].top) + 'px';
                btn.style.fontSize = '12px';
                btn.style.color = '#FFF';

                canvas.add(rectangle);

                saveTempImg[detail[i].name] = new Array();
                saveTempImg[detail[i].name]['result'] = detail[i].src_val;
                saveTempImg[detail[i].name]['type'] = detail[i].objtype;

                countobj++;
                if (detail[i].objtype == 'slide') {
                    var getObjectCanvas = canvas.getObjects();
                    getObjectCanvas.forEach(function (object) {
                        if (object.name == detail[i].name) {
                            var dataplylt = [];
                            var nodataplylst = 0;
                            for (var a = 0; a < playlist.length; a++) {
                                if (playlist[a].obj_name == object.name) {
                                    var splitdate = playlist[a].date_play.split('-');
                                    var splittime = playlist[a].time_play.split('-');

                                    var dataconfigobj = [];
                                    dataconfigobj['title'] = playlist[a].obj_name;
                                    dataconfigobj['ttlobject'] = playlist[a].ttl;
                                    dataconfigobj['dayobject'] = playlist[a].day_play;
                                    dataconfigobj['datestartobject'] = splitdate[0];
                                    dataconfigobj['dateendobject'] = splitdate[1];
                                    dataconfigobj['timestartobject'] = splittime[0];
                                    dataconfigobj['timeendobject'] = splittime[1];
                                    dataconfigobj['priorityobject'] = playlist[a].priority;
                                    dataconfigobj['videotimeobject'] = playlist[a].isvideotime;
                                    dataconfigobj['locationtriggerobject'] = playlist[a].trigger;
                                    dataconfigobj['typelocation'] = playlist[a].typelocation;
                                    dataconfigobj['lineobject'] = playlist[a].line;
                                    dataconfigobj['optionsRadiosLine'] = '';
                                    dataconfigobj['stopobject'] = playlist[a].stop;
                                    dataconfigobj['geofence'] = playlist[a].geofence;
                                    dataconfigobj['mediavalues'] = playlist[a].src_val;
                                    dataconfigobj['mediatype'] = '';
                                    datapush.push(dataconfigobj);


                                    dataplylt.push({
                                        title: playlist[a].obj_name
                                    });
                                    dataplylt[nodataplylst]['ttlobject'] = playlist[a].ttl;
                                    dataplylt[nodataplylst]['dayobject'] = playlist[a].day_play;
                                    dataplylt[nodataplylst]['datestartobject'] = splitdate[0];
                                    dataplylt[nodataplylst]['dateendobject'] = splitdate[1];
                                    dataplylt[nodataplylst]['timestartobject'] = splittime[0];
                                    dataplylt[nodataplylst]['timeendobject'] = splittime[1];
                                    dataplylt[nodataplylst]['priorityobject'] = playlist[a].priority;
                                    dataplylt[nodataplylst]['videotimeobject'] = playlist[a].isvideotime;
                                    dataplylt[nodataplylst]['locationtriggerobject'] = playlist[a].trigger;
                                    dataplylt[nodataplylst]['typelocation'] = playlist[a].typelocation;
                                    dataplylt[nodataplylst]['lineobject'] = playlist[a].line;
                                    dataplylt[nodataplylst]['optionsRadiosLine'] = '';
                                    dataplylt[nodataplylst]['stopobject'] = playlist[a].stop;
                                    dataplylt[nodataplylst]['geofence'] = playlist[a].geofence;
                                    dataplylt[nodataplylst]['mediavalues'] = playlist[a].src_val;
                                    dataplylt[nodataplylst]['mediatype'] = '';
                                    nodataplylst++;
                                }
                            }
                            object.plylstid = detail[i].plylstid;
                            object.plylstnm = detail[i].plylstnm;
                            object.plylstdata = dataplylt;
                            canvas.renderAll();
                        }
                    });
                }
            }
        }
    }

    $(".h-imgRes").click(function () {
        $('.select_screen').removeClass('select_screen');
        $(this).addClass('select_screen');
    });

    $("#chooseScreen").click(function (e) {
        countobj = 0;
        var onCanvas = canvas.getObjects();
        canvas.discardActiveGroup();
        onCanvas.forEach(function (object) {
            canvas.remove(object);
            removeteks(object.name);
        });
        var screen = $('.select_screen').attr('id');
        if (screen == '1366x768') {
            var bgid = 'BG001';
        } else if (screen == '1920x540') {
            var bgid = 'BG002';
        } else if (screen == '1024x768') {
            var bgid = 'BG003';
        } else if (screen == '1920x1080') {
            var bgid = 'BG004';
        }
        $('#idbg').val(bgid);
        var split = screen.split('x');
        var width = parseInt(split[0]);
        var height = parseInt(split[1]);
        var imgScreen = '/assets/Infovision/assets/images/canvas_' + screen + '.png';
        canvas.setDimensions({
            width: width,
            height: height
        });
        canvas.setBackgroundImage(
            imgScreen,
            function () {
                canvas.renderAll();
            }, {
                width: canvas.width,
                height: canvas.height,
                originX: 'left',
                originY: 'top'
            }
        );

        // if (width == 1920) {
        //     var labeledRect = new LabeledRect({
        //         width: 960,
        //         height: height,
        //         left: 0,
        //         top: 0,
        //         objtype: 'labeledRect',
        //         objstyle: '',
        //         name: 'label_' + countobj,
        //         label: 'Name : label_' + countobj,
        //         src_val: 'none',
        //         src_type: 'labeledRect',
        //         getWidth: 'Width : 960',
        //         getHeight: 'Height : 540',
        //         getLeft: 'Left : 0',
        //         getTop: 'Top : 0',
        //         getType: 'Type : api_route_cnxx',
        //         fill: '#7c7779',
        //         evented: 1,
        //         selectable: 0,
        //         hasBorders: 1,
        //         hasControls: 1,
        //         lockScalingFlip: true,
        //         hasRotatingPoint: false,
        //         objstyle: '',
        //         plylstisdb: '0',
        //         plylstid: '',
        //         plylstnm: '',
        //         plylstdata: ''
        //     });
        //     canvas.add(labeledRect);
        // }

        countobj++;
        if (openconfiglayout == 0) {
            e.preventDefault();
            $.magnificPopup.close();
        } else {
            $('.layoutConfig').click();
            openconfiglayout = 0;
        }
    });

    var panel;

    var contentpanel = '';

    function r() {
        return Math.floor(Math.random() * 255)
    }

    $("#addImageScreen").click(function (e) {
        var bgcolor = 'rgb(' + r() + "," + r() + "," + r() + ')';
        var rectangle = new fabric.Rect({
            width: 250,
            height: 150,
            left: 350,
            top: 10,
            name: 'label_' + countobj,
            objtype: 'image',
            objstyle: '',
            fill: bgcolor,
            src_val: 'none',
            src_type: 'image',
            zIndex: countobj,
            evented: 1,
            selectable: 1,
            hasBorders: 1,
            hasControls: 1,
            lockScalingFlip: true,
            hasRotatingPoint: false,
            objstyle: '',
            plylstisdb: '0',
            plylstid: '',
            plylstnm: '',
            plylstdata: ''
        });

        $('<span class="objtxt">').attr({
            name: 'label_' + countobj,
            value: 'label_' + countobj,
            id: 'label_' + countobj,
            style: 'position: absolute;pointer-events: none;'
        }).appendTo("#canvas-container");

        var btn = document.getElementById('label_' + countobj);
        btn.innerHTML = 'Name : label_' + countobj + '<br>Width : 250<br>Height : 150<br>Left : 350<br>Top : 10<br>Type : image';
        btn.style.left = (350 + 130) + 'px';
        btn.style.top = 10 + 'px';
        btn.style.fontSize = '12px';
        btn.style.color = '#FFF';

        canvas.add(rectangle);
        $('[data-toggle="dropdown"]').parent().removeClass('open');

        countobj++;
    });

    $("#addVideoScreen").click(function (e) {
        var bgcolor = 'rgb(' + r() + "," + r() + "," + r() + ')';
        var rectangle = new fabric.Rect({
            width: 250,
            height: 150,
            left: 350,
            top: 10,
            name: 'label_' + countobj,
            objtype: 'movie',
            objstyle: '',
            fill: bgcolor,
            src_val: 'none',
            src_type: 'movie',
            zIndex: countobj,
            evented: 1,
            selectable: 1,
            hasBorders: 1,
            hasControls: 1,
            lockScalingFlip: true,
            hasRotatingPoint: false,
            objstyle: '',
            plylstisdb: '0',
            plylstid: '',
            plylstnm: '',
            plylstdata: ''
        });

        $('<span class="objtxt">').attr({
            name: 'label_' + countobj,
            value: 'label_' + countobj,
            id: 'label_' + countobj,
            style: 'position: absolute;pointer-events: none;'
        }).appendTo("#canvas-container");

        var btn = document.getElementById('label_' + countobj);
        btn.innerHTML = 'Name : label_' + countobj + '<br>Width : 250<br>Height : 150<br>Left : 350<br>Top : 10<br>Type : movie';
        btn.style.left = (350 + 130) + 'px';
        btn.style.top = 10 + 'px';
        btn.style.fontSize = '12px';
        btn.style.color = '#FFF';

        canvas.add(rectangle);
        $('[data-toggle="dropdown"]').parent().removeClass('open');

        countobj++;
    });

    $("#addTextScreen").click(function (e) {
        var bgcolor = 'rgb(' + r() + "," + r() + "," + r() + ')';
        var rectangle = new fabric.Rect({
            width: 250,
            height: 150,
            left: 350,
            top: 10,
            name: 'label_' + countobj,
            objtype: 'text',
            objstyle: '',
            fill: bgcolor,
            src_val: '',
            src_type: 'text',
            zIndex: countobj,
            evented: 1,
            selectable: 1,
            hasBorders: 1,
            hasControls: 1,
            lockScalingFlip: true,
            hasRotatingPoint: false,
            objstyle: '',
            plylstisdb: '0',
            plylstid: '',
            plylstnm: '',
            plylstdata: ''
        });

        $('<span class="objtxt">').attr({
            name: 'label_' + countobj,
            value: 'label_' + countobj,
            id: 'label_' + countobj,
            style: 'position: absolute;pointer-events: none;'
        }).appendTo("#canvas-container");

        var btn = document.getElementById('label_' + countobj);
        btn.innerHTML = 'Name : label_' + countobj + '<br>Width : 250<br>Height : 150<br>Left : 350<br>Top : 10<br>Type : text';
        btn.style.left = (350 + 130) + 'px';
        btn.style.top = 10 + 'px';
        btn.style.fontSize = '12px';
        btn.style.color = '#FFF';

        canvas.add(rectangle);
        $('[data-toggle="dropdown"]').parent().removeClass('open');

        countobj++;
    });

    $("#addTickerScreen").click(function (e) {
        var bgcolor = 'rgb(' + r() + "," + r() + "," + r() + ')';
        var rectangle = new fabric.Rect({
            width: 250,
            height: 150,
            left: 350,
            top: 10,
            name: 'label_' + countobj,
            objtype: 'ticker',
            objstyle: '',
            fill: bgcolor,
            src_val: '',
            src_type: 'ticker',
            zIndex: countobj,
            evented: 1,
            selectable: 1,
            hasBorders: 1,
            hasControls: 1,
            lockScalingFlip: true,
            hasRotatingPoint: false,
            objstyle: '',
            plylstisdb: '0',
            plylstid: '',
            plylstnm: '',
            plylstdata: ''
        });

        $('<span class="objtxt">').attr({
            name: 'label_' + countobj,
            value: 'label_' + countobj,
            id: 'label_' + countobj,
            style: 'position: absolute;pointer-events: none;'
        }).appendTo("#canvas-container");

        var btn = document.getElementById('label_' + countobj);
        btn.innerHTML = 'Name : label_' + countobj + '<br>Width : 250<br>Height : 150<br>Left : 350<br>Top : 10<br>Type : ticker';
        btn.style.left = (350 + 130) + 'px';
        btn.style.top = 10 + 'px';
        btn.style.fontSize = '12px';
        btn.style.color = '#FFF';

        canvas.add(rectangle);
        $('[data-toggle="dropdown"]').parent().removeClass('open');

        countobj++;
    });

    $("#addUrlScreen").click(function (e) {
        var bgcolor = 'rgb(' + r() + "," + r() + "," + r() + ')';
        var rectangle = new fabric.Rect({
            width: 250,
            height: 150,
            left: 350,
            top: 10,
            name: 'label_' + countobj,
            objtype: 'url',
            objstyle: '',
            fill: bgcolor,
            src_val: '',
            src_type: 'url',
            zIndex: countobj,
            evented: 1,
            selectable: 1,
            hasBorders: 1,
            hasControls: 1,
            lockScalingFlip: true,
            hasRotatingPoint: false,
            objstyle: '',
            plylstisdb: '0',
            plylstid: '',
            plylstnm: '',
            plylstdata: ''
        });

        $('<span class="objtxt">').attr({
            name: 'label_' + countobj,
            value: 'label_' + countobj,
            id: 'label_' + countobj,
            style: 'position: absolute;pointer-events: none;'
        }).appendTo("#canvas-container");

        var btn = document.getElementById('label_' + countobj);
        btn.innerHTML = 'Name : label_' + countobj + '<br>Width : 250<br>Height : 150<br>Left : 350<br>Top : 10<br>Type : ticker';
        btn.style.left = (350 + 130) + 'px';
        btn.style.top = 10 + 'px';
        btn.style.fontSize = '12px';
        btn.style.color = '#FFF';

        canvas.add(rectangle);
        $('[data-toggle="dropdown"]').parent().removeClass('open');

        countobj++;
    });

    $("#addSlideScreen").click(function (e) {
        var bgcolor = 'rgb(' + r() + "," + r() + "," + r() + ')';
        var rectangle = new fabric.Rect({
            width: 250,
            height: 150,
            left: 350,
            top: 10,
            name: 'label_' + countobj,
            objtype: 'slide',
            objstyle: '',
            fill: bgcolor,
            src_val: '',
            src_type: 'slide',
            zIndex: countobj,
            evented: 1,
            selectable: 1,
            hasBorders: 1,
            hasControls: 1,
            lockScalingFlip: true,
            hasRotatingPoint: false,
            objstyle: '',
            plylstisdb: '0',
            plylstid: '',
            plylstnm: '',
            plylstdata: ''
        });

        $('<span class="objtxt">').attr({
            name: 'label_' + countobj,
            value: 'label_' + countobj,
            id: 'label_' + countobj,
            style: 'position: absolute;pointer-events: none;'
        }).appendTo("#canvas-container");

        var btn = document.getElementById('label_' + countobj);
        btn.innerHTML = 'Name : label_' + countobj + '<br>Width : 250<br>Height : 150<br>Left : 350<br>Top : 10<br>Type : slide';
        btn.style.left = (350 + 130) + 'px';
        btn.style.top = 10 + 'px';
        btn.style.fontSize = '12px';
        btn.style.color = '#FFF';

        canvas.add(rectangle);
        $('[data-toggle="dropdown"]').parent().removeClass('open');

        countobj++;
    });


    $("#addVIAheader").click(function (e) {
        var bgcolor = 'rgb(' + r() + "," + r() + "," + r() + ')';
        var rectangle = new fabric.Rect({
            width: 1920,
            height: 80,
            left: 0,
            top: 0,
            name: 'label_' + countobj,
            objtype: 'api',
            objstyle: '',
            fill: bgcolor,
            src_val: '',
            src_type: 'api_via_header',
            zIndex: countobj,
            evented: 1,
            selectable: 1,
            hasBorders: 1,
            hasControls: 1,
            lockScalingFlip: true,
            hasRotatingPoint: false,
            objstyle: '',
            plylstisdb: '0',
            plylstid: '',
            plylstnm: '',
            plylstdata: ''
        });

        $('<span class="objtxt">').attr({
            name: 'label_' + countobj,
            value: 'label_' + countobj,
            id: 'label_' + countobj,
            style: 'position: absolute;pointer-events: none;'
        }).appendTo("#canvas-container");

        var btn = document.getElementById('label_' + countobj);
        btn.innerHTML = 'Name : label_' + countobj + '<br>Width : 250<br>Height : 150<br>Left : 350<br>Top : 10<br>Type : api_via_header';
        btn.style.left = (350 + 130) + 'px';
        btn.style.top = 10 + 'px';
        btn.style.fontSize = '12px';
        btn.style.color = '#FFF';

        canvas.add(rectangle);
        $('[data-toggle="dropdown"]').parent().removeClass('open');

        countobj++;
    });

    $("#addVIArouteinfo").click(function (e) {
        var bgcolor = 'rgb(' + r() + "," + r() + "," + r() + ')';
        var rectangle = new fabric.Rect({
            width: 1440,
            height: 460,
            left: 0,
            top: 80,
            name: 'label_' + countobj,
            objtype: 'api',
            objstyle: '',
            fill: bgcolor,
            src_val: '',
            src_type: 'api_via_routeinfo',
            zIndex: countobj,
            evented: 1,
            selectable: 1,
            hasBorders: 1,
            hasControls: 1,
            lockScalingFlip: true,
            hasRotatingPoint: false,
            objstyle: '',
            plylstisdb: '0',
            plylstid: '',
            plylstnm: '',
            plylstdata: ''
        });

        $('<span class="objtxt">').attr({
            name: 'label_' + countobj,
            value: 'label_' + countobj,
            id: 'label_' + countobj,
            style: 'position: absolute;pointer-events: none;'
        }).appendTo("#canvas-container");

        var btn = document.getElementById('label_' + countobj);
        btn.innerHTML = 'Name : label_' + countobj + '<br>Width : 250<br>Height : 150<br>Left : 350<br>Top : 10<br>Type : api_via_routeinfo';
        btn.style.left = (350 + 130) + 'px';
        btn.style.top = 10 + 'px';
        btn.style.fontSize = '12px';
        btn.style.color = '#FFF';

        canvas.add(rectangle);
        $('[data-toggle="dropdown"]').parent().removeClass('open');

        countobj++;
    });


    var verchange = 0;
    var layConfig = [];
    $("#saveLayout").click(function (e) {
        if (loc.search.includes(".") == true) {
            var splitsama = loc.search.split('=');
            if (splitsama[1] !== '') {
                var splitdata = splitsama[1].split('.');
                var layval = splitdata[0];
                var projtxt = splitdata[1];
                var vertxt = splitdata[2];
                var newver = (parseInt(vertxt) + 1);
                swal({
                    title: "Save Confirmation",
                    text: "are you sure want to save this layout?, by saving the layout you will update your campaign layout and automatically directed to campaign page again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#337ab7",
                    confirmButtonText: "Yes, Sure!"
                }).then(result => {
                    if (result.value) {
                        var cleanstring = replaceAll(stringifyCanvas(canvas), ',"line"', '');
                        if (layConfig['mainbc'] == undefined) {
                            var fallbackdata = $('#textSelectFM').text() + '|' + $('#fallbackttl').val() + '|' + '#000000';
                            var screenpos = $('#screenalign').val();
                            var screenoffsetx = $('#screenoffX').val();
                            var usepft = $('input[name="usepft"]:checked').val();
                        } else {
                            if (layConfig['fallbackmt'] == 'Use Media') {
                                var fallbackdata = layConfig['textSelectFM'] + '|' + layConfig['fallbackttl'] + '|' + layConfig['mainbc'];
                            } else {
                                var fallbackdata = layConfig['fallbackmt'];
                            }
                            var screenpos = layConfig['screenalign'];
                            var screenoffsetx = layConfig['screenoffX'];
                            var usepft = layConfig['usepft'];
                        }
                        $.ajax({
                            url: $(this).attr('data-url'),
                            data: {
                                idlayout: layval + "." + projtxt + "." + newver,
                                idbg: $("#idbg").val(),
                                nmlayout: $('#nmlayout').val(),
                                datlayout: cleanstring,
                                canvasimg: canvas.toDataURL('png'),
                                fallbackdata: fallbackdata,
                                screenpos: screenpos,
                                screenoffsetx: screenoffsetx,
                                usepft: usepft,
                                isupdate: 0,
                                ishide: 1,
                                iscampaign: 1
                            },
                            type: "POST",
                            headers: {
                                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (result) {
                                localStorage.setItem('detectMyChange', Math.random());
                                localStorage.setItem('newlayoutver', layval + "." + projtxt + "." + newver);
                                localStorage.setItem('nmlayout', $('#nmlayout').val());
                                window.close();
                            }
                        });
                        return false;
                    }
                });

                return false;
            }
        }

        var cleanstring = replaceAll(stringifyCanvas(canvas), ',"line"', '');
        if ($("#nmlayout").val() === "") {
            swal({
                title: "Save Your Layout",
                text: "Please write name of your layout :",
                input: 'text',
                showCancelButton: true,
                animation: "slide-from-top",
                inputPlaceholder: "Please enter layout name",
                preConfirm: (isnull) => {
                    if (isnull == "") {
                        swal.showValidationError('You need to write the layout name!')
                    } else {
                        swal.resetValidationError();
                    }
                }
            }).then(result => {
                if (result.value) {
                    if (layConfig['mainbc'] == undefined) {
                        var fallbackdata = $('#textSelectFM').text() + '|' + $('#fallbackttl').val() + '|' + '#000000';
                        var screenpos = $('#screenalign').val();
                        var screenoffsetx = $('#screenoffX').val();
                        var usepft = $('input[name="usepft"]:checked').val();
                    } else {
                        if (layConfig['fallbackmt'] == 'Use Media') {
                            var fallbackdata = layConfig['textSelectFM'] + '|' + layConfig['fallbackttl'] + '|' + layConfig['mainbc'];
                        } else {
                            var fallbackdata = layConfig['fallbackmt'];
                        }
                        var screenpos = layConfig['screenalign'];
                        var screenoffsetx = layConfig['screenoffX'];
                        var usepft = layConfig['usepft'];
                    }
                    $.ajax({
                        url: $(this).attr('data-url'),
                        data: {
                            idlayout: $("#idlayout").val(),
                            idbg: $("#idbg").val(),
                            nmlayout: result.value,
                            datlayout: cleanstring,
                            canvasimg: canvas.toDataURL('png'),
                            fallbackdata: fallbackdata,
                            screenpos: screenpos,
                            screenoffsetx: screenoffsetx,
                            usepft: usepft,
                            isupdate: 0
                        },
                        type: "POST",
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            var json = data;
                            swal("Layout Saved!", "Your layout: " + result.value + " has been saved!", "success");
                            $('#idlayout').val(json.idlayout);
                            $('#nmlayout').val(json.nmlayout);
                        }
                    });
                }
            });
        } else {
            if (layConfig['mainbc'] == undefined) {
                var fallbackdata = $('#textSelectFM').text() + '|' + $('#fallbackttl').val() + '|' + '#000000';
                var screenpos = $('#screenalign').val();
                var screenoffsetx = $('#screenoffX').val();
                var usepft = $('input[name="usepft"]:checked').val();
            } else {
                if (layConfig['fallbackmt'] == 'Use Media') {
                    var fallbackdata = layConfig['textSelectFM'] + '|' + layConfig['fallbackttl'] + '|' + layConfig['mainbc'];
                } else {
                    var fallbackdata = layConfig['fallbackmt'];
                }
                var screenpos = layConfig['screenalign'];
                var screenoffsetx = layConfig['screenoffX'];
                var usepft = layConfig['usepft'];
            }
            $.ajax({
                url: $(this).attr('data-url'),
                data: {
                    idlayout: $("#idlayout").val(),
                    idbg: $("#idbg").val(),
                    nmlayout: $('#nmlayout').val(),
                    datlayout: cleanstring,
                    canvasimg: canvas.toDataURL('png'),
                    fallbackdata: fallbackdata,
                    screenpos: screenpos,
                    screenoffsetx: screenoffsetx,
                    usepft: usepft,
                    isupdate: 1
                },
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    var json = data;
                    swal("Layout Saved!", "Your layout: " + json.nmlayout + " has been saved!", "success");
                    $('#idlayout').val(json.idlayout);
                    $('#nmlayout').val(json.nmlayout);
                }
            });
        }

        return false;
    });

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    function stringifyCanvas(canvas) {
        var additionalFields = ['name', 'selectable', 'evented', 'hasBorders', 'hasControls', 'hasRotatingPoint', 'plylstid', 'plylstnm', 'plylstdata', 'objtype', 'objstyle', 'src_val', 'src_type'];
        sCanvas = JSON.stringify(canvas);
        oCanvas = JSON.parse(sCanvas);
        $.each(oCanvas.objects, function (n, object) {
            if (object.type == 'line') {
                oCanvas.objects[n] = "line";
            }
            $.each(additionalFields, function (m, field) {
                oCanvas.objects[n][field] = canvas.item(n)[field];
            });
        });

        return JSON.stringify(oCanvas);
    }

    $(".layoutConfig").click(function (e) {
        var width = canvas.width;
        var height = canvas.height;
        $('#sizeScreen').text(width + 'x' + height);
    });

    $("#saveLayoutConfig").click(function (e) {
        layConfig['mainbc'] = $('#mainbc').val();
        layConfig['fallbackmt'] = $('input[name="fallbackmt"]:checked').val();
        layConfig['textSelectFM'] = $('#textSelectFM').text();
        layConfig['fallbackttl'] = $('#fallbackttl').val();
        layConfig['screenalign'] = $('#screenalign').val();
        layConfig['screenoffX'] = $('#screenoffX').val();
        layConfig['usepft'] = "";
        $('input[name="usepft"]:checked').each(function () {
            layConfig['usepft'] = this.value;
        });
        e.preventDefault();
        $.magnificPopup.close();
    });

    $("#resizeScreen").click(function (e) {
        openconfiglayout = 1;
    });

    $("#actionFallbackMethod").click(function (e) {
        $('#optionsRadiosFM1').prop("checked", true);
        $('.listMovie').css('display', 'block');
        $('#selectMedia').css('display', 'inline-block');
        $('#selectImageLayout').css('display', 'none');
    });

    $("#selectMedia").click(function (e) {
        var imgname = $('.select_imageLayout').find('.mg-title').text();
        var split = imgname.split('.');
        if (split[1] == 'mp4') {
            var text = '[Video] ' + imgname;
        } else {
            var text = '[Image] ' + imgname;
        }
        $('#textSelectFM').text(text);
        $('#selectMedia').css('display', 'none');
        $('#selectImageLayout').css('display', 'inline-block');
        $('.listMovie').css('display', 'none');
        $('.layoutConfig').click();
    });

    $("#prevDisplayScreen").click(function (e) {
        parseLayoutPreview();
    });


    function parseLayoutPreview() {
        var oCanvas = canvas.getObjects();
        $("#canvaspreview").html("");
        if (layConfig['mainbc'] != undefined) {
            var bglayout = layConfig['mainbc'];
        } else {
            var bglayout = 'rgb(0, 0, 0)';
        }
        var targetshow = 'screenbus';
        if (canvas.width == 1920 && canvas.height == 540) {
            $("#panelScreen").css('width', "955px");
            $("#panelScreen").css('height', "");
            $("#panelScreen").css('background-color', bglayout);
            $("#panelScreen").css('border-radius', "45px");
            $("#" + targetshow).removeClass().addClass("screen_maes_29");
        } else if (canvas.width == 1920 && canvas.height == 1080) {
            $("#panelScreen").css('width', "955px");
            $("#panelScreen").css('height', "395px");
            $("#panelScreen").css('background-color', bglayout);
            $("#panelScreen").css('border-radius', "45px");
            $("#" + targetshow).removeClass().addClass("screen_maes_29new");
        } else if (canvas.width == 1366 && canvas.height == 768) {
            $("#panelScreen").css('width', "658px");
            $("#panelScreen").css('height', "");
            $("#panelScreen").css('background-color', bglayout);
            $("#" + targetshow).removeClass().addClass("screen_maes_19new");
        } else if (canvas.width == 1024 && canvas.height == 768) {
            $("#panelScreen").css('width', "529px");
            $("#panelScreen").css('height', "400px");
            $("#panelScreen").css('border-radius', "45px");
            $("#panelScreen").css('background-color', bglayout);
            $("#" + targetshow).removeClass().addClass("screen_maes_19_1024-768");
        }
        var sclfactor = 0.46;
        var spcfactorw = 1;
        var spcfactorh = 1;
        var spchplus = 1;
        if (canvas.width == 1920 && canvas.height == 1080) {
            spcfactorh = 0.60;
        }
        var objcontent = "";
        oCanvas.forEach(object => {
            objcontent = "";
            if (object.objtype == 'labeledRect') {
                objcontent = "<div class='obdata'><img src='/assets/Infovision/assets/images/infovision_cnxx.png' width='" + ((object.width * sclfactor) * spcfactorw) + "px' height='" + ((object.height * sclfactor) * spcfactorh) + "px'></div>";
            } else if (object.objtype == 'image') {
                objcontent = "<div class='objdata'><img src='/admin/infovision/image/" + object.src_val + "' width='" + ((object.width * sclfactor) * spcfactorw) + "px' height='" + (((object.height * sclfactor) * spcfactorh) * spchplus) + "px'></div>";
            } else if (object.objtype == 'text') {
                objcontent = "<div style='height:100%;display:block;background-color:white;vertical-align:middle;'>" + object.src_val + "</div>";
            } else if (object.objtype == 'ticker') {
                objcontent = "<marquee style='height:100%;display:block;background-color:white;vertical-align:middle;'>" + object.src_val.replace(/\\"/g, '"') + "</marquee>";
            } else if (object.objtype == 'url') {
                objcontent = '<iframe height="100%" width="100%" src="' + object.src_val + '" /></iframe>';
            } else if (object.objtype == 'slide') {
                var arrayplaylist = object.plylstdata;
                objcontent = "<div class='slidedata'>";
                for (var i = 0; i < arrayplaylist.length; i++) {
                    objcontent += "<img id='slide-" + i + "' class='group-slide' src='/admin/infovision/image/" + arrayplaylist[i].mediavalues + "' style='background-color: rgb(0, 0, 0); position: absolute; top: 0px; left: 0px; display: block; z-index: 4; opacity: 1; width: " + ((object.width * sclfactor) * spcfactorw) + "px; height: " + ((object.height * sclfactor) * spcfactorh) + "px;'>";
                }
                objcontent += "</div>";
                slideShowAuto(arrayplaylist.length);
            }
            $("#canvaspreview").append('<span style="position: absolute; top: ' + (object.top * sclfactor) + 'px; left: ' + (object.left * sclfactor) + 'px; display: block; background: ' + object.fill + '; width: ' + ((object.width * sclfactor) * spcfactorw) + 'px; height: ' + ((object.height * sclfactor) * spcfactorh) + 'px;">' +
                objcontent +
                '</span>');
        });
    }

    $(".h-layout-preset").click(function (e) {
        var values = $(this).attr('id');
        cleancanvas();
        if (values == 'add-pre-1-1') {
            addwidget("img", 3, 0, 0, 1, 0, 0);
            addwidget("img", 3, 0, 2, 1, 0, 0);
        } else if (values == 'add-pre-1-1_v') {
            addwidget("img", 2, 0, 0, 1, 0, 0);
            addwidget("img", 2, 2, 0, 1, 0, 0);
        } else if (values == 'preset_1-2-1_v') {
            addwidget("img", 5, 0, 0, 1, 0, 0);
            addwidget("img", 6, 3, 0, 1, 0, 0);
            addwidget("img", 6, 3, 2, 1, 0, 0);
            addwidget("img", 5, 4, 0, 1, 0, 0);
        } else if (values == 'preset_1-3') {
            addwidget("img", 3, 0, 0, 1, 0, 0);
            addwidget("img", 6, 0, 2, 1, 0, 0);
            addwidget("img", 6, 3, 2, 1, 0, 0);
            addwidget("img", 6, 4, 2, 1, 0, 0);
        } else if (values == 'preset_2-2') {
            addwidget("img", 4, 0, 0, 1, 0, 0);
            addwidget("img", 4, 2, 0, 1, 0, 0);
            addwidget("img", 4, 0, 2, 1, 0, 0);
            addwidget("img", 4, 2, 2, 1, 0, 0);
        } else if (values == 'preset_3-1-3') {
            addwidget("img", 4, 0, 0, 1, 0, 0);
            addwidget("img", 4, 2, 0, 1, 0, 0);
            addwidget("img", 4, 0, 2, 1, 0, 0);
            addwidget("img", 4, 2, 2, 1, 0, 0);
        }
        console.log(selectPanel);
        if (selectPanel > 0) {
            panel.close();
            selectPanel = 0;
        }
        e.preventDefault();
        $.magnificPopup.close();
    });

    function addwidget(objty, screenfill, nleft, ntop, iscontrol, color, issilent) {
        sCanvas = JSON.stringify(canvas);
        oCanvas = JSON.parse(sCanvas);
        var countobj = 0;
        $.each(oCanvas.objects, function (n, object) {
            if (oCanvas.objects[n]['type'] == "rect") {
                countobj = countobj + 1;
            }
        });
        if (objty == "slide") {
            widnm = "Slide";
            srcval = "plylst_label" + (countobj + 1);
        } else if (objty == "img") {
            widnm = "image";
            srcval = "none";
        } else if (objty == "api_mov") {
            widnm = "Video";
            srcval = "none";
        } else if (objty == "ticker") {
            widnm = "Ticker";
            srcval = "none";
        } else if (objty == "apimuni_maps") {
            widnm = "Muni_Maps";
            objty = "api";
            srcval = "apimuni_maps";
        } else if (objty == "apimuni_inbound") {
            widnm = "Muni Inbound";
            objty = "api";
            srcval = "apimuni_inbound";
        } else if (objty == "apimuni_outbound") {
            widnm = "Muni_Outbound";
            objty = "api";
            srcval = "apimuni_outbound";
        } else if (objty == "apimuni_message") {
            widnm = "Muni Notification";
            objty = "api";
            srcval = "apimuni_message";
        } else if (objty == "api_route4") {
            widnm = "Muni_Route";
            objty = "api";
            srcval = "api_route4";
        } else if (objty == "apimuni_stopname") {
            widnm = "Muni_Stopname";
            objty = "api";
            srcval = "apimuni_stopname";
        } else if (objty == "adddatetime") {
            widnm = "Date_Time";
            objty = "api";
            srcval = "api_time";
        } else if (objty == "api_weather") {
            widnm = "api_weather";
            objty = "api";
            srcval = "api_weather";
        } else if (objty == "url") {
            widnm = "url";
            objty = "url";
            srcval = "https://www.nu.nl";
        } else if (objty == "dummyinbound") {
            widnm = "dummy_inbound";
            objty = "api";
            srcval = "api_inbounddummy";
        } else if (objty == "dummystopname") {
            widnm = "dummy_stopname";
            objty = "api";
            srcval = "api_stopnamedummy";
        } else if (objty == "dummyroute") {
            widnm = "dummy_route";
            objty = "api";
            srcval = "api_routedummy";
        } else if (objty == "dummyroutename") {
            widnm = "dummy_routename";
            objty = "api";
            srcval = "api_routenamedummy";
        } else if (objty == "api_routeexhibit") {
            widnm = "api_routeexhibit";
            objty = "api";
            srcval = "api_routeexhibit";
        } else if (objty == "api_weatherexhibit") {
            widnm = "api_weatherexhibit";
            objty = "api";
            srcval = "api_weatherexhibit";
        } else if (objty == "api_stopnameexhibit") {
            widnm = "api_stopnameexhibit";
            objty = "api";
            srcval = "api_stopnameexhibit";
        } else if (objty == "api_inboundexhibit") {
            widnm = "api_inboundexhibit";
            objty = "api";
            srcval = "api_inboundexhibit";
        } else if (objty == "api_messageexhibit") {
            widnm = "api_messageexhibit";
            objty = "api";
            srcval = "api_messageexhibit";
        } else if (objty == "api_rss") {
            widnm = "api_rss";
            objty = "api";
            srcval = "api_rss";
        } else if (objty == "text") {
            widnm = "text";
            objty = "text";
            srcval = "example text";
        } else if (objty == "api_route") {
            widnm = "api_route";
            objty = "api";
            srcval = "api_route";
        }


        var l, t, w, h;
        if (ntop > 5) {
            t = ntop;
        } else if (ntop == 2) {
            t = ((canvas.height * 1) / 2);
        } else if (ntop == 2) {
            t = ((canvas.height * 1) / 3);
        } else if (ntop == 0) {
            t = 0;
        } else {
            t = 10;
        }

        if (nleft > 5) {
            l = nleft;
        } else if (nleft == 2) {
            l = ((canvas.width * 1) / 2);
        } else if (nleft == 3) {
            l = ((canvas.width * 1) / 3);
        } else if (nleft == 4) {
            l = ((canvas.width * 1) - ((canvas.width * 1) / 3));
        } else if (nleft == 0) {
            l = 0;
        } else {
            l = 350;
        }

        console.log("widget fill " + screenfill + " " + l + " " + t);

        if (screenfill == 1) {
            w = (canvas.width * 1);
            h = (canvas.height * 1);
        } else if (screenfill == 2) {
            w = ((canvas.width * 1) / 2);
            h = (canvas.height * 1);
        } else if (screenfill == 3) {
            w = (canvas.width * 1);
            h = ((canvas.height * 1) / 2);
        } else if (screenfill == 4) {
            w = ((canvas.width * 1) / 2);
            h = ((canvas.height * 1) / 2);
        } else if (screenfill == 5) {
            w = ((canvas.width * 1) / 3);
            h = ((canvas.height * 1));
        } else if (screenfill == 6) {
            w = ((canvas.width * 1) / 3);
            h = ((canvas.height * 1) / 2);
        } else {
            w = 250;
            h = 150;
        }
        var bgcolor = "";
        if (typeof color !== 'undefined' && color.length > 0) {
            bgcolor = color;
        } else {
            bgcolor = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
        }

        var rect = new fabric.Rect({
            top: t,
            left: l,
            width: w,
            height: h,
            fill: bgcolor,
            name: 'label_' + countobj,
            evented: iscontrol,
            selectable: iscontrol,
            hasBorders: iscontrol,
            hasControls: iscontrol,
            lockScalingFlip: true,
            hasRotatingPoint: false,
            objtype: widnm,
            objstyle: '',
            src_type: objty,
            src_val: srcval,
            plylstisdb: '0',
            plylstid: '',
            plylstnm: '',
            plylstdata: ''
        });
        canvas.add(rect);

        $('<span class="objtxt">').attr({
            name: 'label_' + countobj,
            value: 'label_' + countobj,
            id: 'label_' + countobj,
            style: 'position: absolute;pointer-events: none;'
        }).appendTo("#canvas-container");
        canvas.renderAll();

        var btn = document.getElementById('label_' + countobj);
        btn.innerHTML = "Name : " + 'label_' + countobj + "<br>Width : " + w + "<br>Height : " + h + "<br>Left : " + l + "<br>Top : " + t + "<br>Type : " + widnm;
        btn.style.left = (l + 165) + 'px';
        btn.style.top = (t + 10) + 'px';
        btn.style.fontSize = '12px';
    }

    function cleancanvas() {
        $(".objtxt").remove();
        var objects = canvas.getObjects('rect');
        for (let i in objects) {
            canvas.remove(objects[i]);
        }
    }

    //prevent widget move to outside canvas
    function preventLeaving(e) {
        var activeObject = e.target;
        if (activeObject.get('left') < 0) {
            activeObject.set('left', 0);
        }
        if (activeObject.get('top') < 0) {
            activeObject.set('top', 0);
        }
        if (activeObject.get('left') + activeObject.get('width') > parseInt(canvas.getWidth())) {
            activeObject.set('left', (parseInt(canvas.getWidth()) - activeObject.get('width')));
        }
        if (activeObject.get('top') + activeObject.get('height') > parseInt(canvas.getHeight())) {
            activeObject.set('top', (parseInt(canvas.getHeight()) - activeObject.get('height')));
        }
        resizeteks(e, "geo");
    }

    function onChange(e) {
        e.target.setCoords();
        canvas.forEachObject(function (obj) {
            if (obj === e.target) return;
            obj.set('opacity', e.target.intersectsWithObject(obj) ? 0.5 : 1);
        });
        resizeteks(e, "geo");
    }

    function resizeteks(targetid, method) {
        var btnWidth = 125,
            btnHeight = 18;
        var options = targetid;
        var hidden = 0;
        var idealwidth = (canvas.width - 100);
        var idealheight = (canvas.height - 100);
        if (method == "geo") {
            var btn = document.getElementById(options.target.name);
            btn.innerHTML = "Name : " + options.target.name + "<br>Width : " + options.target.width + "<br>Height : " + options.target.height + "<br>Left : " + options.target.left + "<br>Top : " + options.target.top + "<br>Type : " + options.target.objtype;
            btn.style.left = (options.target.left - btnWidth / 2) * canvas.getZoom() - scrollleft + (185 * canvas.getZoom()) + 'px';
            btn.style.top = (options.target.top - btnHeight / 2) * canvas.getZoom() - scrolltop + (20 * canvas.getZoom()) + 'px';
            btn.style.fontSize = 12 * canvas.getZoom() + 'px';
            if (options.target.top < 0 || options.target.top > idealheight) {
                hidden = hidden + 1;
            } else {
                hidden = hidden - 1;
            }
            if (options.target.left < 0 || options.target.left > idealwidth) {
                hidden = hidden + 1;
            } else {
                hidden = hidden - 1;
            }
        } else if (method == "prop") {
            var btn = document.getElementById(options.name);
            btn.innerHTML = "Name : " + options.name + "<br>Width : " + options.width + "<br>Height : " + options.height + "<br>Left : " + options.left + "<br>Top : " + options.top + "<br>Type : " + options.objtype;
            btn.style.left = (options.left - btnWidth / 2) * canvas.getZoom() - scrollleft + (185 * canvas.getZoom()) + 'px';
            btn.style.top = (options.top - btnHeight / 2) * canvas.getZoom() - scrolltop + (20 * canvas.getZoom()) + 'px';
            btn.style.fontSize = 12 * canvas.getZoom() + 'px';
            if (options.top < 0 || options.top > idealheight) {
                hidden = hidden + 1;
            } else {
                hidden = hidden - 1;
            }
            if (options.left < 0 || options.left > idealwidth) {
                hidden = hidden + 1;
            } else {
                hidden = hidden - 1;
            }
        } else if (method == "zoom") {
            var btn = document.getElementById(options.name);
            btn.innerHTML = "Name : " + options.name + "<br>Width : " + options.width + "<br>Height : " + options.height + "<br>Left : " + options.left + "<br>Top : " + options.top + "<br>Type : " + options.objtype;
            btn.style.left = (options.left - btnWidth / 2) * canvas.getZoom() - scrollleft + (185 * canvas.getZoom()) + 'px';
            btn.style.top = (options.top - btnHeight / 2) * canvas.getZoom() - scrolltop + (20 * canvas.getZoom()) + 'px';
            btn.style.fontSize = 12 * canvas.getZoom() + 'px';
            if (options.top < 0 || options.top > idealheight) {
                hidden = hidden + 1;
            } else {
                hidden = hidden - 1;
            }
            if (options.left < 0 || options.left > idealwidth) {
                hidden = hidden + 1;
            } else {
                hidden = hidden - 1;
            }
        }
        if (hidden == 0) {
            btn.style.display = "none";
        } else {
            btn.style.display = "";
        }
    }

    if (loc.pathname == '/admin/infovision/layout-create') {
        canvas.on('object:moving', function (e) {
            var obj = e.target,
                canvasObjects = canvas.getObjects(),
                objCenter = obj.getCenterPoint(),
                objLeft = objCenter.x,
                objTop = objCenter.y,
                objBoundingRect = obj.getBoundingRect(),
                objHeight = objBoundingRect.height / viewportTransform[3],
                objWidth = objBoundingRect.width / viewportTransform[0],
                horizontalInTheRange = false,
                verticalInTheRange = false,
                transform = canvas._currentTransform;

            if (!transform) return;

            for (var i = canvasObjects.length; i--;) {

                if (canvasObjects[i] === obj) continue;

                var objectCenter = canvasObjects[i].getCenterPoint(),
                    objectLeft = objectCenter.x,
                    objectTop = objectCenter.y,
                    objectBoundingRect = canvasObjects[i].getBoundingRect(),
                    objectHeight = objectBoundingRect.height / viewportTransform[3],
                    objectWidth = objectBoundingRect.width / viewportTransform[0];

                // snap by the horizontal center line
                if (isInRange(objectLeft, objLeft)) {
                    verticalInTheRange = true;
                    verticalLines.push({
                        x: objectLeft,
                        y1: (objectTop < objTop) ?
                            (objectTop - objectHeight / 2 - aligningLineOffset) : (objectTop + objectHeight / 2 + aligningLineOffset),
                        y2: (objTop > objectTop) ?
                            (objTop + objHeight / 2 + aligningLineOffset) : (objTop - objHeight / 2 - aligningLineOffset)
                    });
                    obj.setPositionByOrigin(new fabric.Point(objectLeft, objTop), 'center', 'center');
                }

                // snap by the left edge
                if (isInRange(objectLeft - objectWidth / 2, objLeft - objWidth / 2)) {
                    verticalInTheRange = true;
                    verticalLines.push({
                        x: objectLeft - objectWidth / 2,
                        y1: (objectTop < objTop) ?
                            (objectTop - objectHeight / 2 - aligningLineOffset) : (objectTop + objectHeight / 2 + aligningLineOffset),
                        y2: (objTop > objectTop) ?
                            (objTop + objHeight / 2 + aligningLineOffset) : (objTop - objHeight / 2 - aligningLineOffset)
                    });
                    obj.setPositionByOrigin(new fabric.Point(objectLeft - objectWidth / 2 + objWidth / 2, objTop), 'center', 'center');
                }

                // snap by the right edge
                if (isInRange(objectLeft + objectWidth / 2, objLeft + objWidth / 2)) {
                    verticalInTheRange = true;
                    verticalLines.push({
                        x: objectLeft + objectWidth / 2,
                        y1: (objectTop < objTop) ?
                            (objectTop - objectHeight / 2 - aligningLineOffset) : (objectTop + objectHeight / 2 + aligningLineOffset),
                        y2: (objTop > objectTop) ?
                            (objTop + objHeight / 2 + aligningLineOffset) : (objTop - objHeight / 2 - aligningLineOffset)
                    });
                    obj.setPositionByOrigin(new fabric.Point(objectLeft + objectWidth / 2 - objWidth / 2, objTop), 'center', 'center');
                }

                // snap by the vertical center line
                if (isInRange(objectTop, objTop)) {
                    horizontalInTheRange = true;
                    horizontalLines.push({
                        y: objectTop,
                        x1: (objectLeft < objLeft) ?
                            (objectLeft - objectWidth / 2 - aligningLineOffset) : (objectLeft + objectWidth / 2 + aligningLineOffset),
                        x2: (objLeft > objectLeft) ?
                            (objLeft + objWidth / 2 + aligningLineOffset) : (objLeft - objWidth / 2 - aligningLineOffset)
                    });
                    obj.setPositionByOrigin(new fabric.Point(objLeft, objectTop), 'center', 'center');
                }

                // snap by the top edge
                if (isInRange(objectTop - objectHeight / 2, objTop - objHeight / 2)) {
                    horizontalInTheRange = true;
                    horizontalLines.push({
                        y: objectTop - objectHeight / 2,
                        x1: (objectLeft < objLeft) ?
                            (objectLeft - objectWidth / 2 - aligningLineOffset) : (objectLeft + objectWidth / 2 + aligningLineOffset),
                        x2: (objLeft > objectLeft) ?
                            (objLeft + objWidth / 2 + aligningLineOffset) : (objLeft - objWidth / 2 - aligningLineOffset)
                    });
                    obj.setPositionByOrigin(new fabric.Point(objLeft, objectTop - objectHeight / 2 + objHeight / 2), 'center', 'center');
                }

                // snap by the bottom edge
                if (isInRange(objectTop + objectHeight / 2, objTop + objHeight / 2)) {
                    horizontalInTheRange = true;
                    horizontalLines.push({
                        y: objectTop + objectHeight / 2,
                        x1: (objectLeft < objLeft) ?
                            (objectLeft - objectWidth / 2 - aligningLineOffset) : (objectLeft + objectWidth / 2 + aligningLineOffset),
                        x2: (objLeft > objectLeft) ?
                            (objLeft + objWidth / 2 + aligningLineOffset) : (objLeft - objWidth / 2 - aligningLineOffset)
                    });
                    obj.setPositionByOrigin(new fabric.Point(objLeft, objectTop + objectHeight / 2 - objHeight / 2), 'center', 'center');
                }
            }

            if (!horizontalInTheRange) {
                horizontalLines.length = 0;
            }

            if (!verticalInTheRange) {
                verticalLines.length = 0;
            }

            const target = e.target;
            const activeGroup = canvas.getActiveGroup();
            if (activeGroup) {} else {
                e.target.set({
                    left: Math.round(e.target.left / grid) * grid,
                    top: Math.round(e.target.top / grid) * grid
                });

                $("#objtop").val(e.target.top);
                $("#objleft").val(e.target.left);
                var leftpoint = e.target.left;
                var panelpoint = 0;
                if (leftpoint < 500) {
                    if ($('#mypanel').length) {
                        panel.reposition("right-top -50 430");
                    }
                } else {
                    if ($('#mypanel').length) {
                        panel.reposition("left-top 50 430");
                    }
                }

                $('li.dropdown.mega-dropdown').removeClass('open');

                var currentScrollLeft = $('div').scrollLeft;
                var currentScrollTop = $('#canvas-container')[0].scrollTop;
                if ($('div').scrollLeft() > 0) {
                    alert($('#canvas-container').scrollLeft());
                }

                if (currentScrollLeft > e.target.left) {
                    $('#canvas-container')[0].scrollLeft = e.target.left();
                }

                if (currentScrollTop > e.target.top) {
                    $('#canvas-container')[0].scrollTop = e.target.top;
                }
                preventLeaving(e);
                onChange(e);
            }

            // if (parseInt(obj.top) > 0 && parseInt(obj.left) > 0) {
            //     var btn = document.getElementById(obj.name);
            //     btn.innerHTML = 'Name : ' + obj.name + '<br>Width : ' + parseInt(obj.width) + '<br>Height : ' + parseInt(obj.height) + '<br>Left : ' + parseInt(obj.left) + '<br>Top : ' + parseInt(obj.top) + '<br>Type : ' + obj.objtype;
            //     btn.style.left = (obj.left + 130) + 'px';
            //     btn.style.top = obj.top + 'px';
            //     $('#objTop').val(parseInt(obj.top));
            //     $('#objLeft').val(parseInt(obj.left));
            //     $('#objWidth').val(parseInt(obj.width));
            //     $('#objHeight').val(parseInt(obj.height));
            // }
            // if (obj.currentHeight > obj.canvas.height || obj.currentWidth > obj.canvas.width) {
            //     return;
            // }
            // obj.setCoords();
            // if (obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0) {
            //     obj.top = Math.max(obj.top, obj.top - obj.getBoundingRect().top);
            //     obj.left = Math.max(obj.left, obj.left - obj.getBoundingRect().left);
            // }
            // if (obj.getBoundingRect().top + obj.getBoundingRect().height > obj.canvas.height || obj.getBoundingRect().left + obj.getBoundingRect().width > obj.canvas.width) {
            //     obj.top = Math.min(obj.top, obj.canvas.height - obj.getBoundingRect().height + obj.top - obj.getBoundingRect().top);
            //     obj.left = Math.min(obj.left, obj.canvas.width - obj.getBoundingRect().width + obj.left - obj.getBoundingRect().left);
            // }
            // if (obj.left < 500) {
            //     if ($('#mypanel').length) {
            //         panel.reposition("right-top -50 430");
            //     }
            // } else {
            //     if ($('#mypanel').length) {
            //         panel.reposition("left-top 50 430");
            //     }
            // }

            // preventLeaving(e);


        });

        canvas.on('before:render', function () {
            canvas.clearContext(canvas.contextTop);
        });

        canvas.on('after:render', function () {
            for (var i = verticalLines.length; i--;) {
                drawVerticalLine(verticalLines[i]);
            }
            for (var i = horizontalLines.length; i--;) {
                drawHorizontalLine(horizontalLines[i]);
            }

            verticalLines.length = horizontalLines.length = 0;
        });

        canvas.on('object:scaling', function (p) {
            var target = p.target;
            var type = canvas.getActiveObject().get('type');
            var corner = target.__corner;
            var w = target.getWidth();
            var h = target.getHeight();
            var snap = { // Closest snapping points
                top: Math.round(target.top / grid) * grid,
                left: Math.round(target.left / grid) * grid,
                bottom: Math.round((target.top + h) / grid) * grid,
                right: Math.round((target.left + w) / grid) * grid,
            };
            snap.height = snap.top - snap.bottom;
            if (snap.height < 0) {
                snap.height *= -1;
            }
            snap.width = snap.left - snap.right;
            if (snap.width < 0) {
                snap.width *= -1;
            }
            switch (corner) {
                case 'mt':
                case 'mb':
                    target.top = snap.top;
                    target.height = snap.height;
                    target.scaleY = 1;
                    break;
                case 'ml':
                case 'mr':
                    target.left = snap.left;
                    target.width = snap.width;
                    target.scaleX = 1;
                    break;
                case 'tl':
                case 'bl':
                case 'tr':
                case 'br':
                    target.top = snap.top;
                    target.left = snap.left;

                    target.height = snap.height;
                    target.width = snap.width;

                    target.scaleY = 1;
                    target.scaleX = 1;
            }

            if (type == 'ellipse') {
                target.rx = (target.width / 2);
                target.ry = (target.height / 2);
            }

            if (type == 'group') {
                var width = target.getWidth();
                var height = target.getWidth();
                target._objects[1].text = width.toFixed(2) + 'px';
                target._objects[1].scaleX = 1 / target.scaleX;
                target._objects[1].scaleY = 1 / target.scaleY;
                target._objects[2].text = height.toFixed(2) + 'px';
                target._objects[2].scaleX = 1 / target.scaleY;
                target._objects[2].scaleY = 1 / target.scaleX;
            }
            resizeteks(p, "geo");

            $("#objwidth").val(snap.width);
            $("#objheight").val(snap.height);
            onChange(p);
            // var obj = canvas.getActiveObject(),
            //     width = obj.width,
            //     height = obj.height,
            //     scaleX = obj.scaleX,
            //     scaleY = obj.scaleY;

            // obj.set({
            //     width: width * scaleX,
            //     height: height * scaleY,
            //     scaleX: 1,
            //     scaleY: 1
            // });

            // if (parseInt(obj.top) > 0 && parseInt(obj.left) > 0) {
            //     var btn = document.getElementById(obj.name);
            //     btn.innerHTML = 'Name : ' + obj.name + '<br>Width : ' + parseInt(obj.width * scaleX) + '<br>Height : ' + parseInt(obj.height * scaleY) + '<br>Left : ' + parseInt(obj.left) + '<br>Top : ' + parseInt(obj.top) + '<br>Type : ' + obj.objtype;
            //     btn.style.left = (obj.left + 130) + 'px';
            //     btn.style.top = (obj.top + 10) + 'px';
            //     $('#objTop').val(parseInt(obj.top));
            //     $('#objLeft').val(parseInt(obj.left));
            //     $('#objWidth').val(parseInt(obj.width * scaleX));
            //     $('#objHeight').val(parseInt(obj.height * scaleY));
            // }
        });

        canvas.on('object:modified', function (o) {
            const target = o.target;
            const activeGroup = canvas.getActiveGroup();
            if (activeGroup) {} else {
                if (o.target.left < 0) {
                    o.target.set('left', thisobjleft);
                    o.target.set('width', thisobjwidth);
                } else {
                    thisobjleft = o.target.left;
                    thisobjwidth = o.target.width;
                }

                if (o.target.top < 0) {
                    o.target.set('top', thisobjtop);
                    o.target.set('height', thisobjheight);
                } else {
                    thisobjtop = o.target.top;
                    thisobjheight = o.target.height;
                }

                if (o.target.height > parseInt(canvas.getHeight())) {
                    o.target.set('height', parseInt(canvas.getHeight()));
                }

                if (o.target.width > parseInt(canvas.getWidth())) {
                    o.target.set('width', parseInt(canvas.getWidth()));
                }
                resizeteks(o, "geo");
            }
        });
    }

    var selectPanel = 0;

    var saveTempImg = new Array();

    if (loc.pathname == '/admin/infovision/layout-create') {
        canvas.on('object:selected', function (e) {
            if (selectPanel > 0) {
                if (panel.status != 'closed') {
                    panel.close();
                }
            }

            var obj = e.target;
            contentpanel = '<div class="row">' +
                '<div class="col-md-12" style="padding-left: 25px;padding-right: 25px;">' +
                '<div class="panel-group" id="accordion">';
            if (obj.objtype == 'image') {
                contentpanel += '<div class="panel panel-accordion">' +
                    '<div class="panel-heading">' +
                    '<h4 class="panel-title">' +
                    '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#ImageConfig">' +
                    'Image Configuration' +
                    '</a>' +
                    '</h4>' +
                    '</div>' +
                    '<div id="ImageConfig" class="accordion-body collapse in">' +
                    '<div class="panel-body">' +
                    '<table class="table table-striped mb-none">' +
                    '<tbody>' +
                    '<tr>' +
                    '<td><input type="hidden" value="' + obj.name + '" id="objName"><a href="#modalAddImage" class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary"><i class="fa fa-plus"></i> Add Image</a></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>' +
                    'Image Name : <span id="imageName"></span>' +
                    '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>' +
                    'Thumbnail : <img id="thumbnail-layout" src="/assets/Infovision/assets/images/noimage.png" style="width: 30%;">' +
                    '</td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            } else if (obj.objtype == 'movie') {
                contentpanel += '<div class="panel panel-accordion">' +
                    '<div class="panel-heading">' +
                    '<h4 class="panel-title">' +
                    '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#ImageConfig">' +
                    'Video Configuration' +
                    '</a>' +
                    '</h4>' +
                    '</div>' +
                    '<div id="ImageConfig" class="accordion-body collapse in">' +
                    '<div class="panel-body">' +
                    '<table class="table table-striped mb-none">' +
                    '<tbody>' +
                    '<tr>' +
                    '<td><input type="hidden" value="' + obj.name + '" id="objName"><a href="#modalAddVideo" class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary"><i class="fa fa-plus"></i> Add Video</a></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>' +
                    'Video Name : <span id="imageName"></span>' +
                    '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>' +
                    'Thumbnail : <img id="thumbnail-layout" data-value="" src="/nc_video/videothumb.png" style="width: 30%;">' +
                    '</td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            } else if (obj.objtype == 'text') {
                contentpanel += '<div class="panel panel-accordion">' +
                    '<div class="panel-heading">' +
                    '<h4 class="panel-title">' +
                    '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#ImageConfig">' +
                    'Text Configuration' +
                    '</a>' +
                    '</h4>' +
                    '</div>' +
                    '<div id="ImageConfig" class="accordion-body collapse in">' +
                    '<div class="panel-body">' +
                    '<table class="table table-striped mb-none">' +
                    '<tbody>' +
                    '<tr>' +
                    '<td><div class="summernote" id="richEditor" data-plugin-summernote data-plugin-options="{ "height": 180, "codemirror": { "theme": "ambiance" } }"">Start typing...</div></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>' +
                    '<button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="saveText"><i class="fa fa-save"></i> Save</button>' +
                    '</td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            } else if (obj.objtype == 'ticker') {
                contentpanel += '<div class="panel panel-accordion">' +
                    '<div class="panel-heading">' +
                    '<h4 class="panel-title">' +
                    '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#ImageConfig">' +
                    'Text Configuration' +
                    '</a>' +
                    '</h4>' +
                    '</div>' +
                    '<div id="ImageConfig" class="accordion-body collapse in">' +
                    '<div class="panel-body">' +
                    '<table class="table table-striped mb-none">' +
                    '<tbody>' +
                    '<tr>' +
                    '<td><div class="summernote" id="richEditor" data-plugin-summernote data-plugin-options="{ "height": 180, "codemirror": { "theme": "ambiance" } }"">Start typing...</div></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>' +
                    '<button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="saveTicker"><i class="fa fa-save"></i> Save</button>' +
                    '</td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            } else if (obj.objtype == 'url') {
                contentpanel += '<div class="panel panel-accordion">' +
                    '<div class="panel-heading">' +
                    '<h4 class="panel-title">' +
                    '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#ImageConfig">' +
                    'Url Configuration' +
                    '</a>' +
                    '</h4>' +
                    '</div>' +
                    '<div id="ImageConfig" class="accordion-body collapse in">' +
                    '<div class="panel-body">' +
                    '<table class="table table-striped mb-none">' +
                    '<tbody>' +
                    '<tr>' +
                    '<td><div class="form-group"><label for="exampleInputEmail1">Enter Url</label><input type="text" class="form-control" id="textUrlWeb" value="http://www.nu.nl"></div></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>' +
                    '<button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="saveUrlWeb"><i class="fa fa-save"></i> Save</button>' +
                    '</td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            } else if (obj.objtype == 'slide') {
                contentpanel += '<div class="panel panel-accordion">' +
                    '<div class="panel-heading">' +
                    '<h4 class="panel-title">' +
                    '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#ImageConfig">' +
                    'Playlist Configuration : Unsaved Playlist' +
                    '</a>' +
                    '</h4>' +
                    '</div>' +
                    '<div id="ImageConfig" class="accordion-body collapse in">' +
                    '<div class="panel-body">' +
                    '<table class="table table-striped mb-none" id="tabPlaylist-' + obj.name + '">' +
                    '<thead>' +
                    '<tr>' +
                    '<td colspan="7">' +
                    '<a href="#addMedia" id="addMediaChoose" data-value="' + obj.name + '" class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary"><i class="fa fa-plus"></i> Add Media</a>' +
                    '<a href="#loadPlaylist" id="showLoadPlayList" data-value="' + obj.name + '" class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary"><i class="fa fa-folder"></i> Load Playlist</a>' +
                    '<a href="javascript:void(0)" id="savePlaylist" data-value="' + obj.name + '" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="fa fa-save"></i> Save Playlist</a>' +
                    '<a href="javascript:void(0)" id="clearPlayList" data-value="' + obj.name + '" class="mb-xs mt-xs mr-xs btn btn-danger"><i class="fa fa-trash"></i> Clear Playlist</a>' +
                    '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 90px;">' +
                    'No' +
                    '</td>' +
                    '<td>' +
                    'Source Name' +
                    '</td>' +
                    '<td>' +
                    'Num Day' +
                    '</td>' +
                    '<td>' +
                    'Date' +
                    '</td>' +
                    '<td>' +
                    'Time' +
                    '</td>' +
                    '<td>' +
                    'Priority' +
                    '</td>' +
                    '<td>' +
                    'Trigger' +
                    '</td>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody class="row_position">' +
                    '<tr>' +
                    '<td colspan="7" style="text-align: center;">' +
                    'No Media File Added' +
                    '</td>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }
            contentpanel += '<div class="panel panel-accordion">' +
                '<div class="panel-heading">' +
                '<h4 class="panel-title">' +
                '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#objectStyle">' +
                'Object Style' +
                '</a>' +
                '</h4>' +
                '</div>' +
                '<div id="objectStyle" class="accordion-body collapse">' +
                '<div class="panel-body">' +
                '<table class="table table-striped mb-none">' +
                '<tbody>' +
                '<tr>' +
                '<td>' +
                'Top : <input type="text" id="objTop" class="form-control" value="' + parseInt(obj.top) + '" style="width: 60px;display: inline !important;"> Width  : <input type="text" id="objWidth" value="' + parseInt(obj.width) + '" class="form-control" style="width: 60px;display: inline !important;">' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                'Left : <input type="text" id="objLeft" class="form-control" value="' + parseInt(obj.left) + '" style="width: 60px;display: inline !important;"> Height : <input type="text" id="objHeight" class="form-control" value="' + parseInt(obj.height) + '" style="width: 60px;display: inline !important;">' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                'Object Color : <div class="input-group color" data-color="' + obj.fill + '" data-color-format="rgb" data-plugin-colorpicker><span class="input-group-addon"><i></i></span><input type="text" class="form-control" id="objColor" value="' + obj.fill + '"></div>' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                'Object Sheet : <input type="text" class="form-control">' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="saveStyleSheet"><i class="fa fa-save"></i> Save Stylesheet</button>' +
                '</td>' +
                '</tr>' +
                '</tbody>' +
                '</table>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            if (350 < 500) {
                panelpoint = -50;
                align = "right-top";
            } else {
                panelpoint = 30;
                align = "left-top";
            }

            panel = jsPanel.create({
                id: "mypanel",
                contentSize: "600 250",
                headerControls: {
                    maximize: 'remove',
                    minimize: 'remove',
                    iconfont: 'glyphicon'
                },
                headerTitle: " Object Properties : " + obj.name,
                theme: "bootstrap-default",
                position: {
                    my: align,
                    at: align,
                    offsetX: panelpoint,
                    offsetY: 430
                },
                content: contentpanel
            });

            $('.color').colorpicker();

            $('.modal-sizes').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });

            $('#richEditor').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']]
                ]
            });

            if (saveTempImg[obj.name] != undefined) {
                if (saveTempImg[obj.name]['type'] == 'image') {
                    var imgname = saveTempImg[obj.name]['result'];
                    $('#imageName').text(imgname);
                    $('#thumbnail-layout').attr('src', '/admin/infovision/image/thumbnail/' + imgname);
                } else if (saveTempImg[obj.name]['type'] == 'movie') {
                    var moviename = saveTempImg[obj.name]['result'];
                    $('#imageName').text(moviename);
                } else if (saveTempImg[obj.name]['type'] == 'text') {
                    var values = saveTempImg[obj.name]['result'];
                    $('.note-editable').html(values);
                } else if (saveTempImg[obj.name]['type'] == 'ticker') {
                    var values = saveTempImg[obj.name]['result'];
                    $('.note-editable').html(values);
                } else if (saveTempImg[obj.name]['type'] == 'url') {
                    var values = saveTempImg[obj.name]['result'];
                    $('#textUrlWeb').val(values);
                } else if (saveTempImg[obj.name]['type'] == 'slide') {
                    var values = obj.name;
                    setTablePlaylist(values);
                }
            }

            if (obj.left < 500) {
                if ($('#mypanel').length) {
                    panel.reposition("right-top -50 430");
                }
            } else {
                if ($('#mypanel').length) {
                    panel.reposition("left-top 50 430");
                }
            }

            selectPanel++;

            $("#saveText").click(function (e) {
                var values = $('.note-editable').html();
                saveTempImg[obj.name] = new Array();
                saveTempImg[obj.name]['result'] = values;
                saveTempImg[obj.name]['type'] = 'text';
                var objactive = canvas.getActiveObject();
                objactive.src_val = values;
                canvas.renderAll();
            });

            $("#saveTicker").click(function (e) {
                var values = $('.note-editable').html();
                saveTempImg[obj.name] = new Array();
                saveTempImg[obj.name]['result'] = values;
                saveTempImg[obj.name]['type'] = 'text';
                var objactive = canvas.getActiveObject();
                objactive.src_val = values;
                canvas.renderAll();
            });

            $("#saveUrlWeb").click(function (e) {
                var values = $('#textUrlWeb').val();
                saveTempImg[obj.name] = new Array();
                saveTempImg[obj.name]['result'] = values;
                saveTempImg[obj.name]['type'] = 'url';
                var objactive = canvas.getActiveObject();
                objactive.src_val = values;
                canvas.renderAll();
            });

            $("#saveStyleSheet").click(function (e) {
                var objactive = canvas.getActiveObject();
                objactive.set('left', parseInt($('#objLeft').val())).setCoords();
                objactive.set('top', parseInt($('#objTop').val())).setCoords();
                objactive.set('width', parseInt($('#objWidth').val())).setCoords();
                objactive.set('height', parseInt($('#objHeight').val())).setCoords();
                objactive.set('fill', $('#objColor').val()).setCoords();
                canvas.renderAll();
                var btn = document.getElementById(objactive.name);
                btn.innerHTML = 'Name : ' + objactive.name + '<br>Width : ' + parseInt(objactive.width) + '<br>Height : ' + parseInt(objactive.height) + '<br>Left : ' + parseInt(objactive.left) + '<br>Top : ' + parseInt(objactive.top) + '<br>Type : ' + objactive.objtype;
                btn.style.left = (objactive.left + 130) + 'px';
                btn.style.top = (objactive.top + 80) + 'px';
            });

            $(".date-object").click(function (e) {
                if ($(this).hasClass('selectDay') == false) {
                    var values = $(this).attr('id');
                    if (values == 'day-0') {
                        $('.date-object').addClass('selectDay');
                    } else {
                        $(this).addClass('selectDay');
                    }
                } else {
                    var values = $(this).attr('id');
                    if (values == 'day-0') {
                        $('.date-object').removeClass('selectDay');
                    } else {
                        $(this).removeClass('selectDay');
                    }
                }
            });

            $("#locationtriggerobject").change(function (e) {
                var values = $(this).val();
                if (values == 'Yes') {
                    $('#typeLoc').css('display', 'block');
                } else {
                    $('#typeLoc').css('display', 'none');
                    $('#Geo').css('display', 'none');
                    $('#lineOn').css('display', 'none');
                    $('#stopOn').css('display', 'none');
                }
            });

            $('input[name="optionsRadiosLocation"]').click(function (e) {
                var values = $(this).val();
                if (values == '2') {
                    $('#Geo').css('display', 'block');
                    $('#lineOn').css('display', 'none');
                    $('#lineobject').val('').trigger('change');
                    $('#stopOn').css('display', 'none');
                    getShowListGeofence();
                } else {
                    $('#Geo').css('display', 'none');
                    $('#lineOn').css('display', 'block');
                    showListLineObject();
                }
            });

            $("#geofence-title").change(function (e) {
                if ($(this).val() !== null && $(this).val() !== '') {
                    var id = $(this).val().toString();
                    $('.geofencer-map').css('display', 'none');
                    infoPolygon(id);
                }
            });

            function getShowListGeofence() {
                $.ajax({
                    type: "GET",
                    url: '/admin/infovision/layout-create/list/geofence',
                    cache: false,
                    success: function (result) {
                        var models = result.models;
                        $('#geofence-title').empty();
                        var html = '';
                        var statusobj = '';
                        for (var i = 0; i < models.length; i++) {
                            html += '<option value="' + models[i].id_geofence + '">' + models[i].name + '</option>';
                        }
                        $('#geofence-title').append(html);
                        $('#geofence-title').multiselect({
                            enableClickableOptGroups: true,
                            enableCollapsibleOptGroups: true,
                            enableFiltering: true,
                            includeSelectAllOption: true
                        });
                        $('#geofence-title').multiselect('rebuild');
                    }
                });
            }

            var map = null;
            var polygon = null;
            var draggable = false;
            var totalLatLng = [];

            function infoPolygon(id) {
                totalLatLng = [];
                var upObjLabel = new Object();
                $.ajax({
                    type: "GET",
                    url: "/admin/infovision/layout-create/info/geofence",
                    data: {
                        id: id
                    },
                    cache: false,
                    success: function (result) {
                        var detail = result.models;
                        var statusDis = false;
                        for (let a = 0; a < detail.length; a++) {
                            statusDis = true;
                            text = '';
                            var split1fixs = detail[a]['latlong'].split('|');
                            for (let i = 0; i < split1fixs.length; i++) {
                                var split2fixs = split1fixs[i].split(',');
                                text += '(' + split2fixs[0] + ', ' + split2fixs[1] + ')|'
                            }
                            totalLatLng.push(text);
                            if (detail[a]['label'] !== '' && detail[a]['label'] !== null && detail[a]['label'] !== 'undefined') {
                                upObjLabel[detail[a]['name_polygon']] = detail[a]['label'];
                            }
                        }
                        if (statusDis == true) {
                            $('.geofencer-map').css('display', 'block');
                        }
                        initialize();
                    }
                });
            }

            function initialize() {
                if (map == null) {
                    map = new GeofencingMap('map').map;
                    polygon = new MultiPolygon(map, $('#polygon-name').val());
                } else {
                    polygon.deleteAllPolygons();
                }
                if (totalLatLng.length > 0) {
                    for (var a = 0; a < totalLatLng.length; a++) {
                        var coords = new Array();
                        var splitLatLng = totalLatLng[a].split("|");
                        for (var i = 0; i < splitLatLng.length; i++) {
                            var latlng = splitLatLng[i].trim().substring(1, splitLatLng[i].length - 1).split(",");
                            if (latlng.length > 1) {
                                coords.push(L.latLng(latlng[0], latlng[1]));
                            }
                            if (a == 0 && i == 0) {
                                map.setView([latlng[0], latlng[1]], 5);
                            }
                        }
                        polygon.addPolygon(coords, totalLatLng.length, upObjLabel, true);
                        polygon.setAllowDragging(draggable);
                        polygon.setEditable(true);
                    }
                } else {
                    var coords = new Array();
                    polygon.addPolygon(coords, totalLatLng.length, upObjLabel, true);
                    polygon.setAllowDragging(draggable);
                    polygon.setEditable(true);
                }

                polygon.setAllowDragging(draggable);
                polygon.setEditable(false);
                /*map.touchZoom.disable();
                map.doubleClickZoom.disable();
                map.scrollWheelZoom.disable();
                map.boxZoom.disable();
                map.keyboard.disable();
                setTimeout(function(){ map.invalidateSize()}, 400);
                $(".leaflet-control-zoom").css("visibility", "hidden");*/
            }

            $('input[name="optionsRadiosLine"]').click(function (e) {
                var values = $(this).val();
                var id = $('#lineobject').val();
                if (values == '2') {
                    $('#stopOn').css('display', 'block');
                    showListStopObject(id);
                } else {
                    $('#stopOn').css('display', 'none');
                }
            });

            $('.row_position').sortable({
                connectWith: ".row_position",
                stop: function (e, ui) {
                    var htmlString = e.target.innerHTML.toString();
                    var split1 = htmlString.split('<tr class="context-menu-one" ');
                    var datasort = [];
                    for (var i = 0; i < split1.length; i++) {
                        if (split1[i] !== "") {
                            var split2 = split1[i].split(' class="mb-xs mt-xs mr-xs modal-sizes btn btn-xs btn-primary editMediaChoose"');
                            for (var a = 0; a < split2.length; a++) {
                                if (a == 0) {
                                    var split3 = split2[a].split('href="#addMedia" id=')[1];
                                    if (split3 !== undefined) {
                                        var split4 = split3.split('-')[1];
                                        var repl = split4.replace("\"", "");
                                        datasort.push(repl);
                                    }
                                }
                            }
                        }
                    }

                    var dataTemp = [];
                    for (var i = 0; i < datasort.length; i++) {
                        var dataconfigobj = [];
                        dataconfigobj['title'] = datapush[datasort[i]].title;
                        dataconfigobj['ttlobject'] = datapush[datasort[i]].ttlobject;
                        dataconfigobj['dayobject'] = datapush[datasort[i]].dayobject;
                        dataconfigobj['datestartobject'] = datapush[datasort[i]].datestartobject;
                        dataconfigobj['dateendobject'] = datapush[datasort[i]].dateendobject;
                        dataconfigobj['timestartobject'] = datapush[datasort[i]].timestartobject;
                        dataconfigobj['timeendobject'] = datapush[datasort[i]].timeendobject;
                        dataconfigobj['priorityobject'] = datapush[datasort[i]].priorityobject;
                        dataconfigobj['videotimeobject'] = datapush[datasort[i]].videotimeobject;
                        dataconfigobj['locationtriggerobject'] = datapush[datasort[i]].locationtriggerobject;
                        dataconfigobj['typelocation'] = datapush[datasort[i]].typelocation;
                        dataconfigobj['lineobject'] = datapush[datasort[i]].lineobject;
                        dataconfigobj['optionsRadiosLine'] = datapush[datasort[i]].optionsRadiosLine;
                        dataconfigobj['stopobject'] = datapush[datasort[i]].stopobject;
                        dataconfigobj['geofence'] = datapush[datasort[i]].geofence;
                        dataconfigobj['mediavalues'] = datapush[datasort[i]].mediavalues;
                        dataconfigobj['mediatype'] = datapush[datasort[i]].mediatype;
                        dataTemp.push(dataconfigobj);
                    }
                    datapush = [];
                    for (var i = 0; i < dataTemp.length; i++) {
                        var dataconfigobj = [];
                        dataconfigobj['title'] = dataTemp[i].title;
                        dataconfigobj['ttlobject'] = dataTemp[i].ttlobject;
                        dataconfigobj['dayobject'] = dataTemp[i].dayobject;
                        dataconfigobj['datestartobject'] = dataTemp[i].datestartobject;
                        dataconfigobj['dateendobject'] = dataTemp[i].dateendobject;
                        dataconfigobj['timestartobject'] = dataTemp[i].timestartobject;
                        dataconfigobj['timeendobject'] = dataTemp[i].timeendobject;
                        dataconfigobj['priorityobject'] = dataTemp[i].priorityobject;
                        dataconfigobj['videotimeobject'] = dataTemp[i].videotimeobject;
                        dataconfigobj['locationtriggerobject'] = dataTemp[i].locationtriggerobject;
                        dataconfigobj['typelocation'] = dataTemp[i].typelocation;
                        dataconfigobj['lineobject'] = dataTemp[i].lineobject;
                        dataconfigobj['optionsRadiosLine'] = dataTemp[i].optionsRadiosLine;
                        dataconfigobj['stopobject'] = dataTemp[i].stopobject;
                        dataconfigobj['geofence'] = dataTemp[i].geofence;
                        dataconfigobj['mediavalues'] = dataTemp[i].mediavalues;
                        dataconfigobj['mediatype'] = dataTemp[i].mediatype;
                        datapush.push(dataconfigobj);
                    }
                    var labelName = $("#addMediaChoose").attr('data-value');
                    setTablePlaylist(labelName);
                }
            });

            $("#showLoadPlayList").click(function (e) {
                var id = $(this).attr('data-value');
                $('#loadPlaylistMain').attr('data-value', id);
                showLoadPlayList(id);
            });

            $("#savePlaylist").click(function (e) {
                var id = $(this).attr('data-value');
                swal({
                    title: "Save Your Playlist",
                    text: "Please write name of your playlist :",
                    input: "text",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    inputPlaceholder: "Please enter playlist name",
                    preConfirm: (text) => {
                        return new Promise((resolve) => {
                            setTimeout(() => {
                                if (text === '') {
                                    swal.showValidationError(
                                        'You need to write the playlist name!'
                                    )
                                }
                                resolve()
                            }, 100)
                        })
                    }
                }).then((inputValue) => {
                    if (inputValue.value) {
                        saveTempImg[obj.name] = new Array();
                        saveTempImg[obj.name]['result'] = inputValue.value;
                        saveTempImg[obj.name]['type'] = 'slide';
                        var form_data = new FormData();
                        var dataplylt = []
                        var nodataplylst = 0;
                        for (var i = 0; i < datapush.length; i++) {
                            if (datapush[i].title == id) {
                                form_data.append('models[' + i + '][title]', datapush[i].title);
                                form_data.append('models[' + i + '][ttlobject]', datapush[i].ttlobject);
                                form_data.append('models[' + i + '][dayobject]', datapush[i].dayobject);
                                form_data.append('models[' + i + '][datestartobject]', datapush[i].datestartobject);
                                form_data.append('models[' + i + '][dateendobject]', datapush[i].dateendobject);
                                form_data.append('models[' + i + '][timestartobject]', datapush[i].timestartobject);
                                form_data.append('models[' + i + '][timeendobject]', datapush[i].timeendobject);
                                form_data.append('models[' + i + '][priorityobject]', datapush[i].priorityobject);
                                form_data.append('models[' + i + '][videotimeobject]', datapush[i].videotimeobject);
                                form_data.append('models[' + i + '][locationtriggerobject]', datapush[i].locationtriggerobject);
                                form_data.append('models[' + i + '][typelocation]', datapush[i].typelocation);
                                form_data.append('models[' + i + '][lineobject]', datapush[i].lineobject);
                                form_data.append('models[' + i + '][optionsRadiosLine]', datapush[i].optionsRadiosLine);
                                form_data.append('models[' + i + '][stopobject]', datapush[i].stopobject);
                                form_data.append('models[' + i + '][geofence]', datapush[i].geofence);
                                form_data.append('models[' + i + '][mediavalues]', datapush[i].mediavalues);
                                form_data.append('models[' + i + '][mediatype]', datapush[i].mediatype);

                                dataplylt.push({
                                    title: datapush[i].title
                                });
                                dataplylt[nodataplylst]['ttlobject'] = datapush[i].ttlobject;
                                dataplylt[nodataplylst]['dayobject'] = datapush[i].dayobject;
                                dataplylt[nodataplylst]['datestartobject'] = datapush[i].datestartobject;
                                dataplylt[nodataplylst]['dateendobject'] = datapush[i].dateendobject;
                                dataplylt[nodataplylst]['timestartobject'] = datapush[i].timestartobject;
                                dataplylt[nodataplylst]['timeendobject'] = datapush[i].timeendobject;
                                dataplylt[nodataplylst]['priorityobject'] = datapush[i].priorityobject;
                                dataplylt[nodataplylst]['videotimeobject'] = datapush[i].videotimeobject;
                                dataplylt[nodataplylst]['locationtriggerobject'] = datapush[i].locationtriggerobject;
                                dataplylt[nodataplylst]['typelocation'] = datapush[i].typelocation;
                                dataplylt[nodataplylst]['lineobject'] = datapush[i].lineobject;
                                dataplylt[nodataplylst]['optionsRadiosLine'] = datapush[i].optionsRadiosLine;
                                dataplylt[nodataplylst]['stopobject'] = datapush[i].stopobject;
                                dataplylt[nodataplylst]['geofence'] = datapush[i].geofence;
                                dataplylt[nodataplylst]['mediavalues'] = datapush[i].mediavalues;
                                dataplylt[nodataplylst]['mediatype'] = datapush[i].mediatype;
                                nodataplylst++;
                            }
                        }
                        form_data.append('nameplaylist', inputValue.value);
                        $.ajax({
                            url: "/admin/infovision/campaign/playlist",
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (result) {
                                var json = JSON.parse(result);
                                var objactive = canvas.getActiveObject();
                                objactive.plylstid = json.models;
                                objactive.plylstnm = inputValue.value;
                                objactive.plylstdata = dataplylt;
                                objactive.src_val = 'plylst_' + id;
                                canvas.renderAll();
                                swal("Playlist Saved!", "Your playlist: " + inputValue.value + " has been saved!", "success");
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {

                            }
                        });
                        return false;
                    }
                });
            });

            $("#clearPlayList").click(function (e) {
                var labelval = $(this).attr('data-value');
                var arr = [];
                removeItemAll(datapush, labelval);
                var objactive = canvas.getActiveObject();
                objactive.plylstdata = '';
                canvas.renderAll();
                $('#tabPlaylist-' + labelval + ' > tbody').html('<tr><td colspan="7" style="text-align: center;">No Media File Added</td></tr>');
            });
        });
    }

    function removeItemAll(arr, value) {
        var i = 0;
        while (i < arr.length) {
            if (arr[i].title === value) {
                arr.splice(i, 1);
            } else {
                ++i;
            }
        }
        return arr;
    }

    function showLoadPlayList(id) {
        $("#datatable-playlist").dataTable().fnDestroy();
        var tableRowPlayList = $('#datatable-playlist').dataTable({
            'processing': true,
            'serverSide': true,
            'ajax': {
                'url': "/admin/infovision/campaign/show/playlist",
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            'columns': [{
                    data: 'DT_RowIndex',
                    "width": "40px"
                },
                {
                    data: 'nm_playlist'
                },
                {
                    data: 'desc_playlist'
                },
                {
                    data: 'action'
                },
            ],
            'fnDrawCallback': function () {
                $(".loadDataPlayList").click(function (e) {
                    var values = $(this).attr('data-value');
                    $.ajax({
                        type: "POST",
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "/admin/infovision/campaign/show/playlist/detail",
                        data: {
                            id_playlist: values
                        },
                        cache: false,
                        success: function (result) {
                            var data = result;
                            $('#idplaylistchoose').val(values);
                            $('#tabel-detail-playlist').html('');
                            $('#slideshow-playlist').html('');
                            $('#slideshow-playlist').html('<div class="owl-carousel owl-theme" id="slideImgPlayList"></div>');
                            $('#slideImgPlayList').html('');
                            $('#namepl').text(data.models.nm_playlist);
                            $('#descpl').text(data.models.desc_playlist);
                            $('#tcpl').text(data.detail.length);
                            $('#cbpl').text(data.models.createby);
                            $('#copl').text(data.models.created_at);
                            var html;
                            var slide = '';
                            var i;
                            for (i = 0; i < data.detail.length; i++) {
                                html += '<tr>';
                                html += '<td>' + data.detail[i].src_val + '<button type="button" class="btn btn-primary" title="Playlist Information" data-container="body" data-toggle="popover" data-placement="right" data-content="<table><tr><td>Played On</td><td>: ' + data.detail[i].day_play + '</td></tr><tr><td>Play Date</td><td>: ' + data.detail[i].date_play + '</td></tr><tr><td>Play Time</td><td>: ' + data.detail[i].time_play + '</td></tr><tr><td>TTL</td><td>: ' + data.detail[i].ttl + '</td></tr><tr><td>Follow Video</td><td>: ' + data.detail[i].isvideotime + '</td></tr><tr><td>Trigger</td><td>: ' + data.detail[i].trigger + '</td></tr></table>" style="float: right;"><i class="fa fa-info"></i></button></td>';
                                slide += '<div>';
                                slide += '<img alt="" class="img-responsive" src="/admin/infovision/image/' + data.detail[i].src_val + '">';
                                slide += '</div>';
                            }
                            $('#tabel-detail-playlist').html(html);
                            $('[data-toggle="popover"]').popover({
                                html: true,
                                trigger: 'hover click'
                            });
                            $('#slideImgPlayList').html(slide);
                            $(".owl-carousel").owlCarousel({
                                margin: 0,
                                navigation: true,
                                pagination: false,
                                navigationText: ["prev", "next"],
                                items: 1
                            });
                        }
                    });
                });
            }
        });
    }

    function showListLineObject() {
        $.ajax({
            type: "GET",
            url: "/admin/fleet/stops/show/line",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                var data = result.models;
                $('#lineobject').html('');
                $('#lineobject').append('<option>- Select Line -</option>');
                for (var i = 0; i < data.length; i++) {
                    $('#lineobject').append('<option value="' + data[i].id_line + '">' + data[i].id_line + '</option>');
                }
            }
        });
    }

    function showListStopObject(id) {
        var urlStop = window.location.origin + '/admin/infovision/stops/show/stop/' + id;
        $.ajax({
            type: "GET",
            url: urlStop,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                var data = result.models;
                $('#stopobject').empty();
                var html = '';
                var statusobj = '';
                for (var i = 0; i < data.length; i++) {
                    if (statusobj == '') {
                        html = '<optgroup label="' + data[i].idlinenew + '">';
                    } else if (statusobj != data[i].idlinenew) {
                        html += '<optgroup label="' + data[i].idlinenew + '">';
                    } else if (statusobj == data[i].idlinenew) {
                        html += '<option class="' + data[i].idlinenew + '-' + data[i].direction_stop + '" value="' + data[i].id_stop + '">[' + data[i].direction_stop + '] ' + data[i].nm_stop + '</option>';
                    }
                    statusobj = data[i].idlinenew;
                }
                $('#stopobject').append(html);
                $('#stopobject').multiselect({
                    enableClickableOptGroups: true,
                    enableCollapsibleOptGroups: true,
                    enableFiltering: true,
                    includeSelectAllOption: true,
                    templates: {
                        liGroup: '<li><a href="javascript:void(0);" style="background-color: #ccc;"><input type="checkbox" class="groupStop" value="" style="margin-left: 20px;"><label class="multiselect-group" style="padding-left: 7px;font-weight: bold;"></label><input type="checkbox" class="dir-1" value="1" style="margin-left: 20px;"><span style="padding-left: 7px;font-weight: bold;">Direction 1</span><input type="checkbox" class="dir-2" value="2" style="margin-left: 20px;"><span style="padding-left: 7px;font-weight: bold;">Direction 2</span></a></li>'
                    }
                });
                $('#stopobject').multiselect('rebuild');
            }
        });
    }

    if (loc.pathname == '/admin/infovision/layout-create') {
        canvas.on('mouse:up', function (e) {
            var obj = e.target;
            if (obj == null) {
                if (selectPanel > 0) {
                    if (panel.status != 'closed') {
                        panel.close();
                        selectPanel = 0;
                    }
                }
            } else if (obj.objtype == 'labeledRect') {
                if (selectPanel > 0) {
                    if (panel.status != 'closed') {
                        panel.close();
                        selectPanel = 0;
                    }
                }
            }
        });

        canvas.on('mouse:down', function () {
            viewportTransform = canvas.viewportTransform;
            zoom = canvas.getZoom();
        });

    }

    function showListImageLayout() {
        $('#loadContentUpload').css('display', 'inline-block');
        var urlCurrent = window.location.origin;
        $.ajax({
            type: "POST",
            url: "/admin/infovision/content-upload/show",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            success: function (result) {
                $('#loadContentUpload').css('display', 'none');
                $('#showimagelayout').html('');
                $('#showmovielayout').html('');
                $('#showvideolayout').html('');
                var json = JSON.parse(result);
                var statusImg = 0;
                var statusVid = 0;
                var urlImg = "";
                var video_ext = ['mp4', 'mov', 'm4v', 'avi', 'wmv', 'flv', '3gp', 'mkv'];
                for (var i = 0; i < json.length; i++) {
                    if (json[i]['type'] !== 'mp4') {
                        urlImg = urlCurrent + '/admin/infovision/image/thumbnail/' + json[i]['name'];
                    } else {
                        var file_name = json[i]['name'];
                        video_ext.forEach(x => {
                            file_name = file_name.replace(x, 'jpg');
                        });
                        urlImg = urlCurrent + '/admin/infovision/video/thumbnail/' + file_name;
                    }
                    var html = '<div class="isotope-item document h-imgLayout col-md-3">';
                    html += '<div class="thumbnail">';
                    html += '<div class="thumb-preview">';
                    html += '<a class="thumb-image" href="javascript:void(0)">';
                    if (json[i]['type'] !== 'mp4') {
                        html += '<img src="' + urlImg + '" class="img-responsive" alt="Project" style="height: 64px;width: 115px;">';
                    } else {
                        html += '<img src="' + urlImg + '" class="img-responsive" alt="Project" style="width: 118px;height: 74px;">';
                    }
                    html += '</a>';
                    html += '</div>';
                    html += '<h5 class="mg-title text-semibold" style="width: 120px;white-space: pre;overflow: hidden;text-overflow: ellipsis;">' + json[i]['name'] + '</h5>';
                    html += '</div>';
                    html += '</div>';
                    if (json[i]['type'] !== 'mp4') {
                        $('#showimagelayout').append(html);
                        statusImg = 1;
                    } else {
                        $('#showmovielayout').append(html);
                        $('#showvideolayout').append(html);
                        statusVid = 1;
                    }
                }
                if (statusImg === 0) {
                    $('#showimagelayout').append('<p>No data images</p>');
                }
                if (statusVid === 0) {
                    $('#showmovielayout').append('<p>No data video</p>');
                    $('#showvideolayout').append('<p>No data video</p>');
                }

                $(".h-imgLayout").click(function () {
                    $('.select_imageLayout').removeClass('select_imageLayout');
                    $(this).find('.thumbnail').addClass('select_imageLayout');
                });
            }
        });
    }

    document.onkeydown = function (evt) {
        evt = evt || window.event;
        //escapekey
        if (evt.keyCode == 46) {
            deleteObjects();
        }
    };

    function deleteObjects() {
        var activeObject = canvas.getActiveObject(),
            activeGroup = canvas.getActiveGroup();
        if (activeObject) {
            if (confirm('Are you sure?')) {
                canvas.remove(activeObject);
                removeteks(activeObject.name);
                panel.close();
                selectPanel = 0;
            }
        } else if (activeGroup) {
            if (confirm('Are you sure?')) {
                var objectsInGroup = activeGroup.getObjects();
                canvas.discardActiveGroup();
                objectsInGroup.forEach(function (object) {
                    canvas.remove(object);
                    removeteks(object.name);
                    panel.close();
                    selectPanel = 0;
                });
            }
        }
    }

    function removeteks(targetid) {
        if (document.getElementById(targetid) != null) {
            document.getElementById(targetid).remove();
        }
    }

    //todo
    //bug
    // 

    if (loc.pathname == '/admin/infovision/layout-create') {
        showListImageLayout();
        $('.datepicker').datepicker({
            format: 'yyyy/mm/dd'
        });
    }

    $("#selectImageLayout").click(function (e) {
        var imgname = $('.select_imageLayout').find('.mg-title').text();
        $('#imageName').text(imgname);
        $('#thumbnail-layout').attr('src', '/admin/infovision/image/thumbnail/' + imgname);
        var name = $('#objName').val();
        saveTempImg[name] = new Array();
        saveTempImg[name]['result'] = imgname;
        saveTempImg[name]['type'] = 'image';
        var objactive = canvas.getActiveObject();
        objactive.src_val = imgname;
        canvas.renderAll();
        e.preventDefault();
        $.magnificPopup.close();
    });

    $("#selectVideoLayout").click(function (e) {
        var imgname = $('.select_imageLayout').find('.mg-title').text();
        $('#imageName').text(imgname);
        var name = $('#objName').val();
        saveTempImg[name] = new Array();
        saveTempImg[name]['result'] = imgname;
        saveTempImg[name]['type'] = 'movie';
        var objactive = canvas.getActiveObject();
        objactive.src_val = imgname;
        canvas.renderAll();
        e.preventDefault();
        $.magnificPopup.close();
    });

    $("#picksourceobject").click(function (e) {
        showListImageLayout();
        $('#textAreaSum').html('<div class="summernote" id="editorText" data-plugin-summernote data-plugin-options="{ "height": 180, "codemirror": { "theme": "ambiance" } }"">Start typing...</div>');
        $('#editorText').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']]
            ]
        });
    });

    $("#textBgImg").click(function (e) {
        $('#selectTextBgImage').css('display', 'inline-block');
        $('#selectImageLayout').css('display', 'none');
    });

    $("#selectTextBgImage").click(function (e) {
        $('#selectTextBgImage').css('display', 'none');
        $('#selectImageLayout').css('display', 'inline-block');
        var imgname = $('.select_imageLayout').find('.mg-title').text();
        $('#mediaFileImg').text(imgname);
        $('#picksourceobject').click();
    });

    $("#selectAddPro").click(function (e) {
        $('#picksourceobject').click();
    });

    var mediaobj = [];

    $("#selectMediaObject").click(function (e) {
        mediaobj = [];
        var values = $('.tab-media-object .active').attr('id');
        if (values == 'picture') {
            var imgname = $('.select_imageLayout').find('.mg-title').text();
            mediaobj['values'] = imgname;
            mediaobj['type'] = 'picture';
            var val = imgname;
        } else if (values == 'movie') {
            var imgname = $('.select_imageLayout').find('.mg-title').text();
            mediaobj['values'] = imgname;
            mediaobj['type'] = 'movie';
            var val = imgname;
        } else if (values == 'text') {
            var valText = $('.note-editable').html();
            var valArrow = $('#arrowText').val();
            var valBgColor = $('#colorBgText').val();
            var valBgImg = $('#mediaFileImg').text();
            mediaobj['values'] = valText + '|' + valArrow + '|' + valBgColor + '|' + valBgImg;
            mediaobj['type'] = 'text';
            var val = 'Text Data';
        } else if (values == 'url') {
            var urlMedia = $('#urlMediaObject').val();
            mediaobj['values'] = urlMedia;
            mediaobj['type'] = 'url';
            var val = urlMedia;
        } else if (values == 'api') {
            var valrss = $('#rss_source').val() + '|' + $('#rss_category').val() + '|' + $('#rss_stylesheet').val();
            mediaobj['values'] = valrss;
            mediaobj['type'] = 'url';
            var val = 'API RSS';
        }
        $('#textpicksourceobject').text('[' + mediaobj['type'] + '] ' + val);
        $('#addMediaChoose').click();
    });

    var datapush = [];
    $("#saveConfigObjectPro").click(function (e) {
        var labelName = $("#addMediaChoose").attr('data-value');
        var dayArray = [];
        $('.selectDay').each(function (i, e) {
            dayArray.push($(e).attr('id'));
        });
        var dataconfigobj = [];
        var dayselected = dayArray.toString();
        dataconfigobj['title'] = labelName;
        dataconfigobj['ttlobject'] = $('#ttlobject').val();
        dataconfigobj['dayobject'] = dayselected;
        dataconfigobj['datestartobject'] = $('#datestartobject').val();
        dataconfigobj['dateendobject'] = $('#dateendobject').val();
        dataconfigobj['timestartobject'] = $('#timestartobject').val();
        dataconfigobj['timeendobject'] = $('#timeendobject').val();
        dataconfigobj['priorityobject'] = $('#priorityobject').val();
        dataconfigobj['videotimeobject'] = $('#videotimeobject').val();
        dataconfigobj['locationtriggerobject'] = $('#locationtriggerobject').val();
        dataconfigobj['typelocation'] = ($("input[name='optionsRadiosLocation']:checked").val() !== undefined) ? $("input[name='optionsRadiosLocation']:checked").val() : '';
        dataconfigobj['lineobject'] = $("#lineobject").val();
        dataconfigobj['optionsRadiosLine'] = ($("input[name='optionsRadiosLine']:checked").val() !== undefined) ? $("input[name='optionsRadiosLine']:checked").val() : '';
        dataconfigobj['stopobject'] = $('#stopobject').val();
        dataconfigobj['geofence'] = $('#geofence-title').val();
        dataconfigobj['mediavalues'] = mediaobj['values'];
        dataconfigobj['mediatype'] = mediaobj['type'];
        datapush.push(dataconfigobj);

        var dataplylt = []
        var nodataplylst = 0;
        for (var i = 0; i < datapush.length; i++) {
            if (datapush[i].title == labelName) {
                dataplylt.push({
                    title: datapush[i].title
                });
                dataplylt[nodataplylst]['ttlobject'] = datapush[i].ttlobject;
                dataplylt[nodataplylst]['dayobject'] = datapush[i].dayobject;
                dataplylt[nodataplylst]['datestartobject'] = datapush[i].datestartobject;
                dataplylt[nodataplylst]['dateendobject'] = datapush[i].dateendobject;
                dataplylt[nodataplylst]['timestartobject'] = datapush[i].timestartobject;
                dataplylt[nodataplylst]['timeendobject'] = datapush[i].timeendobject;
                dataplylt[nodataplylst]['priorityobject'] = datapush[i].priorityobject;
                dataplylt[nodataplylst]['videotimeobject'] = datapush[i].videotimeobject;
                dataplylt[nodataplylst]['locationtriggerobject'] = datapush[i].locationtriggerobject;
                dataplylt[nodataplylst]['typelocation'] = datapush[i].typelocation;
                dataplylt[nodataplylst]['lineobject'] = datapush[i].lineobject;
                dataplylt[nodataplylst]['optionsRadiosLine'] = datapush[i].optionsRadiosLine;
                dataplylt[nodataplylst]['stopobject'] = datapush[i].stopobject;
                dataplylt[nodataplylst]['geofence'] = datapush[i].geofence;
                dataplylt[nodataplylst]['mediavalues'] = datapush[i].mediavalues;
                dataplylt[nodataplylst]['mediatype'] = datapush[i].mediatype;
                nodataplylst++;
            }
        }

        saveTempImg[labelName] = new Array();
        saveTempImg[labelName]['result'] = 'plylst_' + labelName;
        saveTempImg[labelName]['type'] = 'slide';

        var objactive = canvas.getActiveObject();
        objactive.plylstdata = dataplylt;
        canvas.renderAll();
        setTablePlaylist(labelName);
        resetFormPlayList();
        e.preventDefault();
        $.magnificPopup.close();
    });

    function resetFormPlayList() {
        $('#formObjProData')[0].reset();
        $('#textpicksourceobject').text('Selected None');
        $('.date-object').removeClass('selectDay');
        $('#typeLoc').css('display', 'none');
        $('#Geo').css('display', 'none');
        $('#geofence-title').empty();
        $('.geofencer-map').css('display', 'none');
        $('#lineOn').css('display', 'none');
        $('#lineobject').val('').trigger('change');
        $('#stopobject').empty();
        $('.multiselect').attr('title', 'None selected');
        $('.multiselect').html('None selected');
        $('#stopOn').css('display', 'none');
    }

    $("#simFillObjectPro").click(function (e) {
        resetFormPlayList();
        $.ajax({
            type: "GET",
            url: "/admin/infovision/campaign/show/image/first",
            cache: false,
            success: function (result) {
                var json = result;
                if (result.type == 'mp4') {
                    var type = 'movie';
                } else {
                    var type = 'picture';
                }
                var text = '[' + type + '] ' + result.name;
                $('#textpicksourceobject').text(text);
                mediaobj['values'] = result.name;
                mediaobj['type'] = type;
            }
        });
        $('#ttlobject').val('10');
        $('.date-object').addClass('selectDay');
        $('#datestartobject').val(nowDate());
        $('#dateendobject').val(beforeDate());
        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes();
        $('#timestartobject').timepicker('setTime', time);
        $('#timeendobject').timepicker('setTime', time);
        // $('#timestartobject').val(getTime());
        // $('#timeendobject').val(getTime());
        $('#priorityobject').val('1');
        $('#videotimeobject').val('Yes');
        $('#locationtriggerobject').val('No');
    });

    function setTablePlaylist(labelval) {
        $('#tabPlaylist-' + labelval + ' > tbody').html('');
        console.log(datapush);
        var html;
        var no = 1;
        var checkOrNoData = 0;
        var i;
        for (i = 0; i < datapush.length; i++) {
            if (labelval == datapush[i].title) {
                html += '<tr class="context-menu-one" id="' + datapush[i].title + '-' + i + '">';
                html += '<td><a href="#addMedia" id="' + datapush[i].title + '-' + i + '" class="mb-xs mt-xs mr-xs modal-sizes btn btn-xs btn-primary editMediaChoose"><i class="fa fa-edit"></i></a><a href="javascript:void(0)" id="' + datapush[i].title + '-' + i + '"  class="mb-xs mt-xs mr-xs btn btn-xs btn-danger deleteMediaChoose"><i class="fa fa-trash"></i></a> ' + no + '</td>';
                html += '<td>' + datapush[i].mediavalues + '</td>';
                html += '<td>' + datapush[i].dayobject.split(',').length + '</td>';
                html += '<td>' + datapush[i].datestartobject + '-' + datapush[i].dateendobject + '</td>';
                html += '<td>' + datapush[i].timestartobject + '-' + datapush[i].timeendobject + '</td>';
                html += '<td>' + datapush[i].priorityobject + '</td>';
                if (datapush[i].locationtriggerobject === 'Yes' || datapush[i].locationtriggerobject === 'halte:all') {
                    var htmltrigger;
                    if (datapush[i].typelocation === '1' || datapush[i].typelocation === 'line') {
                        var typelocation = 'By Line';
                        var valLine = datapush[i].lineobject.toString();
                        if (datapush[i].optionsRadiosLine === '1' || datapush[i].stopobject === null) {
                            var typeline = 'Whole Line';
                            htmltrigger = 'Type Location : ' + typelocation + ' </br></br>Line : ' + valLine + '</br></br>Type Line : ' + typeline;
                        } else {
                            var typeline = 'Certain Stop Only ';
                            var valstop = datapush[i].stopobject.toString();
                            htmltrigger = 'Type Location : ' + typelocation + ' </br></br>Line : ' + valLine + '</br></br>Type Line : ' + typeline + '</br></br>Stop : ' + valstop;
                        }
                    } else {
                        var typelocation = 'By Geofence';
                        var valgeofence = datapush[i].geofence.toString();
                        htmltrigger = 'Type Location : ' + typelocation + ' </br></br>Geofence : ' + valgeofence;
                    }


                    var descTrigger = '<a href="javascript:void(0)"><i class="fa fa-2x fa-bolt" data-toggle="popover" title="Information Trigger" data-content="' + htmltrigger + '"></i></a>';
                } else {
                    var descTrigger = 'No';
                }
                html += '<td>' + descTrigger + '</td>';
                html += '<tr>';
                no++;
                checkOrNoData = 1;
            }
        }

        if (checkOrNoData === 0) {
            html = '<tr><td colspan="7" style="text-align: center;">No Media File Added</td></tr>';
        }

        $('#tabPlaylist-' + labelval + ' > tbody').html(html);

        $('[data-toggle="popover"]').popover({
            html: true,
            container: 'body'
        });

        $('html').on('click', function (e) {
            if (typeof $(e.target).data('original-title') == 'undefined' && !$(e.target).parents().is('.popover.in')) {
                $('[data-original-title]').popover('hide');
            }
        });

        $('[data-toggle="popover"]').on('click', function () {
            $('[data-toggle="popover"]').not(this).popover('hide');
        });
        
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                if (key == 'copy') {
                    var m = this.attr('id');
                    var split = m.split('-');
                    var noarray = split[1];
                    var longarray = datapush.length;
                    var dataconfigobj = [];
                    dataconfigobj['title'] = datapush[noarray].title;
                    dataconfigobj['ttlobject'] = datapush[noarray].ttlobject;
                    dataconfigobj['dayobject'] = datapush[noarray].dayobject;
                    dataconfigobj['datestartobject'] = datapush[noarray].datestartobject;
                    dataconfigobj['dateendobject'] = datapush[noarray].dateendobject;
                    dataconfigobj['timestartobject'] = datapush[noarray].timestartobject;
                    dataconfigobj['timeendobject'] = datapush[noarray].timeendobject;
                    dataconfigobj['priorityobject'] = datapush[noarray].priorityobject;
                    dataconfigobj['videotimeobject'] = datapush[noarray].videotimeobject;
                    dataconfigobj['locationtriggerobject'] = datapush[noarray].locationtriggerobject;
                    dataconfigobj['typelocation'] = datapush[noarray].typelocation;
                    dataconfigobj['lineobject'] = datapush[noarray].lineobject;
                    dataconfigobj['optionsRadiosLine'] = datapush[noarray].optionsRadiosLine;
                    dataconfigobj['stopobject'] = datapush[noarray].stopobject;
                    dataconfigobj['geofence'] = datapush[noarray].geofence;
                    dataconfigobj['mediavalues'] = datapush[noarray].mediavalues;
                    dataconfigobj['mediatype'] = datapush[noarray].mediatype;
                    datapush.push(dataconfigobj);
                    var labelName = $("#addMediaChoose").attr('data-value');
                    var dataplylt = []
                    var nodataplylst = 0;
                    for (var i = 0; i < datapush.length; i++) {
                        if (datapush[i].title == labelName) {
                            dataplylt.push({
                                title: datapush[i].title
                            });
                            dataplylt[nodataplylst]['ttlobject'] = datapush[i].ttlobject;
                            dataplylt[nodataplylst]['dayobject'] = datapush[i].dayobject;
                            dataplylt[nodataplylst]['datestartobject'] = datapush[i].datestartobject;
                            dataplylt[nodataplylst]['dateendobject'] = datapush[i].dateendobject;
                            dataplylt[nodataplylst]['timestartobject'] = datapush[i].timestartobject;
                            dataplylt[nodataplylst]['timeendobject'] = datapush[i].timeendobject;
                            dataplylt[nodataplylst]['priorityobject'] = datapush[i].priorityobject;
                            dataplylt[nodataplylst]['videotimeobject'] = datapush[i].videotimeobject;
                            dataplylt[nodataplylst]['locationtriggerobject'] = datapush[i].locationtriggerobject;
                            dataplylt[nodataplylst]['typelocation'] = datapush[i].typelocation;
                            dataplylt[nodataplylst]['lineobject'] = datapush[i].lineobject;
                            dataplylt[nodataplylst]['optionsRadiosLine'] = datapush[i].optionsRadiosLine;
                            dataplylt[nodataplylst]['stopobject'] = datapush[i].stopobject;
                            dataplylt[nodataplylst]['geofence'] = datapush[i].geofence;
                            dataplylt[nodataplylst]['mediavalues'] = datapush[i].mediavalues;
                            dataplylt[nodataplylst]['mediatype'] = datapush[i].mediatype;
                            nodataplylst++;
                        }
                    }

                    saveTempImg[labelName] = new Array();
                    saveTempImg[labelName]['result'] = 'plylst_' + labelName;
                    saveTempImg[labelName]['type'] = 'slide';

                    var objactive = canvas.getActiveObject();
                    objactive.plylstdata = dataplylt;
                    canvas.renderAll();
                    setTablePlaylist(labelName);
                    $('.modal-sizes').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in',
                        modal: true
                    });

                    $(".editMediaChoose").click(function (e) {
                        var values = $(this).attr('id');
                        var split = values.split('-');
                        var valarray = datapush[split[1]];
                        $('#editConfigObjectPro').css('display', 'inline-block');
                        $('#editConfigObjectPro').attr('data-value', split[1]);
                        $('#saveConfigObjectPro').css('display', 'none');
                        $('#textpicksourceobject').text(valarray.mediatype + ' ' + valarray.mediavalues);
                        $('#ttlobject').val(valarray.ttlobject);
                        $('.date-object').removeClass('selectDay');
                        var splitday = valarray.dayobject.split(',');
                        for (i = 0; i < splitday.length; i++) {
                            $('#' + splitday[i]).addClass('selectDay');
                        }
                        $('#datestartobject').val(valarray.datestartobject);
                        $('#dateendobject').val(valarray.dateendobject);
                        $('#timestartobject').val(valarray.timestartobject);
                        $('#timeendobject').val(valarray.timeendobject);
                        $('#priorityobject').val(valarray.priorityobject);
                        $('#videotimeobject').val(valarray.videotimeobject);
                        var loctrigger;
                        if (valarray.locationtriggerobject == '-') {
                            loctrigger = '';
                        } else {
                            loctrigger = valarray.locationtriggerobject;
                        }
                        $('#locationtriggerobject').val(loctrigger);
                        $('#typelocation').val(valarray.typelocation);
                        $("#lineobject").val(valarray.lineobject);
                        $("input[value='" + valarray.optionsRadiosLine + "']").prop('checked', true);
                        $('#stopobject').val(valarray.stopobject);
                        $('#geofence').val(valarray.geofence);
                    });
                }
            },
            items: {
                "copy": {
                    name: "Clone this playlist item",
                    icon: "copy"
                },
                "sep1": "---------",
                "quit": {
                    name: "Cancel",
                    icon: function () {
                        return "context-menu-icon context-menu-icon-quit";
                    }
                }
            }
        });

        $('.modal-sizes').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            modal: true
        });

        $(".editMediaChoose").click(function (e) {
            var values = $(this).attr('id');
            var split = values.split('-');
            var valarray = datapush[split[1]];
            $('#editConfigObjectPro').css('display', 'inline-block');
            $('#editConfigObjectPro').attr('data-value', split[1]);
            $('#saveConfigObjectPro').css('display', 'none');
            $('#typeLoc').css('display', 'none');
            $('#lineOn').css('display', 'none');
            $('#lineobject').val('').trigger('change');
            $('#stopOn').css('display', 'none');
            $('#stopobject').val('');
            $('#stopobject').multiselect('rebuild');
            $('#Geo').css('display', 'none');
            $('#geofence-title').val('');
            $('#geofence-title').multiselect('rebuild');
            $('.geofencer-map').css('display', 'none');
            $('#textpicksourceobject').text(valarray.mediatype + ' ' + valarray.mediavalues);
            $('#ttlobject').val(valarray.ttlobject);
            $('.date-object').removeClass('selectDay');
            if (valarray.dayobject === 'all') {
                var splitday = ['day-0', 'day-1', 'day-2', 'day-3', 'day-4', 'day-5', 'day-6', 'day-7']
            } else {
                var splitday = valarray.dayobject.split(',');
            }
            for (i = 0; i < splitday.length; i++) {
                $('#' + splitday[i]).addClass('selectDay');
            }
            $('#datestartobject').val(valarray.datestartobject);
            $('#dateendobject').val(valarray.dateendobject);
            $('#timestartobject').val(valarray.timestartobject);
            $('#timeendobject').val(valarray.timeendobject);
            $('#priorityobject').val(valarray.priorityobject);
            $('#videotimeobject').val(valarray.videotimeobject);
            var loctrigger;
            var typeline = 1;
            if (valarray.locationtriggerobject == '-' || valarray.locationtriggerobject == 'No') {
                loctrigger = 'No';
            } else {
                loctrigger = 'Yes';
                $('#typeLoc').css('display', 'block');
                if (valarray.typelocation === 'line' || valarray.typelocation === '1') {
                    if (valarray.lineobject !== null) {
                        if (Array.isArray(valarray.lineobject) === true) {
                            var vallineobject = valarray.lineobject;
                        } else {
                            var vallineobject = valarray.lineobject.split(',');
                        }
                        $('#lineOn').css('display', 'block');
                        showListLineObjectEdit(vallineobject);
                    }
                    if (valarray.stopobject !== null) {
                        if (Array.isArray(valarray.stopobject) === true) {
                            var valstopobject = valarray.stopobject;
                        } else {
                            var valstopobject = valarray.stopobject.split(',');
                        }
                        $('#stopOn').css('display', 'block');
                        showListStopObjectEdit(vallineobject, valstopobject);
                        typeline = 2;
                    }
                    var typelocation = 1;
                } else {
                    if (valarray.geofence !== null) {
                        $('#Geo').css('display', 'block');
                        if (Array.isArray(valarray.geofence) === true) {
                            var valgeofence = valarray.geofence;
                        } else {
                            var valgeofence = valarray.geofence.split(',');
                        }
                        getShowListGeofenceEdit(valgeofence);
                    }
                    var typelocation = 2;
                }
            }
            $('#locationtriggerobject').val(loctrigger);
            $("input[name='optionsRadiosLocation'][value='" + typelocation + "']").prop('checked', true);
            $("input[name='optionsRadiosLine'][value='" + typeline + "']").prop('checked', true);
        });

        function getShowListGeofenceEdit(val) {
            $.ajax({
                type: "GET",
                url: '/admin/infovision/layout-create/list/geofence',
                cache: false,
                success: function (result) {
                    var models = result.models;
                    $('#geofence-title').empty();
                    var html = '';
                    var statusobj = '';
                    for (var i = 0; i < models.length; i++) {
                        html += '<option value="' + models[i].id_geofence + '">' + models[i].name + '</option>';
                    }
                    $('#geofence-title').append(html);
                    $('#geofence-title').multiselect({
                        enableClickableOptGroups: true,
                        enableCollapsibleOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true
                    });
                    $('#geofence-title').val(val);
                    $('#geofence-title').multiselect('rebuild');
                    $('.geofencer-map').css('display', 'block');
                    var id = val.toString();
                    infoPolygonEdit(id);
                }
            });
        }

        function infoPolygonEdit(id) {
            totalLatLng = [];
            upObjLabel = new Object();
            $.ajax({
                type: "GET",
                url: "/admin/infovision/layout-create/info/geofence",
                data: {
                    id: id
                },
                cache: false,
                success: function (result) {
                    var detail = result.models;
                    var statusDis = false;
                    for (let a = 0; a < detail.length; a++) {
                        statusDis = true;
                        text = '';
                        var split1fixs = detail[a]['latlong'].split('|');
                        for (let i = 0; i < split1fixs.length; i++) {
                            var split2fixs = split1fixs[i].split(',');
                            text += '(' + split2fixs[0] + ', ' + split2fixs[1] + ')|'
                        }
                        totalLatLng.push(text);
                        if (detail[a]['label'] !== '' && detail[a]['label'] !== null && detail[a]['label'] !== 'undefined') {
                            upObjLabel[detail[a]['name_polygon']] = detail[a]['label'];
                        }
                    }
                    if (statusDis == true) {
                        $('.geofencer-map').css('display', 'block');
                    }
                    initializeEdit();
                }
            });
        }

        function initializeEdit() {
            if (map == null) {
                map = new GeofencingMap('map').map;
                polygon = new MultiPolygon(map, $('#polygon-name').val());
            } else {
                polygon.deleteAllPolygons();
            }
            if (totalLatLng.length > 0) {
                for (var a = 0; a < totalLatLng.length; a++) {
                    var coords = new Array();
                    var splitLatLng = totalLatLng[a].split("|");
                    for (var i = 0; i < splitLatLng.length; i++) {
                        var latlng = splitLatLng[i].trim().substring(1, splitLatLng[i].length - 1).split(",");
                        if (latlng.length > 1) {
                            coords.push(L.latLng(latlng[0], latlng[1]));
                        }
                        if (a == 0 && i == 0) {
                            map.setView([latlng[0], latlng[1]], 5);
                        }
                    }
                    polygon.addPolygon(coords, totalLatLng.length, upObjLabel, true);
                    polygon.setAllowDragging(draggable);
                    polygon.setEditable(true);
                }
            } else {
                var coords = new Array();
                polygon.addPolygon(coords, totalLatLng.length, upObjLabel, true);
                polygon.setAllowDragging(draggable);
                polygon.setEditable(true);
            }

            polygon.setAllowDragging(draggable);
            polygon.setEditable(false);
        }

        function showListLineObjectEdit(val) {
            $.ajax({
                type: "GET",
                url: "/admin/fleet/stops/show/line",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    var data = result.models;
                    $('#lineobject').html('');
                    $('#lineobject').append('<option>- Select Line -</option>');
                    for (var i = 0; i < data.length; i++) {
                        $('#lineobject').append('<option value="' + data[i].id_line + '">' + data[i].id_line + '</option>');
                    }
                    $("#lineobject").val(val).change();
                }
            });
        }
        
        function showListStopObjectEdit(id, val) {
            var urlStop = window.location.origin + '/admin/fleet/stops/show/stop/' + id;
            $.ajax({
                type: "GET",
                url: urlStop,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function (result) {
                    var data = result.models;
                    $('#stopobject').empty();
                    var html = '';
                    var statusobj = '';
                    for (var i = 0; i < data.length; i++) {
                        if (statusobj == '') {
                            html = '<optgroup label="' + data[i].idlinenew + '">';
                        } else if (statusobj != data[i].idlinenew) {
                            html += '<optgroup label="' + data[i].idlinenew + '">';
                        } else if (statusobj == data[i].idlinenew) {
                            html += '<option class="' + data[i].idlinenew + '-' + data[i].direction_stop + '" value="' + data[i].id_stop + '">[' + data[i].direction_stop + '] ' + data[i].nm_stop + '</option>';
                        }
                        statusobj = data[i].idlinenew;
                    }
                    $('#stopobject').append(html);
                    $('#stopobject').multiselect({
                        enableClickableOptGroups: true,
                        enableCollapsibleOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        templates: {
                            liGroup: '<li><a href="javascript:void(0);" style="background-color: #ccc;"><input type="checkbox" class="groupStop" value="" style="margin-left: 20px;"><label class="multiselect-group" style="padding-left: 7px;font-weight: bold;"></label><input type="checkbox" class="dir-1" value="1" style="margin-left: 20px;"><span style="padding-left: 7px;font-weight: bold;">Direction 1</span><input type="checkbox" class="dir-2" value="2" style="margin-left: 20px;"><span style="padding-left: 7px;font-weight: bold;">Direction 2</span></a></li>'
                        }
                    });
                    $('#stopobject').val(val);
                    $('#stopobject').multiselect('rebuild');
                }
            });
        }

        $("#cancelConfigObjectPro").click(function (e) {
            $('#editConfigObjectPro').css('display', 'none');
            $('#editConfigObjectPro').attr('data-value', '');
            $('#saveConfigObjectPro').css('display', 'inline-block');
            $('#formObjProData')[0].reset();
            $('#textpicksourceobject').text('Selected None');
            $('.date-object').removeClass('selectDay');
            resetFormPlayList();
        });

        $(".deleteMediaChoose").click(function (e) {
            var values = $(this).attr('id');
            var split = values.split('-');
            datapush.splice(split[1], 1);
            $("#" + values).remove();
        });
    }

    $("#editConfigObjectPro").click(function (e) {
        var values = $(this).attr('data-value');
        var labelName = $("#addMediaChoose").attr('data-value');
        var dayArray = [];
        $('.selectDay').each(function (i, e) {
            dayArray.push($(e).attr('id'));
        });
        var dayselected = dayArray.toString();
        var dataconfigobj = [];
        datapush[values].title = labelName;
        datapush[values].ttlobject = $('#ttlobject').val();
        datapush[values].dayobject = dayselected;
        datapush[values].datestartobject = $('#datestartobject').val();
        datapush[values].dateendobject = $('#dateendobject').val();
        datapush[values].timestartobject = $('#timestartobject').val();
        datapush[values].timeendobject = $('#timeendobject').val();
        datapush[values].priorityobject = $('#priorityobject').val();
        datapush[values].videotimeobject = $('#videotimeobject').val();
        datapush[values].locationtriggerobject = $('#locationtriggerobject').val();
        datapush[values].typelocation = ($("input[name='optionsRadiosLocation']:checked").val() !== undefined) ? $("input[name='optionsRadiosLocation']:checked").val() : '';
        datapush[values].lineobject = $("#lineobject").val();
        datapush[values].optionsRadiosLine = ($("input[name='optionsRadiosLine']:checked").val() !== undefined) ? $("input[name='optionsRadiosLine']:checked").val() : '';
        datapush[values].stopobject = $('#stopobject').val();
        datapush[values].geofence = $('#geofence').val();
        datapush[values].mediavalues = mediaobj['values'];
        datapush[values].mediatype = mediaobj['type'];
        console.log(datapush);
        setTablePlaylist(labelName);
        $('#editConfigObjectPro').css('display', 'none');
        $('#editConfigObjectPro').attr('data-value', '');
        $('#saveConfigObjectPro').css('display', 'inline-block');
        $('#formObjProData')[0].reset();
        $('#textpicksourceobject').text('Selected None');
        $('.date-object').removeClass('selectDay');
        e.preventDefault();
        $.magnificPopup.close();
    });

    $("#loadPlaylistMain").click(function (e) {
        var values = $(this).attr('data-value');
        var id = $('#idplaylistchoose').val();
        $.ajax({
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            url: "/admin/infovision/campaign/show/playlist/detail",
            data: {
                id_playlist: id
            },
            cache: false,
            success: function (result) {
                var data = result;
                var triggerval = "";
                for (i = 0; i < data.detail.length; i++) {
                    var dataconfigobj = [];
                    var dayselected = 'day-0,day-7,day-1,day-2,day-3,day-4,day-5,day-6';
                    var splitdate = data.detail[i].date_play.split('-');
                    var splittime = data.detail[i].time_play.split('-');
                    if (data.detail[i].trigger == 'halte:all') {
                        triggerval = "Yes";
                    } else {
                        triggerval = "No";
                    }
                    dataconfigobj['title'] = values;
                    dataconfigobj['ttlobject'] = data.detail[i].ttl;
                    dataconfigobj['dayobject'] = dayselected;
                    dataconfigobj['datestartobject'] = splitdate[0];
                    dataconfigobj['dateendobject'] = splitdate[1];
                    dataconfigobj['timestartobject'] = splittime[0];
                    dataconfigobj['timeendobject'] = splittime[1];
                    dataconfigobj['priorityobject'] = data.detail[i].priority;
                    dataconfigobj['videotimeobject'] = data.detail[i].isvideotime;
                    dataconfigobj['locationtriggerobject'] = triggerval;
                    dataconfigobj['typelocation'] = data.detail[i].typelocation;
                    dataconfigobj['lineobject'] = data.detail[i].line;
                    dataconfigobj['optionsRadiosLine'] = 1;
                    dataconfigobj['stopobject'] = '';
                    dataconfigobj['geofence'] = data.detail[i].geofence;
                    dataconfigobj['mediavalues'] = data.detail[i].src_val;
                    dataconfigobj['mediatype'] = 'image';
                    datapush.push(dataconfigobj);
                }

                var dataplylt = []
                var nodataplylst = 0;
                for (var i = 0; i < datapush.length; i++) {
                    if (datapush[i].title == values) {
                        dataplylt.push({
                            title: datapush[i].title
                        });
                        dataplylt[nodataplylst]['ttlobject'] = datapush[i].ttlobject;
                        dataplylt[nodataplylst]['dayobject'] = datapush[i].dayobject;
                        dataplylt[nodataplylst]['datestartobject'] = datapush[i].datestartobject;
                        dataplylt[nodataplylst]['dateendobject'] = datapush[i].dateendobject;
                        dataplylt[nodataplylst]['timestartobject'] = datapush[i].timestartobject;
                        dataplylt[nodataplylst]['timeendobject'] = datapush[i].timeendobject;
                        dataplylt[nodataplylst]['priorityobject'] = datapush[i].priorityobject;
                        dataplylt[nodataplylst]['videotimeobject'] = datapush[i].videotimeobject;
                        dataplylt[nodataplylst]['locationtriggerobject'] = datapush[i].locationtriggerobject;
                        dataplylt[nodataplylst]['typelocation'] = datapush[i].typelocation;
                        dataplylt[nodataplylst]['lineobject'] = datapush[i].lineobject;
                        dataplylt[nodataplylst]['optionsRadiosLine'] = datapush[i].optionsRadiosLine;
                        dataplylt[nodataplylst]['stopobject'] = datapush[i].stopobject;
                        dataplylt[nodataplylst]['geofence'] = datapush[i].geofence;
                        dataplylt[nodataplylst]['mediavalues'] = datapush[i].mediavalues;
                        dataplylt[nodataplylst]['mediatype'] = datapush[i].mediatype;
                        nodataplylst++;
                    }
                }

                var objactive = canvas.getActiveObject();
                objactive.plylstdata = dataplylt;
                canvas.renderAll();

                saveTempImg[values] = new Array();
                saveTempImg[values]['result'] = 'plylst_' + values;
                saveTempImg[values]['type'] = 'slide';

                setTablePlaylist(values);
                e.preventDefault();
                $.magnificPopup.close();
            }
        });
    });

    function slideShowAuto(length) {
        if (nomorslide >= length) {
            nomorslide = 0;
        }
        $('.group-slide').css('opacity', '0');
        $('.group-slide').css('transition', 'opacity 0.3s, visibility 0.3s');
        $('#slide-' + nomorslide).css('opacity', '1');
        $('#slide-' + nomorslide).css('transition', 'opacity 0.3s, visibility 0.3s');
        nomorslide++;
        setSlideImage = setTimeout(function () {
            slideShowAuto(length);
        }, 5000);
    }

    $(".prevClose").click(function (e) {
        clearTimeout(setSlideImage);
    });

    function showLayoutManager() {
        $.ajax({
            type: "GET",
            url: "/admin/infovision/campaign/show/layout/manager",
            cache: false,
            success: function (result) {
                var json = result;
                var models = json['models'];
                var html = '';
                for (var i = 0; i < models.length; i++) {
                    var bgid = models[i].bg_layout;
                    if (bgid == 'BG001') {
                        var screen = '1366x768';
                    } else if (bgid == 'BG002') {
                        var screen = '1920x540';
                    } else if (bgid == 'BG003') {
                        var screen = '1024x768';
                    } else if (bgid == 'BG004') {
                        var screen = '1920x1080';
                    }
                    html += '<div class="isotope-item document col-xs-6 col-md-3">';
                    html += '<div class="thumbnail h-layoutmanager context-menu-two" id="' + models[i].id_layout + '">';
                    html += '<div class="thumb-preview">';
                    if (accesseditlayout == 1) {
                        html += '<a class="thumb-image" href="/admin/infovision/layout-create?id=' + models[i].id_layout + '">';
                    } else {
                        html += '<a class="thumb-image" href="javascript:void(0)">';
                    }
                    html += '<img src="/admin/infovision/layout/' + models[i].pic_layout + '" class="img-responsive" alt="Project">';
                    html += '</a>';
                    html += '</div>';
                    html += '<h5 class="mg-title text-semibold">' + models[i].nm_layout + '</h5>';
                    html += '<small>By: ' + models[i].user + '</small></br>';
                    html += '<small>On: ' + models[i].created_at.replace('T', ' ').replace('.000000Z', '') + '</small></br>';
                    html += '<small>Dimension: ' + screen + '</small>';
                    html += '<div style="text-align: right;">';
                    html += '<a href="#modalDisplayScreen" id="' + models[i].id_layout + '" class="mb-xs mt-xs mr-xs modal-sizes btn btn-info prevLayoutManager"><i class="fa fa-eye"></i></a>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    $('#showLayoutManager').html(html);
                }

                $('.modal-sizes').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in',
                    modal: true
                });

                $(".prevLayoutManager").click(function (e) {
                    var id = $(this).attr('id');
                    $.ajax({
                        type: "GET",
                        url: "/admin/infovision/campaign/prev/layout/manager",
                        data: {
                            id: id
                        },
                        cache: false,
                        success: function (result) {
                            var models = result.models;
                            var detail = result.detail;
                            var playlist = result.playlist;
                            prevLayoutManager(models, detail, playlist);
                        }
                    });
                });

                if (accessdeletelayout == 1) {
                    $.contextMenu({
                        selector: '.context-menu-two',
                        events: {
                            show: function (options) {
                                $(this).addClass('cornersel');
                            },
                            hide: function (options) {
                                $(this).removeClass('cornersel');
                            }
                        },
                        callback: function (key, options) {
                            var values = $(this).attr('id');
                            if (key == 'clonelayout') {
                                cloneLayoutData(values);
                            } else if (key == 'clonelayoutcontent') {
                                cloneLayoutContentData(values);
                            } else if (key == 'rename') {
                                renameLayoutData(values);
                            } else if (key == 'delete') {
                                deleteLayoutData(values)
                            }
                        },
                        items: {
                            "clonelayout": {
                                name: "Clone Layout Only",
                                icon: "copy"
                            },
                            "clonelayoutcontent": {
                                name: "Clone Layout and Content",
                                icon: "copy"
                            },
                            "rename": {
                                name: "Rename Layout",
                                icon: "edit"
                            },
                            "delete": {
                                name: "Delete Layout",
                                icon: "delete"
                            },
                            "sep1": "---------",
                            "quit": {
                                name: "Cancel",
                                icon: function () {
                                    return "context-menu-icon context-menu-icon-quit";
                                }
                            }
                        }
                    });
                } else {
                    $.contextMenu({
                        selector: '.context-menu-two',
                        events: {
                            show: function (options) {
                                $(this).addClass('cornersel');
                            },
                            hide: function (options) {
                                $(this).removeClass('cornersel');
                            }
                        },
                        callback: function (key, options) {
                            var values = $(this).attr('id');
                            if (key == 'clonelayout') {
                                cloneLayoutData(values);
                            } else if (key == 'clonelayoutcontent') {
                                cloneLayoutContentData(values);
                            } else if (key == 'rename') {
                                renameLayoutData(values);
                            } else if (key == 'delete') {
                                deleteLayoutData(values)
                            }
                        },
                        items: {
                            "clonelayout": {
                                name: "Clone Layout Only",
                                icon: "copy"
                            },
                            "clonelayoutcontent": {
                                name: "Clone Layout and Content",
                                icon: "copy"
                            },
                            "rename": {
                                name: "Rename Layout",
                                icon: "edit"
                            },
                            "sep1": "---------",
                            "quit": {
                                name: "Cancel",
                                icon: function () {
                                    return "context-menu-icon context-menu-icon-quit";
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    function cloneLayoutData(values) {
        swal({
            title: "Clone Layout",
            text: "Please enter a name for layout clone :",
            input: 'text',
            showCancelButton: true,
            animation: "slide-from-top",
            inputPlaceholder: "Please enter layout name",
            preConfirm: (isnull) => {
                if (isnull == "") {
                    swal.showValidationError('You need to write the layout name!')
                } else {
                    swal.resetValidationError();
                }
            }
        }).then(result => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/admin/infovision/campaign/clone/layout",
                    data: {
                        idlayout: values,
                        nmlayout: result.value
                    },
                    cache: false,
                    success: function (result) {
                        showLayoutManager();
                        swal("Layout Saved!", "Your layout: " + result.value + " has been saved!", "success");
                    }
                });
            }
        });
    }

    function cloneLayoutContentData(values) {
        swal({
            title: "Clone Layout",
            text: "Please enter a name for layout clone :",
            input: 'text',
            showCancelButton: true,
            animation: "slide-from-top",
            inputPlaceholder: "Please enter layout name",
            preConfirm: (isnull) => {
                if (isnull == "") {
                    swal.showValidationError('You need to write the layout name!')
                } else {
                    swal.resetValidationError();
                }
            }
        }).then(result => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/admin/infovision/campaign/clone/layout/content",
                    data: {
                        idlayout: values,
                        nmlayout: result.value
                    },
                    cache: false,
                    success: function (result) {
                        showLayoutManager();
                        swal("Layout Saved!", "Your layout: " + result.value + " has been saved!", "success");
                    }
                });
            }
        });
    }

    function renameLayoutData(values) {
        swal({
            title: "Rename Layout",
            text: "Please enter the new layout name :",
            input: 'text',
            showCancelButton: true,
            animation: "slide-from-top",
            inputPlaceholder: "Please enter layout name",
            preConfirm: (isnull) => {
                if (isnull == "") {
                    swal.showValidationError('You need to write the layout name!')
                } else {
                    swal.resetValidationError();
                }
            }
        }).then(result => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/admin/infovision/campaign/rename/layout",
                    data: {
                        idlayout: values,
                        nmlayout: result.value
                    },
                    cache: false,
                    success: function (result) {
                        showLayoutManager();
                        swal("Success Rename!", "YLayout has been renamed to : " + result.value, "success");
                    }
                });
            }
        });
    }

    function deleteLayoutData(values) {
        swal({
            title: "Remove Confirmation",
            text: "are you sure want to remove this layout?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#337ab7",
            confirmButtonText: "Yes, Delete!",
        }).then(result => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/admin/infovision/campaign/delete/layout",
                    data: {
                        idlayout: values,
                        nmlayout: result.value
                    },
                    cache: false,
                    success: function (result) {
                        showLayoutManager();
                        swal("Layout Delete!", "layout has been deleted!", "success");
                    }
                });
            }
        });
    }

    showLayoutManager();

    function prevLayoutManager(models, detail, playlist) {
        var models = models;
        var detail = detail;
        var playlist = playlist;
        $("#canvaspreview").html("");
        var bglayout = 'rgb(0, 0, 0)';
        var targetshow = 'screenbus';
        var bgid = models.bg_layout;
        if (bgid == 'BG001') {
            var width = 1366;
            var height = 768;
        } else if (bgid == 'BG002') {
            var width = 1920;
            var height = 540;
        } else if (bgid == 'BG003') {
            var width = 1024;
            var height = 768;
        } else if (bgid == 'BG004') {
            var width = 1920;
            var height = 1080;
        }
        if (width == 1920 && height == 540) {
            $("#panelScreen").css('width', "955px");
            $("#panelScreen").css('height', "");
            $("#panelScreen").css('background-color', bglayout);
            $("#panelScreen").css('border-radius', "45px");
            $("#screenbus").removeClass().addClass("screen_maes_29");
        } else if (width == 1920 && height == 1080) {
            $("#panelScreen").css('width', "955px");
            $("#panelScreen").css('height', "395px");
            $("#panelScreen").css('background-color', bglayout);
            $("#panelScreen").css('border-radius', "45px");
            $("#screenbus").removeClass().addClass("screen_maes_29new");
        } else if (width == 1366 && height == 768) {
            $("#panelScreen").css('width', "658px");
            $("#panelScreen").css('height', "");
            $("#panelScreen").css('background-color', bglayout);
            $("#screenbus").removeClass().addClass("screen_maes_19new");
        } else if (width == 1024 && height == 768) {
            $("#panelScreen").css('width', "529px");
            $("#panelScreen").css('height', "400px");
            $("#panelScreen").css('border-radius', "45px");
            $("#panelScreen").css('background-color', bglayout);
            $("#screenbus").removeClass().addClass("screen_maes_19_1024-768");
        }
        var sclfactor = 0.46;
        var spcfactorw = 1;
        var spcfactorh = 1;
        var spchplus = 1;
        if (canvas.width == 1920 && canvas.height == 1080) {
            spcfactorh = 0.60;
        }
        var objcontent = "";
        for (var i = 0; i < detail.length; i++) {
            objcontent = "";
            if (detail[i].objtype == 'labeledRect') {
                objcontent = "<div class='obdata'><img src='/themes/maestronic/assets/backend/img/infovision_cnxx.png' width='" + ((detail[i].width * sclfactor) * spcfactorw) + "px' height='" + ((detail[i].height * sclfactor) * spcfactorh) + "px'></div>";
            } else if (detail[i].objtype == 'image') {
                objcontent = "<div class='objdata'><img src='/admin/infovision/image/" + detail[i].src_val + "' width='" + ((detail[i].width * sclfactor) * spcfactorw) + "px' height='" + (((detail[i].height * sclfactor) * spcfactorh) * spchplus) + "px'></div>";
            } else if (detail[i].objtype == 'text') {
                objcontent = "<div style='height:100%;display:block;background-color:white;vertical-align:middle;'>" + detail[i].src_val + "</div>";
            } else if (detail[i].objtype == 'ticker') {
                objcontent = "<marquee style='height:100%;display:block;background-color:white;vertical-align:middle;'>" + detail[i].src_val.replace(/\\"/g, '"') + "</marquee>";
            } else if (detail[i].objtype == 'url') {
                objcontent = '<iframe height="100%" width="100%" src="' + detail[i].src_val + '" /></iframe>';
            } else if (detail[i].objtype == 'slide') {
                objcontent = "<div class='slidedata'>";
                for (var a = 0; a < playlist.length; a++) {
                    objcontent += "<img id='slide-" + a + "' class='group-slide' src='/admin/infovision/image/" + playlist[a].src_val + "' style='background-color: rgb(0, 0, 0); position: absolute; top: 0px; left: 0px; display: block; z-index: 4; opacity: 1; width: " + ((detail[i].width * sclfactor) * spcfactorw) + "px; height: " + ((detail[i].height * sclfactor) * spcfactorh) + "px;'>";
                }
                objcontent += "</div>";
                slideShowAuto(playlist.length);
            }
            $("#canvaspreview").append('<span style="position: absolute; top: ' + (detail[i].top * sclfactor) + 'px; left: ' + (detail[i].left * sclfactor) + 'px; display: block; background: ' + detail[i].fill + '; width: ' + ((detail[i].width * sclfactor) * spcfactorw) + 'px; height: ' + ((detail[i].height * sclfactor) * spcfactorh) + 'px;">' +
                objcontent +
                '</span>');
        }
    }
});