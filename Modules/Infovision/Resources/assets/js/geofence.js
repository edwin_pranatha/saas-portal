$(document).ready(function () {

    var map = null;
    var polygon = null;
    var draggable = false;
    var totalLatLng = [];


    $('.modal-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $('.modal-sizes').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $(document).on('click', '.modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    $('select.populate').select2();

    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function (item) {
                return item.el.attr('title') + ' &middot; <a class="image-source-link" href="' + item.el.attr('data-source') + '" target="_blank">image source</a>';
            }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            opener: function (element) {
                return element.find('img');
            }
        }
    });

    var upObjLabel = new Object();
    var tableRowGeofence = $('#dataTableRowGeofence').dataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': $('[name=urlinfo]').attr('data-url'),
            'type': 'POST',
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        'columns': [{
                data: 'DT_RowIndex',
                "width": "10px"
            },
            {
                data: 'name',
                "width": "150px"
            },
            {
                data: 'action',
                "width": "80px"
            },
        ],
        'fnDrawCallback': function () {
            $('.modal-sizes').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });

            $(".infoPolygon").click(function (e) {
                var id = $(this).attr('id');
                var dataurl = $(this).attr('data-url');
                infoPolygon(id, dataurl);
            });

            $(".getPopupDelGeofence").click(function (e) {
                var name = $(this).attr('value');
                var urlGeo = $(this).attr('data-url');
                $('#idGeofenceDel').text(name);
                $('#urlinfoGeofence').val(urlGeo);
            });

            $(".exportPolygon").click(function (e) {
                var id = $(this).attr('id');
                var dataurl = $(this).attr('data-url');
                exportPolygon(id, dataurl);
            });
        }
    });


    function infoPolygon(id, dataurl) {
        totalLatLng = [];
        upObjLabel = new Object();
        $.ajax({
            type: "GET",
            url: dataurl,
            data: {
                id: id
            },
            cache: false,
            success: function (result) {
                var detail = result.detail;
                var models = result.models;
                $('#labelGeofence').text(models.name);
                for (let a = 0; a < detail.length; a++) {
                    text = '';
                    var split1fixs = detail[a]['latlong'].split('|');
                    for (let i = 0; i < split1fixs.length; i++) {
                        var split2fixs = split1fixs[i].split(',');
                        text += '(' + split2fixs[0] + ', ' + split2fixs[1] + ')|'
                    }
                    totalLatLng.push(text);
                    if (detail[a]['label'] !== '' && detail[a]['label'] !== null && detail[a]['label'] !== 'undefined') {
                        upObjLabel[detail[a]['name_polygon']] = detail[a]['label'];
                    }
                }
                initialize();

            }
        });
    }

    function initialize() {
        if(map == null){
            map     = new GeofencingMap('map').map;
            polygon = new MultiPolygon(map, $('#polygon-name').val());
        }else{
            polygon.deleteAllPolygons();
        }
        if(totalLatLng.length > 0){
            for (var a = 0; a < totalLatLng.length; a++) {
                var coords = new Array();
                var splitLatLng = totalLatLng[a].split("|");
                for (var i = 0; i < splitLatLng.length; i++) {
                    var latlng = splitLatLng[i].trim().substring(1, splitLatLng[i].length - 1).split(",");
                    if (latlng.length > 1) {
                        coords.push(L.latLng(latlng[0], latlng[1]));
                    }
                    if (a == 0 && i == 0) {
                        map.setView([latlng[0], latlng[1]], 6);
                    }
                }
                polygon.addPolygon(coords, totalLatLng.length, upObjLabel, true);
                polygon.setAllowDragging(draggable);
                polygon.setEditable(true);
            }
        } else {
            var coords = new Array();
            polygon.addPolygon(coords, totalLatLng.length, upObjLabel, true);
            polygon.setAllowDragging(draggable);
            polygon.setEditable(true);
        }

        polygon.setAllowDragging(draggable);
        polygon.setEditable(false);
        map.touchZoom.disable();
        map.doubleClickZoom.disable();
        map.scrollWheelZoom.disable();
        map.boxZoom.disable();
        map.keyboard.disable();
        $(".leaflet-control-zoom").css("visibility", "hidden");
    }

    function exportPolygon(id, dataurl) {
        $.ajax({
            type: "GET",
            url: dataurl,
            data: {
                id: id
            },
            cache: false,
            success: function (result) {
                var detail = result.detail;
                var models = result.models;
                var jsonArg1 = new Object();
                jsonArg1[models.name] = new Object();
                for (var i = 0; i < detail.length; i++) {
                    if (detail[i].label !== '' && detail[i].label !== null && detail[i].label !== 'undefined') {
                        var label = detail[i].label;
                    } else {
                        var label = detail[i].name_polygon;
                    }
                    jsonArg1[models.name][label] = [];
                    var split = detail[i].latlong.split('|');
                    for (var a = 0; a < split.length; a++) {
                        jsonArg1[models.name][label][a] = split[a];
                    }
                }
                var pluginArrayArg = new Array();
                pluginArrayArg.push(jsonArg1);
                var jsonArray = JSON.parse(JSON.stringify(pluginArrayArg));
                download(JSON.stringify(jsonArray), 'polygon.json', 'application/json');
            }
        });
    }

    function download(content, name, type) {
        const a = document.body.appendChild(document.createElement('a'));
        const file = new Blob([content], {
            type: type
        });
        a.href = URL.createObjectURL(file);
        a.download = name;
        a.click();
    }

    $(".delGeofence").click(function (e) {
        var urlGeo = $('#urlinfoGeofence').val();
        $.ajax({
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            url: urlGeo,
            cache: false,
            success: function (result) {
                e.preventDefault();
                $.magnificPopup.close();
                tableRowGeofence.fnDraw(false);
            }
        });
    });
});