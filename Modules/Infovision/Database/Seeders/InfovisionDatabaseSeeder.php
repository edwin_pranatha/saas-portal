<?php

namespace Modules\Infovision\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Infovision\Database\Seeders\NCConfigTableSeeder;
use Modules\Infovision\Database\Seeders\InfovisionPermissionTableSeeder;

class InfovisionDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        // $this->call(NCProjectActiveTableSeeder::class);
        // $this->call(NCProjectTableSeeder::class);
        // $this->call(NCProjectDtlArchieveTableSeeder::class);
        // $this->call(NCProjectDtlTableSeeder::class);
        $this->call(NCConfigTableSeeder::class);
        $this->call(InfovisionPermissionTableSeeder::class);

    }
}
