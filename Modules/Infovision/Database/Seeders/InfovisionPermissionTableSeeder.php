<?php

namespace Modules\Infovision\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InfovisionPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $permissions_infovision = [
           'content-upload-list',
           'content-upload-create',
           'content-upload-edit',
           'content-upload-delete',

           'layout-list',
           'layout-create',
           'layout-edit',
           'layout-delete',

           'campaign-list',
           'campaign-create',
           'campaign-edit',
           'campaign-delete',
           'report-exporter-list'
           
        ];

        $permissions_geofence = [
          'geofence-show',
          'geofence-create',
          'geofence-edit',
          'geofence-delete'
          
       ];

        foreach ($permissions_infovision as $permission) {
          Permission::firstorCreate(
               [
                    "name" => $permission,
                    "guard_name" => "web",
                    "group" => "Info Vision"
               ]
          );
        }

        
        foreach ($permissions_geofence as $permission) {
          Permission::firstorCreate(
               [
                    "name" => $permission,
                    "guard_name" => "web",
                    "group" => "Geofence"
               ]
          );
        }

        $role = Role::findByName('Admin');
        $role->givePermissionTo($permissions_infovision);
        $role->givePermissionTo($permissions_geofence);
    }
}
