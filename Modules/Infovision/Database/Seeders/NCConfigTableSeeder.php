<?php

namespace Modules\Infovision\Database\Seeders;

use Illuminate\Database\Seeder;

class NCConfigTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('nc_config')->delete();
        
        \DB::table('nc_config')->insert(array (
            0 => 
            array (
                'cfg' => 'dep_method',
                'val' => '0',
                'desc' => '0=php daemon,  1=c++ daemon',
            ),
            1 => 
            array (
                'cfg' => 'dep_tmp',
                'val' => '0',
                'desc' => '0=deploy to production /opt/mnp/nc, 1=deploy to /tmp path for development',
            ),
            2 => 
            array (
                'cfg' => 'dep_exptag',
                'val' => '1',
                'desc' => '0=disable expire file tagging, 1=enable expire file tag 1321312_filename.jpg',
            ),
            3 => 
            array (
                'cfg' => 'dep_refresh',
                'val' => '1',
                'desc' => '0=disable refresh socket call, 1=enable refresh socket call',
            ),
            4 => 
            array (
                'cfg' => 'dep_respatch',
                'val' => '0',
                'desc' => '0=disable deploy with resolution patch function, 1=enable deploy with resolution patch function',
            ),
            5 => 
            array (
                'cfg' => 'dep_dflrembus',
                'val' => '0',
                'desc' => '0=disable set default playlist when removed from campaign',
            ),
            6 => 
            array (
                'cfg' => 'fallback',
                'val' => '||10|*{background-color:#000000;}',
                'desc' => 'default fallback value #img|AML-TFT-Default_slide_betalenindebus.jpg|10|*{background-color:#000000;}',
            ),
            7 => 
            array (
                'cfg' => 'img_public_folder',
                'val' => '/asset_img/',
                'desc' => 'nc image path',
            ),
            8 => 
            array (
                'cfg' => 'vid_public_folder',
                'val' => '/asset_video/',
                'desc' => 'nc video path',
            ),
            9 => 
            array (
                'cfg' => 'lay_public_folder',
                'val' => '/asset_layout/',
                'desc' => 'nc layout path',
            ),
            10 => 
            array (
                'cfg' => 'file_top',
                'val' => 'bustop.ini',
                'desc' => NULL,
            ),
            11 => 
            array (
                'cfg' => 'file_down',
                'val' => 'busdown.ini',
                'desc' => NULL,
            ),
            12 => 
            array (
                'cfg' => 'file_bus',
                'val' => 'bus.ini',
                'desc' => NULL,
            ),
            13 => 
            array (
                'cfg' => 'lay_pftsize',
                'val' => '18',
                'desc' => NULL,
            ),
            14 => 
            array (
                'cfg' => 'asset_storetype',
                'val' => '0',
                'desc' => '0=local server method, 1 = CDN server method',
            ),
            15 => 
            array (
                'cfg' => 'chk_background',
                'val' => '1',
                'desc' => 'allow to automatic refresh in background',
            ),
            16 => 
            array (
                'cfg' => 'debugmode',
                'val' => '0',
                'desc' => 'debug mode used to display developer tool & message 0=production 1=testing',
            ),
            17 => 
            array (
                'cfg' => 'nc_version',
                'val' => '1.2.0',
                'desc' => 'infovision version',
            ),
            18 => 
            array (
                'cfg' => 'max_asset_width',
                'val' => '640',
                'desc' => 'maxium asset width',
            ),
            19 => 
            array (
                'cfg' => 'max_asset_height',
                'val' => '360',
                'desc' => 'maxium asset height',
            ),
            20 => 
            array (
                'cfg' => 'geo_translate',
                'val' => '1',
                'desc' => 'set new geometry translation, which NC started from 0 instead offsetX',
            ),
        ));
        
        
    }
}