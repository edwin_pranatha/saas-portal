<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NcGeofencesDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_geofences_dtl', function (Blueprint $table) {
            $table->string('id_geofence')->nullable();
            $table->string('name_polygon')->nullable();
            $table->string('label')->nullable();
            $table->text('latlong')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_geofences_dtl');
    }
}
