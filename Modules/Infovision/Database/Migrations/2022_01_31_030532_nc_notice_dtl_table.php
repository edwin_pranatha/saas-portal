<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NCNoticeDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_notice_dtl', function (Blueprint $table) {
            $table->string('id_notice')->nullable();
            $table->string('busid')->nullable();
            $table->string('busip')->nullable();
            $table->text('msg_notice')->nullable();
            $table->string('msg_font_fam')->nullable();
            $table->string('msg_font_size')->nullable();
            $table->string('msg_timeout')->nullable();
            $table->text('msg_stylesheet')->nullable();
            $table->string('msg_position')->nullable();
            $table->string('stat_notice')->nullable();
            $table->string('reason')->nullable();
            $table->string('date_start')->nullable();
            $table->string('date_expire')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_notice_dtl');
    }
}
