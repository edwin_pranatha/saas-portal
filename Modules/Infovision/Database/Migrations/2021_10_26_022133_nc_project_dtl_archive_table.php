<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NCProjectDtlArchiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_project_dtl_archive', function (Blueprint $table) {
            $table->string('id_project', 8)->nullable();
            $table->integer('ver_project')->nullable();
            $table->string('bus_id', 200)->nullable();
            $table->string('lay_id', 200)->nullable();
            $table->string('upload_percent', 100)->nullable();
            $table->text('upload_stage')->nullable();
            $table->string('error_cnt', 5)->nullable();
            $table->text('debug_output')->nullable();
            $table->string('upload_stat', 100)->nullable();
            $table->string('upload_script', 100)->nullable();
            $table->string('upload_lasterr', 100)->nullable();
            $table->timestamp('start_deploy')->nullable();
            $table->timestamp('finish_deploy')->nullable();
            $table->timestamp('retry_deploy')->nullable();
            $table->timestamp('lasttry_deploy')->nullable();
            $table->smallInteger('isqueue')->nullable();
            $table->smallInteger('isrem')->nullable();
            $table->smallInteger('isfinish')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_project_dtl_archive');
    }
}
