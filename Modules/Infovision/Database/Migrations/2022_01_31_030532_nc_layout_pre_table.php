<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NCLayoutPreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_layout_pre', function (Blueprint $table) {
            $table->string('id_layout')->nullable();
            $table->string('id_op')->nullable();
            $table->string('nm_layout')->nullable();
            $table->string('bg_layout')->nullable();
            $table->string('bg_fill')->nullable();
            $table->string('pic_layout')->nullable();
            $table->string('id_playlist')->nullable();
            $table->string('createby')->nullable();
            $table->string('modifyby')->nullable();
            $table->string('screen_group')->nullable();
            $table->string('screen_offsetx')->nullable();
            $table->string('screen_offsety')->nullable();
            $table->string('screen_pos')->nullable();
            $table->string('fallback')->nullable();
            $table->string('usepft')->nullable();
            $table->string('isdefault')->nullable();
            $table->string('isshow')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_layout_pre');
    }
}
