<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NCLayoutDtlPreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_layout_dtl_pre', function (Blueprint $table) {
            $table->string('id_layout')->nullable();
            $table->string('type')->nullable();
            $table->string('originX')->nullable();
            $table->string('originY')->nullable();
            $table->string('left')->nullable();
            $table->string('top')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('fill')->nullable();
            $table->string('stroke')->nullable();
            $table->string('strokeWidth')->nullable();
            $table->string('strokeDashArray')->nullable();
            $table->string('strokeLineCap')->nullable();
            $table->string('strokeLineJoin')->nullable();
            $table->string('strokeMiterLimit')->nullable();
            $table->string('scaleX')->nullable();
            $table->string('scaleY')->nullable();
            $table->string('angle')->nullable();
            $table->string('flipX')->nullable();
            $table->string('flipY')->nullable();
            $table->string('opacity')->nullable();
            $table->string('shadow')->nullable();
            $table->string('visible')->nullable();
            $table->string('clipTo')->nullable();
            $table->string('backgroundColor')->nullable();
            $table->string('globalCompositeOperation')->nullable();
            $table->string('transformMatrix')->nullable();
            $table->string('skewX')->nullable();
            $table->string('skewY')->nullable();
            $table->string('rx')->nullable();
            $table->string('ry')->nullable();
            $table->string('name')->nullable();
            $table->string('selectable')->nullable();
            $table->string('evented')->nullable();
            $table->string('hasBorders')->nullable();
            $table->string('hasControls')->nullable();
            $table->string('hasRotatingPoint')->nullable();
            $table->string('plylstid')->nullable();
            $table->string('plylstnm')->nullable();
            $table->string('plylstdata')->nullable();
            $table->string('objtype')->nullable();
            $table->string('objstyle')->nullable();
            $table->string('src_val')->nullable();
            $table->string('src_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_layout_dtl_pre');
    }
}
