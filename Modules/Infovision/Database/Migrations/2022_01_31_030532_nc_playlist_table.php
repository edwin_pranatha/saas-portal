<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NCPlaylistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_playlist', function (Blueprint $table) {
            $table->string('id_playlist')->nullable();
            $table->string('nm_playlist')->nullable();
            $table->string('desc_playlist')->nullable();
            $table->string('createby')->nullable();
            $table->string('modifyby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_playlist');
    }
}
