<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NcContentUploadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_content_upload', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('name')->nullable();
            $table->string('size');
            $table->timestamps();
        });
        \DB::statement("CREATE SEQUENCE id_content_upload_seq;");
        \DB::statement("ALTER TABLE nc_content_upload ALTER COLUMN id SET DEFAULT NEXTVAL('id_content_upload_seq');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_content_upload');
        \DB::statement("DROP SEQUENCE id_content_upload_seq;");
    }
}
