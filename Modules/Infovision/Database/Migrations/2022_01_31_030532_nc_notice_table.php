<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NCNoticeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_notice', function (Blueprint $table) {
            $table->string('id_notice')->nullable();
            $table->text('target_notice')->nullable();
            $table->text('msg_notice')->nullable();
            $table->string('stat_notice')->nullable();
            $table->string('date_start')->nullable();
            $table->string('date_expire')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_notice');
    }
}
