<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NCPlaylistDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_playlist_dtl', function (Blueprint $table) {
            $table->string('id_playlist');
            $table->string('order')->nullable();
            $table->string('src_val')->nullable();
            $table->string('ttl')->nullable();
            $table->string('day_play')->nullable();
            $table->string('date_play')->nullable();
            $table->string('time_play')->nullable();
            $table->string('priority')->nullable();
            $table->string('isvideotime')->nullable();
            $table->string('trigger')->nullable();
            $table->string('line')->nullable();
            $table->string('txtfont')->nullable();
            $table->string('txtsize')->nullable();
            $table->string('txtisbold')->nullable();
            $table->string('txtfcolor')->nullable();
            $table->string('txtbgcolor')->nullable();
            $table->string('txtbgimg')->nullable();
            $table->string('txtalignment')->nullable();
            $table->string('pl_prop')->nullable();
            $table->string('typelocation')->nullable();
            $table->string('stop')->nullable();
            $table->string('geofence')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_playlist_dtl');
    }
}
