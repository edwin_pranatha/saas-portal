<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NCProjectActiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_project_active', function (Blueprint $table) {
            $table->string('bus_id', 70)->nullable();
            $table->string('bus_ip', 30)->nullable();
            $table->string('proj_id', 10)->nullable();
            $table->integer('proj_ver')->nullable();
            $table->string('proj_nm', 200)->nullable();
            $table->string('layout_id', 100)->nullable();
            $table->text('bus_assets')->nullable();
            $table->string('script_upload', 150)->nullable();
            $table->string('bus_cur_res', 150)->nullable();
            $table->smallInteger('isdefault')->nullable();
            $table->string('remark', 200)->nullable();
            $table->text('CRC_output')->nullable();
            $table->string('CRC_stat', 10)->nullable();
            $table->timestamp('apply_date')->nullable();
            $table->timestamp('crc_check_date')->nullable();
            $table->smallInteger('CRCfix')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_project_active');
    }
}
