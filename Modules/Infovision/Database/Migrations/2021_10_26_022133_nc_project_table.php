<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NCProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nc_project', function (Blueprint $table) {
            $table->string('id_project', 8)->nullable();
            $table->integer('ver_project')->nullable();
            $table->string('csr_id', 10)->nullable();
            $table->string('mngr_id', 35)->nullable();
            $table->integer('priority')->nullable();
            $table->string('proj_nm', 200)->nullable();
            $table->timestamp('proj_start')->nullable();
            $table->timestamp('proj_end')->nullable();
            $table->text('group_apply')->nullable();
            $table->text('group_remove')->nullable();
            $table->string('layout_apply', 100)->nullable();
            $table->string('type_apply', 100)->nullable();
            $table->string('proj_email', 100)->nullable();
            $table->timestamp('proj_gen')->nullable();
            $table->timestamp('proj_fin')->nullable();
            $table->timestamp('proj_stop')->nullable();
            $table->text('assetlist')->nullable();
            $table->binary('assetsize')->nullable();
            $table->smallInteger('isscript')->nullable();
            $table->string('status', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nc_project');
    }
}
