<?php

namespace Modules\Infovision\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use Lavary\Menu\Facade as Menu;
use Illuminate\Support\Str;

class MenuBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Menu::make('MenuBackend', function ($menu) {
            
            $menu->add('Info Vision', '#')->data([
                'icon' => 'fas fa-info-circle',
                //'group' => 'Info Vision',
                'permissions' => 'infovision.view',
                'active_match' => 'admin/infovision*',
                'order' => 2,
            ]);
            $menu->infoVision->add('Vehicles', 'admin/infovision/vehicles')->data([
                'permissions' => 'vehicles-list',
                'active_match' => 'admin/infovision/vehicles*',
                'order' => 1
                //'group' => 'Fleet',
            ]);
            $menu->infoVision->add('Content Upload', 'admin/infovision/content-upload')->data([
                'permissions' => 'content-upload.view',
                'active_match' => 'admin/infovision/content-upload*',
                //'group' => 'Info Vision',
                'order' => 2,
            ])->active("admin/infovision/content-upload");

            $menu->infoVision->add('Layout Manager', 'admin/infovision/layout')->data([
                'permissions' => 'campaign.layout-view',
                'active_match' => 'admin/infovision/layout*',
                //'group' => 'Info Vision',
                'order' => 3,
            ])->active("admin/infovision/layout");

            $menu->infoVision->add('Campaign Manager', 'admin/infovision/campaign')->data([
                'permissions' => 'campaign.manager-view',
                'active_match' => 'admin/infovision/campaign*',
                //'group' => 'Info Vision',
                'order' => 4,
            ])->active("admin/infovision/campaign");

            
            $menu->infoVision->add('Geofence', 'admin/infovision/geofence')->data([
                'permissions' => 'campaign.geofence-view',
                'active_match' => 'admin/infovision/geofence*',
                //'group' => 'Info Vision',
                'order' => 5,
            ])->active("admin/infovision/geofence");

            // $menu->reporting->add('Statistic', 'admin/report/statistic')->data([
            //     'permissions' => 'report.statistic-view',
            //     'active_match' => 'admin/infovision/statistic*',
            //     'group' => ' ',
            //     'order' => 1,
            // ])->active("admin/infovision/statistic");

            
            // $menu->reporting->add('History', 'admin/report/history')->data([
            //     'permissions' => 'report.history-view',
            //     'active_match' => 'admin/report/history*',
            //     'group' => ' ',
            //     'order' => 1,
            // ])->active("admin/report/history");
            
        })->filter(function ($item) use ($request) {
            if ($request->url() === rtrim($item->url(), '#/')) {
                $item->active();
            }
            
            if ($item->active_match) {
                $matches = is_array($item->active_match) ? $item->active_match : [$item->active_match];                
                foreach ($matches as $pattern) {
                    if (Str::is($pattern, $request->path())) {
                        $item->activate();
                    }
                }
            }
            return true;
        })->sortBy('order');
        return $next($request);
    }
}