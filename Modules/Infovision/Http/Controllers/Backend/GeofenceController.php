<?php

namespace Modules\Infovision\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Infovision\Entities\NcGeofence;
use Modules\Infovision\Entities\NcGeofenceDetail;
use Location\Coordinate;
use Location\Polygon;
use Auth;
use Validator;

class GeofenceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $models = NcGeofence::orderBy('id_geofence','asc')->get();
            return datatables()->of($models)
                ->addColumn('action', function ($row) {
                    $html = '';
                    if(Auth::user()->can('geofence-edit')==1){
                        $html = '<a href="'.route('admin.infovision.geofence.edit', $row->id_geofence).'" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary">Edit</a> ';
                    }
                    $html .= '<a href="#modalInfoMap" class="mb-xs mt-xs modal-sizes mr-xs btn btn-sm btn-info infoPolygon" id="'.$row->id_geofence.'" data-url="'.route('admin.infovision.geofence.polygon.info').'">Info</a>';
                    if(Auth::user()->can('geofence-delete')==1){
                        $html .= '<a href="#modalDelGeofence" class="mb-xs mt-xs modal-sizes mr-xs btn btn-sm btn-danger getPopupDelGeofence" value="'.$row->name.'" data-url="'.route('admin.infovision.geofence.destroy', $row->id_geofence).'">Delete</a>';
                    }
                    $html .= '<a href="javascript:void(0)" class="mb-xs mt-xs mr-xs btn btn-sm btn-success exportPolygon" id="'.$row->id_geofence.'" data-url="'.route('admin.infovision.geofence.polygon.export').'">Export</a>';
                    return $html;
                })->addIndexColumn()->toJson();
        }
        return view('infovision::geofence.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $id = '';
        return view('infovision::geofence.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required',
            'data'          => 'required',
        ]);

        if($validator->passes()){
            $id = $request->idGeofence;
            if($id==''){
                $max = NcGeofence::max('id_geofence');
                $nomor = (int) $max + 1;

                $models                     = New NcGeofence();
                $models->id_geofence        = $nomor;
                $models->name               = $request->name;
                $models->date_create        = date("Y-m-d H:i:s");
                $models->save();

                foreach ($request->data as $key => $value) {
                    $text = str_replace(',|', ',', $value['polygon']);
                    $text = str_replace('|;', '', $text);
                    $text = str_replace(';', '', $text);
                    if($value['label']=='undefined'){
                        $value['label'] = NULL;
                    }
                    $detail                 = New NcGeofenceDetail;
                    $detail->id_geofence    = $nomor;
                    $detail->name_polygon   = $key;
                    $detail->label          = $value['label'];
                    $detail->latlong        = $text;
                    $detail->save();
                }
            }else{
                $models                     = NcGeofence::where('id_geofence', $id)->first();
                $models->name               = $request->name;
                $models->date_create        = date("Y-m-d H:i:s");
                $models->save();
                $detail = NcGeofenceDetail::where('id_geofence', $id)->delete();

                foreach ($request->data as $key => $value) {
                    $text = str_replace(',|', ',', $value['polygon']);
                    $text = str_replace('|;', '', $text);
                    $text = str_replace(';', '', $text);
                    if($value['label']=='undefined'){
                        $value['label'] = NULL;
                    }
                    $detail                 = New NcGeofenceDetail;
                    $detail->id_geofence    = $id;
                    $detail->name_polygon   = $key;
                    $detail->label          = $value['label'];
                    $detail->latlong        = $text;
                    $detail->save();
                }
            }

            return response()->json(['success' => 'success']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $models = NcGeofence::where('id_geofence', $id)->first();
        $detail = NcGeofenceDetail::where('id_geofence', $id)->get();
        
        return response()->json(['models' => $models, 'detail' => $detail]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('infovision::geofence.create', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        NcGeofence::where('id_geofence', $id)->delete();
        NcGeofenceDetail::where('id_geofence', $id)->delete();
        return 'Geofence deleted successfully';
    }

    public function exportPolygon(Request $request)
    {
        $id = $request->id;
        $models = NcGeofence::where('id_geofence', $id)->first();
        $detail = NcGeofenceDetail::where('id_geofence', $id)->get();
        
        return response()->json(['models' => $models, 'detail' => $detail]);
    }

    public function checkPolygon(Request $request)
    {
        $id = $request->id;
        $lat = $request->lat;
        $lng = $request->lng;

        $geofence = new Polygon();

        $detail = NcGeofenceDetail::where('id_geofence', $id)->get();

        foreach ($detail as $value) {
            $exp = explode('|', $value->latlong);
            for ($i=0; $i < count($exp); $i++) {
                $split = explode(',', $exp[$i]);
                $geofence->addPoint(new Coordinate((int) $split[0],(int) $split[1]));
            }
        }

        $outsidePoint = new Coordinate($lat, $lng);

        return $geofence->contains($outsidePoint);
    }

    public function showMapGeofence($id)
    {
        $detail = NcGeofenceDetail::where('id_geofence', $id)->get();
        return view('infovision::geofence.test', compact('detail','id'));
    }

    public function infoPolygon(Request $request)
    {
        $id = $request->id;
        $models = NcGeofence::where('id_geofence', $id)->first();
        $detail = NcGeofenceDetail::where('id_geofence', $id)->get();
        
        return response()->json(['models' => $models, 'detail' => $detail]);
    }
}
