<?php

namespace Modules\Infovision\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Infovision\Entities\NcContentUpload;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;
use Illuminate\Support\Str;

class ContentuploadController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $currentURL = request()->getHttpHost();
            $image_ext = array('jpg','jpeg','png','gif');
            $video_ext = array('mp4','mov','m4v','avi','wmv','flv','3gp','mkv');
            if($request->hasfile('files')){
                $models = [];
                $no = 0;
                foreach($request->file('files') as $file){

                    try {
                        $info = \FFMpeg::open($file)
                        ->getVideoStream()
                        ->getDimensions();
                        
                        $width = $info->getWidth();
                        $height = $info->getHeight();
                        
                        if($width > 1920 || $height > 1080){
                            //return json error
                            return response()->json([
                                'error' => $file->getClientOriginalName().' is too large. Please upload a file with a resolution of 1920x1080 or less.',
                            ], 422);
                        }


                    } catch (\Exception $e) {
                        $info = $e->getMessage();
                    }
                    
                    if(is_string($info) && Str::contains($info, 'Unable to probe'))
                    {
                        return response()->json([
                            'errors' => [
                                'message' => $file->getClientOriginalName().' cannot be parsed as media file.',
                            ]
                        ], 422);
                    }else{                        
                        $size = $file->getSize();

                        $content = new NcContentUpload();
                        $content->type = $file->getClientOriginalExtension();
                        $content->size = $size;
                        $content->save();

                        $name = $content->id.'-'.str_replace(array(" "),array("-"),$file->getClientOriginalName());
                        Storage::disk('tenant')->makeDirectory("nc_images", 0755, true, true);
                        Storage::disk('tenant')->makeDirectory("nc_videos", 0755, true, true);
                        Storage::disk('tenant')->makeDirectory("nc_images/thumbnail", 0755, true, true);
                        Storage::disk('tenant')->makeDirectory("nc_videos/thumbnail", 0755, true, true);

                        if($file->getClientOriginalExtension()!='mp4'){
                            $image_resize = Image::make($file->getRealPath());              
                            $image_resize->resize(80, 45)->encode('jpg', 80);
                            //$image_resize->save(public_path('nc_images/thumbnail/' .$name));
                            Storage::disk('tenant')->put("nc_images/thumbnail/".$name, $image_resize);
                            //$file->move(public_path().'/nc_images/', $name);
                            $file->move(Storage::disk('tenant')->path("nc_images/"), $name);
                            $models['files'][$no]['thumbnailUrl'] = url('/').'/admin/infovision/image/thumbnail/'.str_replace($image_ext,"jpg",$name);
                            $models['files'][$no]['url'] = url('/').'/admin/infovision/image/'.$name;
                        }else{
                            $file->move(Storage::disk('tenant')->path("nc_videos/"), $name);
                            FFMpeg::fromDisk('tenant')
                            ->Open("nc_videos/".$name)
                            ->getFrameFromSeconds(1)
                            ->export()
                            ->toDisk('tenant')
                            ->save("nc_videos/thumbnail/".str_replace($video_ext,"jpg",$name));
                            $models['files'][$no]['thumbnailUrl'] = url('/').'/admin/infovision/video/thumbnail/'.str_replace($video_ext,"jpg",$name);
                            $models['files'][$no]['url'] = url('/').'/admin/infovision/video/'.$name;
                        }

                        $content->name = $name;
                        $content->save();

                        $models['files'][$no]['name'] = $name;
                        $models['files'][$no]['deleteType'] = 'DELETE';
                        $models['files'][$no]['type'] = $file->getClientOriginalExtension();
                        $models['files'][$no]['deleteUrl'] = 'content-upload/'.$name.'/delete';
                        $models['files'][$no]['size'] = $size;
                        $no++;
                    }

                }

                return json_encode($models);
            }else{
                $data = NcContentUpload::orderBy('created_at', 'desc')->get();
                $models = [];
                $no = 0;
                foreach($data as $dat){
                    $name = $dat->name;
                    $type = $dat->type;
                    $size = (int)$dat->size;
                    if($type!='mp4'){
                        $models['files'][$no]['thumbnailUrl'] = url('/').'/admin/infovision/image/thumbnail/'.$name;
                        $models['files'][$no]['url'] = url('/').'/admin/infovision/image/'.$name;
                    }else{
                        $models['files'][$no]['thumbnailUrl'] = url('/').'/admin/infovision/video/thumbnail/'.str_replace($video_ext,"jpg",$name);
                        $models['files'][$no]['url'] = url('/').'/admin/infovision/video/'.$name;
                    }
                    $models['files'][$no]['name'] = $name;
                    $models['files'][$no]['deleteType'] = 'DELETE';
                    $models['files'][$no]['type'] = $type;
                    $models['files'][$no]['deleteUrl'] = 'content-upload/'.$name.'/delete';
                    $models['files'][$no]['size'] = $size;
                    $no++;
                }

                return json_encode($models);
            }
        }

        return view('infovision::contentupload.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('infovision::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show()
    {
        $models = NcContentUpload::orderBy('created_at', 'desc')->get();

        return json_encode($models);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('campaigns::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $video_ext = array('mp4','mov','m4v','avi','wmv','flv','3gp','mkv');
        $exp = explode('-', $id);
        $models = NcContentUpload::findOrFail($exp[0]);
        $name = $models->name;
        if($models->type!='mp4'){
            $file = public_path('nc_images/'.$id);
        }else{
            $file = public_path('nc_videos/'.$id);
        }
        
        $img = unlink($file);
        if($models->type!='mp4'){
            $fileThumbnail = public_path('nc_images/thumbnail/'.$id);
            $imgThumbnail = unlink($fileThumbnail);
        }
        $models->delete();

        $currentURL = request()->getHttpHost();
        $array = [];
        if($models->type!='mp4'){
            $array[] =  url('/').'/admin/infovision/image/thumbnail/'.$name;
            $array[] =  url('/').'/admin/infovision/image/'.$name;
        }else{
            $array[] =  url('/').'/admin/infovision/video/thumbnail/'.str_replace($video_ext,"jpg",$name);
            $array[] =  url('/').'/admin/infovision/video/'.$name;
        }
        return json_encode($array);
    }
}
