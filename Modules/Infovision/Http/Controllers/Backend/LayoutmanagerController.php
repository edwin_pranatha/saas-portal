<?php

namespace Modules\Infovision\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Infovision\Entities\NcGeofence;
use Modules\Infovision\Entities\NcGeofenceDetail;

class LayoutmanagerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('infovision::layoutmanager.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('infovision::layoutmanager.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('infovision::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('infovision::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function getShowGeofence()
    {
        $models = NcGeofence::all();

        return response()->json(['models' => $models]);
    }

    public function getShowDetailGeofence(Request $request)
    {
        $id = $request->id;
        $split = explode(',', $id);
        $models = NcGeofenceDetail::whereIn('id_geofence', $split)->get();

        return response()->json(['models' => $models]);
    }
}
