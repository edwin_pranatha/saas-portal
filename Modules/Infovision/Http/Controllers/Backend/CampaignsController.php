<?php

namespace Modules\Infovision\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Infovision\Entities\NcContentUpload;
use Modules\Infovision\Entities\NcPlayList;
use Modules\Infovision\Entities\NcPlayListDetail;
use Modules\Infovision\Entities\NcLayoutPre;
use Modules\Infovision\Entities\NcLayoutDetailPre;
use Modules\Infovision\Entities\NcPlayListDetailPre;
use Modules\Infovision\Entities\NcProject;
use Modules\Infovision\Entities\NcProjectDetail;
use Modules\Infovision\Entities\NcNotice;
use Modules\Infovision\Entities\NcNoticeDetail;
use Modules\Infovision\Entities\NcGeofence;
use Modules\Infovision\Entities\NcGeofenceDetail;
use Modules\Fleet\Entities\Vehicles;
use App\Models\Tenant\User;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CampaignsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $models = NcProject::orderBy('proj_gen','asc')->get();
            return datatables()->of($models)
                ->addColumn('statusdeploy', function ($row) {
                    if($row->status=='' || $row->status==null){
                        $status = 'Ready for deployment';
                    }else if($row->status=='2'){
                        $status = 'Updated, ready for deployment';
                    }else if($row->status=='3'){
                        $status = '<div id="progress-'.$row->id_project.'" style="text-align: center;">Processing <div class="progress progress-striped light active m-md"><div class="progress-bar progress-bar-primary" id="progress-bar-'.$row->id_project.'" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div></div><a href="#modelInfoLayout" class="mb-xs mt-xs mr-xs modal-sizes btn btn-sm btn-primary infoCampaign" data-value="'.$row->id_project.'" >Detail</a><a href="javascript:void(0)" data-value="'.$row->id_project.'" class="mb-xs mt-xs mr-xs btn btn-sm btn-danger stopCampaign">Stop</a></div>';
                    }else if($row->status=='1'){
                        $status = '<span style="color: green;">Deployed<span>';
                    }else if($row->status=='4'){
                        $status = 'Stop';
                    }
                    return $status;
                })->addColumn('action', function ($row) {
                    $html = '<a href="#modelInfoLayout" class="btn btn-info modal-sizes infoCampaign" data-value="'.$row->id_project.'" style="margin-right: 5px;"><i class="fa fa-info"></i></a>';
                    if(Auth::user()->can('campaign-edit')==1){
                        $html .= '<a href="#modalFull" class="btn btn-warning modal-sizes editCampaign" data-value="'.$row->id_project.'" style="margin-right: 5px;"><i class="fa fa-edit"></i></a>';
                    }
                    $html .= '<a href="javascript:void(0)" class="btn btn-primary deployCampaign" data-value="'.$row->id_project.'" style="margin-right: 5px;"><i class="fa fa-upload"></i></a>';
                    if(Auth::user()->can('campaign-delete')==1){
                        $html .= '<a href="javascript:void(0)" class="btn btn-danger delCampaign" data-value="'.$row->id_project.'|'.$row->layout_apply.'"><i class="fa fa-trash"></i></a>';
                    }
                    return $html;
                })->addIndexColumn()->toJson();
        }

        return view('infovision::campaignmanager.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('infovision::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $max = NcProject::orderBy('id_project','desc')->limit(1)->value('id_project');
        $nomor = $max;
        $nmlaymaster = NcLayoutPre::where('id_layout', $request->idlayout)->limit(1)->value('nm_layout');
        $layproj = str_pad((int) intval(substr($nomor,4))+1,4,"0",STR_PAD_LEFT).".1";
        $this->CloneLayout($request->idlayout, $nmlaymaster, 1, $request->idlayout.'.'.$layproj);
        $urut = str_pad((int) intval(substr($nomor,4))+1,4,"0",STR_PAD_LEFT);
        $datano = "PROJ".$urut;
        $cfglay = "";

        if($request->typeproject=='Bus Stop'){
            $cfgtype ="Stop";
        }else{
            $cfgtype = "Bus";
        }

        $projprior = 5;
        $emailreport = "";

        if($request->busid == ""){
            $busid = "";
        }
        $imp = implode(',', $request->busid);

        $models = New NcProject;
        $models->id_project = $datano;
        $models->ver_project = '1';
        $models->csr_id = Auth::user()->id;
        $models->mngr_id = Auth::user()->name;
        $models->priority = '1';
        $models->proj_nm = $request->nmproject;
        $models->proj_start = $request->startdate;
        $models->proj_end = $request->enddate;
        $models->group_apply = $imp;
        $models->layout_apply = $request->idlayout.'.'.$layproj;
        $models->type_apply = $cfgtype;
        $models->proj_email = $emailreport;
        $models->isscript = '0';
        $models->proj_gen = date("Y-m-d H:i:s");
        $models->save();

        $busid = $request->busid;
        for ($i=0; $i < count($busid); $i++) {
            if($busid[$i]!='multiselect-all'){
                $detail = New NcProjectDetail;
                $detail->id_project = $datano;
                $detail->ver_project = '1';
                $detail->bus_id = $busid[$i];
                $detail->lay_id = $request->idlayout.'.'.$layproj;
                $detail->upload_percent = '0';
                $detail->start_deploy = date("Y-m-d H:i:s");
                $detail->save();
            }
        }

        return "saveprojok";
    }

    public function CloneLayout($layid, $layname, $isfullclone, $verlayclone)
    {
        $max = NcLayoutPre::orderBy('id_layout', 'desc')->limit(1)->first();
        $max = str_pad((int) intval(substr($max->id_layout, 4)) + 1, 4, "0",STR_PAD_LEFT);
        $idlayout = "LAY".$max;
        if(trim($verlayclone) != "0"){
            $idlayout=$verlayclone;
            $isshow="0";
        }else{
            $isshow="1";
        }

        $idlay = NcLayoutPre::where('id_layout', $layid)->first();
        if(!empty($idlay)){
            $datlayout = NcLayoutDetailPre::where('id_layout', $layid)->get();
            $datplylst = NcPlayListDetailPre::where('id_layout', $layid)->get();

            $models = New NcLayoutPre;
            $models->id_layout = $idlayout;
            $models->id_op = Auth::user()->id;
            $models->nm_layout = $layname;
            $models->bg_layout = $idlay->bg_layout;
            $models->bg_fill = $idlay->bg_fill;
            $models->screen_offsetx = $idlay->screen_offsetx;
            $models->pic_layout = $idlay->pic_layout;
            $models->createby = Auth::user()->id;
            $models->fallback = $idlay->fallback;
            $models->usepft = $idlay->usepft;
            $models->isshow = $isshow;
            $models->save();

            if(count($datlayout) > -1){
                foreach ($datlayout as $datlay){
                    $detail = New NcLayoutDetailPre;
                    $detail->id_layout = $idlayout;
                    $detail->type = $datlay->type;
                    $detail->originX = $datlay->originX;
                    $detail->originY = $datlay->originY;
                    $detail->left = $datlay->left;
                    $detail->top = $datlay->top;
                    $detail->width = $datlay->width;
                    $detail->height = $datlay->height;
                    $detail->fill = $datlay->fill;
                    $detail->stroke = $datlay->stroke;
                    $detail->strokeWidth = $datlay->strokeWidth;
                    $detail->strokeDashArray = $datlay->strokeDashArray;
                    $detail->strokeLineCap = $datlay->strokeLineCap;
                    $detail->strokeLineJoin = $datlay->strokeLineJoin;
                    $detail->strokeMiterLimit = $datlay->strokeMiterLimit;
                    $detail->scaleX = $datlay->scaleX;
                    $detail->scaleY = $datlay->scaleY;
                    $detail->angle = $datlay->angle;
                    $detail->flipX = $datlay->flipX;
                    $detail->flipY = $datlay->flipY;
                    $detail->opacity = $datlay->opacity;
                    $detail->shadow = $datlay->shadow;
                    $detail->visible = $datlay->visible;
                    $detail->clipTo = $datlay->clipTo;
                    $detail->backgroundColor = $datlay->backgroundColor;
                    $detail->globalCompositeOperation = $datlay->globalCompositeOperation;
                    $detail->transformMatrix = $datlay->transformMatrix;
                    $detail->skewX = $datlay->skewX;
                    $detail->skewY = $datlay->skewY;
                    $detail->rx = $datlay->rx;
                    $detail->ry = $datlay->ry;
                    $detail->name = $datlay->name;
                    $detail->selectable = $datlay->selectable;
                    $detail->evented = $datlay->evented;
                    $detail->hasBorders = $datlay->hasBorders;
                    $detail->hasControls = $datlay->hasControls;
                    $detail->hasRotatingPoint = $datlay->hasRotatingPoint;
                    $detail->plylstid = $datlay->plylstid;
                    $detail->plylstnm = $datlay->plylstnm;
                    $detail->plylstdata = $datlay->plylstdata;
                    $detail->objtype = $datlay->objtype;
                    $detail->objstyle = $datlay->objstyle;
                    $detail->src_val = $datlay->src_val;
                    $detail->src_type = $datlay->src_type;
                    $detail->save();
                }
                if($isfullclone == 1){
                    foreach ($datplylst as $datply){
                        $playlist = New NcPlayListDetailPre;
                        $playlist->id_layout = $idlayout;
                        $playlist->obj_name = $datply->obj_name;
                        $playlist->src_val = $datply->src_val;
                        $playlist->ttl = $datply->ttl;
                        $playlist->day_play = $datply->day_play;
                        $playlist->date_play = $datply->date_play;
                        $playlist->time_play = $datply->time_play;
                        $playlist->priority = $datply->priority;
                        $playlist->isvideotime = $datply->isvideotime;
                        $playlist->trigger = $datply->trigger;
                        $playlist->typelocation = $datply->typelocation;
                        $playlist->line = $datply->line;
                        $playlist->stop = $datply->stop;
                        $playlist->geofence = $datply->geofence;
                        $playlist->txtfont = $datply->txtfont;
                        $playlist->txtsize = $datply->txtsize;
                        $playlist->txtisbold = $datply->txtisbold;
                        $playlist->txtfcolor = $datply->txtfcolor;
                        $playlist->txtbgcolor = $datply->txtbgcolor;
                        $playlist->txtbgimg = $datply->txtbgimg;
                        $playlist->txtalignment = $datply->txtalignment;
                        $playlist->pl_prop = $datply->pl_prop;
                        $playlist->save();
                    }
                }
            }
        }

        return "OK";
    }

    public function updateCampaign(Request $request)
    {
        if($request->typeproject=='Bus Stop'){
            $cfgtype ="Stop";
        }else{
            $cfgtype = "Bus";
        }

        $projprior = 5;
        $emailreport = "";

        //todo crosscheck for single busid
        if(is_array($request->busid) && Str::contains($request->busid, ','))
        {
            $imp = implode(',', $request->busid);
        }else{
            $imp = $request->busid;

        }

        $models = NcProject::where('id_project', $request->idproject)->first();
        $models->ver_project = (int) $models->ver_project + 1;
        $models->csr_id = Auth::user()->id;
        $models->mngr_id = Auth::user()->name;
        $models->priority = '1';
        $models->proj_nm = $request->nmproject;
        $models->proj_start = $request->startdate;
        $models->proj_end = $request->enddate;
        $models->group_apply = $imp;
        $models->layout_apply = $request->idlayout;
        $models->type_apply = $cfgtype;
        $models->proj_email = $emailreport;
        $models->isscript = '0';
        $models->proj_gen = date("Y-m-d H:i:s");
        $models->status = '2';
        $models->save();

        $deleteDetail = NcProjectDetail::where('id_project', $request->idproject)->delete();

        $busid = $request->busid;
        //todo crosscheck for count busid
        if(is_array($request->busid)){
            for ($i=0; $i < count($busid); $i++) {
                if($busid[$i]!='multiselect-all'){
                    $detail = New NcProjectDetail;
                    $detail->id_project = $request->idproject;
                    $detail->ver_project = '1';
                    $detail->bus_id = $busid[$i];
                    $detail->lay_id = $request->idlayout;
                    $detail->upload_percent = '0';
                    $detail->start_deploy = date("Y-m-d H:i:s");
                    $detail->save();
                }
            }
        }

        return "saveprojok";
    }

    public function stopCamapaign(Request $request)
    {
        $idproject = $request->id_project;
        $models = NcProject::where('id_project', $idproject)->first();
        $models->status = '4';
        $models->save();

        $detail = NcProjectDetail::where('id_project', $idproject)->get();
        foreach($detail as $det){
            $det->upload_percent = "";
            $det->save();
        }

        return "OK Stop";
    }

    public function deleteCampaign(Request $request)
    {
        $idproject = $request->idproject;
        $idlayout = $request->idlayout;
        $models = NcProject::where('id_project', $idproject)->delete();
        $detail =NcProjectDetail::where('id_project', $idproject)->delete();

        $layout = NcLayoutPre::where('id_layout', 'like', '%'.substr($idlayout,0,11).'%')->delete();
        $laydet = NcLayoutDetailPre::where('id_layout', 'like', '%'.substr($idlayout,0,11).'%')->delete();
        $plylstdet = NcPlayListDetailPre::where('id_layout', 'like', '%'.substr($idlayout,0,11).'%')->delete();

        return "OK Remove";
    }

    public function infoCampaign(Request $request)
    {
        $idproject = $request->idproject;
        $models = NcProject::where('id_project', $idproject)->first();
        $target = NcProjectDetail::where('id_project', $idproject)->count();
        $successfull = NcProjectDetail::where('id_project', $idproject)->where('isfinish', '1')->count();
        $failed = NcProjectDetail::where('id_project', $idproject)->where('isfinish', '0')->orWhere('isfinish', '3')->count();
        $inprogress = NcProjectDetail::where('id_project', $idproject)->where('isfinish', '2')->count();
        $detail = NcProjectDetail::where('id_project', $idproject)->get();
        $lay = NcLayoutPre::where('id_layout', $models->layout_apply)->first();
        $laydet = NcLayoutDetailPre::where('id_layout', $models->layout_apply)->get();
        $playlist = NcPlayListDetailPre::where('id_layout', $models->layout_apply)->get();

        return response()->json(['models' => $models, 'detail' => $detail, 'target' => $target, 'successfull' => $successfull, 'failed' => $failed, 'inprogress' => $inprogress, 'laydet' => $laydet, 'lay' => $lay, 'playlist' => $playlist]);
    }

    public function infoNoticeCampaign()
    {
        $models = NcNotice::orderBy('date_start','asc')->get();
            return datatables()->of($models)
            ->addIndexColumn()->toJson();
    }

    public function saveNoticeCampaign(Request $request)
    {
        $max = NcNotice::max('id_notice');
        $nomor = (int) $max + 1;

        $imp = implode(',', $request->target);

        $models = New NcNotice;
        $models->id_notice = $nomor;
        $models->target_notice = $imp;
        $models->msg_notice = $request->text;
        $models->stat_notice = 'process';
        $models->date_start = date("Y-m-d H:i:s");
        $models->date_expire = date("Y-m-d H:i:s", strtotime("+1 month"));
        $models->save();

        $target = $request->target;
        $start = date("Y-m-d H:i:s");
        $expire=date("Y-m-d H:i:s", strtotime("+1 month"));
        $timeFirst  = strtotime($start);
        $timeSecond = strtotime($expire);
        $timeout = $timeSecond - $timeFirst;

        for ($i=0; $i < count($target); $i++) {
            $busid = $target[$i];
            $vehicle = Vehicles::where('id_vehicle', $busid)->first();

            $detail = New NcNoticeDetail;
            $detail->id_notice = $nomor;
            $detail->busid = $busid;
            $detail->busip = $vehicle->ip_vehicle;
            $detail->msg_notice = $request->text;
            $detail->msg_font_fam  = $request->font;
            $detail->msg_font_size = $request->size;
            $detail->msg_timeout = ($timeout * 60000);
            $detail->msg_stylesheet = "'*{background-color: ".$request->color."; border:0px;overflow-y: hidden;}'";
            $detail->msg_position = $request->position;
            $detail->stat_notice = 'queue';
            $detail->date_start = $start;
            $detail->date_expire = date("Y-m-d H:i:s", strtotime("+1 month"));
            $detail->save();
        }

        return "queueadded";
    }

    public function generateConfig(Request $request)
    {
        $assetlist = [];
        $idproject = $request->idproject;
        $models = NcProject::where('id_project', $idproject)->first();
        $detail = NcProjectDetail::where('id_project', $idproject)->get();
        $lay = NcLayoutPre::where('id_layout', $models->layout_apply)->first();
        $laydet = NcLayoutDetailPre::where('id_layout', $models->layout_apply)->get();
        $playlist = NcPlayListDetailPre::where('id_layout', $models->layout_apply)->get();

        $this->generateMakeConfig($models, $detail, $lay, $laydet, $playlist, $idproject, 'project');

        $this->generateMakeGeofence($playlist, $idproject);

        foreach($detail as $det){
            $this->generateMakeConfig($models, $detail, $lay, $laydet, $playlist, $det->bus_id, 'busip');            
        }

        $fall = explode("|", $lay->fallback);
        $split = explode('] ', $fall[0]);
        $assetlist[] = $split[1];

        foreach($laydet as $ld){
            if($ld->src_type=='image'){
                $assetlist[] = $ld->src_val;
            }
        }

        foreach($playlist as $plylst){
            $assetlist[] = $plylst->src_val;
        }

        $models = NcProject::where('id_project', $idproject)->first();
        $models->status = '3';
        $models->assetlist = implode(",",$assetlist); 
        $models->save();

        $detail = NcProjectDetail::where('id_project', $idproject)->get();
        foreach ($detail as $det){
            $det->isfinish = '0';
            $det->save();
        }
        return 'Update OK';
    }

    public function generateMakeGeofence($playlist, $folder)
    {
        foreach($playlist as $plylst){
            // if(!file_exists("asset_cnxx/".$folder)){
            //     mkdir("asset_cnxx/".$folder, 0777);
            // }
            Storage::disk('tenant')->makeDirectory("nc_config/".$folder, 0755, true, true);
            if($plylst->geofence != "")
            {
                $split = explode(',', $plylst->geofence);
                $data = [];
                for ($x = 0; $x < count($split); $x++) {
                    $geo = NcGeofence::where('id_geofence', $split[$x])->first();
                    $geodtl = NcGeofenceDetail::where('id_geofence', $split[$x])->get();
                    foreach($geodtl as $dtl){
                        $exp = explode('|', $dtl->latlong);
                        for ($a = 0; $a < count($exp); $a++) {
                            if($dtl->label=='' && $dtl->label==null){
                                $data[$geo->name][$dtl->name_polygon][$a] = $exp[$a];
                            }else{
                                $data[$geo->name][$dtl->label][$a] = $exp[$a]; 
                            }
                        }
                    }
                }
            }else{
                $data = [];
            }
            $fbus = fopen(Storage::disk('tenant')->path("nc_config/".$folder."/polygon.json"), "w") or die("Unable to open file!");
            $out = '['.json_encode($data).']';
            fwrite($fbus, trim($out));
            fclose($fbus);
        }
    }

    public function generateMakeConfig($models, $detail, $lay, $laydet, $playlist, $folder, $typefile)
    {
        Storage::disk('tenant')->makeDirectory("nc_config", 0755, true, true);
        // if(!file_exists("asset_cnxx/".$folder)){
        //     mkdir("asset_cnxx/".$folder, 0777);
        // }
        // if($typefile=='busip'){
        //     // $vehicle = Vehicles::where('id_vehicle', $folder)->limit(1)->value('ip_vehicle');
        //     // Storage::disk('tenant')->makeDirectory("nc_config/".$folder, 0755, true, true);
        //     // $fbus = fopen(Storage::disk('tenant')->path("nc_config/".$folder)."/asset_".$vehicle.".txt", "w") or die("Unable to open file!");
        // }else{
            
        // }
        Storage::disk('tenant')->makeDirectory("nc_config/".$folder, 0755, true, true);
        $fbus = fopen(Storage::disk('tenant')->path("nc_config/".$folder)."/asset_list.txt", "w") or die("Unable to open file!");
        $assetlist_write = [];
        foreach($laydet as $det){
            if($det->src_type=='slide'){
                foreach($playlist as $plylst){
                    $assetlist_write[] = $plylst->src_val;//date("Ymd")."_".
                }
            }else if($det->objtype=='image'){
                if($det->src_val !== 'none' && $det->src_val !== '')
                {
                    $assetlist_write[] = $det->src_val;
                }
            }
        }

        if(count($assetlist_write) > 0){
            $assetlist_write = array_unique($assetlist_write);
            foreach ($assetlist_write as $key => $val) {
                fwrite($fbus, $val."\n");
            }
        }

        fclose($fbus);
        $out = "";
        if($lay->bg_layout=='BG001'){
            $width = '1366';
            $height = '768';
        }else if($lay->bg_layout=='BG002'){
            $width = '1920';
            $height = '540';
        }else if($lay->bg_layout=='BG003'){
            $width = '1024';
            $height = '768';
        }else if($lay->bg_layout=='BG004'){
            $width = '1920';
            $height = '1080';
        }

        $globt=0;
        if($lay->screen_offsety!=''||$lay->screen_offsety!=null){
            $globt = $lay->screen_offsety;
        }

        $globl=0;
        if($lay->screen_offsetx!=''||$lay->screen_offsetx!=null){
            $globl = $lay->screen_offsetx;
        }

        $fbus1 = fopen(Storage::disk('tenant')->path("nc_config/".$folder)."/bus.ini", "w") or die("Unable to open file!");
        $out = $out."[main_global]";
        $out = $out."\n"."color='#ffffff'";
        $out = $out."\n"."geometry='".$globl.",".$globt.",".$width.",".$height."'";
        $out = $out."\n";
        $out = $out."\n"."[frame_0]";
        $out = $out."\n".'color="'.$lay->bg_fill.'"';
        $out = $out."\n".'geometry="0,0,100,100"';
        $out = $out."\n";

        $e = 0;
        $i = 0;
        $no = 0;
        $cval = 0;
        foreach($laydet as $det){
            $i =$i + 1;
            $e = $e + 1;
            $cval = $cval + 1;
            if($det->src_type!="ticker")
            {
                $out = $out."\n\n".'[label_0_'.$e.']';
                $out = $out."\n".'color="'.$det->fill.'"';
            }

            if(strpos($det->src_val, 'api_route')!== false){
                if(isset(Lib::ses_get("cfg_nc")['geo_translate']) && Lib::ses_get("cfg_nc")['geo_translate']=='1') {
                    $out = $out."\n".'geometry="'.$det->left.','.$det->top.','.$det->width.','.$det->height.'"';
                }else{
                    $out = $out."\n".'geometry="'.($lay->screen_offsetx + $det->left).','.$det->top.','.$det->width.','.$det->height.'"';
                }
            }else if($det->src_type=="ticker"){
                $ticker_geo = $det->left.','.$det->top.','.$det->width.','.$det->height;
            }else{
                $out = $out."\n".'geometry="'.$det->left.','.$det->top.','.$det->width.','.$det->height.'"';
            }

            if($det->src_type=="slide"){
                $out = $out."\n".'src_type="'.$det->src_type.'"';
                $out = $out."\n".'src_val="plylst_label_'.$e.'"';
                $out = $out."\n\n".'[plylst_label_'.$e.']';
                if(count($playlist) > 0){
                    $out = $out."\n".'total="'.count($playlist).'"';
                    foreach($playlist as $plylst){
                        $no = $no + 1;
                        $out = $out."\n\n".'pl_name_'.$no.'="'.$plylst->src_val.'"';
                        $out = $out."\n".'pl_ttl_'.$no.'="'.$plylst->ttl.'"';
                        $out = $out."\n".'pl_day_'.$no.'="'.$plylst->day_play.'"';
                        $out = $out."\n".'pl_date_'.$no.'="'.$plylst->date_play.'"';
                        $out = $out."\n".'pl_time_'.$no.'="'.$plylst->time_play.'"';
                        $out = $out."\n".'pl_prior_'.$no.'="'.$plylst->priority.'"';
                        $out = $out."\n".'pl_triger_'.$no.'="'.$plylst->trigger.'"';
                        $out = $out."\n".'pl_video_time_'.$no.'="'.$plylst->isvideotime.'"';
                    }
                }
            }else if($det->src_type=="api_mov"){
                $out = $out."\n".'src_type="api"';
                $out = $out."\n".'src_val="api_mov"';
                $out = $out."\n".'video_name="'.$det->src_val.'"';
            }else if($det->src_type=="api_mov"){
                $out = $out."\n".'src_type="api"';
                $out = $out."\n".'src_val="api_mov"';
                $out = $out."\n".'video_name="'.$det->src_val.'"';
            }else if($det->src_val=="api_route"){
                $out = $out."\n".'src_type="api"';
                $out = $out."\n".'src_val="api_route_cnxx"';
            }else{
                if($det->src_type!="ticker"){
                    if($det->src_type=='labeledRect'){
                        $srctype = "api_route_cnxx";
                    }else{
                        $srctype = $det->src_type;
                    }
                    $out = $out."\n".'src_type="'.$srctype.'"';
                    $out = $out."\n".'src_val="'.$det->src_val.'"';
                }
                if($det->src_val=="api_weather" || $det->src_val=="api_weatherexhibit"){
                    $out = $out."\n".'src_city="canada"';
                }
            }

            if(strlen($det->objstyle)>2){
                $out = $out."\n".'stylesheet="'.$det->objstyle.'"';
            }
        }

        if(isset($lay->usepft)){
            $usepft=$lay->usepft;
        }else{
            $usepft=0;
        }

        if($usepft=="Use PFT"){
            $lmagic=0;
            if($width=="1920"){
                $lmagic=((1366+$width)-18);            
            }else{
                $lmagic=(($width+$width)-18);
            }
            $out = $out."\n\n".'[pftui]';
            $out = $out."\n".'geometry="'.$lmagic.',0,18,18"';
            $out = $out."\n".'src_type="zpftui"';
            $out = $out."\n".'stylesheet="*{background-color:black;}"';
        }

        $out = $out."\n\n".'[zlayout_frame_0]';
        $out = $out."\n".'type="horizontal"';
        $out = $out."\n".'margin="0"';

        $out = $out."\n\n".'[zlayout_main]';
        $out = $out."\n".'type="horizontal"';
        $out = $out."\n".'items="frame_0"';
        $out = $out."\n".'margin="0"';
        $out = $out."\n".'spacing="0"';

        $fall=explode("|", $lay->fallback);

        $split = explode('] ', $fall[0]);
        $out = $out."\n\n".'[config_fallback]';
        $out = $out."\n".'src_type="'.str_replace('[', '', $split[0]).'"';
        $out = $out."\n".'src_val="'.$split[1].'"';
        $out = $out."\n".'ttl="'.$fall[1].'"';
        $out = $out."\n".'stylesheet="'.$fall[2].'"';

        fwrite($fbus1, trim($out));
        fclose($fbus1);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('infovision::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('infovision::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function showPlayList(Request $request)
    {
        if ($request->ajax()) {
            $models = NcPlayList::orderBy('created_at','asc')->get();
            return datatables()->of($models)
                ->addColumn('action', function ($row) {
                    $html = '<a href="javascript:void(0)" class="btn btn-primary loadDataPlayList" data-value="'.$row->id_playlist.'" style="margin-right: 5px;"><i class="fa fa-play"></i></a>';
                    $html .= '<a href="javascript:void(0)" class="btn btn-danger removeDataPlayList" data-value="'.$row->id_playlist.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->addIndexColumn()->toJson();
        }
    }

    public function showPlayListDetail(Request $request)
    {
        $values = $request->id_playlist;
        $models = NcPlayList::where('id_playlist', $values)->first();
        $detail = NcPlayListDetail::where('id_playlist', $values)->get();

        return response()->json(['models' => $models, 'detail' => $detail]);
    }

    public function savePlayList(Request $request)
    {
        $max = NcPlayList::max('id_playlist');
        $nomor = $max + 1;
        $models = New NcPlayList;
        $models->id_playlist = $nomor;
        $models->nm_playlist = $request->nameplaylist;
        $models->createby = Auth::user()->id;
        $models->save();

        $detail = $request->models;

        for ($x = 0; $x < count($detail); $x++) {
            $splitday = explode(',', $detail[$x]['dayobject']);
            if(count($splitday)==8){
                $day = 'all';
            }else{
                $day = $detail[$x]['dayobject'];
            }
            if($detail[$x]['locationtriggerobject']=='Yes'){
                $trigger = 'halte:all';
            }else{
                $trigger = '-';
            }
            if($detail[$x]['typelocation']=='2'){
                $typelocation = 'geofence';
            }else{
                $typelocation = 'line';
            }
            
            $geofence = '';
            if(isset($detail[$x]['geofence']) && $detail[$x]['geofence']!=='undefined'){
                $geofence = implode(',', $detail[$x]['geofence']);
            }
            $modelDetail = New NcPlayListDetail;
            $modelDetail->id_playlist = $nomor;
            $modelDetail->src_val = $detail[$x]['mediavalues'];
            $modelDetail->ttl = $detail[$x]['ttlobject'];
            $modelDetail->day_play = $day;
            $modelDetail->date_play = $detail[$x]['datestartobject'].'-'.$detail[$x]['dateendobject'];
            $modelDetail->time_play = $detail[$x]['timestartobject'].'-'.$detail[$x]['timeendobject'];
            $modelDetail->priority = $detail[$x]['priorityobject'];
            $modelDetail->isvideotime = $detail[$x]['videotimeobject'];
            $modelDetail->trigger = $trigger;
            $modelDetail->typelocation = $typelocation;
            $modelDetail->line = $detail[$x]['lineobject'];
            $modelDetail->stop = $detail[$x]['stopobject'];
            $modelDetail->geofence = $geofence;
            $modelDetail->txtfont = "Courier";
            $modelDetail->txtsize = "27";
            $modelDetail->txtfcolor = "#000000";
            $modelDetail->txtbgcolor = "#FFFFFF";
            $modelDetail->txtbgimg = $detail[$x]['mediavalues'];
            $modelDetail->save();
        }

        return response()->json(['models' => $nomor]);
    }

    public function saveLayout(Request $request)
    {
        $max = NcLayoutPre::max('id_layout');
        $id = substr($max,3);
        if($id==false){
            $id = 0000;
        }
        $new = (int) $id + 1;
        $new = str_pad($new, 4, "0", STR_PAD_LEFT);
        if($request->idlayout==''){
            $idlayout = "LY".$new;
        }else{
            $idlayout = $request->idlayout;
        }

        if(isset($request->ishide)){
            $isshow = '0';
        }else{
            $isshow = '1';
        }

        $splitfallbackdata = explode('|', $request->fallbackdata);
        $splitbg = explode(' ', $splitfallbackdata[0]);

        $deleteDetail = NcLayoutDetailPre::where('id_layout', $idlayout)->delete();
        $deletePlaylistPre = NcPlayListDetailPre::where('id_layout', $idlayout)->delete();

        if($request->isupdate==0){
            $deleteModels = NcLayoutPre::where('id_layout', $idlayout)->delete();
            $models = New NcLayoutPre;
            $models->id_layout = $idlayout;
            $models->id_op = Auth::user()->id;
            $models->nm_layout = $request->nmlayout;
            $models->bg_layout = $request->idbg;
            $models->bg_fill = $splitfallbackdata[2];
            $models->pic_layout = $idlayout.'.png';
            $models->fallback = $request->fallbackdata;
            $models->usepft = $request->usepft;
            $models->screen_pos = $request->screenpos;
            $models->screen_offsetx = $request->screenoffsetx;
            $models->createby = Auth::user()->id;
            $models->isdefault = 1;
            $models->isshow = $isshow;
            $models->save();
        }else{
            $models = NcLayoutPre::where('id_layout', $idlayout)->first();
            $models->id_layout = $idlayout;
            $models->id_op = Auth::user()->id;
            $models->nm_layout = $request->nmlayout;
            $models->bg_layout = $request->idbg;
            $models->bg_fill = $splitfallbackdata[2];
            $models->pic_layout = $idlayout.'.png';
            $models->fallback = $request->fallbackdata;
            $models->usepft = $request->usepft;
            $models->screen_pos = $request->screenpos;
            $models->screen_offsetx = $request->screenoffsetx;
            $models->createby = Auth::user()->id;
            $models->isdefault = 1;
            $models->isshow = $isshow;
            $models->save();
        }
        

        $datjs=json_decode($request->datlayout);
        $clayobj=count($datjs->objects);
        foreach ($datjs->objects as $val) {
            if($val->objtype=='slide'){
                $dataplaylist = $val->plylstdata;
                if(is_array($dataplaylist) || $dataplaylist!="")
                {
                    for ($x = 0; $x < count($dataplaylist); $x++) {
                        $splitday = explode(',', $dataplaylist[$x]->dayobject);
                        if(count($splitday)==8){
                            $day = 'all';
                        }else{
                            $day = $dataplaylist[$x]->dayobject;
                        }
                        if($dataplaylist[$x]->locationtriggerobject=='Yes'){
                            $trigger = 'halte:all';
                        }else{
                            $trigger = '-';
                        }
                        //fixme
                        //Undefined property: stdClass::$typelocation
                        if(property_exists($dataplaylist[$x], 'typelocation') == true && $dataplaylist[$x]->typelocation=='2'){
                            $typelocation = 'geofence';
                        }else{
                            $typelocation = 'line';
                        }

                        if(property_exists($dataplaylist[$x], 'geofence') == true &&  $dataplaylist[$x]->geofence!==null &&  $dataplaylist[$x]->geofence!==""){
                            $impgeo = implode(',', $dataplaylist[$x]->geofence);
                        }else{
                            $impgeo = "";//$dataplaylist[$x]->geofence;
                        }

                        if(is_array($dataplaylist[$x]->lineobject) && count($dataplaylist[$x]->lineobject) > 0){
                            $lineobjects = implode(',', $dataplaylist[$x]->lineobject);
                        }else{
                            $lineobjects = "";
                        }

                        $playlist = New NcPlayListDetailPre;
                        $playlist->id_layout = $idlayout;
                        $playlist->obj_name = $dataplaylist[$x]->title;
                        $playlist->src_val = $dataplaylist[$x]->mediavalues;
                        $playlist->ttl = $dataplaylist[$x]->ttlobject;
                        $playlist->day_play = $day;
                        $playlist->date_play = $dataplaylist[$x]->datestartobject.'-'.$dataplaylist[$x]->dateendobject;
                        $playlist->time_play = $dataplaylist[$x]->timestartobject.'-'.$dataplaylist[$x]->timeendobject;
                        $playlist->priority = $dataplaylist[$x]->priorityobject;
                        $playlist->isvideotime = $dataplaylist[$x]->videotimeobject;
                        $playlist->trigger = $trigger;
                        $playlist->typelocation = $typelocation;
                        $playlist->line = $lineobjects;//$dataplaylist[$x]->lineobject;
                        $playlist->geofence = $impgeo;
                        $playlist->txtfont = "Courier";
                        $playlist->txtsize = "27";
                        $playlist->txtfcolor = "#000000";
                        $playlist->txtbgcolor = "#FFFFFF";
                        $playlist->txtbgimg = $dataplaylist[$x]->mediavalues;
   
                        $playlist->save();
                    }
                }
            }
            $detail = New NcLayoutDetailPre;
            $detail->id_layout = $idlayout;
            $detail->type = $val->type;
            $detail->originX = $val->originX;
            $detail->originY = $val->originY;
            $detail->left = $val->left;
            $detail->top = $val->top;
            $detail->width = $val->width;
            $detail->height = $val->height;
            $detail->fill = $val->fill;
            $detail->stroke = $val->stroke;
            $detail->strokeWidth = $val->strokeWidth;
            $detail->strokeDashArray = $val->strokeDashArray;
            $detail->strokeLineCap = $val->strokeLineCap;
            $detail->strokeLineJoin = $val->strokeLineJoin;
            $detail->strokeMiterLimit = $val->strokeMiterLimit;
            $detail->scaleX = $val->scaleX;
            $detail->scaleY = $val->scaleY;
            $detail->angle = $val->angle;
            $detail->flipX = $val->flipX;
            $detail->flipY = $val->flipY;
            $detail->opacity = $val->opacity;
            $detail->shadow = $val->shadow;
            $detail->visible = $val->visible;
            $detail->clipTo = $val->clipTo;
            $detail->backgroundColor = $val->backgroundColor;
            $detail->globalCompositeOperation = $val->globalCompositeOperation;
            $detail->transformMatrix = $val->transformMatrix;
            $detail->skewX = $val->skewX;
            $detail->skewY = $val->skewY;
            $detail->rx = $val->rx;
            $detail->ry = $val->ry;
            $detail->name = $val->name;
            $detail->selectable = $val->selectable;
            $detail->evented = $val->evented;
            $detail->hasBorders = $val->hasBorders;
            $detail->hasControls = $val->hasControls;
            $detail->hasRotatingPoint = $val->hasRotatingPoint;
            $detail->plylstid = $val->plylstid;
            $detail->plylstnm = $val->plylstnm;
            $detail->plylstdata = '';
            $detail->objtype = $val->objtype;
            $detail->objstyle = $val->objstyle;
            $detail->src_val = $val->src_val;
            $detail->src_type = $val->src_type;
            $detail->save();
        }

        $img = trim($request->canvasimg);//$canvasimg;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        //$file = public_path()."/layouts/".$idlayout.".png";
        //$success = file_put_contents($file, $data);
        Storage::disk('tenant')->makeDirectory("nc_layouts", 0755, true, true);
        $success = Storage::disk('tenant')->put("nc_layouts/".$idlayout.".png", $data);

        return response()->json(['idlayout' => $idlayout, 'nmlayout' => $models->nm_layout]);
    }

    public function cloneLayoutMain(Request $request)
    {
        $idlayout = $request->idlayout;
        $nmlayout = $request->nmlayout;

        $models_old = NcLayoutPre::where('id_layout', $idlayout)->first();
        $detail_old = NcLayoutDetailPre::where('id_layout', $idlayout)->get();

        $max = NcLayoutPre::max('id_layout');
        $id = substr($max,3);
        if($id==false){
            $id = 0000;
        }
        $new = (int) $id + 1;
        $new = str_pad($new, 4, "0", STR_PAD_LEFT);
        $idlayout = "LY".$new;

        $models = New NcLayoutPre;
        $models->id_layout = $idlayout; 
        $models->id_op = Auth::user()->id; 
        $models->nm_layout = $nmlayout; 
        $models->bg_layout = $models_old->bg_layout; 
        $models->bg_fill = $models_old->bg_fill; 
        $models->pic_layout = $idlayout.'.png'; 
        $models->fallback = $models_old->fallback; 
        $models->usepft = $models_old->usepft; 
        $models->screen_pos = $models_old->screen_pos; 
        $models->screen_offsetx = $models_old->screen_offsetx; 
        $models->createby = Auth::user()->id; 
        $models->isdefault = $models_old->isdefault; 
        $models->isshow = $models_old->isshow; 
        $models->save();

        foreach ($detail_old as $det) {
            $detail = New NcLayoutDetailPre;
            $detail->id_layout = $idlayout;            
            $detail->type = $det->type;
            $detail->originX = $det->originX;
            $detail->originY = $det->originY;
            $detail->left = $det->left;
            $detail->top = $det->top;
            $detail->width = $det->width;
            $detail->height = $det->height;
            $detail->fill = $det->fill;
            $detail->stroke = $det->stroke;
            $detail->strokeWidth = $det->strokeWidth;
            $detail->strokeDashArray = $det->strokeDashArray;
            $detail->strokeLineCap = $det->strokeLineCap;
            $detail->strokeLineJoin = $det->strokeLineJoin;
            $detail->strokeMiterLimit = $det->strokeMiterLimit;
            $detail->scaleX = $det->scaleX;
            $detail->scaleY = $det->scaleY;
            $detail->angle = $det->angle;
            $detail->flipX = $det->flipX;
            $detail->flipY = $det->flipY;
            $detail->opacity = $det->opacity;
            $detail->shadow = $det->shadow;
            $detail->visible = $det->visible;
            $detail->clipTo = $det->clipTo;
            $detail->backgroundColor = $det->backgroundColor;
            $detail->globalCompositeOperation = $det->globalCompositeOperation;
            $detail->transformMatrix = $det->transformMatrix;
            $detail->skewX = $det->skewX;
            $detail->skewY = $det->skewY;
            $detail->rx = $det->rx;
            $detail->ry = $det->ry;
            $detail->name = $det->name;
            $detail->selectable = $det->selectable;
            $detail->evented = $det->evented;
            $detail->hasBorders = $det->hasBorders;
            $detail->hasControls = $det->hasControls;
            $detail->hasRotatingPoint = $det->hasRotatingPoint;
            $detail->plylstid = $det->plylstid;
            $detail->plylstnm = "";
            $detail->plylstdata = "";
            $detail->objtype = $det->objtype;
            $detail->objstyle = $det->objstyle;
            $detail->src_val = "";
            $detail->src_type = $det->src_type;
            $detail->save();
        }

        //$file = public_path()."/layouts/".$models_old->pic_layout;
        //$newfile = public_path()."/layouts/".$idlayout.".png";
        $file = url('/').'/admin/infovision/layout/'.$models_old->pic_layout;
        $newfile = url('/').'/admin/infovision/layout/'.$idlayout.".png";

        if (!copy($file, $newfile)) {
            echo "failed to copy";
        }

        return "Clone Layout OK";
    }

    public function cloneLayouContenttMain(Request $request)
    {
        $idlayout = $request->idlayout;
        $nmlayout = $request->nmlayout;

        $models_old = NcLayoutPre::where('id_layout', $idlayout)->first();
        $detail_old = NcLayoutDetailPre::where('id_layout', $idlayout)->get();
        $playlist_old = NcPlayListDetailPre::where('id_layout', $idlayout)->get();

        $max = NcLayoutPre::max('id_layout');
        $id = substr($max,3);
        if($id==false){
            $id = 0000;
        }
        $new = (int) $id + 1;
        $new = str_pad($new, 4, "0", STR_PAD_LEFT);
        $idlayout = "LY".$new;

        $models = New NcLayoutPre;
        $models->id_layout = $idlayout; 
        $models->id_op = Auth::user()->id; 
        $models->nm_layout = $nmlayout; 
        $models->bg_layout = $models_old->bg_layout; 
        $models->bg_fill = $models_old->bg_fill; 
        $models->pic_layout = $models_old->pic_layout; 
        $models->fallback = $models_old->fallback; 
        $models->usepft = $models_old->usepft; 
        $models->screen_pos = $models_old->screen_pos; 
        $models->screen_offsetx = $models_old->screen_offsetx; 
        $models->createby = Auth::user()->id; 
        $models->isdefault = $models_old->isdefault; 
        $models->isshow = $models_old->isshow; 
        $models->save();

        foreach ($detail_old as $det) {
            $detail = New NcLayoutDetailPre;
            $detail->id_layout = $idlayout;            
            $detail->type = $det->type;
            $detail->originX = $det->originX;
            $detail->originY = $det->originY;
            $detail->left = $det->left;
            $detail->top = $det->top;
            $detail->width = $det->width;
            $detail->height = $det->height;
            $detail->fill = $det->fill;
            $detail->stroke = $det->stroke;
            $detail->strokeWidth = $det->strokeWidth;
            $detail->strokeDashArray = $det->strokeDashArray;
            $detail->strokeLineCap = $det->strokeLineCap;
            $detail->strokeLineJoin = $det->strokeLineJoin;
            $detail->strokeMiterLimit = $det->strokeMiterLimit;
            $detail->scaleX = $det->scaleX;
            $detail->scaleY = $det->scaleY;
            $detail->angle = $det->angle;
            $detail->flipX = $det->flipX;
            $detail->flipY = $det->flipY;
            $detail->opacity = $det->opacity;
            $detail->shadow = $det->shadow;
            $detail->visible = $det->visible;
            $detail->clipTo = $det->clipTo;
            $detail->backgroundColor = $det->backgroundColor;
            $detail->globalCompositeOperation = $det->globalCompositeOperation;
            $detail->transformMatrix = $det->transformMatrix;
            $detail->skewX = $det->skewX;
            $detail->skewY = $det->skewY;
            $detail->rx = $det->rx;
            $detail->ry = $det->ry;
            $detail->name = $det->name;
            $detail->selectable = $det->selectable;
            $detail->evented = $det->evented;
            $detail->hasBorders = $det->hasBorders;
            $detail->hasControls = $det->hasControls;
            $detail->hasRotatingPoint = $det->hasRotatingPoint;
            $detail->plylstid = $det->plylstid;
            $detail->plylstnm = "";
            $detail->plylstdata = "";
            $detail->objtype = $det->objtype;
            $detail->objstyle = $det->objstyle;
            $detail->src_val = "";
            $detail->src_type = $det->src_type;
            $detail->save();
        }

        foreach ($playlist_old as $ply) {
            $playlist = New NcPlayListDetailPre;
            $playlist->id_layout = $idlayout;
            $playlist->obj_name = $ply->obj_name; 
            $playlist->src_val = $ply->src_val; 
            $playlist->ttl = $ply->ttl; 
            $playlist->day_play = $ply->day_play; 
            $playlist->date_play = $ply->date_play; 
            $playlist->time_play = $ply->time_play; 
            $playlist->priority = $ply->priority; 
            $playlist->isvideotime = $ply->isvideotime; 
            $playlist->trigger = $ply->trigger; 
            $playlist->typelocation = $ply->typelocation; 
            $playlist->line = $ply->line;
            $playlist->stop = $ply->stop;
            $playlist->geofence = $ply->geofence;
            $playlist->txtfont = $ply->txtfont; 
            $playlist->txtsize = $ply->txtsize; 
            $playlist->txtfcolor = $ply->txtfcolor; 
            $playlist->txtbgcolor = $ply->txtbgcolor; 
            $playlist->txtbgimg = $ply->txtbgimg; 
            $playlist->save();
        }

        // $file = public_path()."/layouts/".$models_old->pic_layout;
        // $newfile = public_path()."/layouts/".$idlayout.".png";
        $file = url('/').'/admin/infovision/layout/'.$models_old->pic_layout;
        $newfile = url('/').'/admin/infovision/layout/'.$idlayout.".png";

        if (!copy($file, $newfile)) {
            echo "failed to copy";
        }

        return "Clone Layout & Content OK";
    }

    public function renameLayoutMain(Request $request)
    {
        $idlayout = $request->idlayout;
        $nmlayout = $request->nmlayout;

        $models = NcLayoutPre::where('id_layout', $idlayout)->first();
        $models->nm_layout = $nmlayout;
        $models->save();

        return "Rename Layout OK";
    }

    public function deleteLayoutMain(Request $request)
    {
        $idlayout = $request->idlayout;

        $models = NcLayoutPre::where('id_layout', $idlayout)->delete();
        $detail = NcLayoutDetailPre::where('id_layout', $idlayout)->delete();
        $playlist = NcPlayListDetailPre::where('id_layout', $idlayout)->delete();

        return "Delete Layout OK";
    }

    public function getImageFirst()
    {
        $models = NcContentUpload::orderBy('created_at', 'asc')->first();

        return response()->json(['name' => $models->name, 'type' => $models->type]);
    }

    public function showLayoutManager()
    {
        $models = NcLayoutPre::where('isshow', '1')->get();
        $i = 0;
        foreach ($models as $val) {
            $user = User::where('id', $val->createby)->first();
            $models[$i]['user'] = $user->name;
            $i++;
        }

        return response()->json(['models' => $models]);
    }

    public function prevLayoutManager(Request $request)
    {
        $id = $request->id;
        $models = NcLayoutPre::where('id_layout', $id)->first();
        $detail = NcLayoutDetailPre::where('id_layout', $id)->get();
        $playlist = NcPlayListDetailPre::where('id_layout', $id)->get();

        return response()->json(['models' => $models, 'detail' => $detail, 'playlist' => $playlist]);
    }

    public function showVehicle()
    {
        $models = Vehicles::orderBy('id_region', 'asc')->get();

        return response()->json(['models' => $models]);
    }

    public function editCampaign(Request $request)
    {
        $id = $request->id;
        $models = NcProject::where('id_project', $id)->first();
        $detail = NcLayoutPre::where('id_layout', $models->layout_apply)->first();

        return response()->json(['models' => $models, 'detail' => $detail]);
    }

    public function checkStatusCampaignAll()
    {
        $models = NcProject::where('status', '3')->orWhere('status', '1')->get();
        $i = 0;
        foreach ($models as $val) {
            $detail = NcProjectDetail::where('id_project', $val->id_project)->get();
            $count = count($detail);
            $success = 0;
            foreach ($detail as $det) {
                if($det->upload_percent=='100'){
                    $success = $success + 1;
                }
            }
            $sum = (100/$count) * $success;
            $models[$i]['sum'] = $sum;
            $i++;
        }

        return response()->json(['models' => $models]);   
    }
}
