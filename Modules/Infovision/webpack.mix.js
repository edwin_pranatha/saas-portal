const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('dotenv').config();

require('laravel-mix-merge-manifest');
const path = require('path');

mix.setPublicPath('./').mergeManifest();

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-colorpicker-2.2.0/dist/js/bootstrap-colorpicker.min.js',
    __dirname + '/Resources/assets/js/bootstrap-multiselect.js',
    'node_modules/owlcarousel/owl-carousel/owl.carousel.min.js',
    'node_modules/summernote-0.5.9/dist/summernote.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/jquery-validation/dist/jquery.validate.min.js',
    //'node_modules/jquery-bootstrap-wizard/jquery.bootstrap.wizard.min.js',
    __dirname + '/Resources/assets/js/jquery.bootstrap.wizard.js',
    __dirname + '/Resources/assets/js/wizard.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    'node_modules/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js',   
    __dirname + '/Resources/assets/js/campaign.js'
],  __dirname + '/Resources/assets/compiled/js/campaign.min.js');


mix.scripts([
    'node_modules/jspanel-4.12.0/dist/jspanel.min.js',
    'node_modules/jspanel-4.12.0/dist/extensions/contextmenu/jspanel.contextmenu.js',
    'node_modules/jspanel-4.12.0/dist/extensions/hint/jspanel.hint.js',
    'node_modules/jspanel-4.12.0/dist/extensions/modal/jspanel.modal.js',
    'node_modules/jspanel-4.12.0/dist/extensions/tooltip/jspanel.tooltip.js',
    'node_modules/jspanel-4.12.0/dist/extensions/dock/jspanel.dock.js',
    'node_modules/fabric-1.7.12/dist/fabric.js',
    'node_modules/bootstrap-colorpicker-2.2.0/dist/js/bootstrap-colorpicker.min.js',
    'node_modules/jquery-contextmenu/dist/jquery.contextMenu.min.js',
    'node_modules/jquery-contextmenu/dist/jquery.ui.position.min.js',
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    __dirname + '/Resources/assets/js/bootstrap-multiselect.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/owlcarousel/owl-carousel/owl.carousel.min.js',
    'node_modules/summernote-0.5.9/dist/summernote.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
    'node_modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
    __dirname + '/Resources/assets/js/layoutmanager.js'
],  __dirname + '/Resources/assets/compiled/js/layoutmanager.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    __dirname + '/Resources/assets/js/bootstrap-multiselect.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
    'node_modules/blueimp-tmpl/js/tmpl.min.js',
    'node_modules/blueimp-load-image/js/load-image.all.min.js',
    'node_modules/blueimp-canvas-to-blob/js/canvas-to-blob.min.js',
    'node_modules/blueimp-gallery/js/jquery.blueimp-gallery.min.js',
    'node_modules/blueimp-file-upload/js/jquery.iframe-transport.js',
    'node_modules/blueimp-file-upload/js/jquery.fileupload.js',
    'node_modules/blueimp-file-upload/js/jquery.fileupload-process.js',
    'node_modules/blueimp-file-upload/js/jquery.fileupload-image.js',
    'node_modules/blueimp-file-upload/js/jquery.fileupload-audio.js',
    'node_modules/blueimp-file-upload/js/jquery.fileupload-video.js',
    'node_modules/blueimp-file-upload/js/jquery.fileupload-validate.js',
    'node_modules/blueimp-file-upload/js/jquery.fileupload-ui.js',    
    __dirname + '/Resources/assets/js/contentupload.js'
],  __dirname + '/Resources/assets/compiled/js/contentupload.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    __dirname + '/Resources/assets/js/bootstrap-multiselect.js',
    'node_modules/leaflet-0.7.3/dist/leaflet.js',
    __dirname + '/Resources/assets/js/leaflet.geofencer.js',
    __dirname + '/Resources/assets/js/leaflet.polydrag.js',
    __dirname + '/Resources/assets/js/leaflet.contextmenu.js',
    'node_modules/leaflet.marker.slideto-0.2.0/Leaflet.Marker.SlideTo.js',
    'node_modules/jsts-0.14.0/lib/javascript.util.js',
    'node_modules/jsts-0.14.0/lib/jsts.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/geofence.js',
],  __dirname + '/Resources/assets/compiled/js/geofence.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    __dirname + '/Resources/assets/js/bootstrap-multiselect.js',
    'node_modules/leaflet-0.7.3/dist/leaflet.js',
    __dirname + '/Resources/assets/js/leaflet.geofencer.js',
    __dirname + '/Resources/assets/js/leaflet.polydrag.js',
    __dirname + '/Resources/assets/js/leaflet.contextmenu.js',
    'node_modules/leaflet.marker.slideto-0.2.0/Leaflet.Marker.SlideTo.js',
    'node_modules/jsts-0.14.0/lib/javascript.util.js',
    'node_modules/jsts-0.14.0/lib/jsts.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/geofence-add.js'
],  __dirname + '/Resources/assets/compiled/js/geofence-add.min.js');

mix.options({
    processCssUrls: false,
});
//mix.sass( __dirname + '/Resources/assets/sass/infovision.scss', __dirname + '/Resources/assets/compiled/css/infovision.min.css');
mix.sass( __dirname + '/Resources/assets/sass/campaign.scss', __dirname + '/Resources/assets/compiled/css/campaign.min.css');
mix.sass( __dirname + '/Resources/assets/sass/layoutmanager.scss', __dirname + '/Resources/assets/compiled/css/layoutmanager.min.css');
mix.sass( __dirname + '/Resources/assets/sass/contentupload.scss', __dirname + '/Resources/assets/compiled/css/contentupload.min.css');
mix.sass( __dirname + '/Resources/assets/sass/geofence-add.scss', __dirname + '/Resources/assets/compiled/css/geofence-add.min.css');
mix.sass( __dirname + '/Resources/assets/sass/geofence.scss', __dirname + '/Resources/assets/compiled/css/geofence.min.css');

if (process.env.NODE_ENV === 'production') {
    mix.copy('node_modules/select2-3.5.1/select2.png',  __dirname + '/Resources/assets/compiled/css/select2.png');
    mix.copy('node_modules/select2-3.5.1/select2-spinner.gif',  __dirname + '/Resources/assets/compiled/css/select2-spinner.gif');
    mix.copy('node_modules/select2-3.5.1/select2x2.png',  __dirname + '/Resources/assets/compiled/css/select2x2.png');
    mix.copyDirectory('node_modules/datatables.net-dt/images/',  __dirname + '/Resources/assets/compiled/images/');
    mix.copyDirectory('node_modules/blueimp-gallery/img/',  __dirname + '/Resources/assets/compiled/img/');
    mix.copyDirectory('node_modules/bootstrap-colorpicker-2.2.0/dist/img/bootstrap-colorpicker/',  __dirname + '/Resources/assets/compiled/img/bootstrap-colorpicker/');
    mix.copyDirectory('node_modules/jquery-contextmenu/dist/font/',  __dirname + '/Resources/assets/compiled/css/font/');
}

if (mix.inProduction()) {
    mix.version();
}
