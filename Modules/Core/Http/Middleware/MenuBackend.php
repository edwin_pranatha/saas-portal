<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Lavary\Menu\Facade as Menu;
use \Hyn\Tenancy\Facades\TenancyFacade as Tenancy;
use Illuminate\Support\Str;
use Auth;

class MenuBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check() == false) {
            return redirect()->route('login');
        }
        
        Menu::make('MenuBackend', function ($menu) use ($request){


            $menu->add('Settings', '#')->data([
                'icon' => 'fa fa-cogs',
                'order' => 1,
                'group' => 'Main',
                ]);


                $menu->add('Audit Trail', '#')->data([
                    'icon' => 'fa fa-laptop',
                    'order' => 999,
                    'group' => '',
                    ]);
            // $menu->settings->add('Web Config', 'admin/settings');


            // $menu->add('User Management', '')->data([
            //     'icon' => 'fa fa-user',
            //     'permissions' => 'users-view|roles-view',
            //     'active_match' => ['admin/users*','admin/roles*'],
            //     'group' => 'Main',
            //     'order' => 1
            // ]);
            
            // $menu->userManagement->add('Users', 'admin/users')->data([
            //     'permissions' => 'users-view',
            //     'active_match' => 'admin/users*',
            //     'group' => 'Main',
            //     'order' => 2
            // ])->active("admin/users");

            // $menu->userManagement->add('Roles', 'admin/roles')->data([
            //     'permissions' => 'roles-view',
            //     'active_match' => 'admin/roles*',
            //     'group' => 'Main',
            //     'order' => 3
            // ])->active("admin/roles");

            // $menu->add('Reporting', '#')
            //     ->data([
            //         'icon' => 'fa fa-file',
            //         'group' => ' ',
            //         'active_match' => 'admin/report*',
            //         'order' => 999
            //         ]);

            
            
            $website   = Tenancy::hostname();
            if($website !== null && $website->id !== null && $website->id == 1)
            {
                $menu->add('Admin', '')->data([
                    'icon' => 'fa fa-users',
                    'permissions' => 'tenant.users-view|tenant.modules-view',
                    'active_match' => ['admin/tenant/users','admin/tenant/modules'],
                    'group' => 'Main',
                    'order' => 4
                ]);
                
                $menu->admin->add('Tenant Users', 'admin/tenant/users')->data([
                    'permissions' => 'tenant.users-view',
                    'active_match' => 'admin/tenant/users*',
                    'group' => 'Main',
                    'order' => 5
                ])->active("admin/tenant/users");
                
                $menu->admin->add('Tenant Modules', 'admin/tenant/modules')->data([
                    'permissions' => 'tenant.modules-view',
                    'active_match' => 'admin/tenant/modules*',
                    'group' => 'Main',
                    'order' => 6
                ])->active("admin/tenant/modules");
            }
            
            
        })->filter(function ($item) use ($request) {

            if($item->data('permissions')!=null){
                $permissions = explode('|', $item->data('permissions'));
                if(!Auth::user()->hasAnyPermission($permissions)){
                    return false;
                }
            }


            if ($request->url() === rtrim($item->url(), '#/')) {
                $item->active();
            }
            
            if ($item->active_match) {
                $matches = is_array($item->active_match) ? $item->active_match : [$item->active_match];                
                foreach ($matches as $pattern) {
                    if (Str::is($pattern, $request->path())) {
                        $item->activate();
                    }
                }
            }
            return true;
        })->sortBy('order');
        
        return $next($request);
    }
}
