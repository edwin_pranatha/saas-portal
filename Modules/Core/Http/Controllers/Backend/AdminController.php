<?php

namespace Modules\Core\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Widget;
use Modules\Core\Entities\WidgetDetail;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('core::dashboard.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('core::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function submitWidget(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'label' => 'required',
            'type' => 'required'
        ]);

        if($validator->passes()){
            $label = $request->label;
            $type = $request->type;
            $busid = $request->busid;

            $models = Widget::create([
                'label'  => $label,
                'type'   => $type
            ]);

            if(count($busid) > 0){
                $concession = $request->concession;
                foreach ($busid as $row) {
                    WidgetDetail::create([
                        'concession' => $concession,
                        'busid' => $row,
                        'idwidget' => $models->id
                    ]);
                }
            }

            return response()->json(['success' => $models->id]);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }

}
