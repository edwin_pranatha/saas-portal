<?php

namespace Modules\Core\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;
use DB;
use Hash;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:users-show|users-create|users-edit|users-delete', ['only' => ['index','store']]);
         $this->middleware('permission:users-create', ['only' => ['create','store']]);
         $this->middleware('permission:users-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:users-delete', ['only' => ['destroy']]);
    }
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $models = User::orderBy('id','asc')->get();
            return datatables()->of($models)
                ->addColumn('roles', function ($row) {
                    $html = '';
                    if(!empty($row->getRoleNames())){
                        foreach($row->getRoleNames() as $v){
                            $html .= "<label class='badge badge-success'>".$v."</label>";
                        }
                    }
                    return $html;
                })->addColumn('action', function ($row) {
                    $html = '';
                    $html = "<a class='mb-xs mt-xs mr-xs btn btn-sm btn-info' href='".route('admin.users.show', $row->id)."'>Show</a>";
                    if(Auth::user()->can('users-edit')==1){
                        $html .= "<a class='mb-xs mt-xs mr-xs btn btn-sm btn-primary' href='".route('admin.users.edit', $row->id)."'>Edit</a>";
                    }
                    if(Auth::user()->can('users-delete')==1){
                        $html .= '<a href="#modalUserDel" class="mb-xs mt-xs modal-sizes mr-xs btn btn-sm btn-danger showPopDelUser" value="'.$row->name.'" data-url="'.route('admin.users.destroy', $row->id).'">Delete</a>';
                    }
                    return $html;
                })->addIndexColumn()->toJson();
        }

        return view('core::users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('core::users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);   
        $user = User::create($input);
        $user->assignRole($request->input('roles'));    

        return redirect()->route('admin.users.index')
                        ->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('core::users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();    

        return view('core::users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();    
        $user->assignRole($request->input('roles'));    

        return redirect()->route('admin.users.index')
                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return 'User deleted successfully';
    }
}
