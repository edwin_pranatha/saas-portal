<?php

namespace Modules\Core\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Core\Entities\Tenant;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;
use Hash;
use Yajra\Datatables\Datatables;
use Nwidart\Modules\Facades\Module;
use Modules\Core\Modules\Entities\ModuleEntity;
use Modules\Core\Entities\ModuleData;
use DB;
use Illuminate\Support\Env;
use Illuminate\Support\Str;

class TenantModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        
    }
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $rows = [];
            $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();
            $hostname  = app(\Hyn\Tenancy\Environment::class)->hostname();
            $fqdn      = $hostname->fqdn;
            $tenant    = '';

            if(getenv("TENANCY_MASTER_HOSTNAME") == $fqdn){
                $tenant = 'master';
            }else{
                $models = DB::connection('system')->table('hostnames')
                            ->select('hostnames.fqdn')
                            ->join('websites', 'hostnames.website_id', '=', 'websites.id')
                            ->where('uuid', $website->uuid)
                            ->value('fqdn');
                $tenant = explode('.',$models)[0];
            }
            
            
            $id = 1;
            foreach (Module::all() as $key => $module) {
                if($module->getName()!='Core'){
                    $entity = ModuleEntity::getModuleisActiveTenant($module->getName(), $website->uuid);
                    
                    if($entity!==null) {
                        $status = "";
                        if($entity == 1)
                        {
                            $status = "Yes";
                        }else{
                            $status = "No";
                        }
                        $rows[] = [
                            "id" => $id++,
                            "name" => $module->getName(),
                            "status" => "Installed",
                            "active" => $status,
                            "tenant" => $tenant
                        ];
                    } else {
                        $rows[] = [
                            "id" => $id++,
                            "name" => $module->getName(),
                            "status" => "Not Installed",
                            "active" => "No",
                            "tenant" => $tenant
                        ];
                    }
                }
            }

            return DataTables::of($rows)
                ->addIndexColumn()
                ->addColumn('action', function($data){
                    $data = (object) $data;
                    /*return '<div class="btn-group position-static">
                                <button type="button" class="btn btn-info act-mod-info" data-toggle="modal" data-target="#modal-info-module" data-backdrop="static" data-keyboard="false" data-id="'.$data->name.'"><i class="mr-2 fa fa-info-circle"></i></button>
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></button>
                                <div class="dropdown-menu dropdown-menu-right" style="">
                                    <span role="button" data-id="'.$data->name.'" class="dropdown-item act-mod-enable"><i class="fa fa-check"></i> Enable</span>
                                    <span role="button" data-id="'.$data->name.'" class="dropdown-item act-mod-disable"><i class="fa fa-times"></i> Disable</span>
                                    <span role="button" data-id="'.$data->name.'" class="dropdown-item act-mod-update"><i class="fa fa-refresh"></i> Update</span>
                                    <div class="dropdown-divider"></div>
                                    <span role="button" data-id="'.$data->name.'" class="dropdown-item act-mod-configure"><i class="fa fa-wrench"></i> Configure</span>
                                    <span role="button" data-id="'.$data->name.'" class="dropdown-item act-mod-remove"><i class="fa fa-trash-o"></i> Remove</span>
                                </div>
                            </div>';*/
                    if($data->status=='Not Installed'){
                        $className = ' disabled';
                    }else{
                        $className = '';
                    }
                    return '<div class="btn-group">
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary dropdown-toggle" id="groupAutoAction-'.$data->name.'" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="javascript:void(0)" class="enableWidget'.$className.'" id="'.$data->name.'" data-tenant="'.$data->tenant.'">Enable</a></li>
                                    <li><a href="javascript:void(0)" class="disableWidget'.$className.'" id="'.$data->name.'" data-tenant="'.$data->tenant.'">Disable</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:void(0)" class="installWidget" id="'.$data->name.'" data-tenant="'.$data->tenant.'">Install</a></li>
                                    <li><a href="javascript:void(0)" class="removeWidget" id="'.$data->name.'" data-tenant="'.$data->tenant.'">Remove</a></li>
                                </ul>
                            </div>';
                })
                ->rawColumns(['action'])
                ->make(true);
            /*$databaseName = DB::connection()->getDatabaseName();
            $models = DB::connection('system')->select("select a.*, b.* from modules as a inner join modules_list as b on a.id=b.id where a.uuid='".$databaseName."';");
            return datatables()->of($models)
                ->addColumn('action', function ($row) {
                    $html  = '';
                    $html  = '<a href="#modalFull" class="mb-xs mt-xs mr-xs modal-sizes btn btn-sm btn-info" id="'.$row->id.'">Info</a> ';
                    $html .= '<div class="btn-group">
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary dropdown-toggle" id="groupAutoAction" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="javascript:void(0)" class="enableWidget" id="'.$row->id.'">Enable</a></li>
                                    <li><a href="javascript:void(0)" class="disableWidget" id="'.$row->id.'">Disable</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:void(0)" class="updateWidget" id="'.$row->id.'">Update</a></li>
                                    <li><a href="javascript:void(0)" class="removeWidget" id="'.$row->id.'">Remove</a></li>
                                </ul>
                            </div>';
                    return $html;
                })->addIndexColumn()->toJson();*/
        }else{
            return view('core::tenant.modules_index');
        }
    }

    public function enableModule(Request $request)
    {
        $id = $request->id;
        $tenant = $request->tenant; 
        \Artisan::call('tenant:module-enable '.$id.' '.$tenant);

        return 'Module Success Enable';
    }

    public function disableModule(Request $request)
    {
        $id = $request->id;
        $tenant = $request->tenant;
        \Artisan::call('tenant:module-disable '.$id.' '.$tenant);

        return 'Module Success Disable';
    }

    public function installModule(Request $request)
    {
        $id = $request->id;
        $tenant = $request->tenant;
        \Artisan::call('tenant:module-install '.$id.' '.$tenant);

        return 'Module Success Install';
    }

    public function removeModule(Request $request)
    {
        $id = $request->id;
        $tenant = $request->tenant;
        \Artisan::call('tenant:module-remove '.$id.' '.$tenant);

        return 'Module Success Remove';
    }

    

    public function info(Request $request)
    {
        if ($request->ajax()) {
            $module_name = $request->route()->parameter('name');
            $module = ModuleData::where('name',$module_name)->first();
            if($module !== null)
            {
                $module = $module->toArray();
                return response()->json(
                        [
                            'status' => 'success',
                            'data' => $module
                        ]
                );
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Module not found'
                ])->setStatusCode(404);
            }
        }else{
            return view('core::tenant.modules_index');
        }
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $roles = Role::pluck('name','name')->all();
        // return view('core::users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'name' => 'required',
        //     'email' => 'required|email|unique:users,email',
        //     'password' => 'required|same:confirm-password',
        //     'roles' => 'required'
        // ]);

        // $input = $request->all();
        // $input['password'] = Hash::make($input['password']);   
        // $user = User::create($input);
        // $user->assignRole($request->input('roles'));    

        // return redirect()->route('users.index')
        //                 ->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user = User::find($id);
        //return view('core::users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $user = User::find($id);
        // $roles = Role::pluck('name','name')->all();
        // $userRole = $user->roles->pluck('name','name')->all();    

       // return view('core::users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'name' => 'required',
        //     'email' => 'required|email|unique:users,email,'.$id,
        //     'password' => 'same:confirm-password',
        //     'roles' => 'required'
        // ]);

        // $input = $request->all();
        // if(!empty($input['password'])){ 
        //     $input['password'] = Hash::make($input['password']);
        // }else{
        //     $input = Arr::except($input,array('password'));    
        // }

        // $user = User::find($id);
        // $user->update($input);
        // DB::table('model_has_roles')->where('model_id',$id)->delete();    
        // $user->assignRole($request->input('roles'));    

        // return redirect()->route('users.index')
        //                 ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // User::find($id)->delete();
        // return redirect()->route('users.index')
        //                 ->with('success','User deleted successfully');
    }
}
