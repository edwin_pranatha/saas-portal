<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('core')->group(function() {
    Route::get('/', 'CoreController@index');
});

/*
    /admin/*
*/
Route::prefix('admin')->middleware('auth')->name('admin.')->group(function () {
    // Route::get('users/{user_id}', [
    //     'as' => 'users.info',
    //     'uses' => 'Backend\UserController@info'
    // ]);
    Route::resource('users', Backend\UserController::class);
    Route::post('users/list', 'Backend\UserController@index')->name('users.list');

    Route::resource('roles', Backend\RoleController::class);
    Route::post('roles/list', 'Backend\RoleController@index')->name('roles.list');

    Route::get("settings", function(){
        return view("core::errors.maintenance");
    })->name('settings');

    
    Route::get("version-history", function(){
        return view("core::version.index");
    })->name('settings');

    /*
        /admin/tenant/*
    */
    Route::prefix('tenant')->name('tenant.')->group(function () {
        Route::resource('users', Backend\TenantUserController::class);
        Route::post('users', 'Backend\TenantUserController@index')->name('users.home');
        Route::post('users/get/values', 'Backend\TenantUserController@getValuesModal')->name('users.get.values');
        Route::post('users/set/values', 'Backend\TenantUserController@setValuesModal')->name('users.set.values');
        Route::get('modules/{name}', [
            'as' => 'modules.info',
            'uses' => 'Backend\TenantModuleController@info'
        ]);
        Route::resource('modules', Backend\TenantModuleController::class, ['except' => 'info']);
        Route::post('modules', 'Backend\TenantModuleController@index')->name('modules.home');
        Route::post('modules/enable', 'Backend\TenantModuleController@enableModule')->name('modules.enable');
        Route::post('modules/disable', 'Backend\TenantModuleController@disableModule')->name('modules.disable');
        Route::post('modules/installed', 'Backend\TenantModuleController@installModule')->name('modules.install');
        Route::post('modules/remove', 'Backend\TenantModuleController@removeModule')->name('modules.remove');
    });
});