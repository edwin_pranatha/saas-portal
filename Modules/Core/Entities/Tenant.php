<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Hyn\Tenancy\Traits\UsesSystemConnection;

class Tenant extends Model
{
    use HasFactory;
    use UsesSystemConnection;
    protected $table = 'hostnames';


    protected $fillable = [];
    
    public function getUsers()
    {
        return static::orderBy('created_at','desc')->get();
    }
}
