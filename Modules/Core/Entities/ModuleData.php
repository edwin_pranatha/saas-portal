<?php

namespace Modules\Core\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Hyn\Tenancy\Traits\UsesSystemConnection;

class ModuleData extends Model
{
    use HasFactory;
    use UsesSystemConnection;

    protected $table = 'modules_list';


    protected $fillable = [];
}
