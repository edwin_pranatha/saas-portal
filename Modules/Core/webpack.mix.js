const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');
// const webpack = require('webpack');
// mix.webpackConfig({
// plugins: [
//     new webpack.ProvidePlugin({
//         '$': 'jquery',
//         'jQuery': 'jquery',
//         'window.jQuery': 'jquery',
//     }),
// ]
// });

mix.setPublicPath('./').mergeManifest();

mix.scripts([
    __dirname + '/Resources/assets/js/core.js'
],  __dirname + '/Resources/assets/compiled/js/core.min.js');

//mix.copy(__dirname + '/Resources/assets/js/core.js', __dirname + '/Resources/assets/compiled/js/core.js');
mix.sass( __dirname + '/Resources/assets/sass/core.scss', __dirname + '/Resources/assets/compiled/css/core.css');


mix.js(__dirname + '/Resources/assets/js/dashboard.js', __dirname + '/Resources/assets/compiled/js/dashboard.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    __dirname + '/Resources/assets/js/users.js'
],  __dirname + '/Resources/assets/compiled/js/users.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    __dirname + '/Resources/assets/js/roles.js'
],  __dirname + '/Resources/assets/compiled/js/roles.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    __dirname + '/Resources/assets/js/tenant-user.js'
],  __dirname + '/Resources/assets/compiled/js/tenant-user.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    __dirname + '/Resources/assets/js/tenant-module.js'
],  __dirname + '/Resources/assets/compiled/js/tenant-module.min.js');

mix.js(__dirname + '/Resources/assets/js/extension.js', __dirname + '/Resources/assets/compiled/js/extension.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    __dirname + '/Resources/assets/js/version.js'
],  __dirname + '/Resources/assets/compiled/js/version.min.js');

// mix.js(__dirname + '/Resources/assets/js/tenant.js', __dirname + '/Resources/assets/compiled/js/tenant.min.js');

mix.options({
    processCssUrls: false,
});
mix.postCss(__dirname + '/Resources/assets/css/dashboard.css', __dirname + '/Resources/assets/compiled/css/dashboard.min.css');
mix.sass(__dirname + '/Resources/assets/sass/users.scss', __dirname + '/Resources/assets/compiled/css/users.min.css');
mix.sass(__dirname + '/Resources/assets/sass/roles.scss', __dirname + '/Resources/assets/compiled/css/roles.min.css');
mix.sass(__dirname + '/Resources/assets/sass/tenant.scss', __dirname + '/Resources/assets/compiled/css/tenant.min.css');
mix.sass(__dirname + '/Resources/assets/sass/version.scss', __dirname + '/Resources/assets/compiled/css/version.min.css');


if (process.env.NODE_ENV === 'production') {

// mix.copy('node_modules/select2-3.5.1/select2.png',  __dirname + '/Resources/assets/compiled/css/select2.png');
// mix.copy('node_modules/select2-3.5.1/select2-spinner.gif',  __dirname + '/Resources/assets/compiled/css/select2-spinner.gif');
// mix.copy('node_modules/select2-3.5.1/select2x2.png',  __dirname + '/Resources/assets/compiled/css/select2x2.png');

mix.copyDirectory('node_modules/datatables.net-dt/images/',  __dirname + '/Resources/assets/compiled/images/');
}

if (mix.inProduction()) {
    mix.version();
}
