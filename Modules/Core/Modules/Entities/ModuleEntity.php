<?php

namespace Modules\Core\Modules\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Hyn\Tenancy\Traits\UsesSystemConnection;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class ModuleEntity extends Model
{
    use UsesSystemConnection;
    use LogsActivity;

    protected $table = 'modules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'uuid',
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logAll();
        // Chain fluent methods for configuration options
    }
    
    public static function tableName()
    {
        return (new static)->getTable();
    }
    
    /**
     * @param string $name
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|void|null
     */
    public static function findByName(string $name)
    {
        try {
            $find = static::select('is_active')->where('name', $name)->value('is_active');
            return $find;
        } catch (\Throwable $th) {
            return false;
        }
    }

    /**
     * @param string $name
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|void|null
     */
    public static function findByNameTenant(string $name, string $tenant)
    {
        try {
            $find = static::select('is_active')->where('name', $name)->where('uuid', $tenant)->value('is_active');
            return $find;
        } catch (\Throwable $th) {
            return false;
        }
    }

    /**
     * @param string $name
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|void|null
     */
    public static function getModuleisActiveTenant(string $name, string $tenant)
    {
        try {
            $find = static::select('is_active')->where('name', $name)->where('uuid', $tenant)->value('is_active');
            return $find;
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * @param string $name
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|void|null
     */
    public static function findByNameOrFail(string $name)
    {
        return static::query()->where('name', $name)->firstOrFail();
    }

    /**
     * @param string $name
     * @param string $path
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public static function findByNameOrCreate(string $name)//, string $path
    {
        return static::query()->firstOrCreate(['name' => $name]);//, 'path' => $path
    }

    /**
     * @return mixed
     */
    public static function deleteAll()
    {
        return static::query()
            ->delete();
    }

    /**
     * @param $column
     * @param $direction
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function orderBy($column, $direction)
    {
        return static::query()
            ->orderBy($column, $direction);
    }

    /**
     * Determine whether the given status same with a module status.
     *
     * @param $status
     * @return bool
     */
    public function hasStatus(bool $status)
    {
        return $this->is_active === $status;
    }

    /**
     * Set active state for a module.
     *
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->is_active = $active;
        $this->save();
    }
}
