<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;

class DeleteTenant extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenant:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete a specific tenant.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        echo "Deleting tenant ".$this->argument('fqdn').".".env('TENANCY_MASTER_HOSTNAME')."\n";
        if($this->argument('fqdn') == "master")
        {
            $this->error("Tenant ".env('TENANCY_MASTER_HOSTNAME')." cannot be deleted");
        }else{
            $fqdn = $this->argument('fqdn').".".env('TENANCY_MASTER_HOSTNAME');
            if ($hostname = Hostname::where('fqdn', $fqdn)->firstOrFail()) {
                $website = Website::where('id', $hostname->website_id)->firstOrFail();
                app(HostnameRepository::class)->delete($hostname, true);
                app(WebsiteRepository::class)->delete($website, true);
                $this->info("Tenant ".$fqdn." successfully deleted.");
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['fqdn', InputArgument::REQUIRED, 'FQDN Tenant'],
        ];
    }
}
