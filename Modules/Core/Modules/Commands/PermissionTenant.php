<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use DB;

class PermissionTenant extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenant:permission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grant permission to tenant access master database.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        echo "Fixing Permission all tenant";
        $driver = DB::connection()->getDriverName();
        $database = DB::connection()->getDatabaseName();
        echo "\nDriver: ".$driver." Database: ".$database;
        if($driver == "mysql"){
            DB::statement("GRANT ALL PRIVILEGES ON *.* TO '".getenv('DB_USERNAME')."'@'%'");
        }else if($driver == "pgsql"){
            DB::statement('GRANT ALL ON ALL TABLES IN SCHEMA public TO PUBLIC');
        }
        return "ok";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }
}
