<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use Modules\Core\Modules\Entities\ModuleEntity;
use Nwidart\Modules\Module;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\File;
use DB;

class WipeTenant extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenant:wipe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wipe all data related to tenancy to start fresh system (Dangerous!).';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        echo "wiping all tenant\n";
        try {
            $websites = DB::connection(env("DB_CONNECTION"))->table('websites')->get()->toArray();
            $driver = DB::connection()->getDriverName();
            $databases = [];
            if($driver == "mysql"){
                $databases = DB::select('SHOW DATABASES');
            }else if($driver == "pgsql"){
                $databases = DB::select('SELECT datname FROM pg_database');
            }
            $databases = array_map(function($database){
                return $database->datname;
            }, $databases);

            $websites = array_map(function($website){
                return $website->uuid;
            }, $websites);

            foreach ($databases as $database) {
                if(strlen($database) == 32){
                    DB::connection()->statement('DROP DATABASE "'.$database.'"');
                }
                File::cleanDirectory('storage/app/tenancy/tenants');
                File::cleanDirectory('storage/app/tenancy/webserver/apache2');
                DB::statement('TRUNCATE hostnames CASCADE');
                DB::statement('TRUNCATE websites CASCADE');
                DB::statement('TRUNCATE hostnames, websites RESTART IDENTITY');
            }
        } catch (\Exception $e) {
            if(strpos($e->getMessage(), 'relation "websites" does not exist') !== false){
                echo "websites table does not exist";
            }else{
                echo $e->getMessage();
            }
        }
        
        return "ok";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }
}
