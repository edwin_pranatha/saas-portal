<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Environment;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use Illuminate\Support\Facades\DB;

class CleanTenant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean tenant which consider as trash.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "clean unused tenant\n";
        $hostname = DB::table('hostnames')->select('fqdn')->where("fqdn",'<>',env('TENANCY_MASTER_HOSTNAME'))->get();
        foreach ($hostname as $key => $val) {
            $this->deleteTenant($val->fqdn);
        }
    }

    private function deleteTenant($tenantname)
    {
        $fqdn = "{$tenantname}";
        if ($hostname = Hostname::where('fqdn', $fqdn)->firstOrFail()) {
            $website = Website::where('id', $hostname->website_id)->firstOrFail();
            app(HostnameRepository::class)->delete($hostname, true);
            app(WebsiteRepository::class)->delete($website, true);
            $this->info("Tenant {$tenantname} successfully deleted.");
        }
    }
}
