<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;

class MakeTenant extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenant:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a new tenant';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        echo "Creating tenant...";        
        $website = new Website();
        app(WebsiteRepository::class)->create($website);
        $hostname = new Hostname();

        if($this->argument('fqdn') == "master")
        {
            $hostname->fqdn = env('TENANCY_MASTER_HOSTNAME');
        }else{
            $hostname->fqdn = $this->argument('fqdn').".".env('TENANCY_MASTER_HOSTNAME');
        }
        app(HostnameRepository::class)->attach($hostname, $website);
        echo "Tenant ".$hostname->fqdn." has been created";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['fqdn', InputArgument::REQUIRED, 'FQDN Tenant'],
        ];
    }
}
