<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use Modules\Core\Modules\Entities\ModuleEntity;
use Nwidart\Modules\Module;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\Artisan;
use DB;
use Illuminate\Support\Str;

class InstallModuleTenant extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenant:module-install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the specified module of a tenant by Tenant website it will also run the migration.';

    /**
     * Execute the console command.
     */
    public function handle() : int
    {
        /** @var Module $module */

        
        $uuid = $this->convertWebsitetoUUID($this->argument('tenant'));
        
        if($uuid == null)
        {
            $this->error("Tenant [".$this->argument('tenant')."] not found");
            return 0;
        }

        $entity = ModuleEntity::findByNameTenant($this->argument('module'), $uuid);
        if(is_int($entity)) {
            $this->comment("Module [".$this->argument('module')."] is already installed on [".$this->argument('tenant')."]");
        } else {
            $this->info("Preparing to install module [".$this->argument('module')."] for tenant [".$this->argument('tenant')."]");
            
            $entity = ModuleEntity::create([
                'uuid' => $uuid,
                'name' => $this->argument('module'),
                'is_active' => true,
            ]);
            $this->info("Begin migration process...");
            Artisan::call('tenant:module-migrate', [
                'module' => $this->argument('module'),
                'tenant' => $this->argument('tenant'),
            ]);
            
            $this->info("Migration done.");
            $this->info("Module [".$this->argument('module')."] installed for tenant [".$this->argument('tenant')."]");
            
            activity()
            ->useLog('module')
            ->performedOn(new ModuleEntity())
            ->withProperties(['module' => $this->argument('module'), 'tenant' => $this->argument('tenant')])
            ->log('install');
        }
        return 0;
    }

    protected function convertWebsitetoUUID($fqdn)
    {
        if(Str::contains($fqdn, '.'))
        {
            $fqdn = $fqdn;
        }else{
            if($fqdn == "master")
            {
                $fqdn = getenv('TENANCY_MASTER_HOSTNAME');
            }else{
                $fqdn = $fqdn.".".getenv('TENANCY_MASTER_HOSTNAME');
            }
        }

        $otf = new \App\Models\Database(['database' => getenv("TENANCY_DATABASE")]);
        $id = $otf->getTable('hostnames')->select('websites.uuid')
	                ->join('websites', 'hostnames.website_id', '=', 'websites.id')
	                ->where('fqdn', $fqdn)
                    ->value('uuid');
        return $id;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'Module name.'],
            ['tenant', InputArgument::REQUIRED, 'Tenant ID.'],
        ];
    }
}
