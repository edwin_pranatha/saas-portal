<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use Modules\Core\Modules\Entities\ModuleEntity;
use Nwidart\Modules\Module;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\Artisan;
use DB;
use Illuminate\Support\Str;
use Hyn\Tenancy\Database\Connection;

class RemoveModuleTenant extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenant:module-remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove the specified module of a tenant by Tenant website it will also delete the database.';

    /**
     * Execute the console command.
     */
    public function handle() : int
    {
        /** @var Module $module */

        $uuid = $this->convertWebsitetoUUID($this->argument('tenant'));
        if($uuid == null)
        {
            $this->error("Tenant [".$this->argument('tenant')."] not found");
            return 0;
        }

        $entity = ModuleEntity::findByNameTenant($this->argument('module'), $uuid);
        if(is_int($entity)) {
            $this->info("Module [".$this->argument('module')."] found on tenant [".$this->argument('tenant')."]");
            $this->comment("Preparing to remove module [".$this->argument('module')."] on tenant [".$this->argument('tenant')."]");
        
            $this->connection = app(Connection::class);
            $tenant = $this->argument('tenant');
            $database = $this->connection->tenantName();

            $path_migration = base_path().DIRECTORY_SEPARATOR."Modules".DIRECTORY_SEPARATOR.$this->argument('module').DIRECTORY_SEPARATOR."Database".DIRECTORY_SEPARATOR."Migrations";
            $website_id = $this->convertWebsiteOrHostnameToWebsite($tenant.".".env('TENANCY_MASTER_HOSTNAME'));

            ModuleEntity::where('uuid', $uuid)->where('name',$this->argument('module'))->delete();
            
            Artisan::call('tenancy:migrate:reset', [
                '--database' => $database,
                '--realpath' => true,
                '--website_id' => $website_id,
                '--path' => $path_migration,
                '--force' => 1,
            ]);
            $this->info("Migration reset done.");
            $this->info("Module [".$this->argument('module')."] remove for tenant [".$this->argument('tenant')."]");

            
            activity()
            ->useLog('module')
            ->performedOn(new ModuleEntity())
            ->withProperties(['module' => $this->argument('module'), 'tenant' => $this->argument('tenant')])
            ->log('remove');
        }
        return 0;
    }

    protected function convertWebsiteOrHostnameToWebsite($entity)
    {
        $otf = new \App\Models\Database(['database' => getenv("TENANCY_DATABASE")]);
        $id = $otf->getTable('hostnames')->select('websites.id')
	                ->join('websites', 'hostnames.website_id', '=', 'websites.id')
	                ->where('fqdn', $entity)
                    ->value('id');

        return $id;
    }

    protected function convertWebsitetoUUID($fqdn)
    {
        if(Str::contains($fqdn, '.'))
        {
            $fqdn = $fqdn;
        }else{
            if($fqdn == "master")
            {
                $fqdn = env('TENANCY_MASTER_HOSTNAME');
            }else{
                $fqdn = $fqdn.".".env('TENANCY_MASTER_HOSTNAME');
            }
        }

        $otf = new \App\Models\Database(['database' => getenv("TENANCY_DATABASE")]);
        $id = $otf->getTable('hostnames')->select('websites.uuid')
	                ->join('websites', 'hostnames.website_id', '=', 'websites.id')
	                ->where('fqdn', $fqdn)
                    ->value('uuid');
        return $id;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'Module name.'],
            ['tenant', InputArgument::REQUIRED, 'Tenant ID.'],
        ];
    }
}
