<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use Modules\Core\Modules\Entities\ModuleEntity;
use Nwidart\Modules\Module;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use Illuminate\Support\Str;

class DisableModuleTenant extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenant:module-disable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable the specified module of a tenant by Tenant Website.';

    /**
     * Execute the console command.
     */
    public function handle() : int
    {
        /** @var Module $module */

        $tenant = $this->convertWebsitetoUUID($this->argument('tenant'));
        if($tenant == null)
        {
            $this->error("Tenant [".$this->argument('tenant')."] not found in active module list");
            return 0;
        }

        $entity = ModuleEntity::findByNameTenant($this->argument('module'), $tenant);
        if(is_int($entity)) {
            ModuleEntity::where('name', $this->argument('module'))
            ->where('uuid', $tenant)
            ->update(['is_active' => 0, 'updated_at' => now()]);
            $this->info("Module [".$this->argument('module')."] disabled for tenant [".$tenant."]");

            
            activity()
            ->useLog('module')
            ->performedOn(new ModuleEntity())
            ->withProperties(['module' => $this->argument('module'), 'tenant' => $this->argument('tenant')])
            ->log('disabled');
        } else {
            $this->error("Module [".$this->argument('module')."] not found for tenant [".$tenant."]");
        }
        return 0;
    }

    protected function convertWebsitetoUUID($fqdn)
    {
        if(Str::contains($fqdn, '.'))
        {
            $fqdn = $fqdn;
        }else{
            if($fqdn == "master")
            {
                $fqdn = env('TENANCY_MASTER_HOSTNAME');
            }else{
                $fqdn = $fqdn.".".env('TENANCY_MASTER_HOSTNAME');
            }
        }
        $otf = new \App\Models\Database(['database' => getenv("TENANCY_DATABASE")]);
        $id = $otf->getTable('hostnames')->select('websites.uuid')
	                ->join('websites', 'hostnames.website_id', '=', 'websites.id')
	                ->where('fqdn', $fqdn)
                    ->value('uuid');
        return $id;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'Module name.'],
            ['tenant', InputArgument::REQUIRED, 'Tenant ID.'],
        ];
    }
}
