<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use Nwidart\Modules\Module;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Core\Modules\Entities\ModuleEntity;
use DB;
use Illuminate\Support\Str;

class ListModuleTenant extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenant:module-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show list of all modules available of tenant default global.';

    /**
     * Execute the console command.
     */
    public function handle() : int
    {
        $tenant = $this->argument('tenant');
        if($tenant == null)
        {
            $tenant = "master";
            $uuid = $this->convertWebsitetoUUID($tenant);
        }else{
            $uuid = $this->convertWebsitetoUUID($this->argument('tenant'));
        }

        if($uuid == null)
        {
            $this->error("Tenant [".$tenant."] not found");
            return 0;
        }

        $rows = [];
        $this->comment("Module information tenant [".$tenant."]");
        foreach ($this->laravel['modules']->all() as $module) {
            $entity = ModuleEntity::getModuleisActiveTenant($module->getName(), $uuid);
            if($entity!==null) {
                $status = "";
                if($entity == 1)
                {
                    $status = "Yes";
                }else{
                    $status = "No";
                }
                $rows[] = [
                    $module->getName(),
                    "Installed",
                    $status
                ];
            } else {
                $rows[] = [
                    $module->getName(),
                    "Not Installed",
                    "No"
                ];
            }
        }
        $this->table(['Name', 'Status', 'Active'], $rows);
        return 0;
    }


    protected function convertWebsitetoUUID($fqdn)
    {
        if(Str::contains($fqdn, '.'))
        {
            $fqdn = $fqdn;
        }else{
            if($fqdn == "master")
            {
                $fqdn = env('TENANCY_MASTER_HOSTNAME');
            }else{
                $fqdn = $fqdn.".".env('TENANCY_MASTER_HOSTNAME');
            }
        }

        $id = DB::table('hostnames')
	                ->select('websites.uuid')
	                ->join('websites', 'hostnames.website_id', '=', 'websites.id')
	                ->where('fqdn', $fqdn)
                    ->value('uuid');
        return $id;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['only', 'o', InputOption::VALUE_OPTIONAL, 'Types of modules will be displayed.', null],
            ['direction', 'd', InputOption::VALUE_OPTIONAL, 'The direction of ordering.', 'asc'],
        ];
    }

     /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['tenant', InputArgument::OPTIONAL, 'Tenant ID.'],
        ];
    }
}
