<?php

namespace Modules\Core\Modules\Commands;

use Illuminate\Console\Command;
use Nwidart\Modules\Migrations\Migrator;
use Nwidart\Modules\Module;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Hyn\Tenancy\Database\Connection;
use DB;

class MigrateModuleTenant extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenant:module-migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate the migrations from the specified module by Tenant website.';

    /**
     * @var \Nwidart\Modules\Contracts\RepositoryInterface
     */
    protected $module;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() : int
    {
        $this->module = $this->laravel['modules'];

        $name = $this->argument('module');

        if ($name) {
            $module = $this->module->findOrFail($name);

            $this->migrate($module);

            return 0;
        }

        foreach ($this->module->getOrdered($this->option('direction')) as $module) {
            $this->line('Running for module: <info>' . $module->getName() . '</info>');

            $this->migrate($module);
        }

        return 0;
    }

    /**
     * Run the migration from the specified module.
     *
     * @param Module $module
     */
    protected function migrate(Module $module)
    {
        $path = str_replace(base_path(), '', (new Migrator($module, $this->getLaravel()))->getPath());

        if ($this->option('subpath')) {
            $path = $path . "/" . $this->option("subpath");
        }

        $this->connection = app(Connection::class);
        $tenant = $this->argument('tenant');
        $database = $this->connection->tenantName();

        $path_migration = base_path().DIRECTORY_SEPARATOR."Modules".DIRECTORY_SEPARATOR.$module->getName().DIRECTORY_SEPARATOR."Database".DIRECTORY_SEPARATOR."Migrations";
        
        $website_id = $this->convertWebsiteOrHostnameToWebsite($tenant.".".env('TENANCY_MASTER_HOSTNAME'));
        
        $this->call('tenancy:migrate', [
            '--database' => $database,
            '--website_id' => $website_id,
            '--path' => $path_migration,
            '--realpath' => true,
            '--force' => 1,
        ]);

        $module_name = $module->getName();
        $class = "\Modules\\${module_name}\\Database\\Seeders\\${module_name}DatabaseSeeder";

        $this->call('tenancy:db:seed', [
            '--database' => $database,
            '--class' => $class,
            '--force' => 1,
        ]);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'The name of module will be used.'],
            ['tenant', InputArgument::REQUIRED, 'The tenant id will be used.'],
        ];
    }

    protected function convertWebsiteOrHostnameToWebsite($entity)
    {
        $id = DB::table('hostnames')
	                ->select('websites.id')
	                ->join('websites', 'hostnames.website_id', '=', 'websites.id')
	                ->where('fqdn', $entity)
                    ->value('id');

        return $id;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['direction', 'd', InputOption::VALUE_OPTIONAL, 'The direction of ordering.', 'asc'],
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to use.'],
            ['pretend', null, InputOption::VALUE_NONE, 'Dump the SQL queries that would be run.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run when in production.'],
            ['seed', null, InputOption::VALUE_NONE, 'Indicates if the seed task should be re-run.'],
            ['subpath', null, InputOption::VALUE_OPTIONAL, 'Indicate a subpath to run your migrations from'],
        ];
    }
}
