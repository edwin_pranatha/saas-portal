@extends('theme::views.backend.app')

@section('page-title')
<h2>Roles</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Show Role</span></li>
</ol>
@endsection

@section('content')

<div class="row">

    <div class="col-md-12 col-lg-12 col-xl-12">
        <a class="mb-xs mt-xs mr-xs btn btn-primary" href="{{ route('admin.roles.index') }}"> Back</a>
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Show Role</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {{ $role->name }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Permissions:</strong>
                            @if(!empty($rolePermissions))
                            @foreach($rolePermissions as $v)
                            <label class="label label-success">{{ $v->name }},</label>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

</section>  

@endsection