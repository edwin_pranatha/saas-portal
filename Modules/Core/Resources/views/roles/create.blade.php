@extends('theme::views.backend.app')

@section('page-title')
<h2>Roles</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Create New Role</span></li>
</ol>
@endsection

@section('content')

<div class="row">
    <div class="col-md-6 col-lg-12 col-xl-6">
        <a class="mb-xs mt-xs mr-xs btn btn-primary" href="{{ route('admin.roles.index') }}"> Back</a>
    </div>

    <div class="col-md-6 col-lg-12 col-xl-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Create New Roles</h2>
            </header>
            <div class="panel-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {!! Form::open(array('route' => 'admin.roles.store','method'=>'POST')) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-mdd-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Permission:</strong>
                            <br/>
                            <div class="row">
                                @php $group = '' @endphp
                                @foreach($permission as $value)
                                @if($group=='')
                                <div class="col-md-12"><label><b>{{ ucfirst($value->group) }}</b></label></div>
                                @elseif($group!=$value->group)
                                <div class="col-md-12"><label><b>{{ ucfirst($value->group) }}</b></label></div>
                                @endif
                                <div class="col-md-4">
                                    <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                                        {{ $value->name }}</label>
                                </div>
                                @php $group = $value->group @endphp
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
</div>

@endsection