@extends('theme::views.backend.app')

@section('page-title')
<h2>Dashboard</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Dashboard</span></li>
</ol>
@endsection

@section('module_css')
<link rel="stylesheet" href="/assets/Core/css/dashboard.min.css" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:datatable/buttons.dataTables.min.css') }}" >
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/morris/morris.css') }}" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endpush --}}

<div class="row">
    <div class="col-md-12 col-lg-9">
        <section class="panel">
            <header class="panel-heading" style="background-color: #34495e;">
                <div class="panel-actions">
                    <a href="#" class="fa fa-long-arrow-right"></a>
                </div>
                <h2 class="panel-title" style="color: #FFF;">Live Busses</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <h3 style="margin: 0px;">Bus</h3>
                        <table class="table table-striped mb-none" id="dataTableRowBusList">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Bus ID</th>
                                    <th>Trip ID</th>
                                    <th>Route ID</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h3 style="margin: 0px;">Device</h3>
                        <table class="table table-striped mb-none">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name Device</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Router</td>
                                    <td>Running</td>
                                    <td>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</button>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>TFT</td>
                                    <td>Running</td>
                                    <td>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</button>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Camera</td>
                                    <td>Running</td>
                                    <td>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</button>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>GPS</td>
                                    <td>Not Running</td>
                                    <td>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</button>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>People Counting</td>
                                    <td>Running</td>
                                    <td>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</button>
                                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-12 col-lg-3">
        <section class="panel">
            <div class="panel-body" style="height: 454px;background-image: linear-gradient(to left, rgb(120, 142, 164), rgb(52, 73, 94));color: #FFF;">
                <h1 id="sumBusActive" style="margin-top: 95px;">0</h1>
                <h3>Currently Active Busses</h3>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-6">
        <select class="form-control mb-md busListData" style="width: 150px;">
        </select>
        <section class="panel">
            <header class="panel-heading" style="background-color: #34495e;">
                <div class="panel-actions">
                    <a href="#" class="fa fa-long-arrow-right"></a>
                </div>
                <h2 class="panel-title" style="color: #FFF;">Content Management</h2>
            </header>
            <div class="panel-body" style="height: 257px;">
                <input type="file" id="uploadImage" accept="image/*"  onchange="showMyImage(this)" style="display: none;" />
                <div id="clickAreaUploadImage" style="border: 2px solid #34495e;width: 200px;height: 200px;border-radius: 5px;cursor: pointer;">
                    <span id="textUploadImage" style="position: absolute;bottom: 155px;left: 63px;font-weight: bold;font-size: 14px;">Click Upload Image</span>
                    <img id="thumbnil" style="width:90%;"  src="" alt=""/>
                </div>
                <div style="float: right;position: absolute;top: 145px;left: 292px;">
                    <button type="button" id="clickBtnUpload" class="mb-xs mt-xs mr-xs btn btn-lg btn-primary" style="width: 300px;background-color: #34495e;">Upload</button>
                    <div class="progress progress-striped light active m-md" style="margin: 15px 0px !important;">
                        <div class="progress-bar progress-bar-primary" id="progressbarUploadImage" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            60%
                        </div>
                    </div>
                    <p id="textDone" style="margin-top: 20px;font-size: 16px;font-weight: bold;display: none;">Done</p>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-12 col-lg-6">
        <select class="form-control mb-md busListData" id="changeBusList" style="width: 150px;">
        </select>
        <section class="panel">
            <header class="panel-heading" style="background-color: #34495e;">
                <div class="panel-actions">
                    <a href="#" class="fa fa-long-arrow-right"></a>
                </div>
                <h2 class="panel-title" style="color: #FFF;">Portal</h2>
            </header>
            <div class="panel-body" style="height: 257px;">
                <div class="container-fluid" style="">
                    <div id="vehicle-no"><center><h3 style='margin-top: 0px;'>Vehicle No:</h3></center></div>
                    <div class="container" style="width: 1280px;position: absolute;padding: 0px 20px;border: 30px black;border-style: solid;border-radius: 30px;background-color: white;transform: scale(0.514);transform-origin: 0% 0% 0px;">
                        <div class="row" style="font-size: 10px;font-weight: bold;">
                            <div class="col-md-8" style="height:280px;">
                                <div class="row">
                                    <div class="col-md-1 circle" id="journey_destination_time" style="font-size: 35px;font-weight: bold;text-align:center;padding-top: 20px;">
                                    </div>
                                    <div class="col-md-4" id="journey_destination" style="font-size: 20px;margin-top: 6px;height: 60px;">
                                    </div>
                                    <div class="col-md-7" style="">
                                        <div style="margin-top:10px;">
                                            <img src="/assets/Cad/assets/images/wifi.png" alt="" style="width:60px;margin-right:10px;">
                                            <img src="/assets/Cad/assets/images/maestronic.png" alt="" style="width:240px;">
                                            <img src="/assets/Cad/assets/images/cctv.png" alt="" style="width:55px;margin-left:10px;">
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="font-size: 11px;font-weight: bold;text-align:center;">
                                    <div class="col-md-1 timelinelast" style="">
                                        <div class="finaldestination"></div>
                                    </div>
                                    <div class="col-md-3" style="text-align:left;">
                                        NEXT STOPS
                                    </div>
                                    <div class="col-md-2" style="">
                                        ETA (Min:Sec)
                                    </div>
                                    <div class="col-md-3" style="">
                                        SPECIAL SERVICES
                                    </div>
                                    <div class="col-md-3" style="font-size: 10px;">
                                        CONNECTING ROUTE AT STOP
                                    </div>
                                </div>

                                <div id="journey-content"></div>
                                <div id="journey-departure"></div>
                            </div>
                            <div class="col-md-4" style="height:280px;">
                                <div class="row">
                                    <div id="current_time" class="col-md-12" style="font-size: 44px;font-weight: bold;text-align: right;height: 60px;padding-top: 31px;">
                                    </div>
                                </div>
                                <div class="row" style="font-size: 11px;font-weight: bold;">
                                    <div class="col-md-12" style="text-align:center;">
                                       POINT OF INTEREST                                            
                                   </div>
                                </div>
                                <div class="row" style="height: 180px;">
                                    <div id="my-pics" class="carousel slide" data-ride="carousel" style="width:300px;margin:auto;">
                                        <ol class="carousel-indicators">
                                            <li data-target="#my-pics" data-slide-to="0" class="active"></li>
                                            <li data-target="#my-pics" data-slide-to="1"></li>
                                            <li data-target="#my-pics" data-slide-to="2"></li>
                                        </ol>
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img src="/assets/Cad/assets/images/slides/slide1.png" alt="">
                                            </div>
                                            <div class="item">
                                                <img src="/assets/Cad/assets/images/slides/slide2.png" alt="">
                                            </div>
                                            <div class="item">
                                                <img src="/assets/Cad/assets/images/slides/slide3.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-6">
        <section class="panel">
            <header class="panel-heading" style="background-color: #34495e;">
                <div class="panel-actions">
                    <a href="#" class="fa fa-long-arrow-right"></a>
                </div>
                <h2 class="panel-title" style="color: #FFF;">Settings</h2>
            </header>
            <div class="panel-body" style="text-align: center;height: 316px;">
                <a href="{{ route('admin.settings') }}"><i class="fa fa-gears" style="font-size: 150px;margin-top: 55px;"></i></a>
            </div>
        </section>
    </div>
    <div class="col-md-12 col-lg-6">
        <section class="panel">
            <header class="panel-heading" style="background-color: #34495e;">
                <div class="panel-actions">
                    <a href="#" class="fa fa-long-arrow-right"></a>
                </div>
                <h2 class="panel-title" style="color: #FFF;">History</h2>
            </header>
            <div class="panel-body">
                <table class="table table-striped mb-none">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Log Name</th>
                            <th>Description</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Update Form User</td>
                            <td>Update Name User Maestronic</td>
                            <td>2022-04-20 10:30:45</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Update Form User</td>
                            <td>Update Name User Maestronic</td>
                            <td>2022-04-20 10:30:45</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Update Form User</td>
                            <td>Update Name User Maestronic</td>
                            <td>2022-04-20 10:30:45</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Update Form User</td>
                            <td>Update Name User Maestronic</td>
                            <td>2022-04-20 10:30:45</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Update Form User</td>
                            <td>Update Name User Maestronic</td>
                            <td>2022-04-20 10:30:45</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Update Form User</td>
                            <td>Update Name User Maestronic</td>
                            <td>2022-04-20 10:30:45</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Update Form User</td>
                            <td>Update Name User Maestronic</td>
                            <td>2022-04-20 10:30:45</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('core:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/raphael/raphael.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/morris/morris.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/flot/jquery.flot.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/flot/jquery.flot.categories.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('core:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('core:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('core:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script type="text/javascript" src="{{ Module::asset('cad:js/paho-mqtt.js') }}"></script>
<script src="{{ Module::asset('core:js/dashboardglobal.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush --}}

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Core/js/dashboard.min.js"></script>
@endsection
