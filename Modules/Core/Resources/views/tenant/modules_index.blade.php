@extends('theme::views.backend.app')

@section('page-title')
<h2>Tenant Module</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Tenant Module</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Core/css/tenant.min.css" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:datatable/buttons.dataTables.min.css') }}" >
@endpush --}}
<style type="text/css">
    a.disabled {
        pointer-events: none;
        cursor: default;
        color: #acacac !important;;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                    <div id="showMessageAction"></div>
                    <table class="table display nowrap table-hover table-striped table-bordered dataTable table_tenant_modules_list" id="table_tenant_modules_list" data-source="{{ route('admin.tenant.modules.index') }}">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-info-module">
    {{-- <div class="modal show modal-static" id="modal-info-module" style="display: block; overflow-y: hidden;" aria-modal="true" role="dialog"> --}}
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Module Information - <span class="module-name"></span></h4>
                <button type="button" class="close modelClose" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Article was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <ul class="nav nav-tabs customtab" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#module-description" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Detail</span></a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#module-changelog" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Changelog</span></a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#module-feedback" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Feedback</span></a> </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="module-description" role="tabpanel">
                    </div>
                    <div class="p-20 tab-pane" id="module-changelog" role="tabpanel"></div>
                    <div class="p-20 tab-pane" id="module-feedback" role="tabpanel"></div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitEditArticleForm">Update</button>
                <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Core/js/tenant-module.min.js"></script>
@endsection