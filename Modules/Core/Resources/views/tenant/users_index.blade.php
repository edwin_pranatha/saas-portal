@extends('theme::views.backend.app')

@section('page-title')
<h2>Tenant Users</h2>
@endsection


@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Tenant User</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Core/css/tenant.min.css" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:datatable/buttons.dataTables.min.css') }}" >
@endpush --}}
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                    <div id="showMessageAction"></div>
                    <table class="table display nowrap table-hover table-striped table-bordered dataTable table_tenant_users_list" id="table_tenant_users_list" data-source="{{ route('admin.tenant.users.index') }}">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Domain</th>
                                <th>Active Theme</th>
                                <th>Created</th>
                                <th width="280px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="EditUserTenant">
    {{-- <div class="modal show modal-static" id="modal-info-module" style="display: block; overflow-y: hidden;" aria-modal="true" role="dialog"> --}}
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" style="float: left;">Edit Tenant User<span class="module-name"></span></h4>
                <button type="button" class="close modelClose" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <input type="hidden" id="valid">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputDefault">FQDN</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="valfqdn">
                    </div>
                </div>   
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="updateModalTenantUser">Update</button>
                <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Core/js/tenant-user.min.js"></script>
@endsection