@extends('theme::views.backend.app')

@section('page-title')
<h2>User</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Show User</span></li>
</ol>
@endsection

@section('content')

<div class="row">

    <div class="col-md-12 col-lg-12 col-xl-12">
        <a class="mb-xs mt-xs mr-xs btn btn-primary" href="{{ route('admin.users.index') }}"> Back</a>
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Show User</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {{ $user->name }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Email:</strong>
                            {{ $user->email }}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Roles:</strong>
                            @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                            <label class="badge badge-success">{{ $v }}</label>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Core/js/users.min.js"></script>
@endsection
