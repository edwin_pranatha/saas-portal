@extends('theme::views.backend.app')

@section('page-title')
<h2>User</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>User</span></li>
</ol>
@endsection


@section('module_css')
    <link rel="stylesheet" href="/assets/Core/css/users.min.css" />
@endsection

@section('content')
{{-- 
@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('core:datatable/buttons.dataTables.min.css') }}" >
@endpush --}}

<div class="row">
    <div id="modalUserDel" class="modal-block modal-block-primary mfp-hide">
        <section class="panel">
            <div class="text-center panel-body">
                <div class="modal-wrapper">
                    <div class="modal-icon center">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <h4>Are you sure?</h4>
                        <p>Are you sure that you want to delete this user <span id="idUserDel"></span>?</p>
                        <input type="hidden" id="urlDeleteUser" value="">
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="text-right col-md-12">
                        <button class="btn btn-primary" id="delUsers">Yes</button>
                        <button class="btn btn-default modal-dismiss">No</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>

    <div class="col-md-12 col-lg-12 col-xl-12">
        
        @can('users-create')
        <a class="mb-xs mt-xs mr-xs btn btn-primary" href="{{ route('admin.users.create') }}"> Create New User</a>
        @endcan

        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Users</h2>
            </header>
            <div class="panel-body">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif
                <input type="hidden" name="urlinfo" data-url="{{ route('admin.users.list') }}">
                <table class="table table-bordered table-striped mb-none" id="dataTableRowUser">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('core:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('core:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('core:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('core:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('core:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush --}}
{{-- @push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush --}}

{{-- @push('data-scripts')
<script src="{{ Module::asset('core:compiled/users.min.js') }}"></script>
@endpush --}}

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Core/js/users.min.js"></script>
@endsection
