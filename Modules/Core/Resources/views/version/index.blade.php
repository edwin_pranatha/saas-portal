@extends('theme::views.backend.app')

@section('page-title')
<h2>Version History</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Version History</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Core/css/version.min.css" />
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12 col-xl-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>
                <h2 class="panel-title">Changelogs</h2>
            </header>
            <div class="panel-body">
                <div class="timeline timeline-simple mt-xlg mb-md">
                    <div class="tm-body">
                        <div class="tm-title">
                            <h3 class="h5 text-uppercase">May 2022</h3>
                        </div>
                        <ol class="tm-items">
                            <li>
                                <div class="tm-box">
                                    <p class="text-muted mb-none">Version v1.0.3 - 05 May 2022</p>
                                    <p>
                                        <ul>
                                            <li><span class="label label-info">Improvement</span> General
                                                <ul>
                                                    <li><span class="label label-success">Added</span> Versioning is now added please click the version number in bottom right screen to access changelogs</li>
                                                    <li><span class="label label-success">Added</span> Side menu is now grouped for specific functionality</li>
                                                    <li><span class="label label-success">Added</span> Font library has been upgraded from fontAwesome v4 to fontAwesome v6</li>
                                                </ul>
                                            </li>
                                            <li><span class="label label-info">Improvement</span> Transit > Vehicle Monitoring
                                                <ul>
                                                    <li><span class="label label-warning">Fixed</span> Mobile view is now added, user experience issue with mobile devices improved</li>
                                                </ul>
                                            </li>
                                            <li><span class="label label-info">Improvement</span> Info Vision
                                                <ul>
                                                    <li><span class="label label-success">Added</span> Layout manager is improved, added snap to grid feature on WYSIWYG Editor</li>
                                                    <li><span class="label label-success">Added</span> Layout manager not allow widget outscreen anymore</li>
                                                    <li><span class="label label-warning">Fixed</span> Several layout issue has been fixed, which previously marked as not used or working (Sample Layout added)</li>
                                                    <li><span class="label label-warning">Fixed</span> Geofence display issue has been solved, allow to add marker which previously marked as not working</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </p>
                                </div>
                            </li>
                        </ol>
                    </div>




                    <div class="tm-body">
                        <div class="tm-title">
                            <h3 class="h5 text-uppercase">April 2022</h3>
                        </div>
                        <ol class="tm-items">
                            <li>
                                <div class="tm-box">
                                    <p class="text-muted mb-none">Version v1.0.2</p>
                                    <p>
                                        <ul>
                                            <li><span class="label label-warning">Added</span> New Dashboard design<br>
                                                <a class="image-link" href="/assets/Core/assets/img/version/1.0.1_dashboard.png">
                                                    <img src="/assets/Core/assets/img/version/1.0.1_dashboard.png" width="215">
                                                </a>
                                            </li>
                                            <li><span class="label label-info">Improvement</span> Vehicle Monitoring
                                                <ul>
                                                    <li><span class="label label-warning">Fixed</span> Estimated arrival no more display 0, but replaced with PASSED</li>
                                                    <li><span class="label label-success">Added</span> Detour Alert + Alternative route is now showed on stop information (previously skipped)</li>
                                                    <li><span class="label label-warning">Fixed</span> UI while select bus will only focus for that bus specific information </li>
                                                    <li><span class="label label-warning">Fixed</span> UI while select bus it will automatically centered in the screen </li>
                                                    <li><span class="label label-warning">Fixed</span> UI while dragging map will collapsed windows and release mouse drag 
                                                        will show windows again </li>
                                                </ul>
                                            </li>
                                            <li><span class="label label-info">Improvement</span> Screen Mockup
                                                <ul>
                                                    <li><span class="label label-warning">Fixed</span> Estimated arrival no more display 0, but replaced with PASSED</li>
                                                    <li><span class="label label-warning">Fixed</span> Estimated arrival for GTFS Canada is now display actual estimated time, time localization has been fixed </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="tm-box">
                                    <p class="text-muted mb-none">Version v1.0.1</p>
                                    <p>
                                        <ul>
                                            <li><span class="label label-warning">Fixed</span> Fixed Vehicle monitoring bug during select vehicle which still display other buses</li>
                                            <li><span class="label label-warning">Fixed</span> Fixed mockup screen search and next page feature can cause pop is now showed up</li>
                                            <li><span class="label label-warning">Fixed</span> Menu in portal has been adjusted conform the request</li>
                                            <li><span class="label label-warning">Fixed</span> Geofence has been fixed which display invalid information due script conflict</li>
                                            <li><span class="label label-success">Added</span> Support consume data for GTFSR Canada </li>
                                            <li><span class="label label-success">Added</span> Support consume data for GTFS Canada </li>
                                        </ul>
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="tm-box">
                                    <p class="text-muted mb-none">Intial Version v1.0.0</p>
                                    <p>
                                        <ul>
                                            <li><span class="label label-success">Added</span> Support consume data for GTFSR Via</li>
                                            <li><span class="label label-success">Added</span> Support import data for GTFS Via</li>
                                            <li><span class="label label-success">Added</span> Display realtime bus position based on GTFSR on map</li>
                                            <li><span class="label label-success">Added</span> Display mockup screen for upcoming journey information</li>
                                            <li><span class="label label-success">Added</span> Multi tenant support for different operators</li>
                                        </ul>
                                    </p>
                                </div>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Core/js/version.min.js"></script>
@endsection
