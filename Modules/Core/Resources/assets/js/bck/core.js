var ip = APP_URL;
var port = APP_PORT;
if(port !== '' && port !== null){
    var ipaddress = ip + ":" + port;
}else{
    var ipaddress = ip;
}

var ip_siri = SIRI_URL;
var port_siri = SIRI_PORT;
if(port_siri !== '' && port_siri !== null){
    var ipaddressSiri = ip_siri + ":" + port_siri;
}else{
    var ipaddressSiri = ip_siri;
}

$(document).ready(function(){ 
    $('.modal-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,   
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $('.modal-sizes').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,   
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $(document).on('click', '.modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    $('select.populate').select2();

    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function(item) {
                return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
            }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, 
            opener: function(element) {
                return element.find('img');
            }
        }       
    });
});


window.printErrorMsg = function printErrorMsg(msg){
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}

window.time_format = function time_format(d) {
    hours = format_two_digits(d.getHours());
    minutes = format_two_digits(d.getMinutes());
    return hours + ":" + minutes;
}

window.format_two_digits = function format_two_digits(n) {
    return n < 10 ? '0' + n : n;
}

window.getNowDate = function getNowDate(){
    var today = new Date();
    var date = today.getFullYear() + '-' + getValDateTime((today.getMonth()+1)) + '-' + getValDateTime(today.getDate()) + ' ' + getValDateTime(today.getHours()) + ":" + getValDateTime(today.getMinutes());
    return date;
}

window.nowDate = function nowDate(){
    var today = new Date();
    var date = today.getFullYear() + '/' + getValDateTime((today.getMonth()+1)) + '/' + getValDateTime(today.getDate());

    return date;
}

window.beforeDate = function beforeDate(){
    var befdate = new Date(new Date().setDate(new Date().getDate() - 7));
    var date = befdate.getFullYear() + '/' + getValDateTime((befdate.getMonth()+1)) + '/' + getValDateTime(befdate.getDate());

    return date;
}

window.getValDateTime = function getValDateTime(values){
    if(values < 9){
        values = '0' + values;
    }else{
        values;
    }

    return values;
}

window.getTime = function getTime(){
    var today = new Date();
    var time = today.getMinutes() + ":" + today.getSeconds();

    return time;
}


window.bootstrapTabControl = function bootstrapTabControl(){
    var i, items = $('.nav-link'), pane = $('.tab-pane');

    $('.nexttab').on('click', function(){
        for(i = 0; i < items.length; i++){
            if($(items[i]).hasClass('active') == true){
                break;
            }
        }
        if(i < items.length - 1){

            $(items[i]).removeClass('active');
            $(items[i+1]).addClass('active');
            $(items[i+1]).focus();

            $(pane[i]).removeClass('active');
            $(pane[i+1]).addClass('active');
        }
        $('#div1').animate( { scrollLeft: '+=50' }, 1000);
    });

    $('.prevtab').on('click', function(){
        for(i = 0; i < items.length; i++){
            if($(items[i]).hasClass('active') == true){
                break;
            }
        }
        if(i != 0){

            $(items[i]).removeClass('active');
            $(items[i-1]).addClass('active');
            $(items[i-1]).focus();

            $(pane[i]).removeClass('active');
            $(pane[i-1]).addClass('active');
        }
        $('#div1').animate( { scrollLeft: '-=50' }, 1000);
    });
}

bootstrapTabControl();