        $(document).ready(function(){
            var tableRowUser = $('#dataTableRowUser').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'DT_RowIndex', "width" : "80px", "orderable" : true },
                    { data: 'name' },
                    { data: 'email' },
                    { data: 'roles' },
                    { data: 'action' },
                ],
                'columnDefs': [{
                    "targets": 3,
                    "render": function (data, type, row) {
                        var parser = new DOMParser();
                        var doc = parser.parseFromString(data, 'text/html');
                        if(doc.body.firstChild != null){
                            var checkbox = doc.body.firstChild.data;
                        }else{
                            var checkbox = '';
                        }
                        return checkbox;
                    }
                }],
                'fnDrawCallback': function () {
                    $('.modal-sizes').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,   
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in',
                        modal: true
                    });

                    $(".showPopDelUser").click(function() {
                        var urldel = $(this).attr('data-url');
                        var namedel = $(this).attr('value');
                        $('#urlDeleteUser').val(urldel);
                        $('#idUserDel').text(namedel);
                    });
                }
            });

            $("#delUsers").click(function(e) {
                var urldel = $('#urlDeleteUser').val();
                $.ajax({
                    type: "DELETE",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    url: urldel,
                    cache: false,
                    success: function(result){
                        e.preventDefault();
                        $.magnificPopup.close();
                        tableRowUser.fnDraw(false);
                    }
                });
            });
        });