var ip = APP_URL;
var port = APP_PORT;
if(port !== '' && port !== null){
    var ipaddress = ip + ":" + port;
}else{
    var ipaddress = ip;
}

var ip_siri = SIRI_URL;
var port_siri = SIRI_PORT;
if(port_siri !== '' && port_siri !== null){
    var ipaddressSiri = ip_siri + ":" + port_siri;
}else{
    var ipaddressSiri = ip_siri;
}

var ip_gtfs = GTFS_URL;
var port_gtfs = GTFS_PORT;
if(port_gtfs !== '' && port_gtfs !== null){
    var ipaddressGtfs = ip_gtfs + ":" + port_gtfs;
}else{
    var ipaddressGtfs = ip_gtfs;
}

var ip_gtfs_static = GTFS_URL_STATIC;
var port_gtfs_static = GTFS_PORT_STATIC;
var ipaddressGtfs_static;
if(port_gtfs_static !== '' && port_gtfs_static !== null){
    ipaddressGtfs_static = ip_gtfs_static + ":" + port_gtfs_static;
}else{
    ipaddressGtfs_static = ip_gtfs_static;
}

var ip_gtfs_dynamic = GTFS_URL_DYNAMIC;
var port_gtfs_dynamic = GTFS_PORT_DYNAMIC;
var ipaddressGtfs_dynamic;
if(port_gtfs_dynamic !== '' && port_gtfs_dynamic !== null){
    ipaddressGtfs_dynamic = ip_gtfs_dynamic + ":" + port_gtfs_dynamic;
}else{
    ipaddressGtfs_dynamic = ip_gtfs_dynamic;
}

var gtfs_map_lat = GTFS_MAP_LAT;
var gtfs_map_long = GTFS_MAP_LONG;
var gtfs_timezone = GTFS_TIMEZONE;
var gtfs_agency = GTFS_AGENCY;

$(document).ready(function(){ 
    // $('.modal-with-zoom-anim').magnificPopup({
    //     type: 'inline',
    //     fixedContentPos: false,
    //     fixedBgPos: true,
    //     overflowY: 'auto',
    //     closeBtnInside: true,
    //     preloader: false,   
    //     midClick: true,
    //     removalDelay: 300,
    //     mainClass: 'my-mfp-zoom-in',
    //     modal: true
    // });

    // $('.modal-sizes').magnificPopup({
    //     type: 'inline',
    //     fixedContentPos: false,
    //     fixedBgPos: true,
    //     overflowY: 'auto',
    //     closeBtnInside: true,
    //     preloader: false,   
    //     midClick: true,
    //     removalDelay: 300,
    //     mainClass: 'my-mfp-zoom-in',
    //     modal: true
    // });

    // $(document).on('click', '.modal-dismiss', function (e) {
    //     e.preventDefault();
    //     $.magnificPopup.close();
    // });

    // $('select.populate').select2();

    // $('.zoom-gallery').magnificPopup({
    //     delegate: 'a',
    //     type: 'image',
    //     closeOnContentClick: false,
    //     closeBtnInside: false,
    //     mainClass: 'mfp-with-zoom mfp-img-mobile',
    //     image: {
    //         verticalFit: true,
    //         titleSrc: function(item) {
    //             return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
    //         }
    //     },
    //     gallery: {
    //         enabled: true
    //     },
    //     zoom: {
    //         enabled: true,
    //         duration: 300, 
    //         opener: function(element) {
    //             return element.find('img');
    //         }
    //     }       
    // });
});


function printErrorMsg(msg){
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}

function time_format(d) {
    hours = format_two_digits(d.getHours());
    minutes = format_two_digits(d.getMinutes());
    return hours + ":" + minutes;
}

function format_two_digits(n) {
    return n < 10 ? '0' + n : n;
}

function getNowDate(){
    var today = new Date();
    var date = today.getFullYear() + '-' + getValDateTime((today.getMonth()+1)) + '-' + getValDateTime(today.getDate()) + ' ' + getValDateTime(today.getHours()) + ":" + getValDateTime(today.getMinutes());
    return date;
}

function nowDate(){
    var today = new Date();
    var date = today.getFullYear() + '/' + getValDateTime((today.getMonth()+1)) + '/' + getValDateTime(today.getDate());

    return date;
}

function beforeDate(){
    var befdate = new Date(new Date().setDate(new Date().getDate() - 7));
    var date = befdate.getFullYear() + '/' + getValDateTime((befdate.getMonth()+1)) + '/' + getValDateTime(befdate.getDate());

    return date;
}

function getValDateTime(values){
    if(values < 9){
        values = '0' + values;
    }else{
        values;
    }

    return values;
}

function getTime(){
    var today = new Date();
    var time = today.getMinutes() + ":" + today.getSeconds();

    return time;
}


// fix deprecated size method in older jquery
$(function() {
    $.fn.size = function() {
        return this.length;
    }
});
