$(document).ready(function(){                
    var tableRowModuleTenant = $('#table_tenant_modules_list').dataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': $(this).attr('data-source'),
            'type': 'POST',
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        'columns': [
            { data: 'DT_RowIndex', "width" : "30px" },
            { data: 'name', "width" : "250px" },
            { data: 'status', "width" : "250px" },
            { data: 'active', "width" : "250px" },
            { data: 'action', "width" : "250px" },
        ],
        'fnDrawCallback': function () {
            $(".enableWidget").click(function() {
                var values = $(this).attr('id');
                var tenant = $(this).attr('data-tenant');
                setEnableModule(values, tenant);
            });

            $(".disableWidget").click(function() {
                var values = $(this).attr('id');
                var tenant = $(this).attr('data-tenant');
                setDisableModule(values, tenant);
            });

            $(".installWidget").click(function() {
                var values = $(this).attr('id');
                var tenant = $(this).attr('data-tenant');
                setInstallModule(values, tenant);
            });

            $(".removeWidget").click(function() {
                var values = $(this).attr('id');
                var tenant = $(this).attr('data-tenant');
                setRemoveModule(values, tenant);
            });
        }
    });

    function setEnableModule(values, tenant){
        $('#showMessageAction').html('');
        $('#groupAutoAction-' + values).attr('disabled', 'disabled');
        $('#groupAutoAction-' + values).html('Loading...');
        $.ajax({
            type: "POST",
            url: '/admin/tenant/modules/enable',
            data: {
                id: values,
                tenant: tenant
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(result){
                console.log(result);
                $('#groupAutoAction-' + values).removeAttr('disabled');
                $('#groupAutoAction-' + values).html('<span class="caret"></span>');
                $('#showMessageAction').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + result + '</div>');
                tableRowModuleTenant.fnDraw(false);
            }
        });
    }

    function setDisableModule(values, tenant){
        $('#showMessageAction').html('');
        $('#groupAutoAction-' + values).attr('disabled', 'disabled');
        $('#groupAutoAction-' + values).html('Loading...');
        $.ajax({
            type: "POST",
            url: '/admin/tenant/modules/disable',
            data: {
                id: values,
                tenant: tenant
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(result){
                console.log(result);
                $('#groupAutoAction-' + values).removeAttr('disabled');
                $('#groupAutoAction-' + values).html('<span class="caret"></span>');
                $('#showMessageAction').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + result + '</div>');
                tableRowModuleTenant.fnDraw(false);                    
            }
        });
    }

    function setInstallModule(values, tenant){
        $('#showMessageAction').html('');
        $('#groupAutoAction-' + values).attr('disabled', 'disabled');
        $('#groupAutoAction-' + values).html('Loading...');
        $.ajax({
            type: "POST",
            url: '/admin/tenant/modules/installed',
            data: {
                id: values,
                tenant: tenant
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(result){
                console.log(result);
                $('#groupAutoAction-' + values).removeAttr('disabled');
                $('#groupAutoAction-' + values).html('<span class="caret"></span>');
                $('#showMessageAction').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + result + '</div>');
                tableRowModuleTenant.fnDraw(false);
            }
        });
    }

    function setRemoveModule(values, tenant){
        $('#showMessageAction').html('');
        $('#groupAutoAction-' + values).attr('disabled', 'disabled');
        $('#groupAutoAction-' + values).html('Loading...');
        $.ajax({
            type: "POST",
            url: '/admin/tenant/modules/remove',
            data: {
                id: values,
                tenant: tenant
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(result){
                console.log(result);
                $('#groupAutoAction-' + values).removeAttr('disabled');
                $('#groupAutoAction-' + values).html('<span class="caret"></span>');
                $('#showMessageAction').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + result + '</div>');
                tableRowModuleTenant.fnDraw(false);
            }
        });
    }
});