$(document).ready(function(){                
    var tableRowUserTenant = $('#table_tenant_users_list').dataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': $(this).attr('data-source'),
            'type': 'POST',
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        'columns': [
            { data: 'DT_RowIndex', "width" : "30px" },
            { data: 'fqdn', "width" : "250px" },
            { data: 'theme', "width" : "250px" },
            { data: 'created_at', "width" : "250px" },
            { data: 'action', "width" : "250px" },
        ],
        'fnDrawCallback': function () {
            $(".getUserTenant").click(function() {
                var values = $(this).attr('data-id');
                getValUserTenant(values);
            });
        }
    });

    function getValUserTenant(values){
        $.ajax({
            type: "POST",
            url: '/admin/tenant/users/get/values',
            data: {
                id: values
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(result){
                var data = result['data'];
                $('#valfqdn').val(data['fqdn']);
                $('#valid').val(values);
            }
        });
    }

    $("#updateModalTenantUser").click(function() {
        var id = $('#valid').val();
        var fqdn = $('#valfqdn').val();
        setValUserTenant(id, fqdn);
    });

    function setValUserTenant(id, fqdn){
        $("#updateModalTenantUser").html('Loading...');
        $("#updateModalTenantUser").attr('disabled', 'disabled');
        $.ajax({
            type: "POST",
            url: '/admin/tenant/users/set/values',
            data: {
                id: id,
                fqdn: fqdn
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(result){
                $("#updateModalTenantUser").html('Update');
                $("#updateModalTenantUser").removeAttr('disabled');
                $('.modelClose').click();
                tableRowUserTenant.fnDraw(false);
                $('#showMessageAction').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + result + '</div>');
            }
        }); 
    }
});