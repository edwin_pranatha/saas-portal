        $(document).ready(function(){
            $('.modal-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,   
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });
        
            $('.modal-sizes').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,   
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
                modal: true
            });
        
            $(document).on('click', '.modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
        
            $('select.populate').select2();
        
            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: false,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: true,
                    titleSrc: function(item) {
                        return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
                    }
                },
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, 
                    opener: function(element) {
                        return element.find('img');
                    }
                }       
            });

            var tableRowRole = $('#dataTableRowRole').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'language': { search: "" },
                'columns': [
                    { data: 'DT_RowIndex', "width" : "80px", "orderable" : true },
                    { data: 'name' },
                    { data: 'action' },
                ],
                'fnDrawCallback': function () {
                    $('.modal-sizes').magnificPopup({
                        type: 'inline',
                        fixedContentPos: false,
                        fixedBgPos: true,
                        overflowY: 'auto',
                        closeBtnInside: true,
                        preloader: false,   
                        midClick: true,
                        removalDelay: 300,
                        mainClass: 'my-mfp-zoom-in',
                        modal: true
                    });

                    $(".showPopDelRole").click(function() {
                        var urldel = $(this).attr('data-url');
                        var namedel = $(this).attr('value');
                        $('#urlDeleteRole').val(urldel);
                        $('#idRoleDel').text(namedel);
                    });
                }
            });

            $("#delRoles").click(function(e) {
                var urldel = $('#urlDeleteRole').val();
                $.ajax({
                    type: "DELETE",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    url: urldel,
                    cache: false,
                    success: function(result){
                        e.preventDefault();
                        $.magnificPopup.close();
                        tableRowRole.fnDraw(false);
                    }
                });
            });
        });