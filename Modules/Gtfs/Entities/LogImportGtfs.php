<?php

namespace Modules\Gtfs\Entities;

use Illuminate\Database\Eloquent\Model;

class LogImportGtfs extends Model
{
    protected $table = 'log_import_timetable';
    public $incrementing = false;
}
