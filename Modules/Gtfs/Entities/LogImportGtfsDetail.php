<?php

namespace Modules\Gtfs\Entities;

use Illuminate\Database\Eloquent\Model;

class LogImportGtfsDetail extends Model
{
    protected $table = 'log_import_timetable_detail';
    public $incrementing = false;
}
