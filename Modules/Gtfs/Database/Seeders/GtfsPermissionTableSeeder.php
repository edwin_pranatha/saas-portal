<?php

namespace Modules\Gtfs\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class GtfsPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $permissions = [
           'gtfs-view',
           'gtfs-create',
           'gtfs-edit',
           'gtfs-delete',

           'gtfs.import-view',
           'gtfs.routes-view',
           'gtfs.stops-view',
           'gtfs.vehicle.monitoring-view',

           'gtfs.trips-view',
           'gtfs.display-view',
           'gtfs.announce-view',

        ];


        $permissions_canada = [
             'gtfs.canada-view',
             'gtfs.canada.vehicle.monitoring-view',
             'gtfs.canada.display-view',
       ];

        foreach ($permissions as $permission) {
          Permission::firstorCreate(
               [
                    "name" => $permission,
                    "guard_name" => "web",
                    "group" => "GTFS"
               ]
          );
        }

        foreach ($permissions_canada as $permission) {
          Permission::firstorCreate(
               [
                    "name" => $permission,
                    "guard_name" => "web",
                    "group" => "GTFS"
               ]
          );
        }

        $role_admin = Role::findByName('Admin');
        $role_admin->givePermissionTo($permissions);

        $role_canada = Role::findByName('Canada');
        $role_canada->givePermissionTo($permissions_canada);
    }
}
