<?php

namespace Modules\Gtfs\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Gtfs\Database\Seeders\GtfsPermissionTableSeeder;

class GtfsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(GtfsPermissionTableSeeder::class);
        // $this->call("OthersTableSeeder");
    }
}
