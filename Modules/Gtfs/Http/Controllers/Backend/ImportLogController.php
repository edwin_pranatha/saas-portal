<?php

namespace Modules\Gtfs\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Modules\Gtfs\Entities\LogImport;
use Modules\Gtfs\Entities\LogImportDetail;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ImportLogController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('core::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('core::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('core::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('core::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function saveImportSuccess(Request $request)
    {
        $id = $request->id;
        $taskName = $request->task_name;
        $fileName = $request->file_name;
        $dateTime = substr(str_replace('T',' ',$request->date_time),0,19);
        $lastAction = $request->last_action;
        $detail = $request->detail;
        $status = $request->status;
        $type = $request->type;

        $check = LogImport::where('id', $id)->first();
        if(empty($check)){
            $models = New LogImport;
            $models->id = $id;
            $models->task_name = $taskName;
            $models->file_name = $fileName;
            $models->date_time = $dateTime;
            $models->last_action = $lastAction;
            $models->status = $status;
            $models->type = $type;
            $models->save();
        }else{
            $check->task_name = $taskName;
            $check->file_name = $fileName;
            $check->date_time = $dateTime;
            $check->last_action = $lastAction;
            $check->status = $status;
            $check->type = $type;
            $check->save();
        }

        $rows = LogImportDetail::where('id', $id)->delete();
        if($type=='gtfs'){
            $row = New LogImportDetail();
            $row->id = $id;
            $row->date = $dateTime;
            $row->type = $lastAction;
            $row->message = $detail;
            $row->save();
        }else{
            if(count($detail) > 0){
                for ($x = 0; $x < count($detail); $x++) {
                    $row = New LogImportDetail();
                    $row->id = $id;
                    $row->date = substr(str_replace('T',' ',$detail[$x]['date']),0,19);
                    $row->type = $detail[$x]['type'];
                    $row->message = $detail[$x]['message'];
                    $row->save();
                }
            }
        }

        return 'success';
    }

    public function viewLogImportSuccess()
    {
        $models = LogImport::orderBy('date_time', 'desc')->get();

        return view('core::logimport.index', compact('models'));
    }

    public function process_ild(Request $request)
    {
        $id = $request->id;
        $models = LogImportDetail::where('id', $id)->get();
        $data = array();
        foreach($models as $row){
            $data[] = array(
                "date"=>$row->date,
                "type"=>$row->type,
                "message"=>$row->message
            );
        }
        $response = array(
            "aaData" => $data
        );
        return json_encode($response);
    }
}