<?php

namespace Modules\Gtfs\Http\Controllers\Backend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;

class GtfsController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:gtfs-show|gtfs-create|gtfs-edit|gtfs-delete|gtfs.import-view|gtfs.routes-view|gtfs.stops-view|gtfs.vehicle.monitoring-view|gtfs.trips-view|gtfs.display-view|gtfs.announce-view', ['only' => ['index','show','view','store']]);
        $this->middleware('permission:gtfs-create', ['only' => ['create','store']]);
        $this->middleware('permission:gtfs-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:gtfs-delete', ['only' => ['destroy']]);
    }


    public function agency()
    {
        return view('gtfs::agency.index');
    }

    public function process_agency(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (id like '%".$searchValue."%' or 
                agency_id like '%".$searchValue."%' or 
                agency_name like'%".$searchValue."%' ) ";
        }

        // TODO: where for search value need to be improved!
        //$records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from agency;");
        //$totalRecords = $records[0]->allcount;
        //$records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from agency ".$searchQuery);
        //$totalRecordwithFilter = $records[0]->allcount;
        //$records[0]->allcount;    
        //$empRecords = DB::connection('pgsqlgtfs')->select("select * from agency ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'gtfs']);
        $totalRecords = $otf->getTable('agency')->count();
        $totalRecordwithFilter = $otf->getTable('agency')->selectraw("select count(*) as allcount from agency ".$searchQuery)->count();
        $empRecords = $otf->getTable('agency')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "id" => $empRecords[$x]->id,
                    "agency_id" => $empRecords[$x]->agency_id,
                    "agency_name" => $empRecords[$x]->agency_name,
                    "agency_url" => $empRecords[$x]->agency_url,
                    "agency_timezone" => $empRecords[$x]->agency_timezone,
                    "agency_lang" => $empRecords[$x]->agency_lang,
                    "agency_phone" => $empRecords[$x]->agency_phone,
                    "agency_fare_url" => $empRecords[$x]->agency_fare_url,
                    "agency_email" => $empRecords[$x]->agency_email,
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function calendar_date()
    {
        return view('gtfs::calendardates.index');
    }

    public function process_calendardates(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (service_id like '%".$searchValue."%' or 
                date like '%".$searchValue."%' or 
                exception_type like'%".$searchValue."%' ) ";
        }

        $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from calendar_dates;");
        $totalRecords = $records[0]->allcount;
        
        $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from calendar_dates ".$searchQuery);
        $totalRecordwithFilter = $records[0]->allcount;
    
        $empRecords = DB::connection('pgsqlgtfs')->select("select * from calendar_dates ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "service_id" => $empRecords[$x]->service_id,
                    "date" => $empRecords[$x]->date,
                    "exception_type" => $empRecords[$x]->exception_type,
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function feed_info()
    {
        return view('gtfs::feedinfo.index');   
    }

    public function process_feedinfo(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (feed_id like '%".$searchValue."%' or 
                feed_publisher_name like '%".$searchValue."%' or 
                feed_publisher_url like'%".$searchValue."%' ) ";
        }

        // TODO: where for search value need to be improved!
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from feed_info;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from feed_info ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsqlgtfs')->select("select * from feed_info ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'gtfs']);
        $totalRecords = $otf->getTable('feed_info')->count();
        $totalRecordwithFilter = $otf->getTable('feed_info')->selectraw("select count(*) as allcount from feed_info ".$searchQuery)->count();
        $empRecords = $otf->getTable('feed_info')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "feed_id" => $empRecords[$x]->feed_id,
                    "feed_publisher_name" => $empRecords[$x]->feed_publisher_name,
                    "feed_publisher_url" => $empRecords[$x]->feed_publisher_url,
                    "feed_lang" => $empRecords[$x]->feed_lang,
                    "default_lang" => $empRecords[$x]->default_lang,
                    "feed_start_date" => $empRecords[$x]->feed_start_date,
                    "feed_end_date" => $empRecords[$x]->feed_end_date,
                    "feed_version" => $empRecords[$x]->feed_version,
                    "feed_contact_email" => $empRecords[$x]->feed_contact_email,
                    "feed_contact_url" => $empRecords[$x]->feed_contact_url,
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function routes()
    {
        return view('gtfs::routes.index');   
    }

    public function process_routes(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (routes_id like '%".$searchValue."%' or 
                agency_id like '%".$searchValue."%' or 
                route_short_name like'%".$searchValue."%' ) ";
        }

        // TODO: where for search value need to be improved!
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from routes;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from routes ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsqlgtfs')->select("select * from routes ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'gtfs']);
        $totalRecords = $otf->getTable('routes')->count();
        $totalRecordwithFilter = $otf->getTable('routes')->selectraw("select count(*) as allcount from routes ".$searchQuery)->count();
        $empRecords = $otf->getTable('routes')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "route_id" => $empRecords[$x]->route_id,
                    "agency_id" => $empRecords[$x]->agency_id,
                    "route_short_name" => $empRecords[$x]->route_short_name,
                    "route_long_name" => $empRecords[$x]->route_long_name,
                    "route_desc" => $empRecords[$x]->route_desc,
                    "route_type" => $empRecords[$x]->route_type,
                    "route_url" => $empRecords[$x]->route_url,
                    "route_color" => $empRecords[$x]->route_color,
                    "route_text_color" => $empRecords[$x]->route_text_color,
                    "route_sort_order" => $empRecords[$x]->route_sort_order,
                    "continuous_pickup" => $empRecords[$x]->continuous_pickup,
                    "continuous_drop_off" => $empRecords[$x]->continuous_drop_off,
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function shapes()
    {
        return view('gtfs::shapes.index');   
    }

    public function process_shapes(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (shape_id like '%".$searchValue."%' or 
                shape_pt_lat '%".$searchValue."%' or 
                shape_pt_lon like'%".$searchValue."%' ) ";
        }

        // TODO: where for search value need to be improved!
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from shapes;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from shapes ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsqlgtfs')->select("select * from shapes ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'gtfs']);
        $totalRecords = $otf->getTable('shapes')->count();
        $totalRecordwithFilter = $otf->getTable('shapes')->selectraw("select count(*) as allcount from shapes ".$searchQuery)->count();
        $empRecords = $otf->getTable('shapes')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "shape_id" => $empRecords[$x]->shape_id,
                    "shape_pt_lat" => $empRecords[$x]->shape_pt_lat,
                    "shape_pt_lon" => $empRecords[$x]->shape_pt_lon,
                    "shape_pt_sequence" => $empRecords[$x]->shape_pt_sequence,
                    "shape_dist_traveled" => $empRecords[$x]->shape_dist_traveled,
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function stop_times()
    {
        return view('gtfs::stoptimes.index');   
    }

    public function process_stoptimes(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (trip_id like '%".$searchValue."%' or 
                arrival_time '%".$searchValue."%' or 
                departure_time like'%".$searchValue."%' ) ";
        }

        // TODO: where for search value need to be improved!
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from stop_times;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from stop_times ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsqlgtfs')->select("select * from stop_times ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'gtfs']);
        $totalRecords = $otf->getTable('stop_times')->count();
        $totalRecordwithFilter = $otf->getTable('stop_times')->selectraw("select count(*) as allcount from stop_times ".$searchQuery)->count();
        $empRecords = $otf->getTable('stop_times')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "trip_id" => $empRecords[$x]->trip_id,
                    "arrival_time" => $empRecords[$x]->arrival_time,
                    "departure_time" => $empRecords[$x]->departure_time,
                    "stop_id" => $empRecords[$x]->stop_id,
                    "stop_sequence" => $empRecords[$x]->stop_sequence,
                    "stop_headsign" => $empRecords[$x]->stop_headsign,
                    "pickup_type" => $empRecords[$x]->pickup_type,
                    "drop_off_type" => $empRecords[$x]->drop_off_type,
                    "continuous_pickup" => $empRecords[$x]->continuous_pickup,
                    "continuous_drop_off" => $empRecords[$x]->continuous_drop_off,
                    "shape_dist_traveled" => $empRecords[$x]->shape_dist_traveled,
                    "fare_units_traveled" => $empRecords[$x]->fare_units_traveled,
                    "timepoint" => $empRecords[$x]->timepoint,
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function stops()
    {
        return view('gtfs::stops.index');   
    }

    public function process_stops(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (stop_id like '%".$searchValue."%' or 
                stop_code '%".$searchValue."%' or 
                stop_name like'%".$searchValue."%' ) ";
        }

        // TODO: where for search value need to be improved!
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from stops;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from stops ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsqlgtfs')->select("select * from stops ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'gtfs']);
        $totalRecords = $otf->getTable('stops')->count();
        $totalRecordwithFilter = $otf->getTable('stops')->selectraw("select count(*) as allcount from stops ".$searchQuery)->count();
        $empRecords = $otf->getTable('stops')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "stop_id" => $empRecords[$x]->stop_id,
                    "stop_code" => $empRecords[$x]->stop_code,
                    "stop_name" => $empRecords[$x]->stop_name,
                    "tts_stop_name" => $empRecords[$x]->tts_stop_name,
                    "stop_desc" => $empRecords[$x]->stop_desc,
                    "stop_lat" => $empRecords[$x]->stop_lat,
                    "stop_lon" => $empRecords[$x]->stop_lon,
                    "zone_id" => $empRecords[$x]->zone_id,
                    "stop_url" => $empRecords[$x]->stop_url,
                    "location_type" => $empRecords[$x]->location_type,
                    "parent_station" => $empRecords[$x]->parent_station,
                    "stop_timezone" => $empRecords[$x]->stop_timezone,
                    "wheelchair_boarding" => $empRecords[$x]->wheelchair_boarding,
                    "level_id" => $empRecords[$x]->level_id,
                    "platform_code" => $empRecords[$x]->platform_code,
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function transfers()
    {
        return view('gtfs::transfers.index');   
    }

    public function process_transfers(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (id like '%".$searchValue."%' or 
                from_stop_id '%".$searchValue."%' or 
                to_stop_id like'%".$searchValue."%' ) ";
        }

        // TODO: where for search value need to be improved!
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from transfers;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from transfers ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsqlgtfs')->select("select * from transfers ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'gtfs']);
        $totalRecords = $otf->getTable('transfers')->count();
        $totalRecordwithFilter = $otf->getTable('transfers')->selectraw("select count(*) as allcount from transfers ".$searchQuery)->count();
        $empRecords = $otf->getTable('transfers')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "id" => $empRecords[$x]->id,
                    "from_stop_id" => $empRecords[$x]->from_stop_id,
                    "to_stop_id" => $empRecords[$x]->to_stop_id,
                    "from_route_id" => $empRecords[$x]->from_route_id,
                    "to_route_id" => $empRecords[$x]->to_route_id,
                    "from_trip_id" => $empRecords[$x]->from_trip_id,
                    "to_trip_id" => $empRecords[$x]->to_trip_id,
                    "transfer_type" => $empRecords[$x]->transfer_type,
                    "min_transfer_time" => $empRecords[$x]->min_transfer_time,
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function trips()
    {
        return view('gtfs::trips.index');   
    }

    public function process_trips(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (trip_id like '%".$searchValue."%' or 
                route_id '%".$searchValue."%' or 
                service_id like'%".$searchValue."%' ) ";
        }

        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from trips;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsqlgtfs')->select("select count(*) as allcount from trips ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsqlgtfs')->select("select * from trips ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'gtfs']);
        $totalRecords = $otf->getTable('trips')->count();
        $totalRecordwithFilter = $otf->getTable('trips')->selectraw("select count(*) as allcount from trips ".$searchQuery)->count();
        $empRecords = $otf->getTable('trips')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "trip_id" => $empRecords[$x]->trip_id,
                    "route_id" => $empRecords[$x]->route_id,
                    "service_id" => $empRecords[$x]->service_id,
                    "realtime_trip_id" => $empRecords[$x]->realtime_trip_id,
                    "trip_headsign" => $empRecords[$x]->trip_headsign,
                    "trip_short_name" => $empRecords[$x]->trip_short_name,
                    "trip_long_name" => $empRecords[$x]->trip_long_name,
                    "direction_id" => $empRecords[$x]->direction_id,
                    "block_id" => $empRecords[$x]->block_id,
                    "shape_id" => $empRecords[$x]->shape_id,
                    "wheelchair_accessible" => $empRecords[$x]->wheelchair_accessible,
                    "bikes_allowed" => $empRecords[$x]->bikes_allowed,
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function import()
    {
        return view('gtfs::import.index');
    }
}
