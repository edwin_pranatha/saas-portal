<?php

namespace Modules\Gtfs\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Lavary\Menu\Facade as Menu;
use Illuminate\Support\Str;
use Auth;

class MenuBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Menu::make('MenuBackend', function ($menu) {


            $menu->settings->add('Transit Feed settings', '#')->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 1,
            ]);

            $menu->add('GTFS', [
                'url' => 'admin/settings/transit-feed/gtfs',
                'parent' => $menu->transitFeedSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 1,
            ]);

            $menu->add('GTFS-R', [
                'url' => 'admin/settings/transit-feed/gtfs-r',
                'parent' => $menu->transitFeedSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 2,
            ]);



            $menu->settings->add('User Role settings', '#')->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 2,
            ]);

            $menu->add('Access Rights', [
                'url' => 'admin/settings/user-role/access-rights',
                'parent' => $menu->userRoleSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 1,
            ]);

            $menu->add('Permissions', [
                'url' => 'admin/settings/user-role/permissions',
                'parent' => $menu->userRoleSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 2,
            ]);



            $menu->settings->add('Info Vision settings', '#')->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 3,
            ]);

            $menu->add('Screen Size', [
                'url' => 'admin/settings/info-vision/screen-size',
                'parent' => $menu->infoVisionSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 1,
            ]);

            $menu->add('Font', [
                'url' => 'admin/settings/info-vision/font',
                'parent' => $menu->infoVisionSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 2,
            ]);

            $menu->add('Widget', [
                'url' => 'admin/settings/info-vision/widget',
                'parent' => $menu->infoVisionSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 3,
            ]);



            $menu->settings->add('Log settings', '#')->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 3,
            ]);

            $menu->add('Backup', [
                'url' => 'admin/settings/info-vision/backup',
                'parent' => $menu->logSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 1,
            ]);

            $menu->add('Purge', [
                'url' => 'admin/settings/info-vision/purge',
                'parent' => $menu->logSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 2,
            ]);

            $menu->add('Log Level', [
                'url' => 'admin/settings/info-vision/log-level',
                'parent' => $menu->logSettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 3,
            ]);




            $menu->settings->add('Security settings', '#')->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 3,
            ]);

            $menu->add('2FA', [
                'url' => 'admin/settings/security/2fa',
                'parent' => $menu->securitySettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 1,
            ]);

            $menu->add('Password Management', [
                'url' => 'admin/settings/security/password-management',
                'parent' => $menu->securitySettings->id
                ])
            ->data([
                'icon' => '',
                'group' => '',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/settings/*',
                'order' => 2,
            ]);









            $menu->add('Transit', '#')->data([
                'icon' => 'fa fa-bus',
                //'group' => 'Transit',
                'permissions' => 'gtfs-view',
                'active_match' => 'admin/gtfs/*',
                'order' => 1,
            ]);

            $menu->transit->add('Import Data', 'admin/gtfs/import/show')->data([
                'permissions' => 'gtfs.import-view',
                'active_match' => 'admin/gtfs/import*',
                'group' => 'GTFS',
                'order' => 1,
            ])->active("admin/gtfs/content-upload");

            $menu->transit->add('Routes', 'admin/gtfs/routes')->data([
                'permissions' => 'gtfs.routes-view',
                'active_match' => 'admin/gtfs/routes*',
                'group' => 'GTFS',
                'order' => 4,
            ])->active("admin/gtfs/routes");

            
            $menu->transit->add('Stops', 'admin/gtfs/stops')->data([
                'permissions' => 'gtfs.stops-view',
                'active_match' => 'admin/gtfs/stops*',
                'group' => 'GTFS',
                'order' => 7,
            ])->active("admin/gtfs/stop");

            
            $menu->transit->add('Vehicle Monitoring', 'admin/gtfs/vehicle-monitoring')->data([
                'permissions' => 'gtfs.vehicle.monitoring-view',
                'active_match' => 'admin/gtfs/vehicle-monitoring*',
                'group' => 'GTFS',
                'order' => 9,
            ])->active("admin/gtfs/vehicle-monitoring");

            
            $menu->transit->add('Trips Planner', 'admin/gtfs/trips')->data([
                'permissions' => 'gtfs.trips-view',
                'active_match' => 'admin/gtfs/trips*',
                'group' => 'GTFS',
                'order' => 10,
            ])->active("admin/gtfs/trips");

            
            $menu->transit->add('View Display', 'admin/gtfs/view-display')->data([
                'permissions' => 'gtfs.display-view',
                'active_match' => 'admin/gtfs/view-display*',
                'group' => 'GTFS',
                'order' => 11,
            ])->active("admin/gtfs/view-display");
            
            $menu->transit->add('Announce/Alert', 'admin/gtfs/announce/alert')->data([
                'permissions' => 'gtfs.announce-view',
                'active_match' => 'admin/gtfs/announce/alert*',
                'group' => 'GTFS',
                'order' => 12,
            ])->active("admin/gtfs/announce/alert");

            $menu->add('Transit Canada', '#')->data([
                'icon' => 'fa fa-briefcase',
                //'group' => 'Transit',
                'permissions' => 'gtfs.canada-view',
                'active_match' => 'admin/gtfs-canada*',
                'order' => 1,
            ]);

            $menu->transitCanada->add('Vehicle Monitoring', 'admin/gtfs-canada/vehicle-monitoring')->data([
                'permissions' => 'gtfs.canada.vehicle.monitoring-view',
                'active_match' => 'admin/gtfs-canada/vehicle-monitoring*',
                //'group' => 'Transit',
                'order' => 9,
            ])->active("admin/gtfs-canada/vehicle-monitoring");
            
            $menu->transitCanada->add('View Display', 'admin/gtfs-canada/view-display')->data([
                'permissions' => 'gtfs.canada.display-view',
                'active_match' => 'admin/gtfs-canada/view-display*',
                'group' => 'GTFS',
                'order' => 11,
            ])->active("admin/gtfs-canada/view-display");

            // $menu->add('GTFS', '#')->data([
            //     'icon' => 'fa fa-briefcase',
            //     'group' => 'GTFS',
            //     'permissions' => 'gtfs.view',
            //     'active_match' => 'admin/gtfs*',
            //     'order' => 2,
            // ]);

            // $menu->gTFS->add('Import Data', 'admin/gtfs/import/show')->data([
            //     'permissions' => 'gtfs.import.show',
            //     'active_match' => 'admin/gtfs/import*',
            //     'group' => 'GTFS',
            //     'order' => 1,
            // ])->active("admin/gtfs/content-upload");

            // $menu->gTFS->add('Agency', 'admin/gtfs/agency')->data([
            //     'permissions' => 'layout.view',
            //     'active_match' => 'admin/gtfs/agency*',
            //     'group' => 'GTFS',
            //     'order' => 2,
            // ])->active("admin/gtfs/layout");

            // $menu->gTFS->add('Feed Info', 'admin/gtfs/feed/info')->data([
            //     'permissions' => 'gtfs.feed.info.view',
            //     'active_match' => 'admin/gtfs/feed/info*',
            //     'group' => 'GTFS',
            //     'order' => 3,
            // ])->active("admin/gtfs/feed/info");

            // $menu->gTFS->add('Routes', 'admin/gtfs/routes')->data([
            //     'permissions' => 'gtfs.routes',
            //     'active_match' => 'admin/gtfs/routes*',
            //     'group' => 'GTFS',
            //     'order' => 4,
            // ])->active("admin/gtfs/routes");

            // $menu->gTFS->add('Shapes', 'admin/gtfs/shapes')->data([
            //     'permissions' => 'gtfs.routes',
            //     'active_match' => 'admin/gtfs/shapes*',
            //     'group' => 'GTFS',
            //     'order' => 5,
            // ])->active("admin/gtfs/shapes");
            
            // $menu->gTFS->add('Stop Times', 'admin/gtfs/stop/times')->data([
            //     'permissions' => 'gtfs.routes',
            //     'active_match' => 'admin/gtfs/stops/times*',
            //     'group' => 'GTFS',
            //     'order' => 6,
            // ])->active("admin/gtfs/stop/times");

            // $menu->gTFS->add('Stops', 'admin/gtfs/stops')->data([
            //     'permissions' => 'gtfs.stops',
            //     'active_match' => 'admin/gtfs/stops*',
            //     'group' => 'GTFS',
            //     'order' => 7,
            // ])->active("admin/gtfs/stop");

            // $menu->gTFS->add('Transfers', 'admin/gtfs/transfers')->data([
            //     'permissions' => 'gtfs.stops',
            //     'active_match' => 'admin/gtfs/trasfers*',
            //     'group' => 'GTFS',
            //     'order' => 8,
            // ])->active("admin/gtfs/transfers");

            
            // $menu->gTFS->add('Trips', 'admin/gtfs/trips')->data([
            //     'permissions' => 'gtfs.stops',
            //     'active_match' => 'admin/gtfs/trips*',
            //     'group' => 'GTFS',
            //     'order' => 9,
            // ])->active("admin/gtfs/trips");
            
        })->filter(function ($item) use ($request) {

            if($item->data('permissions')!=null){
                $permissions = explode('|', $item->data('permissions'));
                if(!Auth::user()->hasAnyPermission($permissions)){
                    return false;
                }
            }

            if ($request->url() === rtrim($item->url(), '#/')) {
                $item->active();
            }
            
            if ($item->active_match) {
                $matches = is_array($item->active_match) ? $item->active_match : [$item->active_match];                
                foreach ($matches as $pattern) {
                    if (Str::is($pattern, $request->path())) {
                        $item->activate();
                    }
                }
            }
            return true;
        })->sortBy('order');
        return $next($request);
    }
}