        $(document).ready(function(){
            $('#dataTableRowRoutes').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'route_id', "width" : "80px", "orderable" : true },
                    { data: 'agency_id' },
                    { data: 'route_short_name' },
                    { data: 'route_long_name' },
                    { data: 'route_desc' },
                    { data: 'route_type' },
                    { data: 'route_url' },
                    { data: 'route_color' },
                    { data: 'route_text_color' },
                    { data: 'route_sort_order' },
                    { data: 'continuous_pickup' },
                    { data: 'continuous_drop_off' },
                ]
            });
        });