        $(document).ready(function(){
            $('#dataTableRowCalendarDates').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'service_id' },
                    { data: 'date' },
                    { data: 'exception_type' },
                ]
            });
        });