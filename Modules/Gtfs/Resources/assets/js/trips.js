        $(document).ready(function(){
            $('#dataTableRowTrips').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'trip_id', "width" : "80px", "orderable" : true },
                    { data: 'route_id', "width" : "80px", "orderable" : true },
                    { data: 'service_id' },
                    { data: 'realtime_trip_id' },
                    { data: 'trip_headsign' },
                    { data: 'trip_short_name' },
                    { data: 'trip_long_name' },
                    { data: 'direction_id' },
                    { data: 'block_id' },
                    { data: 'shape_id' },
                    { data: 'wheelchair_accessible' },
                    { data: 'bikes_allowed' },
                ]
            });
        });