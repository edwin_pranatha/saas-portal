        $(document).ready(function(){
            $('#dataTableRowFeedInfo').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'feed_id', "width" : "80px", "orderable" : true },
                    { data: 'feed_publisher_name', "width" : "80px", "orderable" : true },
                    { data: 'feed_publisher_url' },
                    { data: 'feed_lang' },
                    { data: 'default_lang' },
                    { data: 'feed_start_date' },
                    { data: 'feed_end_date' },
                    { data: 'feed_version' },
                    { data: 'feed_contact_email' },
                    { data: 'feed_contact_url' },
                ]
            });
        });