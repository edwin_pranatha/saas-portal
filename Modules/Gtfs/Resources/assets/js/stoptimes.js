        $(document).ready(function(){
            $('#dataTableRowStopTimes').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'trip_id', "width" : "80px", "orderable" : true },
                    { data: 'arrival_time', "width" : "80px", "orderable" : true },
                    { data: 'departure_time' },
                    { data: 'stop_id' },
                    { data: 'stop_sequence' },
                    { data: 'stop_headsign' },
                    { data: 'pickup_type' },
                    { data: 'drop_off_type' },
                    { data: 'continuous_pickup' },
                    { data: 'continuous_drop_off' },
                    { data: 'shape_dist_traveled' },
                    { data: 'fare_units_traveled' },
                    { data: 'timepoint' },
                ]
            });
        });