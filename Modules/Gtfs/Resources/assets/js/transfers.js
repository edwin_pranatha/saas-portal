        $(document).ready(function(){
            $('#dataTableRowTransfer').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'id', "width" : "80px", "orderable" : true },
                    { data: 'from_stop_id', "width" : "80px", "orderable" : true },
                    { data: 'to_stop_id' },
                    { data: 'from_route_id' },
                    { data: 'to_route_id' },
                    { data: 'from_trip_id' },
                    { data: 'to_trip_id' },
                    { data: 'transfer_type' },
                    { data: 'min_transfer_time' },
                ]
            });
        });