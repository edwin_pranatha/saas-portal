        $(document).ready(function(){
            $('#dataTableRowAgency').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'id', "width" : "80px", "orderable" : true },
                    { data: 'agency_id', "width" : "80px", "orderable" : true },
                    { data: 'agency_name' },
                    { data: 'agency_url' },
                    { data: 'agency_timezone' },
                    { data: 'agency_lang' },
                    { data: 'agency_phone' },
                    { data: 'agency_fare_url' },
                    { data: 'agency_email' },
                ]
            });
        });