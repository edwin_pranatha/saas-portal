        $(document).ready(function(){
            $('#dataTableRowStops').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'stop_id', "width" : "80px", "orderable" : true },
                    { data: 'stop_code', "width" : "80px", "orderable" : true },
                    { data: 'stop_name' },
                    { data: 'tts_stop_name' },
                    { data: 'stop_desc' },
                    { data: 'stop_lat' },
                    { data: 'stop_lon' },
                    { data: 'zone_id' },
                    { data: 'stop_url' },
                    { data: 'location_type' },
                    { data: 'parent_station' },
                    { data: 'stop_timezone' },
                    { data: 'wheelchair_boarding' },
                    { data: 'level_id' },
                    { data: 'platform_code' },
                ]
            });
        });