        $(document).ready(function(){
        
            $("#importSubmit").click(function() {
                $('#alertSuccess').html('');
                if($('#task_name').val()===''){
                    $('#alertSuccess').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Warning!</strong> Task Name cannot be empty</div>');
                }else if($('#file').val()==''){
                    $('#alertSuccess').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Warning!</strong> Import .zip or .gz cannot be empty</div>');
                }else{
                    $('#pleaseWait').css('display','block');
                    var file_data = $('#file').prop('files')[0];
                    var task_name = $('#task_name').val();   
                    var form_data = new FormData();                  
                    form_data.append('file', file_data);
                    form_data.append('task_name', task_name);
                    $.ajax({
                        url: ipaddressGtfs +"/api/gtfs/import",
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,                         
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(result){
                            var json = JSON.parse(result)['data'];
                            if(json['status']=='IN PROGRESS'){
                                $('#pleaseWait').css('display','none');
                                $('#alertSuccess').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Success!</strong> The file has successfully uploaded and to be processed, please check notification for status</div>');
                                $('#file').val('');
                                $('#task_name').val('');
                                var id = json['id'];
                                checkSuccessImport(id);
                            }else{
                                $('#pleaseWait').css('display','none');
                                $('#alertSuccess').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Warning!</strong> '+ json['lastAction'] +'</div>');
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
                            var split1 = XMLHttpRequest.responseText.split(',');
                            var split2 = split1[1].split(':');
                            var rpl = split2[1].replace(/"/g, '');
                            $('#pleaseWait').css('display','none');
                            $('#alertSuccess').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Warning!</strong> '+ rpl.replace('}','') +'</div>');
                        }
                    });
                }
            });

            checkAllSuccessImport();

            function checkAllSuccessImport(){
                $.ajax({
                    type: "GET",
                    url: ipaddressGtfs +"/api/gtfs/import/status",
                    cache: false,
                    success: function(result){
                        var json = result;
                        for (var i = 0; i < json.length; i++) {
                            if(json[i]['status']=='IN PROGRESS'){
                                var id = json[i]['id'];
                                checkSuccessImport(id);
                            }
                        }
                    }
                });
            }

            function checkSuccessImport(id){
                var intervalId = setInterval(function(){ 
                    $.ajax({
                        type: "GET",
                        url: ipaddressGtfs +"/api/gtfs/import/status?id=" + id,
                        cache: false,
                        success: function(result){
                            var json = result['data'][0];
                            saveDataGtfsSuccess(json);
                            $('#new-notif-add').css('display', 'block');
                            $('#new-notif-add').html('');
                            if(json['status']=='SUCCEED'){
                                clearInterval(intervalId);
                                var notice = new PNotify({
                                    title: 'Notification',
                                    text: 'Netex data upload process with id: ' + id + ' done',
                                    addclass: 'notification-success stack-topleft',
                                    icon: 'fa fa-bell'
                                });
                                notice.get().click(function() {
                                    var UrlLink = window.location.href;
                                    window.location.href = UrlLink + '/import/log';
                                });
                                $('#notif-add').append('<span class="badge">*</span>');
                                $('#new-notif-add').append("<a href='/u/import/log' class='clearfix'><div class='image'><i class='fa fa-bell bg-success'></i></div><span class='title'>" + json['taskName'] + "</span><span class='message'>Success " + json['createdAt'].replace('T',' ').slice(0, 19) + "</span></a>");
                            }else{
                                $('#notif-add').append('<span class="badge">*</span>');
                                $('#new-notif-add').append("<a href='/u/import/log' class='clearfix'><div class='image'><i class='fa fa-bell bg-success'></i></div><span class='title'>" + json['taskName'] + "</span><span class='message'>" + json['status'] + " " + json['createdAt'].replace('T',' ').slice(0, 19) + "</span></a>");
                            }
                        }
                    });
                }, 10000);
            }

            function saveDataGtfsSuccess(json){
                if(json['status']=='SUCCEED'){
                    var status = 'Success'
                }else{
                    var status = json['status'];
                }
                $.ajax({
                    url: $('[name=urlinfo]').attr('data-url'),
                    data: {
                        id: json['id'],
                        task_name: json['taskName'],
                        file_name: json['fileName'],
                        date_time: json['createdAt'],
                        last_action: json['lastState'],
                        detail: json['detail'],
                        status: status,
                        type: 'gtfs'
                    },                         
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(result){
                        console.log(result);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        console.log(XMLHttpRequest.responseText);  
                    }
                });
            }
        });