        $(document).ready(function(){
            $('#dataTableRowShapes').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'shape_id', "width" : "80px", "orderable" : true },
                    { data: 'shape_pt_lat', "width" : "80px", "orderable" : true },
                    { data: 'shape_pt_lon' },
                    { data: 'shape_pt_sequence' },
                    { data: 'shape_dist_traveled' },
                ]
            });
        });