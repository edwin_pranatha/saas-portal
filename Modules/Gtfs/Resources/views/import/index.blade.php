@extends('theme::views.backend.app')

@section('page-title')
<h2>Import GTFS</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Import GTFS</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Gtfs/css/import-gtfs.min.css" media="screen" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:sass/import-gtfs.min.css') }}" />
@endpush --}}

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="row">
			<div class="col-md-6 col-lg-12 col-xl-6">
				<input type="hidden" name="urlinfo" data-url="{{ route('admin.gtfs.import.log.view') }}">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>
						<h2 class="panel-title">Import GTFS</h2>
					</header>
					<form enctype="multipart/form-data">
						<div id="pleaseWait" class="bg-pleaseWait" style="display: none;"><img src="/assets/Gtfs/images/loading-buffering.gif" class="img-pleaseWait"><br>Please Wait...</div>
						<div class="panel-body">
							<div id="alertSuccess"></div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Task Name <span class="required">*</span></label>
								<div class="col-sm-9">
									<input type="text" id="task_name" name="task_name" class="form-control" placeholder="Task Name..." required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Import .zip or .gz <span class="required">*</span></label>
								<div class="col-sm-9">
									<input type="file" id="file" name="file" class="form-control" required>
								</div>
							</div>
						</div>
						<footer class="panel-footer">
							<div class="row">
								<div class="col-sm-9 col-sm-offset-3">
									<button class="btn btn-primary" id="importSubmit" type="button">Submit</button>
								</div>
							</div>
						</footer>
					</form>
				</section>
			</div>
		</div>
	</div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('gtfs:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('gtfs:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('gtfs:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('gtfs:compiled/import-gtfs.min.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush --}}

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Gtfs/js/import-gtfs.min.js"></script>
@endsection
