@extends('theme::views.backend.app')

@section('page-title')
<h2>GTFS</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>GTFS</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Gtfs/css/routes.min.css" media="screen" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:datatable/buttons.dataTables.min.css') }}" >
@endpush --}}

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>
						<h2 class="panel-title">Routes</h2>
					</header>
					<input type="hidden" name="urlinfo" data-url="{{ route('admin.gtfs.process.routes') }}">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="dataTableRowRoutes">
							<thead>
								<tr>
									<th>Route ID</th>
									<th>Agency ID</th>
									<th>Route Short Name</th>
									<th>Route Long Name</th>
									<th>Route Desc</th>
									<th>Route Type</th>
									<th>Route URL</th>
									<th>Route Color</th>
									<th>Route Text Color</th>
									<th>Route Sort Order</th>
									<th>Continuous Pickup</th>
									<th>Continuous Drop Off</th>
								</tr>
							</thead>
						</table>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('gtfs:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('gtfs:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('gtfs:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('gtfs:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('gtfs:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('gtfs:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('gtfs:compiled/routes.min.js') }}"></script>
@endpush --}}

@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Gtfs/js/routes.min.js"></script>
@endsection
