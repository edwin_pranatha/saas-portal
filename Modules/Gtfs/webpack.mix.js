const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('/').mergeManifest();
mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    __dirname + '/Resources/assets/js/import-gtfs.js'
],  __dirname + '/Resources/assets/compiled/js/import-gtfs.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/agency.js'
],  __dirname + '/Resources/assets/compiled/js/agency.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/feedinfo.js'
],  __dirname + '/Resources/assets/compiled/js/feedinfo.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/routes.js'
],  __dirname + '/Resources/assets/compiled/js/routes.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/shapes.js'
],  __dirname + '/Resources/assets/compiled/js/shapes.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/stoptimes.js'
],  __dirname + '/Resources/assets/compiled/js/stoptimes.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/stops.js'
],  __dirname + '/Resources/assets/compiled/js/stops.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/transfers.js'
],  __dirname + '/Resources/assets/compiled/js/transfers.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/trips.js'
],  __dirname + '/Resources/assets/compiled/js/trips.min.js');

//mix.postCss(__dirname + '/Resources/assets/css/import-gtfs.css', __dirname + '/Resources/assets/sass/import-gtfs.min.css');

// mix.js(__dirname + '/Resources/assets/js/agency.js', __dirname + '/Resources/assets/compiled/agency.min.js');
// mix.js(__dirname + '/Resources/assets/js/calendardates.js', __dirname + '/Resources/assets/compiled/calendardates.min.js');
// mix.js(__dirname + '/Resources/assets/js/feedinfo.js', __dirname + '/Resources/assets/compiled/feedinfo.min.js');
// mix.js(__dirname + '/Resources/assets/js/routes.js', __dirname + '/Resources/assets/compiled/routes.min.js');
// mix.js(__dirname + '/Resources/assets/js/shapes.js', __dirname + '/Resources/assets/compiled/shapes.min.js');
// mix.js(__dirname + '/Resources/assets/js/stoptimes.js', __dirname + '/Resources/assets/compiled/stoptimes.min.js');
// mix.js(__dirname + '/Resources/assets/js/stops.js', __dirname + '/Resources/assets/compiled/stops.min.js');
// mix.js(__dirname + '/Resources/assets/js/transfers.js', __dirname + '/Resources/assets/compiled/transfers.min.js');
// mix.js(__dirname + '/Resources/assets/js/trips.js', __dirname + '/Resources/assets/compiled/trips.min.js');
// mix.js(__dirname + '/Resources/assets/js/import-gtfs.js', __dirname + '/Resources/assets/compiled/import-gtfs.min.js');


mix.options({
    processCssUrls: false,
});

mix.sass( __dirname + '/Resources/assets/sass/import-gtfs.scss', __dirname + '/Resources/assets/compiled/css/import-gtfs.min.css');
mix.sass( __dirname + '/Resources/assets/sass/agency.scss', __dirname + '/Resources/assets/compiled/css/agency.min.css');
mix.sass( __dirname + '/Resources/assets/sass/feedinfo.scss', __dirname + '/Resources/assets/compiled/css/feedinfo.min.css');
mix.sass( __dirname + '/Resources/assets/sass/routes.scss', __dirname + '/Resources/assets/compiled/css/routes.min.css');
mix.sass( __dirname + '/Resources/assets/sass/shapes.scss', __dirname + '/Resources/assets/compiled/css/shapes.min.css');
mix.sass( __dirname + '/Resources/assets/sass/stoptimes.scss', __dirname + '/Resources/assets/compiled/css/stoptimes.min.css');
mix.sass( __dirname + '/Resources/assets/sass/stops.scss', __dirname + '/Resources/assets/compiled/css/stops.min.css');
mix.sass( __dirname + '/Resources/assets/sass/transfers.scss', __dirname + '/Resources/assets/compiled/css/transfers.min.css');
mix.sass( __dirname + '/Resources/assets/sass/trips.scss', __dirname + '/Resources/assets/compiled/css/trips.min.css');

if (process.env.NODE_ENV === 'production') {
mix.copy('node_modules/select2-3.5.1/select2.png',  __dirname + '/Resources/assets/compiled/css/select2.png');
mix.copy('node_modules/select2-3.5.1/select2-spinner.gif',  __dirname + '/Resources/assets/compiled/css/select2-spinner.gif');
mix.copy('node_modules/select2-3.5.1/select2x2.png',  __dirname + '/Resources/assets/compiled/css/select2x2.png');
mix.copyDirectory(__dirname + '/Resources/assets/images/',  __dirname + '/Resources/assets/compiled/images/');
mix.copyDirectory('node_modules/datatables.net-dt/images/',  __dirname + '/Resources/assets/compiled/images/');
}

if (mix.inProduction()) {
    mix.version();
}
