<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::group(['prefix'=>'/u/gtfs','middleware' => ['auth','tenancy.enforce']],function (){
Route::prefix('/admin/gtfs')->middleware(['auth'])->name('admin.')->group(function() {
    Route::get('/agency', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'agency'])->name('gtfs.agency');
    Route::post('/processagency', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'process_agency'])->name('gtfs.process.agency');
    Route::get('/calendar/dates', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'calendar_date'])->name('gtfs.calendar.dates');
    Route::post('/processcalendardates', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'process_calendardates'])->name('gtfs.process.calendar.dates');
    Route::get('/feed/info', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'feed_info'])->name('gtfs.feed.info');
    Route::post('/processfeedinfo', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'process_feedinfo'])->name('gtfs.process.feed.info');
    Route::get('/routes', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'routes'])->name('gtfs.routes');
    Route::post('/processroutes', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'process_routes'])->name('gtfs.process.routes');
    Route::get('/shapes', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'shapes'])->name('gtfs.shapes');
    Route::post('/processshapes', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'process_shapes'])->name('gtfs.process.shapes');
    Route::get('/stop/times', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'stop_times'])->name('gtfs.stop.times');
    Route::post('/processstoptimes', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'process_stoptimes'])->name('gtfs.process.stop.times');
    Route::get('/stops', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'stops'])->name('gtfs.stops');
    Route::post('/processstops', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'process_stops'])->name('gtfs.process.stops');
    Route::get('/transfers', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'transfers'])->name('gtfs.transfers');
    Route::post('/processtransfers', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'process_transfers'])->name('gtfs.process.transfers');
    Route::get('/trips', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'trips'])->name('gtfs.trips');
    Route::post('/processtrips', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'process_trips'])->name('gtfs.process.trips');
    Route::get('/import/show', [Modules\Gtfs\Http\Controllers\Backend\GtfsController::class, 'import'])->name('gtfs.import.show');

    //Import Log
    Route::post('/import/log', [Modules\Gtfs\Http\Controllers\Backend\ImportLogController::class, 'saveImportSuccess'])->name('gtfs.import.log.save');
    Route::get('/import/log', [Modules\Gtfs\Http\Controllers\Backend\ImportLogController::class, 'viewLogImportSuccess'])->name('gtfs.import.log.view');
    Route::post('/import/log/detail', [Modules\Gtfs\Http\Controllers\Backend\ImportLogController::class, 'process_ild'])->name('gtfs.import.log.detail');
});
