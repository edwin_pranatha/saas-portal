@extends('theme::views.backend.app')

@section('page-title')
<h2>Journey Layover Time Table</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Journey Layover Time Table</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:datatable/buttons.dataTables.min.css') }}" >
@endpush

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>
						<h2 class="panel-title">Journey Layover Time Table</h2>
					</header>
					<input type="hidden" name="urlinfo" data-url="{{ route('netex.processjl') }}">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="dataTableRowJL">
							<thead>
								<tr>
									<th>ID</th>
									<th>Layover</th>
									<th>Scheduled Stop Point Ref</th>
									<th>Time Demand Type Ref</th>
									<th>Version</th>
								</tr>
							</thead>
						</table>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
@push('data-table')
<script src="{{ Module::asset('netex:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('netex:compiled/journeylayover.min.js') }}"></script>
@endpush

@endsection