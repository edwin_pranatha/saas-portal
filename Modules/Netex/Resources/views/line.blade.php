@extends('theme::views.backend.app')

@section('page-title')
<h2>Line Table</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Line Table</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Netex/css/line.min.css" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:datatable/buttons.dataTables.min.css') }}" >
@endpush --}}

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>
						<h2 class="panel-title">Line Table</h2>
					</header>
					<input type="hidden" name="urlinfo" data-url="{{ route('admin.netex.processline') }}">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="dataTableRowLine">
							<thead>
								<tr>
									<th>ID</th>
									<th>Accessibility Assessment</th>
									<th>Authority Ref</th>
									<th>Branding Ref</th>
									<th>Description</th>
									<th>External Line Ref</th>
									<th>Monitored</th>
									<th>Name</th>
									<th>Operational Context Ref</th>
									<th>Private Code</th>
									<th>Public Code</th>
									<th>Responsibility Set Ref</th>
									<th>Transport Mode</th>
									<th>Type Of Product Category Ref</th>
									<th>Type Of Service Ref</th>
									<th>Version</th>
								</tr>
							</thead>
						</table>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('netex:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('netex:compiled/line.min.js') }}"></script>
@endpush --}}

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Netex/js/line.min.js"></script>
@endsection
