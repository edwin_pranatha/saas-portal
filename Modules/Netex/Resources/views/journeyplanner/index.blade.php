@extends('theme::views.backend.app')

@section('page-title')
<h2>Journey Planner Time Table</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Journey Planner Time Table</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Netex/css/journeyplanner.min.css" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('netex:css/bootstrap-datetimepicker.css') }}" media="screen">
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
@endpush --}}
<style type="text/css">
	.load-bgImage-Point {
		background: url("/themes/maestronic/assets/backend/assets/vendor/jstree/themes/default/throbber.gif") 270px 6px no-repeat !important;
	}
</style>
<div class="row">
	<div class="col-md-4 col-lg-4">
		<section class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
				<h2 class="panel-title">Journey Planner Time Table</h2>
			</header>
			<input type="hidden" name="urlinfo" data-url="{{ route('admin.netex.processsp') }}">
			<div class="panel-body">
				<div class="tabs">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#popular" data-toggle="tab" aria-expanded="true">Planner</a>
						</li>
						<li class="">
							<a href="#recent" data-toggle="tab" aria-expanded="false">Extra Opties</a>
						</li>
					</ul>
					<style type="text/css">
						.select2-arrow {
							display: none !important;
						}
					</style>
					<div class="tab-content">
						<div id="popular" class="tab-pane active">
							<form action="{{ route('admin.netex.journeyplanner.detail') }}" type="GET">
								<div class="timeline timeline-simple mt-xlg mb-md">
									<div class="tm-body">
										<div class="tm-title">
											<h3 class="h5 text-uppercase">Location</h3>
										</div>
										<ol class="tm-items">
											<li>
												<div class="tm-box">
													<div class="row">
														<div class="col-sm-12">
															<div class="form-group">
																<select data-plugin-selectTwo class="form-control populate selectClass1" id="locationStart">
																</select>
															</div>
														</div>
													</div>
													<div class="toggle" data-plugin-toggle="">
														<section class="toggle">
															<label>Voeg 'via' toe</label>
															<div class="toggle-content" style="display: none;">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="form-group">
																			<select data-plugin-selectTwo class="form-control populate selectClass2"  id="locationCenter">
																			</select>
																		</div>
																	</div>
																</div>
															</div>
														</section>
													</div>
												</div>
											</li>
											<li>
												<div class="tm-box">
													<div class="row">
														<div class="col-sm-12">
															<div class="form-group">
																<select data-plugin-selectTwo class="form-control populate selectClass3" id="locationEnd">
																</select>
															</div>
														</div>
													</div>
												</div>
											</li>
										</ol>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-8">
										<div class="form-group">
											<label class="control-label">Date</label>
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</span>
												<input type="text" data-plugin-datepicker class="form-control" name="dates">
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label class="control-label">Time</label>
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-clock-o"></i>
												</span>
												<input type="text" name="times" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false }'>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-8">
										<div class="form-group">
											<label class="control-label">Type reis</label>
											<div class="row">
												<div class="col-sm-6">
													<div class="radio-custom radio-primary">
														<input type="radio" id="radioExample1" name="reis" checked>
														<label for="radioExample1">Vertrek</label>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="radio-custom radio-primary">
														<input type="radio" id="radioExample2" name="reis">
														<label for="radioExample2">Aankomst</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Plan mijn reis</button>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div id="recent" class="tab-pane">
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group">
										<label class="control-label">Extra Opties</label>
										<div class="row">
											<div class="col-sm-6">
												<div class="radio-custom radio-primary">
													<input type="radio" id="radioExample1" name="radioExample" checked>
													<label for="radioExample1">Standaard</label>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="radio-custom radio-primary">
													<input type="radio" id="radioExample2" name="radioExample">
													<label for="radioExample2">+5 min.</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group">
										<label class="control-label">Vervoermiddelen</label>
										<div class="row">
											<div class="col-sm-2">
												<div class="checkbox-custom checkbox-primary">
													<input type="checkbox" id="checkboxExample1" name="checkboxExample" checked>
													<label for="checkboxExample1"><i class="fa fa-bus"></i></label>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="checkbox-custom checkbox-primary">
													<input type="checkbox" id="checkboxExample2" name="checkboxExample" checked>
													<label for="checkboxExample2"><i class="fa fa-train"></i></label>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="checkbox-custom checkbox-primary">
													<input type="checkbox" id="checkboxExample2" name="checkboxExample" checked>
													<label for="checkboxExample2"><i class="fa fa-subway"></i></label>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="checkbox-custom checkbox-primary">
													<input type="checkbox" id="checkboxExample2" name="checkboxExample" checked>
													<label for="checkboxExample2"><i class="fa fa-subway"></i></label>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="checkbox-custom checkbox-primary">
													<input type="checkbox" id="checkboxExample2" name="checkboxExample" checked>
													<label for="checkboxExample2"><i class="fa fa-ship"></i></label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group">
										<label class="control-label">Toegankelijk plannen</label>
										<div class="row">
											<div class="col-sm-12">
												<div class="checkbox-custom checkbox-primary">
													<input type="checkbox" id="checkboxExample1" name="checkboxExample">
													<label for="checkboxExample1"><i class="fa  fa-question-circle"></i></label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<a href="{{ route('admin.netex.journeyplanner.detail') }}" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Plan mijn reis</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</section>	
	</div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('netex:js/stockpoint.js') }}"></script>
@endpush --}}

@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Netex/js/journeyplanner.min.js"></script>
@endsection
