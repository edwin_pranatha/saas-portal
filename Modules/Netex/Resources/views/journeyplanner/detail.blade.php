@extends('theme::views.backend.app')

@section('page-title')
<h2>Journey Planner Time Table</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Journey Planner Time Table</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets-custom')
<link rel="stylesheet" href="{{ Module::asset('netex:sass/stockpoint.min.css') }}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
@endpush

<div class="row">
	<div class="col-md-12 col-lg-12">
		<button class="prevtab btn btn-primary" style="height: 100px;float: left;max-width: 50px;"><i class="fa fa-arrow-circle-left"></i></button>		
		<ul class="nav nav-tabs" id="div1" style="border: none;display: flex;overflow-x: auto;overflow-y: hidden;float: left;width: 1200px;">
			@php $no = 1; $nomor = 0; $starttime = 0; $endtime = 0; $switch = 1; $tempstarttime = 0; $diff = 0; $sum = 0; $nextruntimebef = 0; $waittimestopbef = 0; @endphp
			@foreach($result as $row)
			@if($row->orders=='1.00')
			@php $switch = 2; $sum = 0; @endphp
			@endif
			@if($row->orders=='1.00')
			@php 
			$starttime = strtotime($row->departure_time) + $row->stop_wait_time;
			$nextruntimebef = $row->next_run_time;
			$waittimestopbef = $row->stop_wait_time;
			@endphp
			@else
			@php 
			$sum = $sum + $row->stop_wait_time + $nextruntimebef + $waittimestopbef;
			$nextruntimebef = $row->next_run_time;
			$waittimestopbef = $row->stop_wait_time;
			@endphp		
			@endif
			@if($no != 1 && $switch==2)
			@php 
			$nomor++; 
			@endphp
			<li class="nav-link @if($nomor==1) active @endif">
				<a href="#popular{{ $nomor }}" data-toggle="tab" aria-expanded="true">
					<div class="font-weight-bold m-0 text-left">
						<span class="journey-option__time-option">{{ gmdate("H:i:s", $tempstarttime) }}</span>
						<i class="fas fa-arrow-right"></i> <br>
						<span class="journey-option__time-option">{{ gmdate("H:i:s", $endtime) }}</span>
					</div>
					<div class="m-t-5 text-left">
						<span title="Overstappen"><i class="far icon-overstappen"></i> 0</span> | <span title="Reistijd"><i class="far icon-clock"></i>   <span>{{ gmdate("H:i:s", $diff) }}</span>
					</span>
				</div>
			</a>
		</li>
		@else
		@php 
		$tempstarttime = $starttime;
		$endtime = strtotime($row->departure_time) + $sum;
		if($endtime > $starttime){
			$diff = $endtime - $starttime;
		}else{
			$diff = $starttime - $endtime;
		}
		@endphp
		@endif
		@php $no++; $switch = 1; @endphp
		@endforeach
	</ul>	
	<button class="nexttab btn btn-primary" style="height: 100px;float: left;margin-left: 2px;max-width: 50px;"><i class="fa fa-arrow-circle-right"></i></button>	
</div>
</div>

<div class="row">
	<div class="col-md-12 col-lg-12">
		
		<div class="timeline">
			<div class="tm-body">
				<div class="tab-content" style="background-color: transparent;box-shadow: none;">
					@php $no = 1; $nomor = 0; $noarray = 1; $starttime = 0; $endtime = 0; $switch = 1; $tempstarttime = 0; $diff = 0; $arraytemp = []; $array = []; $sum = 0; $nextruntimebef = 0; $waittimestopbef = 0; @endphp
					@foreach($result as $row)
					@if($row->orders=='1.00')
					@php $switch = 2; $array = []; $noarray = 1; $sum = 0; @endphp
					@endif
					@php 
					if($row->orders=='1.00'){
						$sum = $sum  + $row->stop_wait_time;
						$nextruntimebef = $row->next_run_time;
						$waittimestopbef = $row->stop_wait_time;
					}else{
						$sum = $sum + $row->stop_wait_time + $nextruntimebef + $waittimestopbef;
						$nextruntimebef = $row->next_run_time;
						$waittimestopbef = $row->stop_wait_time;
					}
					$array[$noarray]['name'] = $row->stop_name;
					$array[$noarray]['time'] = strtotime($row->departure_time) + $sum;
					@endphp
					@if($no != 1 && $switch==2)
					@php 
					$nomor++;
					@endphp
					<div id="popular{{ $nomor }}" class="tab-pane @if($nomor==1) active @endif">
						<ol class="tm-items">
							@for ($i = 1; $i <= count($arraytemp); $i++)
							<li>
								<div class="tm-info">
									<div class="tm-icon"><i class="fa fa-map-marker"></i></div>
									<time class="tm-datetime" datetime="2013-11-22 19:13">
										<div class="tm-datetime-date">Departure Time</div>
										<div class="tm-datetime-time">{{ gmdate("H:i:s", $arraytemp[$i]['time']) }}</div>
									</time>
								</div>
								<div class="tm-box appear-animation" data-appear-animation="fadeInRight"data-appear-animation-delay="100">
									<p>
										<i class="fa fa-star" style="font-size: 15px;"></i>		{{ $arraytemp[$i]['name'] }}
									</p>
									<div class="well info" style="padding-bottom: 2px !important;padding-top: 8px !important;margin-bottom: 0px;">
										<b>Arrival Time</b>
										<div class="well primary" style="padding: 5px !important;">
											<i class="fa fa-map-marker"></i> {{ gmdate("H:i:s", $arraytemp[$i]['time'] - $row->stop_wait_time) }}
										</div>
									</div>
								</div>
							</li>
							@endfor
						</ol>
					</div>
					@php $arraytemp = []; @endphp
					@else
					@for ($i = 1; $i <= count($array); $i++)
					@php 
					$arraytemp[$i]['name'] = $array[$i]['name'];
					$arraytemp[$i]['time'] = $array[$i]['time'];
					@endphp
					@endfor
					@endif
					@php $no++; $noarray++; $switch = 1; @endphp
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@push('data-table')
<script src="{{ Module::asset('netex:assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('netex:compiled/stockpoint.min.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush

@endsection