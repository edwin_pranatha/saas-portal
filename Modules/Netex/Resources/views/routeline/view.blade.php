@extends('theme::views.backend.app')

@section('page-title')
<h2>Route Line Time Table View</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Route Line Time Table View</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('netex:css/bootstrap-datetimepicker.css') }}" media="screen">
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:datatable/buttons.dataTables.min.css') }}" >
<link rel="stylesheet" href="{{ Module::asset('netex:sass/routeline.min.css') }}">
<script src="{{ Module::asset('netex:mapbox/js/mapbox-gl.js') }}"></script>
<link rel="stylesheet" href="{{ Module::asset('netex:mapbox/css/mapbox-gl.css') }}" />
@endpush


<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>
						<h2 class="panel-title">Time Table</h2>
					</header>
					<div class="panel-body">
						<div class="col-md-8 col-xl-12">
							<div class="row">
								<div class="col-md-12">
									<h3>{{ $data['linename'] }}</h3>
									<div id="map-route" class="tf-map" style='width: 100%; height: 500px;'></div>
									<style>
										.marker {
											display: block;
											border: none;
											cursor: pointer;
											padding: 0;
											text-align: center;
											height: 38px;
											line-height: 28px;
										}
										.marker-text {
											color: #000000;
											font-size: 16px;
											font-weight: bold;
										}
										.datepicker table tr td {
											border: 1px solid #6193ac;	
										}
										.datepicker.datepicker-primary table thead tr:first-child {
										    border: 1px solid #0088cc;
										}
										.datepicker.datepicker-primary table thead tr:last-child {
										    border: 1px solid #0099e6;
										}
									</style>
									<script>
										function html(point) {
											var parts = [];

											parts[parts.length] = '<strong>Name:</strong> ' + point[4];
											parts[parts.length] = "<strong>ID:</strong> <a href='/u/netex/scheduledstoppoint?private_code=" + point[2] + "' target='_blank'>" + point[2] + "</a>";

											var code = point[3] || '';

											if (code == 0) {
												parts[parts.length] = "<strong>Version:</strong> Not specified";
											}
											else {
												parts[parts.length] = "<strong>Version:</strong> " + code;
											}

											return parts.join("<br />");
										}

										function initialize() {
											mapboxgl.accessToken = 'pk.eyJ1IjoiYWRpcmFzbWF3YW4iLCJhIjoiY2tvaTlreXlvMGlvZzJ3czFoNnl4NDlsMyJ9.GvptfkWvi295drtnIRH4EA';

											var map = new mapboxgl.Map({
												container: 'map-route',
												style: 'mapbox://styles/mapbox/streets-v9',
												zoom: 50
											});

											map.addControl(new mapboxgl.FullscreenControl());
											map.addControl(new mapboxgl.NavigationControl());

											var bounds = new mapboxgl.LngLatBounds();

											var empty = false;

											if (empty) {
												map.zoom = 50;
												bounds.extend(new mapboxgl.LngLat({{ $data['defaultpoint'] }}));
											}

											var points = [];
											var coordinates = [];
											@foreach($data['models'] as $detail)
											points[{{ $detail['no'] }}] = [
											{{ $detail['long'] }},
											{{ $detail['lat'] }},
											"{{ $detail['private_code'] }}",
											"{{ $detail['version'] }}",
											"{{ $detail['name'] }}",
											0,
											"\/p\/ov\/814\/latest\/stop\/361484"];
											@endforeach
											for (var i = 0; i < points.length; i++) {
												var point = points[i];
												coordinates.push([point[1], point[0]]);
												bounds.extend(new mapboxgl.LngLat(point[1], point[0]));

												var el = document.createElement('div');
												var span = document.createElement('span');
												var text = document.createTextNode(i + 1);
												span.className = 'marker-text';
												span.appendChild(text);
												el.appendChild(span);
												el.className = 'marker';
												el.style.marginTop = '-17px';
												el.style.backgroundImage = 'url(https://transitfeeds.com/img/marker-g.png)';
												el.style.width = '26px';
												el.style.height = '38px';

												var popup = new mapboxgl.Popup({closeButton: false, offset: 25}).setHTML(html(point))

												new mapboxgl.Marker(el)
												.setLngLat([point[1], point[0]])
												.setPopup(popup)
												.addTo(map);
											}

											if (empty) {
												map.setCenter(bounds.getCenter());
											} else {
												map.fitBounds(bounds, {duration: 0, zoom:13});
											}

											map.on('load', function () {
												map.addLayer({
													"id": "route",
													"type": "line",
													"source": {
														"type": "geojson",
														"data": {
															"type": "Feature",
															"properties": {},
															"geometry": {
																"type": "LineString",
																"coordinates": coordinates
															}
														}
													},
													"layout": {
														"line-join": "round",
														"line-cap": "round"
													},
													"paint": {
														"line-color": "#25a5e6",
														"line-width": 4
													}
												});
												map.setZoom(12);
											});
										}

										initialize();
									</script>
								</div>
								<div class="col-md-12" style="margin-top: 20px;">
									<h3 class="dateClass">{{ $data['date'] }}</h3>
									<div class="tabs">
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#popular" data-toggle="tab">Inbound</a>
											</li>
											<li>
												<a href="#recent" data-toggle="tab">Outbound</a>
											</li>
										</ul>
										<div class="tab-content">
											<div id="popular" class="tab-pane active">
												<table class="table table-striped mb-none">
													<thead>
														<tr>
															<th>When</th>
															<th>Trip</th>
															<th>Headsign</th>
															<th width="100px"></th>
															<th width="100px"></th>
														</tr>
													</thead>
													<tbody>
														@php $statusIn = 0; @endphp
														@foreach($data['detail'] as $row)
														@if($row->direction_type=='INBOUND')
														<tr>
															<td class="tf-trips-when">
																{{ $row->departure_time }} - {{ $row->end_time }}
															</td>
															<td><code>{{ $row->private_code }}</code></td>
															<td>
																{{ $row->name }}
															</td>
															<td>
																<img src="https://transitfeeds.com/img/wca-yes.png" alt="Wheelchair accessible">
															</td>
															<td>
																<a class="btn btn-primary btn-xs" href="{{ route('netex.routeline.detail', ['private_code' => Request::get('private_code'), 'branding_id' => Request::get('branding_id'), 'line_name' => Request::get('line_name'), 'branding_name' => Request::get('branding_name'), 'sj_id' => $row->id, 'startdatefixs' => $data['startdatefixs'], 'enddatefixs' => $data['enddatefixs'], 'date' => $data['date'], 'starttime' => $row->departure_time, 'endtime' => $row->end_time]) }}"><i class="fa fa-map-marker"></i> Details</a>
															</td>
														</tr>
														@php $statusIn = 1; @endphp
														@endif
														@endforeach
														@if($statusIn==0)
														<tr>
															<td colspan="5">
																<div class="alert alert-warning" style="text-align: center;">
																	<strong>No Data</strong>
																</div>
															</td>
														</tr>
														@endif
													</tbody>
												</table>
											</div>
											<div id="recent" class="tab-pane">
												<table class="table table-striped mb-none">
													<thead>
														<tr>
															<th>When</th>
															<th>Trip</th>
															<th>Headsign</th>
															<th width="100px"></th>
															<th width="100px"></th>
														</tr>
													</thead>
													<tbody>
														@php $statusOut = 0; @endphp
														@foreach($data['detail'] as $row)
														@if($row->direction_type=='OUTBOUND')
														<tr>
															<td class="tf-trips-when">
																{{ $row->departure_time }} - {{ $row->end_time }}
															</td>
															<td><code>{{ $row->private_code }}</code></td>
															<td>
																<span class="route-code">1</span>
																{{ $row->name }}
															</td>
															<td>
																<img src="https://transitfeeds.com/img/wca-yes.png" alt="Wheelchair accessible">
															</td>
															<td>
																<a class="btn btn-primary btn-xs" href="{{ route('netex.routeline.detail', ['private_code' => Request::get('private_code'), 'branding_id' => Request::get('branding_id'), 'line_name' => Request::get('line_name'), 'branding_name' => Request::get('branding_name'), 'sj_id' => $row->id, 'startdatefixs' => $data['startdatefixs'], 'enddatefixs' => $data['enddatefixs'], 'date' => $data['date'], 'starttime' => $row->departure_time, 'endtime' => $row->end_time]) }}"><i class="fa fa-map-marker"></i> Details</a>
															</td>
														</tr>
														@php $statusOut = 1; @endphp
														@endif
														@endforeach
														@if($statusOut==0)
														<tr>
															<td colspan="5">
																<div class="alert alert-warning" style="text-align: center;">
																	<strong>No Data</strong>
																</div>
															</td>
														</tr>
														@endif
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xl-12">
							<section class="panel panel-dark">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Route Summary</h2>
								</header>
								<div class="panel-body" style="background-color: #f6f6f6;">
									<div class="sidebar-widget widget-calendar">
										<ul class="route-summary" style="border-top: none;padding: 0px;border-bottom: 1px solid #000;">
											<li>
												<strong>Agency:</strong>
												{{ $data['brandingname'] }}
											</li>
											<li>
												<strong>Route Type:</strong> {{ $data['transportmode'] }}
											</li>
											<li>
												<strong>Route ID:</strong> <code>{{ $data['privatecode'] }}</code>
											</li>
											<li>
												<strong>Color:</strong>
												<span class="route-code-empty" style="background-color: {{ $data['color'] }}"></span>
												<code>{{ $data['color'] }}</code>
											</li>
											<li>
												<strong>Text Color:</strong>
												<span class="route-code-empty" style="background-color: {{ $data['textcolor'] }}"></span>
												<code>{{ $data['textcolor'] }}</code>
											</li>
										</ul>
										<ul class="route-summary" style="border-top: none;padding: 0px;border-bottom: 1px solid #000;">
											<li><strong>Starts:</strong> {{ $data['startdate'] }}</li>
											<li><strong>Ends:</strong> {{ $data['enddate'] }}</li>
											<input type="hidden" class="startdatefixs" value="{{ $data['startdatefixs'] }}">
											<input type="hidden" class="enddatefixs" value="{{ $data['enddatefixs'] }}">
										</ul>
										<div data-plugin-datepicker data-plugin-skin="primary" ></div>
									</div>
								</div>
							</section>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
@push('data-table')
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('netex:compiled/routeline.min.js') }}"></script>
@endpush

@endsection