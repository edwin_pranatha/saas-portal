@extends('theme::views.backend.app')

@section('page-title')
<h2>Route Line Time Table</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Route Line Time Table</span></li>
</ol>
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:datatable/buttons.dataTables.min.css') }}" >
@endpush --}}

@section('module_css')
    <link rel="stylesheet" href="/assets/Netex/css/routeline.min.css" />
@endsection

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>
						<h2 class="panel-title">Route Line Time Table</h2>
					</header>
					<input type="hidden" name="urlinfo" data-url="{{ route('admin.netex.processrl') }}">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="dataTableRowRL">
							<thead>
								<tr>
									<th>Public Code</th>
									<th>Branding ID</th>
									<th>Branding Name</th>
									<th>Line Name</th>
									<th>Private Code</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('netex:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('netex:compiled/routeline.min.js') }}"></script>
@endpush --}}

@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Netex/js/routeline.min.js"></script>
@endsection
