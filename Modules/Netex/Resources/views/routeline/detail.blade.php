@extends('theme::views.backend.app')

@section('page-title')
<h2>Route Line Time Table Detail</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Route Line Time Table Detail</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('netex:css/bootstrap-datetimepicker.css') }}" media="screen">
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:datatable/buttons.dataTables.min.css') }}" >
<link rel="stylesheet" href="{{ Module::asset('netex:sass/routeline.min.css') }}">
<script src="{{ Module::asset('netex:mapbox/js/mapbox-gl.js') }}"></script>
<link rel="stylesheet" href="{{ Module::asset('netex:mapbox/css/mapbox-gl.css') }}" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
@endpush

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-xl-12">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>
						<h2 class="panel-title">Time Table</h2>
					</header>
					<div class="panel-body">
						<div class="col-md-8 col-xl-12">
							<div class="row">
								<div class="col-md-12">
									<h3>{{ $data['starttime']}} - {{ $data['endtime'] }} {{ $data['linename'] }}</h3>
									<div id="map-route" class="tf-map" style='width: 100%; height: 500px;'></div>
									<style>
										.marker {
											display: block;
											border: none;
											cursor: pointer;
											padding: 0;
											text-align: center;
											height: 38px;
											line-height: 28px;
										}
										.marker-text {
											color: #000000;
											font-size: 16px;
											font-weight: bold;
										}
										.datepicker table tr td {
											border: 1px solid #6193ac;	
										}
										.datepicker.datepicker-primary table thead tr:first-child {
										    border: 1px solid #0088cc;
										}
										.datepicker.datepicker-primary table thead tr:last-child {
										    border: 1px solid #0099e6;
										}
									</style>
									<script type="text/javascript">var points = [];</script>
									<script>
										function html(point) {
											var parts = [];

											parts[parts.length] = '<strong>Name:</strong> ' + point[4];
											parts[parts.length] = "<strong>ID:</strong> <a href='/u/netex/scheduledstoppoint?private_code=" + point[2] + "' target='_blank'>" + point[2] + "</a>";

											var code = point[3] || '';

											if (code == 0) {
												parts[parts.length] = "<strong>Version:</strong> Not specified";
											}
											else {
												parts[parts.length] = "<strong>Version:</strong> " + code;
											}

											return parts.join("<br />");
										}

										function initialize() {
											mapboxgl.accessToken = 'pk.eyJ1IjoiYWRpcmFzbWF3YW4iLCJhIjoiY2tvaTlreXlvMGlvZzJ3czFoNnl4NDlsMyJ9.GvptfkWvi295drtnIRH4EA';

											var map = new mapboxgl.Map({
												container: 'map-route',
												style: 'mapbox://styles/mapbox/streets-v9',
												zoom: 50
											});

											map.addControl(new mapboxgl.FullscreenControl());
											map.addControl(new mapboxgl.NavigationControl());

											var bounds = new mapboxgl.LngLatBounds();

											var empty = false;

											if (empty) {
												map.zoom = 50;
												bounds.extend(new mapboxgl.LngLat({{ $data['defaultpoint'] }}));
											}

											var coordinates = [];
											@foreach($data['models'] as $detail)
											points[{{ $detail['no'] }}] = [
											{{ $detail['long'] }},
											{{ $detail['lat'] }},
											"{{ $detail['privatecodes'] }}",
											"{{ $detail['version'] }}",
											"{{ $detail['name'] }}",
											0,
											"\/p\/ov\/814\/latest\/stop\/361484"        ];
											@endforeach
											for (var i = 0; i < points.length; i++) {
												var point = points[i];
												coordinates.push([point[1], point[0]]);
												bounds.extend(new mapboxgl.LngLat(point[1], point[0]));

												var el = document.createElement('div');
												var span = document.createElement('span');
												var text = document.createTextNode(i + 1);
												span.className = 'marker-text';
												span.appendChild(text)
												el.appendChild(span);
												el.className = 'marker mar-' + i;
												el.style.marginTop = '-17px';
												el.style.backgroundImage = 'url(/modules/netex/mapbox/images/marker-g.png)';
												el.style.width = '26px';
												el.style.height = '38px';

												var popup = new mapboxgl.Popup({closeButton: true, offset: 25}).setHTML(html(point))

												new mapboxgl.Marker(el)
												.setLngLat([point[1], point[0]])
												.setPopup(popup)
												.addTo(map);
											}

											if (empty) {
												map.setCenter(bounds.getCenter());
											} else {
												map.fitBounds(bounds, {duration: 0, zoom:13});
											}

											map.on('load', function () {
												map.addLayer({
													"id": "route",
													"type": "line",
													"source": {
														"type": "geojson",
														"data": {
															"type": "Feature",
															"properties": {},
															"geometry": {
																"type": "LineString",
																"coordinates": coordinates
															}
														}
													},
													"layout": {
														"line-join": "round",
														"line-cap": "round"
													},
													"paint": {
														"line-color": "#25a5e6",
														"line-width": 4
													}
												});
												map.setZoom(12);
											});

											map.on('click', 'places', (e) => {
												// Copy coordinates array.
												const coordinates = e.features[0].geometry.coordinates.slice();
												const description = e.features[0].properties.description;
												 
												// Ensure that if the map is zoomed out such that multiple
												// copies of the feature are visible, the popup appears
												// over the copy being pointed to.
												while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
												coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
												}
												 
												new mapboxgl.Popup()
												.setLngLat(coordinates)
												.setHTML(description)
												.addTo(map);
												});
										}

										initialize();
									</script>
								</div>
								<div class="col-md-12" style="margin-top: 20px;">
									<section class="panel panel-dark" id="panel-8" data-portlet-item>
										<header class="panel-heading portlet-handler">
											<div class="panel-actions">
												<a href="#" class="fa fa-caret-down"></a>
												<a href="#" class="fa fa-times"></a>
											</div>
											<h3 class="dateClass" style="display: none;">{{ $data['date'] }}</h3>
											<h2 class="panel-title">Stop List</h2>
										</header>
										<div class="panel-body" style="background-color: #f6f6f6;">
											<div class="timeline">
												<div class="tm-body">
													<ol class="tm-items">
														@php $no=1; $nextruntimebef = 0; $stopwaittimebef = 0; $sum = 0; $noMar = 0; @endphp
														@foreach($data['detail'] as $detail)
														@php
														if($no==1){
															$sum=$detail->stop_wait_time;
															$nextruntimebef = $detail->next_run_time;
															$stopwaittimebef = $detail->stop_wait_time;
														}else{
															$sum=$sum + $detail->stop_wait_time + $nextruntimebef + $stopwaittimebef;
															$nextruntimebef = $detail->next_run_time;
															$stopwaittimebef = $detail->stop_wait_time;
														}
														$total=strtotime($detail->departure_time)+$sum;
														@endphp
														<li>
															<div class="tm-info">
																<div class="tm-icon"><a href="javascript:void(0)" class="marShow-{{ $noMar }}"><i class="fa fa-bus"></i></a></div>
																<time class="tm-datetime" datetime="2013-11-22 19:13">
																	<div class="tm-datetime-time">{{ gmdate("H:i:s", $total) }}</div>
																</time>
															</div>
															<div class="tm-box appear-animation" data-appear-animation="fadeInRight"data-appear-animation-delay="100">
																<p>{{ $detail->stop_name }}</p>
															</div>
														</li>
														@php $no++; $noMar++; @endphp
														@endforeach
													</ol>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xl-12">
							<section class="panel panel-dark">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Route Summary</h2>
								</header>
								<div class="panel-body" style="background-color: #f6f6f6;">
									<div class="sidebar-widget widget-calendar">
										<ul class="route-summary" style="border-top: none;padding: 0px;border-bottom: 1px solid #000;">
											<li>
												<strong>Agency:</strong>
												{{ $data['brandingname'] }}
											</li>
											<li>
												<strong>Route Type:</strong> {{ $data['transportmode'] }}
											</li>
											<li>
												<strong>Route ID:</strong> <code>{{ $data['privatecode'] }}</code>
											</li>
											<li>
												<strong>Color:</strong>
												<span class="route-code-empty" style="background-color: {{ $data['color'] }}"></span>
												<code>{{ $data['color'] }}</code>
											</li>
											<li>
												<strong>Text Color:</strong>
												<span class="route-code-empty" style="background-color: {{ $data['textcolor'] }}"></span>
												<code>{{ $data['textcolor'] }}</code>
											</li>
										</ul>
										<ul class="route-summary" style="border-top: none;padding: 0px;border-bottom: 1px solid #000;">
											<li><strong>Starts:</strong> {{ $data['startdate'] }}</li>
											<li><strong>Ends:</strong> {{ $data['enddate'] }}</li>
											<input type="hidden" class="startdatefixs" value="{{ $data['startdatefixs'] }}">
											<input type="hidden" class="enddatefixs" value="{{ $data['enddatefixs'] }}">
										</ul>
										<div data-plugin-datepicker data-plugin-skin="primary" ></div>
									</div>
								</div>
							</section>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
@push('data-table')
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){   
		@php $noPop = 0; @endphp
		@foreach($data['models'] as $detail)
            $(".marShow-{{ $noPop }}").click(function() {
            	document.activeElement.blur();
                var css = $('.mar-{{ $noPop }}').css('background-image');
                $('.marker').css('background-image', 'url("http://127.0.0.1:8000/modules/netex/mapbox/images/marker-g.png")');
                if(css=='url("http://127.0.0.1:8000/modules/netex/mapbox/images/mar-red.png")'){
                    $('.mar-{{ $noPop }}').css('background-image', 'url("http://127.0.0.1:8000/modules/netex/mapbox/images/marker-g.png")');
                }else{
                    $('.mar-{{ $noPop }}').css('background-image', 'url("http://127.0.0.1:8000/modules/netex/mapbox/images/mar-red.png")');
                }
                $('.mar-{{ $noPop }}').click();
            });
            @php $noPop++; @endphp
        @endforeach
    });
</script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('netex:compiled/routeline.min.js') }}"></script>
@endpush

@endsection