@extends('theme::views.backend.app')

@section('page-title')
<h2>Log Import Time Table</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
	<li>
		<a href="index.html">
			<i class="fa fa-home"></i>
		</a>
	</li>
	<li><span>Log Import Time Table</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('netex:datatable/buttons.dataTables.min.css') }}" >
@endpush

<div class="row">
	<div class="col-md-12 col-lg-12">
		<div class="row">
			<div class="col-md-6 col-lg-12 col-xl-6">
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>
						<h2 class="panel-title">Log Import Time Table</h2>
					</header>
					<input type="hidden" name="urlinfo" data-url="{{ route('netex.import.log.detail') }}">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="dataTableRowLI">
							<thead>
								<tr>
									<th>ID</th>
									<th>Task Name</th>
									<th>File Name</th>
									<th>Status</th>
									<th>Last Action</th>
									<th>Date Time</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($models as $row)
								<tr>
									<td>{{ $row->id }}</td>
									<td id="get-{{ $row->id }}">{{ $row->task_name }}</td>
									<td>{{ $row->file_name }}</td>
									<td>{{ $row->status }}</td>
									<td>{{ $row->last_action }}</td>
									<td>{{ $row->date_time }}</td>
									<td><button class="btn btn-primary showdetaillog" id="id-{{ $row->id }}" type="button" data-toggle="modal" data-target="#modalBootstrap">Detail</button></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalBootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myTaskName">-</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-striped mb-none" id="dataTableRowLID" style="width: 100%;">
					<thead>
						<tr>
							<th>Date</th>
							<th>Type</th>
							<th>Message</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@push('data-table')
<script src="{{ Module::asset('netex:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('netex:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('netex:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('netex:compiled/import-netex-log.min.js') }}"></script>
@endpush

@endsection