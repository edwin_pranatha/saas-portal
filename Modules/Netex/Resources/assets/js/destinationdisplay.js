        $(document).ready(function(){    
            $('#dataTableRowDD').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'id', "width" : "80px", "orderable" : true },
                    { data: 'front_text', "width" : "80px", "orderable" : true },
                    { data: 'name' },
                    { data: 'private_code' },
                    { data: 'short_name' },
                    { data: 'side_text' },
                    { data: 'variant' },
                    { data: 'version' },
                    { data: 'via' }
                ]
            });
        });