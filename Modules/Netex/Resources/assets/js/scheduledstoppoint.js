        $(document).ready(function(){    
            var url_string = window.location.href;
            var url = new URL(url_string);
            var c = url.searchParams.get("private_code");
            $('#dataTableRowSsp').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    'data': {
                        'private_code': c
                    }
                },
                'columns': [
                    { data: 'name' },
                    { data: 'private_code' },
                    { data: 'rd_x' },
                    { data: 'rd_y' },
                    { data: 'version' },
                ]
            });
        });