        $(document).ready(function(){    
            $('#dataTableRowSA').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'id', "width" : "80px", "orderable" : true },
                    { data: 'description', "width" : "80px", "orderable" : true },
                    { data: 'name' },
                    { data: 'private_code' },
                    { data: 'public_code' },
                    { data: 'topographic_place_view' },
                    { data: 'version' }
                ]
            });
        });