        $(document).ready(function(){        
            $('#dataTableRowJL').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'id', "width" : "80px", "orderable" : true },
                    { data: 'layover', "width" : "80px", "orderable" : true },
                    { data: 'scheduled_stop_point_ref' },
                    { data: 'time_demand_type_ref' },
                    { data: 'version' }
                ]
            });
        });