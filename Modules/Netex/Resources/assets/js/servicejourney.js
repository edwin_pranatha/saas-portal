        $(document).ready(function(){         
            $('#dataTableRow').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'id', "width" : "80px", "orderable" : true },
                    { data: 'availability_condition_ref', "width" : "80px", "orderable" : true },
                    { data: 'block_ref' },
                    { data: 'data_source_ref' },
                    { data: 'day_type' },
                    { data: 'departure_day_offset' },
                    { data: 'departure_time' },
                    { data: 'dynamic' },
                    { data: 'service_journey_pattern_ref' },
                    { data: 'monitored' },
                    { data: 'operator_ref' },
                    { data: 'print' },
                    { data: 'private_code' },
                    { data: 'time_demand_type_ref' },
                    { data: 'type_of_product_category_ref' },
                    { data: 'vehicle_type' },
                    { data: 'version' },
                ]
            });
        });