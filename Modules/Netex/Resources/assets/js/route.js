        $(document).ready(function(){
            $('#dataTableRowRoute').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                    { data: 'id', "width" : "80px", "orderable" : true },
                    { data: 'direction_type', "width" : "80px", "orderable" : true },
                    { data: 'line_ref' },
                    { data: 'name' },
                    { data: 'version' },
                ]
            });
        });