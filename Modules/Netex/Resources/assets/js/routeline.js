        $(document).ready(function(){   
            $('#dataTableRowRL').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                'columns': [
                { data: 'public_code', "width" : "80px", "orderable" : true },
                { data: 'branding_id', "width" : "80px", "orderable" : true },
                { data: 'branding_name', "width" : "80px", "orderable" : true },
                { data: 'line_name' },
                { data: 'private_code' },
                { data: 'detail', "width" : "80px", "orderable" : true }
                ]
            });
            
            var nomor = 0;
            $("[data-plugin-datepicker]").datepicker().on('changeDate', function (ev) {
                if(nomor > 0){
                    var split = window.location.href.split('&date');
                    var url = split[0] + '&date=' + ev.format('yyyy-mm-dd');
                    window.location.href = url;
                }
                nomor++;
            });

            $("[data-plugin-datepicker]").datepicker().on('changeMonth', function (ev) {
                setTimeout(function(){ highlight(); }, 100);
            });

            setTimeout(function(){ if(window.location.href.includes('/line/view')==true||window.location.href.includes('/line/detail')==true) highlight(); }, 100);

            var datadate = $('.dateClass').text();
            var date = new Date(datadate);
            var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            $("[data-plugin-datepicker]").datepicker("setDate", today );

            function highlight(){
                var array = $("[data-plugin-datepicker]").find(".day").toArray();
                var months = $(".datepicker-switch")[0].innerText;
                for(var i=0;i < array.length;i++){
                    var date = array[i].innerText; 
                    if(array[i].className.includes('old')==true){
                        var status = 1;
                    }else if(array[i].className.includes('new')==true){
                        var status = 2;
                    }else{
                        var status = 3;
                    }
                    if(new Date($('.startdatefixs').val()) <= +new Date(formatDate(date, months, status)) && new Date($('.enddatefixs').val()) >= new Date(formatDate(date, months, status))){
                        var e = document.createElement('div');
                            e.className = 'line-bgGreen';
                        array[i].appendChild(e);
                    }  
                }
            }

            function formatDate(dates, months, status){
                var d = new Date('01 ' + months);

                if(status==1){
                    var month = d.getMonth();
                }else if(status==2){
                    var month = d.getMonth() + 2;
                }else{
                    var month = d.getMonth() + 1;
                }

                if(parseInt(month) < 10){
                    month = '0' + month;
                }

                if(parseInt(dates) < 10){
                    dates = '0' + dates;
                }
                return d.getFullYear() + '-' + month + '-' + dates;
            }
        });