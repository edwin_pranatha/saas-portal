         $(document).ready(function(){ 
            var timeoutLocationStart = ''; 
            var timeoutLocationCenter = ''; 
            var timeoutLocationEnd = '';
            var valueTimeout = 1000;
            $('body').on('keyup', '.select2-input', function(e) {
                var val = this.value;
                var split = e.currentTarget.id.split('_');
                var idSelect = $('#' + split[0] + '_' + split[1])[0].offsetParent.id;
                var splitNew = idSelect.split('_');
                var idNewSelect = splitNew[1];
                var idinput = this.id;
                $('#' + idinput).addClass('load-bgImage-Point');
                if(idNewSelect=='locationStart'){
                    if(timeoutLocationStart!=''){
                        clearTimeout(timeoutLocationStart);
                    }
                    autogenSelectSearch1(val, idinput);
                }else if(idNewSelect=='locationCenter'){
                    if(timeoutLocationCenter!=''){
                        clearTimeout(timeoutLocationCenter);
                    }
                    autogenSelectSearch2(val, idinput);
                }else{
                    if(timeoutLocationEnd!=''){
                        clearTimeout(timeoutLocationEnd);
                    }
                    autogenSelectSearch3(val, idinput);
                }
            });

            $('body').on('keydown', '.select2-input', function(e) {
                var split = e.currentTarget.id.split('_');
                var idSelect = $('#' + split[0] + '_' + split[1])[0].offsetParent.id;
                var splitNew = idSelect.split('_');
                var idNewSelect = splitNew[1];
                if(idNewSelect=='locationStart'){
                    if(timeoutLocationStart!=''){
                        clearTimeout(timeoutLocationStart);
                    }
                }else if(idNewSelect=='locationCenter'){
                    if(timeoutLocationCenter!=''){
                        clearTimeout(timeoutLocationCenter);
                    }
                }else{
                    if(timeoutLocationEnd!=''){
                        clearTimeout(timeoutLocationEnd);
                    }
                }
            });

            function autogenSelectSearch1(val, idinput){
                var search = val
                var ajaxFnStart = function () { 
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('[name=urlinfo]').attr('data-url'),
                        data: {
                            search : search
                        },
                        cache: false,
                        success: function(data){
                            $('#locationStart').empty();
                            var result = JSON.parse(data);
                            for (var i = 0; i < result.length; i++) {
                                var newState = '<option value="'+ result[i]['private_code'] +'">'+ result[i]['name'] +'</option>'
                                $("#locationStart").append(newState).trigger('change');
                            }
                            $('#locationStart').select2('open');
                            $('#' + idinput).val(val);
                            $('#' + idinput).removeClass('load-bgImage-Point');
                        }
                    });
                }
                timeoutLocationStart = setTimeout(ajaxFnStart, valueTimeout);
            }

            function autogenSelectSearch2(val, idinput){
                var search = val;
                var ajaxFnCenter = function () { 
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('[name=urlinfo]').attr('data-url'),
                        data: {
                            search : search
                        },
                        cache: false,
                        success: function(data){
                            $('#locationCenter').empty();
                            var result = JSON.parse(data);
                            for (var i = 0; i < result.length; i++) {
                                var newState = '<option value="'+ result[i]['private_code'] +'">'+ result[i]['name'] +'</option>'
                                $("#locationCenter").append(newState);
                            }
                            $('#locationCenter').select2('open');
                            $('#' + idinput).val(val);
                            $('#' + idinput).removeClass('load-bgImage-Point');
                        }
                    });
                }
                timeoutLocationCenter = setTimeout(ajaxFnCenter, valueTimeout);
            }

            function autogenSelectSearch3(val, idinput){
                var search = val;
                var ajaxFnEnd = function () { 
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: $('[name=urlinfo]').attr('data-url'),
                        data: {
                            search : search
                        },
                        cache: false,
                        success: function(data){
                            $('#locationEnd').empty();
                            var result = JSON.parse(data);
                            for (var i = 0; i < result.length; i++) {
                                var newState = '<option value="'+ result[i]['private_code'] +'">'+ result[i]['name'] +'</option>'
                                $("#locationEnd").append(newState);
                            }
                            $('#locationEnd').select2('open');
                            $('#' + idinput).val(val);
                            $('#' + idinput).removeClass('load-bgImage-Point');
                        }
                    });
                }
                timeoutLocationEnd = setTimeout(ajaxFnEnd, valueTimeout);
            }
        });