        $(document).ready(function(){        
            $('.showdetaillog').on('click', function(){
                var splitid = this.id.split('-');
                $('#myTaskName').html($('#get-' + splitid[1]).html());
                $('#dataTableRowLID').dataTable({
                    'processing': true,
                    'serverSide': true,
                    "bPaginate": false,
                    "info": false,
                    "searching": false,
                    "bDestroy": true,
                    'serverMethod': 'post',
                    'ajax': {
                        'url': $('[name=urlinfo]').attr('data-url'),
                        'type': 'POST',
                        'headers': {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        'data': {
                            'id': splitid[1]
                        }
                    },
                    'columns': [
                        { data: 'date' },
                        { data: 'type' },
                        { data: 'message' }
                    ]
                });
            });

            $('#dataTableRowLI').dataTable({
                "order": [[ 5, "desc" ]],
                "columnDefs" : [{"targets":5, "type":"date"}],
            });
        });