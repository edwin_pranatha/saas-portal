        $(document).ready(function(){    
            var url_string = window.location.href;
            var url = new URL(url_string);
            var c = url.searchParams.get("lineref");
            $('#dataTableRowLine').dataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url': $('[name=urlinfo]').attr('data-url'),
                    'type': 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    'data': {
                        'lineref': c
                    }
                },
                'columns': [
                    { data: 'id', "width" : "80px", "orderable" : true },
                    { data: 'accessibility_assessment', "width" : "80px", "orderable" : true },
                    { data: 'authority_ref' },
                    { data: 'branding_ref' },
                    { data: 'description' },
                    { data: 'external_line_ref' },
                    { data: 'monitored' },
                    { data: 'name' },
                    { data: 'operational_context_ref' },
                    { data: 'private_code' },
                    { data: 'public_code' },
                    { data: 'responsibility_set_ref' },
                    { data: 'transport_mode' },
                    { data: 'type_of_product_category_ref' },
                    { data: 'type_of_service_ref' },
                    { data: 'version' },
                ]
            });
        });