<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/admin/netex')->middleware(['auth'])->name('admin.')->group(function() {
    Route::view('/','netex::import')->name('netex.index');
    Route::post('/import', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'import'])->name('netex.import');
    Route::post('/import/log', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'saveImportNetexSuccess'])->name('netex.import.log');
    Route::get('/import/log', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'viewLogImportNetexSuccess'])->name('netex.import.log');
    Route::post('/import/log/detail', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_ild'])->name('netex.import.log.detail');
    Route::get('/line', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'line'])->name('netex.line');
    Route::post('/processline', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_line'])->name('netex.processline');
    Route::get('/route', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'route'])->name('netex.route');
    Route::post('/processroute', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_route'])->name('netex.processroute');
    Route::get('/servicejourney', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'service_journey'])->name('netex.servicejourney');
    Route::post('/processsj', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_sj'])->name('netex.processsj');
    Route::get('/destinationdisplay', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'destination_display'])->name('netex.destinationdisplay');
    Route::post('/processdd', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_dd'])->name('netex.processdd');
    Route::get('/journeylayover', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'journey_layover'])->name('netex.journeylayover');
    Route::post('/processjl', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_jl'])->name('netex.processjl');
    Route::get('/stoparea', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'stop_area'])->name('netex.stoparea');
    Route::post('/processsa', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_sa'])->name('netex.processsa');

    Route::get('/scheduledstoppoint', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'scheduled_stop_point'])->name('netex.scheduledstoppoint');
    Route::post('/process_ssp', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_ssp'])->name('netex.process_ssp');

    Route::get('/route/line', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'route_line'])->name('netex.routeline');
    Route::post('/processrl', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_rl'])->name('netex.processrl');
    Route::get('/journeyplanner', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'journeyplanner'])->name('netex.journeyplanner.index');
    Route::post('/processsp', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'process_sp'])->name('netex.processsp');
    Route::get('/route/line/view', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'routeline_view'])->name('netex.routeline.view');
    Route::get('/route/line/detail', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'routeline_detail'])->name('netex.routeline.detail');
    Route::get('/journeyplanner/detail', [Modules\Netex\Http\Controllers\Backend\NetexController::class, 'journeyplannerdetail'])->name('netex.journeyplanner.detail');
});
