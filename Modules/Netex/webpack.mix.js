const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('/').mergeManifest();

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/import-netex.js'
],  __dirname + '/Resources/assets/compiled/js/import-netex.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/line.js'
],  __dirname + '/Resources/assets/compiled/js/line.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/route.js'
],  __dirname + '/Resources/assets/compiled/js/route.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/stoparea.js'
],  __dirname + '/Resources/assets/compiled/js/stoparea.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/destinationdisplay.js'
],  __dirname + '/Resources/assets/compiled/js/destinationdisplay.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/scheduledstoppoint.js'
],  __dirname + '/Resources/assets/compiled/js/scheduledstoppoint.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    __dirname + '/Resources/assets/js/routeline.js'
],  __dirname + '/Resources/assets/compiled/js/routeline.min.js');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/sweetalert2-7.12.15/dist/sweetalert2.all.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    'node_modules/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js', 
    __dirname + '/Resources/assets/js/journeyplanner.js'
],  __dirname + '/Resources/assets/compiled/js/journeyplanner.min.js');

mix.options({
    processCssUrls: false,
});

mix.sass( __dirname + '/Resources/assets/sass/import-netex.scss', __dirname + '/Resources/assets/compiled/css/import-netex.min.css');
mix.sass( __dirname + '/Resources/assets/sass/line.scss', __dirname + '/Resources/assets/compiled/css/line.min.css');
mix.sass( __dirname + '/Resources/assets/sass/route.scss', __dirname + '/Resources/assets/compiled/css/route.min.css');
mix.sass( __dirname + '/Resources/assets/sass/stoparea.scss', __dirname + '/Resources/assets/compiled/css/stoparea.min.css');
mix.sass( __dirname + '/Resources/assets/sass/destinationdisplay.scss', __dirname + '/Resources/assets/compiled/css/destinationdisplay.min.css');
mix.sass( __dirname + '/Resources/assets/sass/scheduledstoppoint.scss', __dirname + '/Resources/assets/compiled/css/scheduledstoppoint.min.css');
mix.sass( __dirname + '/Resources/assets/sass/routeline.scss', __dirname + '/Resources/assets/compiled/css/routeline.min.css');
mix.sass( __dirname + '/Resources/assets/sass/journeyplanner.scss', __dirname + '/Resources/assets/compiled/css/journeyplanner.min.css');

if (process.env.NODE_ENV === 'production') {
mix.copy('node_modules/select2-3.5.1/select2.png',  __dirname + '/Resources/assets/compiled/css/select2.png');
mix.copy('node_modules/select2-3.5.1/select2-spinner.gif',  __dirname + '/Resources/assets/compiled/css/select2-spinner.gif');
mix.copy('node_modules/select2-3.5.1/select2x2.png',  __dirname + '/Resources/assets/compiled/css/select2x2.png');
mix.copyDirectory('node_modules/datatables.net-dt/images/',  __dirname + '/Resources/assets/compiled/images/');
}
// mix.postCss(__dirname + '/Resources/assets/css/routeline.css', __dirname + '/Resources/assets/sass/routeline.min.css');
// mix.postCss(__dirname + '/Resources/assets/css/stockpoint.css', __dirname + '/Resources/assets/sass/stockpoint.min.css');

// mix.js(__dirname + '/Resources/assets/js/destinationdisplay.js', __dirname + '/Resources/assets/compiled/destinationdisplay.min.js');
// mix.js(__dirname + '/Resources/assets/js/import-netex-log.js', __dirname + '/Resources/assets/compiled/import-netex-log.min.js');
// mix.js(__dirname + '/Resources/assets/js/import-netex.js', __dirname + '/Resources/assets/compiled/import-netex.min.js');
// mix.js(__dirname + '/Resources/assets/js/journeylayover.js', __dirname + '/Resources/assets/compiled/journeylayover.min.js');
// mix.js(__dirname + '/Resources/assets/js/line.js', __dirname + '/Resources/assets/compiled/line.min.js');
// mix.js(__dirname + '/Resources/assets/js/route.js', __dirname + '/Resources/assets/compiled/route.min.js');
// mix.js(__dirname + '/Resources/assets/js/routeline.js', __dirname + '/Resources/assets/compiled/routeline.min.js');
// mix.js(__dirname + '/Resources/assets/js/servicejourney.js', __dirname + '/Resources/assets/compiled/servicejourney.min.js');
// mix.js(__dirname + '/Resources/assets/js/stockpoint.js', __dirname + '/Resources/assets/compiled/stockpoint.min.js');
// mix.js(__dirname + '/Resources/assets/js/stoparea.js', __dirname + '/Resources/assets/compiled/stoparea.min.js');
// mix.js(__dirname + '/Resources/assets/js/scheduledstoppoint.js', __dirname + '/Resources/assets/compiled/scheduledstoppoint.min.js');

if (mix.inProduction()) {
    mix.version();
}
