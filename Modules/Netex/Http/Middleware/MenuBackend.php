<?php

namespace Modules\Netex\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use Lavary\Menu\Facade as Menu;
use Illuminate\Support\Str;

class MenuBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Menu::make('MenuBackend', function ($menu) {
            
            $menu->add('NETEX', '#')->data([
                'icon' => 'fa fa-flag',
                'group' => 'NETEX',
                'permissions' => 'netex.view',
                'active_match' => 'admin/netex*',
                'order' => 4,
            ]);
            $menu->nETEX->add('Import Data', 'admin/netex')->data([
                'permissions' => 'content-upload.view',
                'active_match' => 'admin/netex/content-upload*',
                'group' => 'NETEX',
                'order' => 1,
            ])->active("admin/netex");

            $menu->nETEX->add('Line', 'admin/netex/line')->data([
                'permissions' => 'layout.view',
                'active_match' => 'admin/netex/line*',
                'group' => 'NETEX',
                'order' => 2,
            ])->active("admin/netex/line");

            $menu->nETEX->add('Route', 'admin/netex/route')->data([
                'permissions' => 'campaign.view',
                'active_match' => 'admin/netex/route',
                'group' => 'NETEX',
                'order' => 3,
            ])->active("admin/netex/route");

            
            $menu->nETEX->add('Destination Display', 'admin/netex/destinationdisplay')->data([
                'permissions' => 'campaign.view',
                'active_match' => 'admin/netex/destinationdisplay*',
                'group' => 'NETEX',
                'order' => 4,
            ])->active("admin/netex/destinationdisplay");

            
            $menu->nETEX->add('Stop Area', 'admin/netex/stoparea')->data([
                'permissions' => 'campaign.view',
                'active_match' => 'admin/netex/stoparea*',
                'group' => 'NETEX',
                'order' => 4,
            ])->active("admin/netex/stoparea");

            $menu->nETEX->add('Scheduled Stop Point', 'admin/netex/scheduledstoppoint')->data([
                'permissions' => 'campaign.view',
                'active_match' => 'admin/netex/scheduledstoppoint*',
                'group' => 'NETEX',
                'order' => 5,
            ])->active("admin/netex/scheduledstoppoint");

            $menu->nETEX->add('Route Line', 'admin/netex/route/line')->data([
                'permissions' => 'campaign.view',
                'active_match' => 'admin/netex/route/line*',
                'group' => 'NETEX',
                'order' => 6,
            ])->active("admin/netex/route/line");

            
            $menu->nETEX->add('Journey Planner', 'admin/netex/journeyplanner')->data([
                'permissions' => 'campaign.view',
                'active_match' => 'admin/netex/journeyplanner*',
                'group' => 'NETEX',
                'order' => 7,
            ])->active("admin/netex/journeyplanner");
            
        })->filter(function ($item) use ($request) {
            if ($request->url() === rtrim($item->url(), '#/')) {
                $item->active();
            }
            
            if ($item->active_match) {
                $matches = is_array($item->active_match) ? $item->active_match : [$item->active_match];                
                foreach ($matches as $pattern) {
                    if (Str::is($pattern, $request->path())) {
                        $item->activate();
                    }
                }
            }
            return true;
        })->sortBy('order');
        return $next($request);
    }
}