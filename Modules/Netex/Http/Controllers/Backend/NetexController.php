<?php

namespace Modules\Netex\Http\Controllers\Backend;

use Modules\Netex\Entities\ImportNetex;
use Modules\Netex\Entities\LogImportNetex;
use Modules\Netex\Entities\LogImportNetexDetail;
use Modules\Netex\Entities\Line;
use Modules\Netex\Entities\Route;
use Modules\Netex\Entities\ServiceJourney;
use Modules\Netex\Entities\DestinationDisplay;
use Modules\Netex\Entities\JourneyLayover;
use Modules\Netex\Entities\StopArea;
use Modules\Netex\Entities\ScheduledStopPoint;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use proj4php\Proj4php;
use proj4php\proj;
use proj4php\Point;

class NetexController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('netex::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('netex::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('netex::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('netex::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function import(Request $request)
    {
        $import = $request->file('file');
        $models = New ImportNetex;
        $models->name = $import->getClientOriginalName();
        $models->status = 1;
        $models->desc = $request->desc;
        $models->save();

        $destination = 'importxml';
        $import->move($destination,$import->getClientOriginalName());

        return "success";
    }

    public function line(Request $request)
    {
        return view('netex::line');
    }

    public function process_line(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);
        $lineref = $request->post('lineref');
        $wherelineref = "";
        if(!empty($lineref)){
            $wherelineref = " where id='".$lineref."'";
        }

        $searchQuery = " ";
        if($searchValue != ''){
            if(!empty($lineref)){
                $searchQuery = " AND (id like '%".$searchValue."%' or 
                    CAST(accessibility_assessment AS TEXT) like '%".$searchValue."%' or 
                    authority_ref like '%".$searchValue."%' ) ";
            }else{
                $searchQuery = " where (id like '%".$searchValue."%' or 
                    CAST(accessibility_assessment AS TEXT) like '%".$searchValue."%' or 
                    authority_ref like '%".$searchValue."%' ) ";
            }
        }

        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from line;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from line ".$wherelineref."".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsql1')->select("select * from line ".$wherelineref."".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'timetable']);
        $totalRecords = $otf->getTable('line')->count();
        $totalRecordwithFilter = $otf->getTable('line')->selectraw("select count(*) as allcount from line ".$searchQuery)->count();
        $empRecords = $otf->getTable('line')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "id"=>str_replace(':',': ',$empRecords[$x]->id),
                    "accessibility_assessment"=>$empRecords[$x]->accessibility_assessment,
                    "authority_ref"=>str_replace(':',': ',$empRecords[$x]->authority_ref),
                    "branding_ref"=>$empRecords[$x]->branding_ref,
                    "description"=>$empRecords[$x]->description,
                    "external_line_ref"=>$empRecords[$x]->external_line_ref,
                    "monitored"=>$empRecords[$x]->monitored,
                    "name"=>$empRecords[$x]->name,
                    "operational_context_ref"=>$empRecords[$x]->operational_context_ref,
                    "private_code"=>$empRecords[$x]->private_code,
                    "public_code"=>$empRecords[$x]->public_code,
                    "responsibility_set_ref"=>$empRecords[$x]->responsibility_set_ref,
                    "transport_mode"=>$empRecords[$x]->transport_mode,
                    "type_of_product_category_ref"=>$empRecords[$x]->type_of_product_category_ref,
                    "type_of_service_ref"=>$empRecords[$x]->type_of_service_ref,
                    "version"=>$empRecords[$x]->version
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function route()
    {
        return view('netex::route');
    }

    public function process_route(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (id like '%".$searchValue."%' or 
                direction_type like '%".$searchValue."%' or 
                line_ref like'%".$searchValue."%' ) ";
        }

        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from route;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from route ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsql1')->select("select * from route ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'timetable']);
        $totalRecords = $otf->getTable('route')->count();
        $totalRecordwithFilter = $otf->getTable('route')->selectraw("select count(*) as allcount from route ".$searchQuery)->count();
        $empRecords = $otf->getTable('route')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "id"=>$empRecords[$x]->id,
                    "direction_type"=>$empRecords[$x]->direction_type,
                    "line_ref"=>"<a href='".route('admin.netex.line', ['lineref' => $empRecords[$x]->line_ref])."'>".$empRecords[$x]->line_ref."</a>",
                    "name"=>$empRecords[$x]->name,
                    "version"=>$empRecords[$x]->version
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function service_journey()
    {
        return view('netex::servicejourney');
    }

    public function process_sj(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (id like '%".$searchValue."%' or 
                availability_condition_ref like '%".$searchValue."%' or 
                block_ref like'%".$searchValue."%' ) ";
        }

        $records = DB::connection('pgsql1')->select("select count(*) as allcount from service_journey;");
        $totalRecords = $records[0]->allcount;
        
        $records = DB::connection('pgsql1')->select("select count(*) as allcount from service_journey ".$searchQuery);
        $totalRecordwithFilter = $records[0]->allcount;
    
        $empRecords = DB::connection('pgsql1')->select("select * from service_journey ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "id"=>str_replace(':',': ',$empRecords[$x]->id),
                    "availability_condition_ref"=>str_replace(':',': ',$empRecords[$x]->availability_condition_ref),
                    "block_ref"=>$empRecords[$x]->block_ref,
                    "data_source_ref"=>$empRecords[$x]->data_source_ref,
                    "day_type"=>$empRecords[$x]->day_type,
                    "departure_day_offset"=>$empRecords[$x]->departure_day_offset,
                    "departure_time"=>$empRecords[$x]->departure_time,
                    "dynamic"=>$empRecords[$x]->dynamic,
                    "service_journey_pattern_ref"=>str_replace(':',': ',$empRecords[$x]->service_journey_pattern_ref),
                    "monitored"=>$empRecords[$x]->monitored,
                    "operator_ref"=>$empRecords[$x]->operator_ref,
                    "print"=>$empRecords[$x]->print,
                    "private_code"=>$empRecords[$x]->private_code,
                    "time_demand_type_ref"=>str_replace(':',': ',$empRecords[$x]->time_demand_type_ref),
                    "type_of_product_category_ref"=>$empRecords[$x]->type_of_product_category_ref,
                    "vehicle_type"=>$empRecords[$x]->vehicle_type,
                    "version"=>$empRecords[$x]->version
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function destination_display()
    {
        return view('netex::destinationdisplay');
    }

    public function process_dd(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (id like '%".$searchValue."%' or 
                front_text like '%".$searchValue."%' or 
                name like'%".$searchValue."%' ) ";
        }

        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from destination_display;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from destination_display ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsql1')->select("select * from destination_display ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'timetable']);
        $totalRecords = $otf->getTable('destination_display')->count();
        $totalRecordwithFilter = $otf->getTable('destination_display')->selectraw("select count(*) as allcount from destination_display ".$searchQuery)->count();
        $empRecords = $otf->getTable('destination_display')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "id"=>$empRecords[$x]->id,
                    "front_text"=>$empRecords[$x]->front_text,
                    "name"=>$empRecords[$x]->name,
                    "private_code"=>$empRecords[$x]->private_code,
                    "short_name"=>$empRecords[$x]->short_name,
                    "side_text"=>$empRecords[$x]->side_text,
                    "variant"=>$empRecords[$x]->variant,
                    "version"=>$empRecords[$x]->version,
                    "via"=>$empRecords[$x]->via
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function journey_layover()
    {
        return view('netex::journeylayover');
    }

    public function process_jl(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (id like '%".$searchValue."%' or 
                front_text like '%".$searchValue."%' or 
                name like'%".$searchValue."%' ) ";
        }

        $records = DB::connection('pgsql1')->select("select count(*) as allcount from journey_layover;");
        $totalRecords = $records[0]->allcount;
        
        $records = DB::connection('pgsql1')->select("select count(*) as allcount from journey_layover ".$searchQuery);
        $totalRecordwithFilter = $records[0]->allcount;
    
        $empRecords = DB::connection('pgsql1')->select("select * from journey_layover ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "id"=>$empRecords[$x]->id,
                    "layover"=>$empRecords[$x]->layover,
                    "scheduled_stop_point_ref"=>$empRecords[$x]->scheduled_stop_point_ref,
                    "time_demand_type_ref"=>$empRecords[$x]->time_demand_type_ref,
                    "version"=>$empRecords[$x]->version
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function stop_area()
    {
        return view('netex::stoparea');
    }

    public function process_sa(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (id like '%".$searchValue."%' or 
                front_text like '%".$searchValue."%' or 
                name like'%".$searchValue."%' ) ";
        }

        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from stop_area;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from stop_area ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsql1')->select("select * from stop_area ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'timetable']);
        $totalRecords = $otf->getTable('stop_area')->count();
        $totalRecordwithFilter = $otf->getTable('stop_area')->selectraw("select count(*) as allcount from stop_area ".$searchQuery)->count();
        $empRecords = $otf->getTable('stop_area')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "id"=>$empRecords[$x]->id,
                    "description"=>$empRecords[$x]->description,
                    "name"=>$empRecords[$x]->name,
                    "private_code"=>$empRecords[$x]->private_code,
                    "public_code"=>$empRecords[$x]->public_code,
                    "topographic_place_view"=>$empRecords[$x]->topographic_place_view,
                    "version"=>$empRecords[$x]->version
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function scheduled_stop_point()
    {
        return view('netex::scheduledstoppoint');
    }

    public function process_ssp(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);
        $privatecode = $request->post('private_code');
        $whereprivatecode = "";
        if(!empty($privatecode)){
            $whereprivatecode = " where private_code='".$privatecode."'";
        }

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " where (name like '%".$searchValue."%' or 
                private_code like '%".$searchValue."%' or 
                version like'%".$searchValue."%' ) ";
        }

        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from scheduled_stop_point;");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from scheduled_stop_point ".$whereprivatecode."".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsql1')->select("select * from scheduled_stop_point ".$whereprivatecode."".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'timetable']);
        $totalRecords = $otf->getTable('scheduled_stop_point')->count();
        $totalRecordwithFilter = $otf->getTable('scheduled_stop_point')->selectraw("select count(*) as allcount from scheduled_stop_point ".$searchQuery)->count();
        $empRecords = $otf->getTable('scheduled_stop_point')->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "name"=>$empRecords[$x]->name,
                    "private_code"=>$empRecords[$x]->private_code,
                    "rd_x"=>$empRecords[$x]->rd_x,
                    "rd_y"=>$empRecords[$x]->rd_y,
                    "version"=>$empRecords[$x]->version
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function route_line()
    {
        return view('netex::routeline.index');
    }

    public function process_rl(Request $request)
    {
        $draw = $request->post('draw');
        $row = $request->post('start');
        $rowperpage = $request->post('length');
        $columnIndex = $request->post('order')[0]['column'];
        $columnName = $request->post('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->post('order')[0]['dir'];
        $searchValue = addslashes($request->post('search')['value']);

        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (l.private_code like '%".$searchValue."%' or 
                l.name like '%".$searchValue."%' or 
                l.public_code like'%".$searchValue."%' ) ";
        }

        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from line l left join branding b on l.branding_ref = b.id where l.version = (select version from version where modification = 'NEW' and version_type = 'BASELINE'and '2021-06-18'::timestamp >= start_date and '2021-06-18'::timestamp <= end_date order by start_date desc limit 1);");
        // $totalRecords = $records[0]->allcount;
        
        // $records = DB::connection('pgsql1')->select("select count(*) as allcount from line l left join branding b on l.branding_ref = b.id where l.version = (select version from version where modification = 'NEW' and version_type = 'BASELINE'and '2021-06-18'::timestamp >= start_date and '2021-06-18'::timestamp <= end_date order by start_date desc limit 1) ".$searchQuery);
        // $totalRecordwithFilter = $records[0]->allcount;
    
        // $empRecords = DB::connection('pgsql1')->select("select l.private_code, b.id branding_id, b.name branding_name, l.public_code, l.name line_name from line l left join branding b on l.branding_ref = b.id where l.version = (select version from version where modification = 'NEW' and version_type = 'BASELINE' and '2021-06-18'::timestamp >= start_date and '2021-06-18'::timestamp <= end_date order by start_date desc limit 1) ".$searchQuery." order by b.id ".$columnSortOrder." limit ".$rowperpage." OFFSET ".$row);
        $otf = new \App\Models\Database(['database' => 'timetable']);
        $totalRecords = $otf->getTable('line')->count();
        $totalRecordwithFilter = $otf->getTable('line')->selectraw("select count(*) as allcount from line l left join branding b on l.branding_ref = b.id where l.version = (select version from version where modification = 'NEW' and version_type = 'BASELINE'and '2021-06-18'::timestamp >= start_date and '2021-06-18'::timestamp <= end_date order by start_date desc limit 1);")->count();
        $empRecords = $otf->getTable('line')->selectRaw("select l.private_code, b.id branding_id, b.name branding_name, l.public_code, l.name line_name")->reorder($columnName, $columnSortOrder)->limit($rowperpage)->offset($row)->get();
        $data = array();
        for ($x = 0; $x < count($empRecords); $x++) {
            $data[] = array(
                    "public_code"=>$empRecords[$x]->public_code,
                    "branding_id"=>$empRecords[$x]->branding_id,
                    "branding_name"=>$empRecords[$x]->branding_name,
                    "line_name"=>$empRecords[$x]->line_name,
                    "private_code"=>$empRecords[$x]->private_code,
                    "detail"=>"<a href='".route('netex.routeline.view', ['private_code' => $empRecords[$x]->private_code, 'branding_id' => $empRecords[$x]->branding_id, 'line_name' => $empRecords[$x]->line_name, 'branding_name' => $empRecords[$x]->branding_name])."' class='mb-xs mt-xs mr-xs modal-with-zoom-anim btn btn-primary'>Detail</a>"
                );
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return json_encode($response);
    }

    public function routeline_view(Request $request)
    {
        $privatecode = $request->get('private_code');
        $brandingid = $request->get('branding_id');
        $brandingname = $request->get('branding_name');
        $linename = $request->get('line_name');
        if(!empty($request->get('date'))){
            $date = $request->get('date');
        }else{
            $date =  date('Y-m-d');
        }
        $models = DB::connection('pgsql1')->select("select ssp.rd_x, ssp.rd_y, ssp.name, ssp.private_code, ssp.version, l.color, l.text_color, l.transport_mode, v.start_date, v.end_date, spjp.orders from line l left join (select version, start_date, end_date from version where modification = 'NEW' and version_type = 'BASELINE' and '".$date."'::timestamp >= start_date and '".$date."'::timestamp <= end_date order by start_date desc limit 1) v on v.version = l.version left join route r on r.line_ref = l.id left join service_journey_pattern sjp on sjp.route_ref = r.id left join service_journey sj on sjp.id = sj.service_journey_pattern_ref left join stop_point_journey_pattern spjp on spjp.service_journey_pattern_ref = sjp.id left join scheduled_stop_point ssp on ssp.id = spjp.scheduled_stop_point_ref where l.branding_ref = '".$brandingid."' and l.private_code = '".$privatecode."' group by ssp.rd_x, ssp.rd_y, ssp.name, ssp.private_code, spjp.orders, v.start_date, v.end_date, l.color, l.text_color, l.transport_mode, ssp.id order by spjp.orders, ssp.private_code asc;");
        $datamodels = array();
        $defaultpoint = '';
        $startdate = '';
        $enddate = '';
        $startdatefixs = '';
        $enddatefixs = '';
        $transportmode = '';
        $textcolor = '';
        $color = '';
        for ($x = 0; $x < count($models); $x++) {
            $proj4 = new Proj4php();
            $projL93    = new Proj('EPSG:28992', $proj4);
            $projWGS84  = new Proj('EPSG:4326', $proj4);
            $pointSrc = new Point($models[$x]->rd_x, $models[$x]->rd_y, $projL93);
            $pointDest = $proj4->transform($projWGS84, $pointSrc);
            if($x==0){
                $defaultpoint = str_replace(' ',',',$pointDest->toShortString());
                $date1=date_create($models[$x]->start_date);
                $startdate = date_format($date1,"l, d F Y");
                $date2=date_create($models[$x]->end_date);
                $enddate = date_format($date2,"l, d F Y");
                $startdatefixs = explode(' ', $models[$x]->start_date);
                $enddatefixs = explode(' ', $models[$x]->end_date);
                $transportmode = $models[$x]->transport_mode;
                $textcolor = $models[$x]->text_color;
                $color = $models[$x]->color;
            }
            $exp = explode(' ', $pointDest->toShortString());
            $datamodels[] = array(
                "lat" => $exp[0],
                "long" => $exp[1],
                "name" => $models[$x]->name,
                "private_code" => $models[$x]->private_code,
                "version" => $models[$x]->version,
                "orders" => $models[$x]->orders,
                "no" => $x
            );
        }
        $detail = DB::connection('pgsql1')->select("select sj.id, l.name, sj.departure_time, sj.departure_time + (sum((coalesce(jwt.wait_time, 0)/1000000000) + (coalesce(jrt.run_time, 0)/1000000000)) * interval '1 second') end_time,sj.private_code, dd.side_text,sjp.direction_type from line l left join route r on r.line_ref = l.id left join service_journey_pattern sjp on sjp.route_ref = r.id left join service_journey sj on sjp.id = sj.service_journey_pattern_ref left join destination_display dd on dd.id = sjp.destination_display_ref right join (select * from (select id, coalesce(((string_to_array(valid_day_bits, null))[extract(days from ('".$date."'::timestamp - from_date))::integer + 1])::integer, 0) validated_journey from availability_condition) a where a.validated_journey = 1) ac on ac.id = sj.availability_condition_ref left join stop_point_journey_pattern spjp on spjp.service_journey_pattern_ref = sjp.id left join scheduled_stop_point ssp on ssp.id = spjp.scheduled_stop_point_ref left join journey_wait_time jwt on jwt.time_demand_type_ref = sj.time_demand_type_ref and jwt.scheduled_stop_point_ref = ssp.id left join timing_link tl on tl.id = spjp.timing_link_ref and tl.from_point_ref = ssp.id left join journey_run_time jrt on jrt.time_demand_type_ref = sj.time_demand_type_ref and jrt.timing_link_ref = tl.id where l.branding_ref = '".$brandingid."' and l.private_code = '".$privatecode."' and l.version = (select version from version where modification = 'NEW' and version_type = 'BASELINE' and '".$date."'::timestamp >= start_date and '".$date."'::timestamp <= end_date order by start_date desc limit 1) group by l.name, sj.departure_time, sj.private_code, dd.side_text,sjp.direction_type, sj.id order by sjp.direction_type, sj.departure_time asc;");
        if(empty($detail)){
            $datadate = DB::connection('pgsql1')->select("select ssp.rd_x, ssp.rd_y, ssp.name, ssp.private_code, l.color, l.text_color, l.transport_mode, v.start_date, v.end_date, spjp.orders from line l left join (select version, start_date, end_date from version where modification = 'NEW' and version_type = 'BASELINE' order by start_date desc limit 1) v on v.version = l.version left join route r on r.line_ref = l.id left join service_journey_pattern sjp on sjp.route_ref = r.id left join service_journey sj on sjp.id = sj.service_journey_pattern_ref left join stop_point_journey_pattern spjp on spjp.service_journey_pattern_ref = sjp.id left join scheduled_stop_point ssp on ssp.id = spjp.scheduled_stop_point_ref where l.branding_ref = '".$brandingid."' and l.private_code = '".$privatecode."' group by ssp.rd_x, ssp.rd_y, ssp.name, ssp.private_code, spjp.orders, v.start_date, v.end_date, l.color, l.text_color, l.transport_mode order by spjp.orders, ssp.private_code asc LIMIT 1;");
            $date1=date_create($datadate[0]->start_date);
            $startdate = date_format($date1,"l, d F Y");
            $date2=date_create($datadate[0]->end_date);
            $startdatefixs = explode(' ', $datadate[0]->start_date);
            $enddatefixs = explode(' ', $datadate[0]->end_date);
            $date = date('Y-m-d', strtotime('-3 days', strtotime($enddatefixs[0])));
            $detail = DB::connection('pgsql1')->select("select sj.id, l.name, sj.departure_time, sj.departure_time + (sum((coalesce(jwt.wait_time, 0)/1000000000) + (coalesce(jrt.run_time, 0)/1000000000)) * interval '1 second') end_time,sj.private_code, dd.side_text,sjp.direction_type from line l left join route r on r.line_ref = l.id left join service_journey_pattern sjp on sjp.route_ref = r.id left join service_journey sj on sjp.id = sj.service_journey_pattern_ref left join destination_display dd on dd.id = sjp.destination_display_ref right join (select * from (select id, coalesce(((string_to_array(valid_day_bits, null))[extract(days from ('".$date."'::timestamp - from_date))::integer + 1])::integer, 0) validated_journey from availability_condition) a where a.validated_journey = 1) ac on ac.id = sj.availability_condition_ref left join stop_point_journey_pattern spjp on spjp.service_journey_pattern_ref = sjp.id left join scheduled_stop_point ssp on ssp.id = spjp.scheduled_stop_point_ref left join journey_wait_time jwt on jwt.time_demand_type_ref = sj.time_demand_type_ref and jwt.scheduled_stop_point_ref = ssp.id left join timing_link tl on tl.id = spjp.timing_link_ref and tl.from_point_ref = ssp.id left join journey_run_time jrt on jrt.time_demand_type_ref = sj.time_demand_type_ref and jrt.timing_link_ref = tl.id where l.branding_ref = '".$brandingid."' and l.private_code = '".$privatecode."' and l.version = (select version from version where modification = 'NEW' and version_type = 'BASELINE' and '".$date."'::timestamp >= start_date and '".$date."'::timestamp <= end_date order by start_date desc limit 1) group by l.name, sj.departure_time, sj.private_code, dd.side_text,sjp.direction_type, sj.id order by sjp.direction_type, sj.departure_time asc;");
        }
        $data = array();
        $data['models'] = $datamodels;
        $data['detail'] = $detail;
        $data['defaultpoint'] = $defaultpoint;
        $data['linename'] = $linename;
        $data['privatecode'] = $privatecode;
        $data['brandingname'] = $brandingname;
        $data['date'] = $date;
        $data['startdate'] = $startdate;
        $data['enddate'] = $enddate;
        $data['startdatefixs'] = $startdatefixs[0];
        $data['enddatefixs'] = $enddatefixs[0];
        $data['transportmode'] = $transportmode;
        $data['textcolor'] = $textcolor;
        $data['color'] = $color;
        return view('netex::routeline.view', compact('data'));
    }

    public function routeline_detail(Request $request)
    {
        $privatecode = $request->get('private_code');
        $brandingid = $request->get('branding_id');
        $brandingname = $request->get('branding_name');
        $linename = $request->get('line_name');
        $starttime = $request->get('starttime');
        $endtime = $request->get('endtime');
        $sjid = $request->get('sj_id');
        if(!empty($request->get('date'))){
            $date = $request->get('date');
        }else{
            $date =  date('Y-m-d');
        }
        $models = DB::connection('pgsql1')->select("select sj.id, l.name line_name, ssp.name stop_name, ssp.private_code as privatecodes, ssp.version, ssp.rd_x, ssp.rd_y, l.color, l.text_color, l.transport_mode, spjp.orders, sj.departure_time, (coalesce(jwt.wait_time, 0)/1000000000) stop_wait_time, (coalesce(jrt.run_time, 0)/1000000000) next_run_time, sj.private_code, dd.side_text, sjp.direction_type from line l left join route r on r.line_ref = l.id left join service_journey_pattern sjp on sjp.route_ref = r.id left join service_journey sj on sjp.id = sj.service_journey_pattern_ref left join destination_display dd on dd.id = sjp.destination_display_ref right join (select * from (select id, coalesce(((string_to_array(valid_day_bits, null))[extract(days from ('2021-06-18'::timestamp - from_date))::integer + 1])::integer, 0) validated_journey from availability_condition) a where a.validated_journey = 1) ac on ac.id = sj.availability_condition_ref left join stop_point_journey_pattern spjp on spjp.service_journey_pattern_ref = sjp.id left join scheduled_stop_point ssp on ssp.id = spjp.scheduled_stop_point_ref left join journey_wait_time jwt on jwt.time_demand_type_ref = sj.time_demand_type_ref and jwt.scheduled_stop_point_ref = ssp.id left join timing_link tl on tl.id = spjp.timing_link_ref and tl.from_point_ref = ssp.id left join journey_run_time jrt on jrt.time_demand_type_ref = sj.time_demand_type_ref and jrt.timing_link_ref = tl.id where l.branding_ref = '".$brandingid."' and l.private_code = '".$privatecode."' and sj.id = '".$sjid."' and l.version = (select version from version where modification = 'NEW' and version_type = 'BASELINE' and '".$date."'::timestamp >= start_date and '".$date."'::timestamp <= end_date order by start_date desc limit 1) order by sjp.direction_type, spjp.orders asc;");
        $datamodels = array();
        $defaultpoint = '';
        $startdatefixs = $request->get('startdatefixs');
        $enddatefixs = $request->get('enddatefixs');
        $startdate = '';
        $enddate = '';
        $transportmode = '';
        $textcolor = '';
        $color = '';
        for ($x = 0; $x < count($models); $x++) {
            $proj4 = new Proj4php();
            $projL93    = new Proj('EPSG:28992', $proj4);
            $projWGS84  = new Proj('EPSG:4326', $proj4);
            $pointSrc = new Point($models[$x]->rd_x, $models[$x]->rd_y, $projL93);
            $pointDest = $proj4->transform($projWGS84, $pointSrc);
            if($x==0){
                $defaultpoint = str_replace(' ',',',$pointDest->toShortString());
                $date1=date_create($startdatefixs);
                $startdate = date_format($date1,"l, d F Y");
                $date2=date_create($enddatefixs);
                $enddate = date_format($date2,"l, d F Y");
                $startdatefixs = explode(' ', $startdatefixs);
                $enddatefixs = explode(' ', $enddatefixs);
                $transportmode = $models[$x]->transport_mode;
                $textcolor = $models[$x]->text_color;
                $color = $models[$x]->color;
            }
            $exp = explode(' ', $pointDest->toShortString());
            $datamodels[] = array(
                "lat" => $exp[0],
                "long" => $exp[1],
                "name" => $models[$x]->stop_name,
                "privatecodes" => $models[$x]->privatecodes,
                "version" => $models[$x]->version,
                "orders" => $models[$x]->orders,
                "no" => $x
            );
        }
        $data = array();
        $data['models'] = $datamodels;
        $data['detail'] = $models;
        $data['defaultpoint'] = $defaultpoint;
        $data['linename'] = $linename;
        $data['privatecode'] = $privatecode;
        $data['brandingname'] = $brandingname;
        $data['date'] = $date;
        $data['startdate'] = $startdate;
        $data['enddate'] = $enddate;
        $data['starttime'] = $starttime;
        $data['endtime'] = $endtime;
        if(strlen($startdatefixs[0]) > 1){
            $data['startdatefixs'] = $startdatefixs[0];
        }else{
            $data['startdatefixs'] = $startdatefixs;
        }
        if(strlen($enddatefixs[0]) > 1){
            $data['enddatefixs'] = $enddatefixs[0];
        }else{
            $data['enddatefixs'] = $enddatefixs;
        }
        $data['transportmode'] = $transportmode;
        $data['textcolor'] = $textcolor;
        $data['color'] = $color;
        return view('netex::routeline.detail', compact('data'));
    }

    public function journeyplanner()
    {
        return view('netex::journeyplanner.index');   
    }

    public function process_sp(Request $request)
    {
        $search = $request->get('search');
        $models = ScheduledStopPoint::select('name','private_code')->where('name','like','%'.$search.'%')->limit(100)->get();

        return json_encode($models);
    }

    public function journeyplannerdetail(Request $request)
    {
        $dates = date_create($request->get('dates'));
        $fixdates = date_format($dates,"Y-m-d");
        $times = $request->get('times');
        $starttimestamp = strtotime($times) - 60*60;
        $starttime = date('H:i:s', $starttimestamp);
        $endtimestamp = strtotime($times) + 60*60;
        $endtime = date('H:i:s', $endtimestamp);
        $result = DB::connection('pgsql1')->select("select l.private_code line_ref, l.name line_name, spjp.orders, sj.departure_time, (coalesce(jwt.wait_time, 0)/1000000000) stop_wait_time, (coalesce(jrt.run_time, 0)/1000000000) next_run_time, ssp.private_code stop_code, ssp.name stop_name, dd.front_text, sjp.direction_type from line l left join route r on r.line_ref = l.id left join service_journey_pattern sjp on sjp.route_ref = r.id left join service_journey sj on sjp.id = sj.service_journey_pattern_ref left join destination_display dd on dd.id = sjp.destination_display_ref right join (select * from (select id, coalesce(((string_to_array(valid_day_bits, null))[extract(days from ('2021-07-28'::timestamp - from_date))::integer + 1])::integer, 0) validated_journey from availability_condition) a where a.validated_journey = 1) ac on ac.id = sj.availability_condition_ref left join operator o on o.id = sj.operator_ref left join stop_point_journey_pattern spjp on spjp.service_journey_pattern_ref = sjp.id left join scheduled_stop_point ssp on ssp.id = spjp.scheduled_stop_point_ref left join journey_wait_time jwt on jwt.time_demand_type_ref = sj.time_demand_type_ref and jwt.scheduled_stop_point_ref = ssp.id left join timing_link tl on tl.id = spjp.timing_link_ref and tl.from_point_ref = ssp.id left join journey_run_time jrt on jrt.time_demand_type_ref = sj.time_demand_type_ref and jrt.timing_link_ref = tl.id where o.short_name = 'CXX' and sjp.direction_type = 'INBOUND' and sj.departure_time >= '".$starttime."' and sj.departure_time <= '".$endtime."' and l.version = (select version from version where modification = 'NEW' and version_type = 'BASELINE' and '".$fixdates."'::timestamp >= start_date and '".$fixdates."'::timestamp <= end_date order by start_date desc limit 1) order by l.private_code, sj.id, sjp.direction_type, spjp.orders asc;");
        return view('netex::journeyplanner.detail', compact('result'));   
    }


    public function saveImportNetexSuccess(Request $request)
    {
        $id = $request->id;
        $taskName = $request->task_name;
        $fileName = $request->file_name;
        $dateTime = substr(str_replace('T',' ',$request->date_time),0,19);
        $lastAction = $request->last_action;
        $detail = $request->detail;
        $status = $request->status;

        $check = LogImportNetex::where('id', $id)->first();
        if(empty($check)){
            $models = New LogImportNetex;
            $models->id = $id;
            $models->task_name = $taskName;
            $models->file_name = $fileName;
            $models->date_time = $dateTime;
            $models->last_action = $lastAction;
            $models->status = $status;
            $models->save();
        }else{
            $check->task_name = $taskName;
            $check->file_name = $fileName;
            $check->date_time = $dateTime;
            $check->last_action = $lastAction;
            $check->status = $status;
            $check->save();
        }

        $rows = LogImportNetexDetail::where('id', $id)->delete();
        if(count($detail) > 0){
            for ($x = 0; $x < count($detail); $x++) {
                $row = New LogImportNetexDetail();
                $row->id = $id;
                $row->date = substr(str_replace('T',' ',$detail[$x]['date']),0,19);
                $row->type = $detail[$x]['type'];
                $row->message = $detail[$x]['message'];
                $row->save();
            }
        }

        return 'success';
    }

    public function viewLogImportNetexSuccess()
    {
        $models = LogImportNetex::all();

        return view('netex::logimport', compact('models'));
    }

    public function process_ild(Request $request)
    {
        $id = $request->id;
        $models = LogImportNetexDetail::where('id', $id)->get();
        $data = array();
        foreach($models as $row){
            $data[] = array(
                "date"=>$row->date,
                "type"=>$row->type,
                "message"=>$row->message
            );
        }
        $response = array(
            "aaData" => $data
        );
        return json_encode($response);
    }
}