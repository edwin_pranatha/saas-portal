<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class JourneyLayover extends Model
{
	protected $connection = 'pgsql1';
    protected $table = 'journey_layover';
    public $incrementing = false;
}
