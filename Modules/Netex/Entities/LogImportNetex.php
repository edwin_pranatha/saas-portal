<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class LogImportNetex extends Model
{
    protected $table = 'log_import_netex';
    public $incrementing = false;
}
