<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class DestinationDisplay extends Model
{
	protected $connection = 'pgsql1';
    protected $table = 'destination_display';
    public $incrementing = false;
}
