<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class LogImportNetexDetail extends Model
{
    protected $table = 'log_import_netex_detail';
    public $incrementing = false;
}
