<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class ImportNetex extends Model
{
    protected $table = 'import_netex';
}
