<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class ScheduledStopPoint extends Model
{
    protected $connection = 'pgsql1';
    protected $table = 'scheduled_stop_point';
    public $incrementing = false;
}
