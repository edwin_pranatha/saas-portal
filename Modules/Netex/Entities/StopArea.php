<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class StopArea extends Model
{
	protected $connection = 'pgsql1';
    protected $table = 'stop_area';
    public $incrementing = false;
}
