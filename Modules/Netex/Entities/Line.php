<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class Line extends Model
{
	protected $connection = 'pgsql1';
    protected $table = 'line';
    public $incrementing = false;
}
