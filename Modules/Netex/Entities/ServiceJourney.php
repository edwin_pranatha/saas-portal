<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class ServiceJourney extends Model
{
	protected $connection = 'pgsql1';
    protected $table = 'service_journey';
    public $incrementing = false;
}
