<?php

namespace Modules\Netex\Entities;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
	protected $connection = 'pgsql1';
    protected $table = 'route';
    public $incrementing = false;
}
