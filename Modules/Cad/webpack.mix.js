const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('./').mergeManifest();
// mix.js(__dirname + '/Resources/assets/js/app.js', 'js/cad.js')
//     .sass( __dirname + '/Resources/assets/sass/app.scss', 'css/cad.css');

mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/leaflet-0.7.3/dist/leaflet.js',
    __dirname + '/Resources/assets/js/leaflet.polydrag.js',
    __dirname + '/Resources/assets/js/leaflet.contextmenu.js',
    'node_modules/leaflet.marker.slideto-0.2.0/Leaflet.Marker.SlideTo.js',
    'node_modules/jsts-0.14.0/lib/javascript.util.js',
    'node_modules/jsts-0.14.0/lib/jsts.js',
    __dirname + '/Resources/assets/js/cad.js'
],  __dirname + '/Resources/assets/compiled/js/cad.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    'node_modules/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js',
    __dirname + '/Resources/assets/js/viewdisplay.js',
],  __dirname + '/Resources/assets/compiled/js/viewdisplay.min.js');


mix.scripts([
    'node_modules/magnific-popup-0.9.9/dist/jquery.magnific-popup.min.js',
    'node_modules/select2-3.5.1/select2.js',
    'node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-buttons-dt/js/buttons.dataTables.min.js',
    'node_modules/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js',
    __dirname + '/Resources/assets/js/announce_alert.js',
],  __dirname + '/Resources/assets/compiled/js/announce_alert.min.js');


mix.options({
    processCssUrls: false,
});

mix.sass( __dirname + '/Resources/assets/sass/cad.scss', __dirname + '/Resources/assets/compiled/css/cad.min.css');
mix.sass( __dirname + '/Resources/assets/sass/viewdisplay.scss', __dirname + '/Resources/assets/compiled/css/viewdisplay.min.css');
mix.sass( __dirname + '/Resources/assets/sass/announce_alert.scss', __dirname + '/Resources/assets/compiled/css/announce_alert.min.css');

if (process.env.NODE_ENV === 'production') {
mix.copy('node_modules/select2-3.5.1/select2.png',  __dirname + '/Resources/assets/compiled/css/select2.png');
mix.copy('node_modules/select2-3.5.1/select2-spinner.gif',  __dirname + '/Resources/assets/compiled/css/select2-spinner.gif');
mix.copy('node_modules/select2-3.5.1/select2x2.png',  __dirname + '/Resources/assets/compiled/css/select2x2.png');
mix.copyDirectory('node_modules/datatables.net-dt/images/',  __dirname + '/Resources/assets/compiled/images/');
}

if (mix.inProduction()) {
    mix.version();
}
