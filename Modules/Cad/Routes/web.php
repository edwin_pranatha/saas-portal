<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/admin/cad')->middleware(['auth'])->name('admin.cad.')->group(function() {
    Route::get('/', 'Backend\CadController@index')->name('index');
    Route::get('calls', 'Backend\CallsController@index')->name('calls.index');
    Route::get('calls/create', 'Backend\CallsController@create')->name('calls.create');
    Route::get('calls/archived', 'Backend\CallsController@archived')->name('calls.archived');
    Route::get('calls/archived/create', 'Backend\CallsController@archived_create')->name('calls.archived.create');

    Route::get('personnel', 'Backend\PersonnelController@index')->name('personnel.index');
    Route::get('personnel/create', 'Backend\PersonnelController@create')->name('personnel.create');
    Route::get('personnel/roles', 'Backend\PersonnelController@roles')->name('personnel.roles');
    Route::get('personnel/roles/create', 'Backend\PersonnelController@roles_create')->name('personnel.roles.create');
    Route::get('personnel/invite', 'Backend\PersonnelController@invite')->name('personnel.invite');

    Route::get('units', 'Backend\UnitsController@index')->name('units.index');
    Route::get('units/create', 'Backend\UnitsController@create')->name('units.create');
    Route::get('units/staffing', 'Backend\UnitsController@staffing')->name('units.staffing');

    

});


Route::prefix('/admin/gtfs')->middleware(['auth'])->name('admin.gtfs.')->group(function() {
    Route::get('vehicle-monitoring', 'Backend\CadController@index')->name('index');
    Route::get('view-display', 'Backend\MockupController@index')->name('mockup.screen.index');
    Route::get('announce/alert', 'Backend\AnnounceAlertController@index')->name('announce.alert.index');
    Route::get('announce/alert/create', 'Backend\AnnounceAlertController@create')->name('announce.alert.create');
    Route::get('announce/alert/list', 'Backend\AnnounceAlertController@list_alert')->name('announce.alert.list');
    Route::get('announce/alert/list/create', 'Backend\AnnounceAlertController@create_alert')->name('announce.alert.list.create');
});

Route::prefix('/admin/gtfs-canada')->middleware(['auth'])->name('admin.gtfs.canada.')->group(function() {
    Route::get('vehicle-monitoring', 'Backend\CadController@canada')->name('index');
    Route::get('view-display', 'Backend\MockupController@canada')->name('mockup.screen.index');
});