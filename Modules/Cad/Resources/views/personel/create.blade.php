@extends('theme::views.backend.app')

@section('page-title')
<h2>Add Person</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Add Person</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:datatable/buttons.dataTables.min.css') }}" >

<script src="{{ Module::asset('cad:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />

<style>
    #map { height: 300px; }
    #controlbox {
        position: absolute;
        left: 50px;
        top: 35px;
        width: 250px;
        max-width: 625pt;
        z-index: 999;
        box-shadow: 0 1.5pt 2pt rgba(0, 0, 0, 0.3);
        border-radius: 2pt;
    }
    .bus_list {
        cursor: pointer;
    }
    .panel-template {
        position: absolute;
        right: 15px;
    }
</style>
@endpush

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="panel-body">
                <form class="form-horizontal" method="get">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault" style="text-align: left;font-weight: bold;font-size: 17px;">Account Information</label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Username</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                            <p class="output">The User Name must be unique</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">New Password</label>
                        <div class="col-md-8">
                            <input type="password" class="form-control" id="inputDefault">
                            <p class="output">Passwords must be at least 6 characters long</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Confirm Password</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                            <p class="output">Confirm the users password (must match the one above)</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault" style="text-align: left;font-weight: bold;font-size: 17px;">User Detail</label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">ID Number</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">First Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Last Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Email Address</label>
                        <div class="col-md-8">
                            <input type="email" class="form-control" id="inputDefault">
                            <p class="output">Email Addresses must be unique</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault" style="text-align: left;font-weight: bold;font-size: 17px;">Group Details </label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Group</label>
                        <div class="col-md-8">
                             <select data-plugin-selectTwo class="form-control populate">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Is Group Admin?</label>
                        <div class="col-md-8">
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Roles</label>
                        <div class="col-md-8">
                             <select data-plugin-selectTwo class="form-control populate">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault" style="text-align: left;font-weight: bold;font-size: 17px;">Contact Details </label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Mobile Number</label>
                        <div class="col-md-8">
                             <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Mobile Carrier</label>
                        <div class="col-md-8">
                             <select data-plugin-selectTwo class="form-control populate">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Call Options</label>
                        <div class="col-md-8">
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;"> Email
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;"> Text/SMS
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;"> Push
                            <p class="output"><b>Note:</b> You may incur additional changes for SMS/Text depending on your mobile plan.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Message Options</label>
                        <div class="col-md-8">
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;"> Email
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;"> Text/SMS
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;"> Push
                            <p class="output"><b>Note:</b> You may incur additional changes for SMS/Text depending on your mobile plan.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Notification Options</label>
                        <div class="col-md-8">
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;"> Email
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;"> Text/SMS
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;"> Push
                            <p class="output"><b>Note:</b> You may incur additional changes for SMS/Text depending on your mobile plan.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Notify User?</label>
                        <div class="col-md-8">
                            <input id="for-project" value="project" type="checkbox" name="for[]" checked style="margin: 11px;">
                            <p class="output"><b>Note:</b> Uncheck if you don't want Resgrid to notify the user of this account creation.</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <a href="{{ route('personnel.index') }}" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary">Add User</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>

@push('data-table')
<script src="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/javascript.util.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/jsts.js') }}"></script>
<script src="{{ Module::asset('cad:js/personnel-add.js') }}"></script>
@endpush

@endsection