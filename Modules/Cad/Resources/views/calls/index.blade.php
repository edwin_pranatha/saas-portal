@extends('theme::views.backend.app')

@section('page-title')
<h2>Calls List</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Calls</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:datatable/buttons.dataTables.min.css') }}" >

<script src="{{ Module::asset('cad:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />

<style>
    #map { height: 300px; }
    #controlbox {
        position: absolute;
        left: 50px;
        top: 35px;
        width: 250px;
        max-width: 625pt;
        z-index: 999;
        box-shadow: 0 1.5pt 2pt rgba(0, 0, 0, 0.3);
        border-radius: 2pt;
    }
    .bus_list {
        cursor: pointer;
    }
    .panel-template {
        position: absolute;
        right: 15px;
    }
</style>
@endpush

<div class="row">
    <div class="col-md-12">
        <a href="{{ route('calls.create') }}" class="mb-xs mt-xs mr-xs btn btn-primary" style="float: right;">New Call</a>
        <a href="{{ route('calls.archived') }}" class="mb-xs mt-xs mr-xs btn btn-primary" style="float: right;">Archived Calls</a>
    </div>
</div>
<div class="row">
    <div class="col-md-7">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Unit Statuses</h2>
            </header>
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="dataTableUnitStatus">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Station</th>
                            <th>State</th>
                            <th>Timestamp</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="gradeX">
                            <td>Trident</td>
                            <td>.org</td>
                            <td>Station A</td>
                            <td class="center hidden-phone">4</td>
                            <td class="center hidden-phone">2022-05-10 20:30</td>
                        </tr>
                        <tr class="gradeX">
                            <td>Trident</td>
                            <td>.org</td>
                            <td>Station A</td>
                            <td class="center hidden-phone">4</td>
                            <td class="center hidden-phone">2022-05-10 20:30</td>
                        </tr>
                        <tr class="gradeX">
                            <td>Trident</td>
                            <td>.org</td>
                            <td>Station A</td>
                            <td class="center hidden-phone">4</td>
                            <td class="center hidden-phone">2022-05-10 20:30</td>
                        </tr>
                        <tr class="gradeX">
                            <td>Trident</td>
                            <td>.org</td>
                            <td>Station A</td>
                            <td class="center hidden-phone">4</td>
                            <td class="center hidden-phone">2022-05-10 20:30</td>
                        </tr>
                        <tr class="gradeX">
                            <td>Trident</td>
                            <td>.org</td>
                            <td>Station A</td>
                            <td class="center hidden-phone">4</td>
                            <td class="center hidden-phone">2022-05-10 20:30</td>
                        </tr>
                        <tr class="gradeX">
                            <td>Trident</td>
                            <td>.org</td>
                            <td>Station A</td>
                            <td class="center hidden-phone">4</td>
                            <td class="center hidden-phone">2022-05-10 20:30</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    <div class="col-md-5">
        <section class="panel">
            <div class="panel-body">
                <div id="map" class="leaflet-container leaflet-touch leaflet-fade-anim leaflet-touch-zoom leaflet-grab leaflet-touch-drag" style="position: relative;" tabindex="0"></div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-template" style="top: 7px;">
                    <a href="{{ route('calls.create') }}" class="mb-xs mt-xs mr-xs btn btn-primary">New Call</a>
                </div>
                <h2 class="panel-title">Active Calls</h2>
            </header>
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="dataTableActiveCalls">
                    <thead>
                        <tr>
                            <th>Number</th>
                            <th>Name</th>
                            <th>Timestamp</th>
                            <th>Priority</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="gradeX">
                            <td>1</td>
                            <td>Project 1</td>
                            <td>2022-05-10 20:30</td>
                            <td class="center hidden-phone">1</td>
                            <td class="center hidden-phone"><a href="#" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</a></td>
                        </tr>
                        <tr class="gradeX">
                            <td>1</td>
                            <td>Project 1</td>
                            <td>2022-05-10 20:30</td>
                            <td class="center hidden-phone">1</td>
                            <td class="center hidden-phone"><a href="#" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</a></td>
                        </tr>
                        <tr class="gradeX">
                            <td>1</td>
                            <td>Project 1</td>
                            <td>2022-05-10 20:30</td>
                            <td class="center hidden-phone">1</td>
                            <td class="center hidden-phone"><a href="#" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</a></td>
                        </tr>
                        <tr class="gradeX">
                            <td>1</td>
                            <td>Project 1</td>
                            <td>2022-05-10 20:30</td>
                            <td class="center hidden-phone">1</td>
                            <td class="center hidden-phone"><a href="#" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</a></td>
                        </tr>
                        <tr class="gradeX">
                            <td>1</td>
                            <td>Project 1</td>
                            <td>2022-05-10 20:30</td>
                            <td class="center hidden-phone">1</td>
                            <td class="center hidden-phone"><a href="#" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</a></td>
                        </tr>
                        <tr class="gradeX">
                            <td>1</td>
                            <td>Project 1</td>
                            <td>2022-05-10 20:30</td>
                            <td class="center hidden-phone">1</td>
                            <td class="center hidden-phone"><a href="#" class="mb-xs mt-xs mr-xs btn btn-primary">Edit</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
@push('data-table')
<script src="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/javascript.util.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/jsts.js') }}"></script>
<script src="{{ Module::asset('cad:js/calls.js') }}"></script>
@endpush

@endsection