@extends('theme::views.backend.app')

@section('page-title')
<h2>New Call</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Calls</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:datatable/buttons.dataTables.min.css') }}" >

<script src="{{ Module::asset('cad:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />

<style>
    #map { height: 300px; }
    #controlbox {
        position: absolute;
        left: 50px;
        top: 35px;
        width: 250px;
        max-width: 625pt;
        z-index: 999;
        box-shadow: 0 1.5pt 2pt rgba(0, 0, 0, 0.3);
        border-radius: 2pt;
    }
    .bus_list {
        cursor: pointer;
    }
    .panel-template {
        position: absolute;
        right: 15px;
    }
</style>
@endpush

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-template" style="top: 7px;">
                    <a href="#modalTemplate" class="mb-xs mt-xs mr-xs modal-sizes btn btn-primary">Template</a>
                </div>
                <h2 class="panel-title">New Call</h2>
            </header>
            <div class="panel-body">
                <form class="form-horizontal" method="get">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Priority</label>
                        <div class="col-md-8">
                            <select class="form-control mb-md">
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Type</label>
                        <div class="col-md-8">
                            <select class="form-control mb-md">
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Reporter Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Reporter Contact Info</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Call ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Incident ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Reference ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Nature of the Call</label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="3" id="textareaDefault"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Notes</label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="3" id="textareaDefault"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Map Page</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Location</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group mb-md">
                                        <input type="text" class="form-control">
                                        <div class="input-group-btn">
                                            <button tabindex="-1" class="btn btn-primary" type="button" style="width: 120px;">Find Address</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-md">
                                        <input type="text" class="form-control">
                                        <div class="input-group-btn">
                                            <button tabindex="-1" class="btn btn-primary" type="button" style="width: 150px;">Find w3w Location</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="map" class="leaflet-container leaflet-touch leaflet-fade-anim leaflet-touch-zoom leaflet-grab leaflet-touch-drag" style="position: relative;" tabindex="0"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Protocols Personnel</label>
                        <div class="col-md-8">
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab1" data-toggle="tab">Personnel</a>
                                    </li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab">Groups</a>
                                    </li>
                                    <li>
                                        <a href="#tab3" data-toggle="tab">Roles</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active">
                                        <table class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr>
                                                    <th><input id="for-project" value="project" type="checkbox" name="for[]" required=""></th>
                                                    <th>Name</th>
                                                    <th>Eta</th>
                                                    <th>Status</th>
                                                    <th>Staffing</th>
                                                    <th>Group</th>
                                                    <th>Roles</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gradeX">
                                                    <td><input id="for-project" value="project" type="checkbox" name="for[]" required=""></td>
                                                    <td>Internet
                                                        Explorer 4.0
                                                    </td>
                                                    <td>Win 95+</td>
                                                    <td class="center hidden-phone">4</td>
                                                    <td class="center hidden-phone">X</td>
                                                    <td class="center hidden-phone">X</td>
                                                    <td class="center hidden-phone">X</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="tab2" class="tab-pane">
                                        <table class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr>
                                                    <th><input id="for-project" value="project" type="checkbox" name="for[]" required=""></th>
                                                    <th>Name</th>
                                                    <th>Personnel Count</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gradeX">
                                                    <td><input id="for-project" value="project" type="checkbox" name="for[]" required=""></td>
                                                    <td>Trident</td>
                                                    <td>Internet
                                                        Explorer 4.0
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="tab3" class="tab-pane">
                                        <table class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr>
                                                    <th><input id="for-project" value="project" type="checkbox" name="for[]" required=""></th>
                                                    <th>Name</th>
                                                    <th>Personnel Count</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gradeX">
                                                    <td><input id="for-project" value="project" type="checkbox" name="for[]" required=""></td>
                                                    <td>Trident</td>
                                                    <td>Internet
                                                        Explorer 4.0
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <a href="{{ route('calls.index') }}" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary">Create and Dispatch Call</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <div id="modalTemplate" class="modal-block modal-block-lg mfp-hide">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Select Call Template</h2>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" id="formRoutesData" method="get">
                        <div id="idRoutes"></div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="control-label" for="inputDefault">Call Template</label>
                                <select data-plugin-selectTwo class="form-control populate">
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-default modal-dismiss">Close</button>
                            <button type="button" class="btn btn-primary" id="saveRoutesData" data-url="">Set Template</button>
                        </div>
                    </div>
                </footer>
            </section>
        </div>
    </div>
</div>

@push('data-table')
<script src="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/javascript.util.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/jsts.js') }}"></script>
<script src="{{ Module::asset('cad:js/calls-add.js') }}"></script>
@endpush

@endsection