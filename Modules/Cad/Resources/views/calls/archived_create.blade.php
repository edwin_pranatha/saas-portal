@extends('theme::views.backend.app')

@section('page-title')
<h2>Archived Call</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Archived Call</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('campaigns:css/bootstrap-datetimepicker.css') }}" media="screen">
<link rel="stylesheet" href="{{ Module::asset('campaigns:assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:datatable/buttons.dataTables.min.css') }}" >

<script src="{{ Module::asset('cad:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />

<style>
    #map { height: 300px; }
    #controlbox {
        position: absolute;
        left: 50px;
        top: 35px;
        width: 250px;
        max-width: 625pt;
        z-index: 999;
        box-shadow: 0 1.5pt 2pt rgba(0, 0, 0, 0.3);
        border-radius: 2pt;
    }
    .bus_list {
        cursor: pointer;
    }
    .panel-template {
        position: absolute;
        right: 15px;
    }
    .switch {
        display: table-cell !important;
    }
</style>
@endpush

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Add Archived Call</h2>
            </header>
            <div class="panel-body">
                <form class="form-horizontal" method="get">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Timestamp</label>
                        <div class="col-md-8">
                            <div class="control-group">
                                <div class="controls input-append date form_datetime" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                    <input size="16" type="text" class="form-control input-sm" name="startdate_project" id="startdate_project" value="" style="width: 96%;display: inline;" readonly>
                                    <span class="add-on"><i class="icon-remove fa fa-trash"></i></span>
                                    <span class="add-on"><i class="icon-th fa fa-th"></i></span>
                                </div>
                                <input type="hidden" id="dtp_input1" class="startdate_project" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Priority</label>
                        <div class="col-md-8">
                            <select class="form-control mb-md">
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Type</label>
                        <div class="col-md-8">
                            <select class="form-control mb-md">
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Reporter Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Reporter Contact Info</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Call Identifier</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Reference ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Nature of the Call</label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="3" id="textareaDefault"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Notes</label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="3" id="textareaDefault"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Map Page</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="inputDefault">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Location</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group mb-md">
                                        <input type="text" class="form-control">
                                        <div class="input-group-btn">
                                            <button tabindex="-1" class="btn btn-primary" type="button" style="width: 120px;">Find Address</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-md">
                                        <input type="text" class="form-control">
                                        <div class="input-group-btn">
                                            <button tabindex="-1" class="btn btn-primary" type="button" style="width: 150px;">Find w3w Location</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="map" class="leaflet-container leaflet-touch leaflet-fade-anim leaflet-touch-zoom leaflet-grab leaflet-touch-drag" style="position: relative;" tabindex="0"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Dispatch</label>
                        <div class="col-md-8">
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab1" data-toggle="tab">Personnel</a>
                                    </li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab">Groups</a>
                                    </li>
                                    <li>
                                        <a href="#tab3" data-toggle="tab">Units</a>
                                    </li>
                                    <li>
                                        <a href="#tab4" data-toggle="tab">Roles</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active">
                                        <table class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr>
                                                    <th><input id="for-project" value="project" type="checkbox" name="for[]" required=""></th>
                                                    <th>Name</th>
                                                    <th>Eta</th>
                                                    <th>Status</th>
                                                    <th>Staffing</th>
                                                    <th>Group</th>
                                                    <th>Roles</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gradeX">
                                                    <td><input id="for-project" value="project" type="checkbox" name="for[]" required=""></td>
                                                    <td>Internet
                                                        Explorer 4.0
                                                    </td>
                                                    <td>Win 95+</td>
                                                    <td class="center hidden-phone">4</td>
                                                    <td class="center hidden-phone">X</td>
                                                    <td class="center hidden-phone">X</td>
                                                    <td class="center hidden-phone">X</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="tab2" class="tab-pane">
                                        <table class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr>
                                                    <th><input id="for-project" value="project" type="checkbox" name="for[]" required=""></th>
                                                    <th>Name</th>
                                                    <th>Personnel Count</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gradeX">
                                                    <td><input id="for-project" value="project" type="checkbox" name="for[]" required=""></td>
                                                    <td>Trident</td>
                                                    <td>Internet
                                                        Explorer 4.0
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="tab3" class="tab-pane">
                                        <table class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr>
                                                    <th><input id="for-project" value="project" type="checkbox" name="for[]" required=""></th>
                                                    <th>Name</th>
                                                    <th>Eta</th>
                                                    <th>Type</th>
                                                    <th>Type</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gradeX">
                                                    <td><input id="for-project" value="project" type="checkbox" name="for[]" required=""></td>
                                                    <td>Trident</td>
                                                    <td>Internet
                                                        Explorer 4.0
                                                    </td>
                                                    <td>Trident</td>
                                                    <td>Trident</td>
                                                    <td>Trident</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="tab4" class="tab-pane">
                                        <table class="table table-bordered table-striped mb-none">
                                            <thead>
                                                <tr>
                                                    <th><input id="for-project" value="project" type="checkbox" name="for[]" required=""></th>
                                                    <th>Name</th>
                                                    <th>Personnel Count</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="gradeX">
                                                    <td><input id="for-project" value="project" type="checkbox" name="for[]" required=""></td>
                                                    <td>Trident</td>
                                                    <td>Internet
                                                        Explorer 4.0
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Regenerate Call Numbers</label>
                        <div class="col-md-8">
                            <input id="for-project" value="project" type="checkbox" name="for[]" style="margin: 11px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <a href="{{ route('calls.archived') }}" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary">Add Archived Call</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>

@push('data-table')
<script src="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/javascript.util.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/jsts.js') }}"></script>
<script src="{{ Module::asset('campaigns:js/bootstrap-datetimepicker.js') }}" charset="UTF-8"></script>
<script src="{{ Module::asset('campaigns:js/locales/bootstrap-datetimepicker.fr.js') }}" charset="UTF-8"></script>
<script src="{{ Module::asset('cad:js/archived-add.js') }}"></script>
@endpush

@endsection