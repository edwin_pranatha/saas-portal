@extends('theme::views.backend.app')

@section('page-title')
<h2>Vehicle Monitoring</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Vehicle Monitoring</span></li>
</ol>
@endsection

@section('module_css')
<link rel="stylesheet" href="/assets/Cad/css/cad.min.css" media="screen" />
@endsection

@section('content')

<div class="row">
    <input type="hidden" value="{{ $vehicleid }}" id="vehicleIDBus">
    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div id="controlboxRight" style="display: none;">
                    <section class="panel">
                        <header class="panel-heading header-panel">
                            <div class="panel-actions group-panel">
                                <a href="#" class="fa fa-caret-down"></a>
                                <i class="fa fa-times closeControlDetour"></i>
                            </div>
                            <h2 class="panel-title titleGroup-panel">Information</h2>
                        </header>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-accordion">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse"
                                                data-parent="#collapse1One" href="#collapse1One">
                                                VIA <span id="idbus-popup"></span> <span id="title-popup">
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse1One" class="accordion-body collapse in">
                                        <div class="panel-body">
                                            <div class="overTableView">
                                                <div class="overTableViewInfo"></div>
                                                <table class="vehicle-stops">
                                                    <thead>
                                                        <tr>
                                                            <td class="vehicle-stop" style="width: 234px;"><b>Stop</b>
                                                            </td>
                                                            <td><b>Estimated Arrival Time</b></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-accordion">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse"
                                                data-parent="#collapse1Two" href="#collapse1Two">
                                                Detour Alert
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse1Two" class="accordion-body collapse in">
                                        <div class="panel-body">
                                            <div class="overTableViewInfoDetour"></div>
                                            <div class="panel-group vehicle-detour" id="accordion">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div id="map"
                    class="leaflet-container leaflet-touch leaflet-fade-anim leaflet-touch-zoom leaflet-grab leaflet-touch-drag"
                    style="position: relative;" tabindex="0"></div>
                <div id="controlbox">
                    <section class="panel">
                        <header class="panel-heading header-panel">
                            <div class="panel-actions group-panel">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>
                            <h2 class="panel-title titleGroup-panel">Vehicle</h2>
                        </header>
                        <div class="panel-body" style="display: block;">
                            <input class="form-control" id="myInput" type="text" placeholder="Search..">
                            <div class="scrollBarMap">
                                <table id="vehicle-list" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Bus ID</th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable"></tbody>
                                </table>
                            </div>
                            <div id="online_bus" class="fLeft">Total Online: 0</div>
                            <div><button type="button"
                                    class="mb-xs mt-xs mr-xs btn btn-sm btn-primary showAllBus fRight"
                                    style="display: none;">Show All</button></div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Cad/js/cad.min.js"></script>
@endsection