@extends('theme::views.backend.app')

@section('page-title')
<h2>Vehicle Monitoring</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Vehicle Monitoring</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Cad/css/cad.min.css" media="screen" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<script src="{{ Module::asset('cad:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<script src='https://unpkg.com/leaflet.marker.slideto@0.2.0/Leaflet.Marker.SlideTo.js'></script>
<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="{{ Module::asset('cad:css/cad.css') }}" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pako/1.0.11/pako_inflate.min.js"></script>
<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/fullscreen/leaflet.fullscreen.css') }}" />
@endpush --}}

<div class="row">
    <input type="hidden" value="{{ $vehicleid }}" id="vehicleIDBus">
    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div id="controlbox">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>
                            <h2 class="panel-title">Vehicle</h2>
                        </header>
                        <div class="panel-body" style="display: block;">
                            <input class="form-control" id="myInput" type="text" placeholder="Search..">
                            <div class="scrollBarMap">
                                <table id="vehicle-list" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Bus ID</th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable"></tbody>
                                </table>
                            </div>
                            <div id="online_bus" class="fLeft">Total Online: 0</div>
                            <div><button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary showAllBus fRight" style="display: none;">Show All</button></div>
                        </div>
                    </section>
                </div>
                <div id="controlboxRight" style="display: none;">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                                <i class="fa fa-times closeControlDetour"></i>
                            </div>
                            <h2 class="panel-title">Information</h2>
                        </header>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-accordion">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse1One" href="#collapse1One">
                                                <span id="idbus-popup"></span> <span id="title-popup">
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse1One" class="accordion-body collapse in">
                                        <div class="panel-body">
                                            <div class="overTableView">
                                                <div class="overTableViewInfo"></div>
                                                <table class="vehicle-stops">
                                                    <thead>
                                                        <tr>
                                                            <td class="vehicle-stop" style="width: 234px;"><b>Stop</b></td>
                                                            <td><b>Estimated Arrival Time</b></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-accordion">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse1Two" href="#collapse1Two">
                                               Detour Alert
                                           </a>
                                       </h4>
                                    </div>
                                    <div id="collapse1Two" class="accordion-body collapse in">
                                        <div class="panel-body">
                                            <div class="overTableViewInfoDetour"></div>
                                            <div class="panel-group vehicle-detour" id="accordion">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>        
                <div id="map" class="leaflet-container leaflet-touch leaflet-fade-anim leaflet-touch-zoom leaflet-grab leaflet-touch-drag" style="position: relative;" tabindex="0"></div>
            </div>
        </div>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
@endpush
@push('data-scripts')
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/javascript.util.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/jsts.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/fullscreen/Leaflet.fullscreen.min.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:js/cad.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush --}}

@endsection
@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Cad/js/canada.js"></script>
<script src="/assets/Cad/js/cad.min.js"></script>
@endsection