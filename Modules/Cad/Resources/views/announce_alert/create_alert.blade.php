@extends('theme::views.backend.app')

@section('page-title')
<h2>Announce Alert</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Announce Alert</span></li>
</ol>
@endsection


@section('module_css')
    <link rel="stylesheet" href="/assets/Cad/css/announce_alert.min.css" media="screen" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('cad:css/bootstrap-datetimepicker.css') }}" media="screen">
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:datatable/buttons.dataTables.min.css') }}" >
<link rel="stylesheet" href="{{ Module::asset('cad:css/announcealert.css') }}" />
@endpush --}}

<div class="row">
    <div class="col-md-12 col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Create Announce Alert</h2>
            </header>
            <div class="panel-body">
                <form class="form-horizontal" method="get">
                    <div class="col-md-10">
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea class="form-control" rows="6" id="textareaDefault" placeholder="Enter text here..."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <select class="form-control mb-md">
                                <option>Select Language</option>
                                <option>Option 1</option>
                                <option>Option 2</option>
                                <option>Option 3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control mb-md">
                                <option>Select Voice</option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 20px;">
                        <div class="form-group">
                            <div class="col-md-12">
                                <a href="{{ route('admin.gtfs.announce.alert.list') }}" class="btn btn-default">Cancel</a>
                                <button class="btn btn-primary">Listen</button>
                                <button class="btn btn-primary">Create Audio</button>
                                <button class="btn btn-primary">Create Text</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('cad:js/bootstrap-datetimepicker.js') }}" charset="UTF-8"></script>
<script src="{{ Module::asset('cad:js/locales/bootstrap-datetimepicker.fr.js') }}" charset="UTF-8"></script>
<script type="text/javascript" src="{{ Module::asset('cad:js/announcealert.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush --}}

@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Cad/js/announce_alert.min.js"></script>
@endsection
