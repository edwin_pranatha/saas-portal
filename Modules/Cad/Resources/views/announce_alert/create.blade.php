@extends('theme::views.backend.app')

@section('page-title')
<h2>Announce Alert</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Announce Alert</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Cad/css/announce_alert.min.css" media="screen" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('cad:css/bootstrap-datetimepicker.css') }}" media="screen">
<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:datatable/buttons.dataTables.min.css') }}" >
<link rel="stylesheet" href="{{ Module::asset('cad:css/announcealert.css') }}" />
@endpush --}}

<div class="row">
    <div class="col-md-12 col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Create Announce Alert</h2>
            </header>
            <div class="panel-body">
                <form class="form-horizontal" method="get">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">File Mp3 or Text</label>
                        <div class="col-md-8">
                            <select class="form-control mb-md">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Destination</label>
                        <div class="col-md-8">
                            <label class="checkbox-inline">
                                <input type="radio" name="optionsDestination" id="optionsRadios1" value="1"> By Bus
                            </label>
                            <label class="checkbox-inline">
                                <input type="radio" name="optionsDestination" id="optionsRadios1" value="2"> By Route
                            </label>
                        </div>
                    </div>
                    <div class="form-group" id="byBus" style="display: none;">
                        <label class="col-md-2 control-label" for="inputDefault">Bus</label>
                        <div class="col-md-8">
                            <select multiple data-plugin-selectTwo class="form-control populate">
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="IN">Indiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="OH">Ohio</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WV">West Virginia</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="byRoute" style="display: none;">
                        <label class="col-md-2 control-label" for="inputDefault">Route</label>
                        <div class="col-md-8">
                            <select multiple data-plugin-selectTwo class="form-control populate">
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="IN">Indiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="OH">Ohio</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WV">West Virginia</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Priority</label>
                        <div class="col-md-8">
                            <select class="form-control mb-md">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="inputDefault">Validation</label>
                        <div class="col-md-4">
                            <div class="control-group">
                                <div class="controls input-append date form_datetime" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                    <input size="16" type="text" class="form-control input-sm" name="startdate_project" id="startdate_project" value="" style="width: 90%;display: inline;" readonly>
                                    <span class="add-on"><i class="icon-remove fa fa-trash"></i></span>
                                    <span class="add-on"><i class="icon-th fa fa-th"></i></span>
                                </div>
                                <input type="hidden" id="dtp_input1" class="startdate_project" value="" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="control-group">
                                <div class="controls input-append date form_datetime" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                    <input size="16" type="text" class="form-control input-sm" name="startdate_project" id="startdate_project" value="" style="width: 90%;display: inline;" readonly>
                                    <span class="add-on"><i class="icon-remove fa fa-trash"></i></span>
                                    <span class="add-on"><i class="icon-th fa fa-th"></i></span>
                                </div>
                                <input type="hidden" id="dtp_input1" class="startdate_project" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <a href="{{ route('admin.gtfs.announce.alert.index') }}" class="btn btn-default">Cancel</a>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script src="{{ Module::asset('cad:js/bootstrap-datetimepicker.js') }}" charset="UTF-8"></script>
<script src="{{ Module::asset('cad:js/locales/bootstrap-datetimepicker.fr.js') }}" charset="UTF-8"></script>
<script src="{{ Module::asset('cad:js/announcealert.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush --}}

@endsection

@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Cad/js/announce_alert.min.js"></script>
@endsection
