@extends('theme::views.backend.app')

@section('page-title')
<h2>Units List</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Units</span></li>
</ol>
@endsection

@section('content')

@push('data-stylesheets')
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:datatable/buttons.dataTables.min.css') }}" >

<script src="{{ Module::asset('cad:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />

<style>
    #map { height: 300px; }
    #controlbox {
        position: absolute;
        left: 50px;
        top: 35px;
        width: 250px;
        max-width: 625pt;
        z-index: 999;
        box-shadow: 0 1.5pt 2pt rgba(0, 0, 0, 0.3);
        border-radius: 2pt;
    }
    .bus_list {
        cursor: pointer;
    }
    .panel-template {
        position: absolute;
        right: 15px;
    }
</style>
@endpush

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-template" style="top: 7px;">
                    <a href="{{ route('units.create') }}" class="mb-xs mt-xs mr-xs btn btn-success">New Unit</a>
                    <a href="{{ route('units.staffing') }}" class="mb-xs mt-xs mr-xs btn btn-info">Unit Staffing</a>
                </div>
                <h2 class="panel-title">Units</h2>
            </header>
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="dataTableArchived">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Timestamp</th>
                            <th>Set Status</th>
                            <th width="400"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="gradeX">
                            <td><input id="for-project" value="project" type="checkbox" name="for[]" required=""></td>
                            <td>Trident</td>
                            <td>.org</td>
                            <td>Enable</td>
                            <td class="center hidden-phone">2022-10-02 10:30</td>
                            <td class="center hidden-phone">
                                <select class="form-control mb-md">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </td>
                            <td class="center hidden-phone">
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary">View Events</button>
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-info">Logs</button>
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-warning">Edit</button>
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-danger">Delete</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>

@push('data-table')
<script src="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush
@push('data-scripts')
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/javascript.util.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/jsts.js') }}"></script>
<script src="{{ Module::asset('cad:js/units.js') }}"></script>
@endpush

@endsection