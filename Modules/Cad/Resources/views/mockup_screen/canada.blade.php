@extends('theme::views.backend.app')

@section('page-title')
<h2>Mockup Screen</h2>
@endsection

@section('breadcrumb')
<ol class="breadcrumbs">
    <li>
        <a href="index.html">
            <i class="fa fa-home"></i>
        </a>
    </li>
    <li><span>Mockup Screen Canada</span></li>
</ol>
@endsection

@section('module_css')
    <link rel="stylesheet" href="/assets/Cad/css/viewdisplay.min.css" media="screen" />
@endsection

@section('content')

{{-- @push('data-stylesheets')
<script src="{{ Module::asset('cad:leaflet/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.polydrag/leaflet.polydrag.js') }}"></script>
<script src="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.js') }}"></script>

<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet/dist/leaflet.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/magnific-popup/magnific-popup.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('gtfs:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
<link rel="stylesheet" href="{{ Module::asset('cad:datatable/buttons.dataTables.min.css') }}" >
<script src='https://unpkg.com/leaflet.marker.slideto@0.2.0/Leaflet.Marker.SlideTo.js'></script>
<link rel="stylesheet" href="{{ Module::asset('cad:leaflet/bower_components/leaflet.contextmenu/dist/leaflet.contextmenu.css') }}" />

<link rel="stylesheet" href="{{ Module::asset('cad:css/cad.css') }}" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pako/1.0.11/pako_inflate.min.js"></script>
@endpush --}}

<div class="row">
    <div class="col-md-12 col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Mockup Screen</h2>
            </header>
            <div class="panel-body">
                <div>
                    <span style="float: left;width: 50%;font-size: 20px;font-weight: 600;">Online : <span id="online_bus">0</span></span><span style="float: right;width: 50%;text-align: right;font-size: 20px;font-weight: 600;">Driving : <span id="trip_bus">0</span></span>
                </div>
                <div>
                    <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="refreshMockup">Refresh</button>
                </div>
                <table class="table table-bordered table-striped mb-none" id="dataTableMockupScreen">
                    <thead>
                        <tr>
                            <th>Bus ID</th>
                            <th>Trip</th>
                            <th>Longitude & Lattitude</th>
                            <th>Timestamp</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </section>
        <div id="modal-screen" class="modal-block modal-block-lg mfp-hide" style="max-width: 1400px;">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Vehicle Monitor</h2>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xl-12">
                                    <section class="panel">
                                        <div class="panel-body" style="padding-bottom: 325px;">
                                            <div class="container-fluid" style="">
                                                <div id="vehicle-no"></div>
                                                <div class="container" style="width: 1280px;position: absolute;padding: 0px 20px;border: 30px black;border-style: solid;border-radius: 30px;background-color: white;">
                                                    <div class="row" style="font-size: 10px;font-weight: bold;">
                                                        
                                                        <div class="col-md-8" style="height:280px;">
                                                            
                                                            <div class="row">
                                                                <div class="col-md-1 circle" id="journey_destination_time" style="font-size: 35px;font-weight: bold;text-align:center;padding-top: 20px;">
                                                                </div>
                                                                <div class="col-md-4" id="journey_destination" style="font-size: 20px;margin-top: 6px;height: 60px;">
                                                                </div>
                                                                <div class="col-md-7" style="">
                                                                    <div style="margin-top:10px;">
                                                                        <img src="/assets/Cad/assets/images/wifi.png" alt="" style="width:60px;margin-right:10px;">
                                                                        <img src="/assets/Cad/assets/images/maestronic.png" alt="" style="width:240px;">
                                                                        <img src="/assets/Cad/assets/images/cctv.png" alt="" style="width:55px;margin-left:10px;">
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
            
            
                                                            <div class="row" style="font-size: 11px;font-weight: bold;text-align:center;">
                                                                <div class="col-md-1 timelinelast" style="">
                                                                    <div class="finaldestination"></div>
                                                                </div>
                                                                <div class="col-md-3" style="text-align:left;">
                                                                  NEXT STOPS
                                                                </div>
                                                                <div class="col-md-2" style="">
                                                                  ETA (Min:Sec)
                                                                </div>
                                                                <div class="col-md-3" style="">
                                                                 SPECIAL SERVICES
                                                                </div>
                                                                <div class="col-md-3" style="font-size: 10px;">
                                                                 CONNECTING ROUTE AT STOP
                                                                </div>
                                                              </div>
            
            
                                                              <div id="journey-content"></div>
            
                                                                <!-- Loop function here -->
                                                                <!-- <div class="row" style="font-size: 20px;text-align:center;background-color:a6a6a6;color:white;">
                                                                    <div class="col-md-1 timeline" style=""></div>
                                                                    <div class="col-md-3" style="text-align:left;">
                                                                    Pine
                                                                    </div>
                                                                    <div class="col-md-2" style="">
                                                                    10
                                                                    </div>
                                                                    <div class="col-md-3" style="">
                                                                        <i style="background-color: #155196;padding:2;width: 25px;margin-top: 3;" class="fas fa-wheelchair fa-fw fa-xl"></i>
                                                                    </div>
                                                                    <div class="col-md-3" style="">
                                                                    1A/BK
                                                                    </div>
                                                                </div> -->
            
                                                                <div id="journey-departure"></div>
                                                                
                                                                
            
                                                        </div>
                                                        <div class="col-md-4" style="height:280px;">
                                                            <div class="row">
                                                                <div id="current_time" class="col-md-12" style="font-size: 44px;font-weight: bold;text-align: right;height: 60px;padding-top: 31px;">
                                                                </div>
                                                            </div>
                                                            <div class="row" style="font-size: 11px;font-weight: bold;">
                                                                <div class="col-md-12" style="text-align:center;">
                                                                 POINT OF INTEREST                                            
                                                                </div>
                                                              </div>
                                                              <div class="row" style="height: 180px;">
                                                                <div id="my-pics" class="carousel slide" data-ride="carousel" style="width:300px;margin:auto;">

                                                                    <!-- Indicators -->
                                                                    <ol class="carousel-indicators">
                                                                    <li data-target="#my-pics" data-slide-to="0" class="active"></li>
                                                                    <li data-target="#my-pics" data-slide-to="1"></li>
                                                                    <li data-target="#my-pics" data-slide-to="2"></li>
                                                                    </ol>
                                                                    
                                                                    <!-- Content -->
                                                                    <div class="carousel-inner" role="listbox">
                                                                    
                                                                    <!-- Slide 1 -->
                                                                    <div class="item active">
                                                                    <img src="/assets/Cad/assets/images/slides/slide1.png" alt="">
                                                                    </div>
                                                                    
                                                                    <!-- Slide 2 -->
                                                                    <div class="item">
                                                                    <img src="/assets/Cad/assets/images/slides/slide2.png" alt="">
                                                                    </div>
                                                                    
                                                                    <!-- Slide 3 -->
                                                                    <div class="item">
                                                                    <img src="/assets/Cad/assets/images/slides/slide3.png" alt="">
                                                                   
                                                                    </div>
                                                                    
                                                                    </div>
                                                              </div>
                                                        </div>
                                                      </div>
            
                                                </div>          
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="text-right col-md-12">
                            <button type="button" class="btn btn-default modal-dismiss">Close</button>
                        </div>
                    </div>
                </footer>
            </section>
        </div>
    </div>
</div>
{{-- @push('data-table')
<script src="{{ Module::asset('cad:assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/select2/select2.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/buttons.html5.min.js') }}"></script>
<script src="{{ Module::asset('cad:datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ Module::asset('cad:assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
@endpush
@push('data-scripts')
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/javascript.util.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:leaflet/bower_components/jsts/lib/jsts.js') }}"></script>
<script type="text/javascript" src="{{ Module::asset('cad:js/mockup.js') }}"></script>
@endpush
@push('core-scripts')
<script src="{{ $urltheme }}/backend/js/core.js"></script>
@endpush --}}

@endsection


@section('module_javascript')
<script src="/assets/Core/js/core.min.js"></script>
<script src="/assets/Cad/js/canada.js"></script>
<script src="/assets/Cad/js/viewdisplay.min.js"></script>
@endsection
