        $(document).ready(function(){     
            function showDataDetourAlert(){
                var detour = [];
                $.ajax({
                    type: "GET",
                    url: ipaddressCadStatic + '/api/gtfs/service-alert',
                    cache: false,
                    success: function(result){
                        var data = result['data'];
                        for (var i = 0; i < data.length; i++) {
                            if(detour.includes(data[i]['headerText'])===false){
                                detour.push(data[i]['headerText']);
                                var html  = '<tr>';
                                    html += '<td>' + data[i]['routeId'] + '</td>';
                                    html += '<td>' + data[i]['effect'] + '</td>';
                                    html += '<td>' + data[i]['start'].replace('T', ' ').replace('Z', '') + '</td>';
                                    html += '<td>' + data[i]['end'].replace('T', ' ').replace('Z', '') + '</td>';
                                    html += '<td>' + data[i]['headerText'] + '</td>';
                                    html += '<td>' + data[i]['cause'] + '</td>';
                                    html += '<td>' + data[i]['descriptionText'] + '</td>';
                                    html += '</tr>';
                                $('#dataTableDetourAlert tbody').append(html);
                            }
                        }
                        $('#dataTableDetourAlert').dataTable();
                    }
                });
            }

            showDataDetourAlert();
        });