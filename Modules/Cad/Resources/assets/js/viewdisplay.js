$(document).ready(function () {
    var online_bus = [];
    var trip_bus = [];
    var tableMockupScreen = '';
    var interval_time;
    var interval_journey;

    $('.modal-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $('.modal-sizes').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
        modal: true
    });

    $(document).on('click', '.modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    $('select.populate').select2();

    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function (item) {
                return item.el.attr('title') + ' &middot; <a class="image-source-link" href="' + item.el.attr('data-source') + '" target="_blank">image source</a>';
            }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            opener: function (element) {
                return element.find('img');
            }
        }
    });

    var vehicle_id;

    function convertTZ(date, tzString) {
        return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {
            timeZone: tzString
        }));
    }

    function time_format(dateStr, isheader) {
        const date = new Date(dateStr);
        var server_date = new Date(date.toISOString().slice(0, -1));
        var now_date = convertTZ(new Date(), gtfs_timezone);
        var differenceTravel = server_date.getTime() - now_date.getTime();
        var seconds = Math.floor((differenceTravel) / (1000));
        var second = Math.floor(seconds / 60);
        if (second < 0) {
            return "PASSED";
        }
        if (isheader == true) {
            return second;
        }
        return new Date(seconds * 1000).toISOString().substr(14, 5);
    }

    function date_AMPM_AMERICA() {
        var new_date = convertTZ(new Date(), gtfs_timezone);
        var strTime = new_date.toLocaleString([], {
            hour: '2-digit',
            minute: '2-digit'
        });
        return strTime;
    }


    function ajax_screen(vehicle_id) {
        $.ajax({
            url: ipaddressGtfs_static + '/api/gtfs/vehiclemonitoring?agency_id='+gtfs_agency+'&format=json&vehicle_id=' + vehicle_id,
            crossDomain: true,
            success: function (data) {
                if (typeof data.data.Gtfs == "undefined") {
                    console.log("No data 1");
                    $("#journey-content").html("<center><h1 style='margin-top:20px;'>No Data Available</h1></center>");
                    $(".timelinelast").hide();
                    $("#journey_destination_time").hide();
                    $("#journey-departure").hide();
                    $("#journey_destination").html("");

                } else {

                    var gtfs_data = data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall;

                    $(".timelinelast").show();
                    $("#journey_destination_time").show();
                    $("#journey-departure").show();
                    $("#journey_destination").html("");

                    var siri_content = "";
                    var siri_content_size = 5;
                    if (typeof gtfs_data !== "undefined" && gtfs_data.length > 0) {
                        gtfs_data.reverse();
                        var journey_size = (data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall.length - 1);
                        $("#journey_destination").html(data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.DestinationName);
                        $("#journey_destination_time").html(time_format(data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall[0].ExpectedArrivalTime, true));

                        var iswheelchair;
                        if (data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.MonitoredCall.WheelchairBoarding == 1) {
                            iswheelchair = '<span style="background-color: #32a0ff;padding: 3px;color: white;border-radius: 5px;"><i aria-hidden="true" class="fa fa-wheelchair"></i></span>';
                        } else {
                            iswheelchair = '';
                        }
                        $("#journey-departure").html('<div class="row" style="font-size: 20px;text-align:center;background-color:d9d9d9;"><div id="bulletbig" class="col-md-1" style="left: -2px;"><div class="bullet big"><svg aria-hidden="true" viewBox="0 0 32 32" focusable="false"><path d="M16 4c6.6 0 12 5.4 12 12s-5.4 12-12 12S4 22.6 4 16 9.4 4 16 4zm0-4C7.2 0 0 7.2 0 16s7.2 16 16 16 16-7.2 16-16S24.8 0 16 0z"></path><circle cx="16" cy="16" r="6"></circle></svg></div></div><div class="col-md-3" style="text-align:left;font-size: 14px;">' + data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.MonitoredCall.StopPointName + '</div><div class="col-md-2" style="">' + time_format(data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.MonitoredCall.ExpectedArrivalTime) + '</div><div class="col-md-3" style="">' + iswheelchair + '</div><div class="col-md-3" style=""></div></div>');

                        //gtfs_data.pop();
                        gtfs_data = gtfs_data.slice(-3);
                        gtfs_data.forEach(el => {
                            var onward_date = new Date(el.AimedArrivalTime);
                            var iswheelchair;
                            if (el.WheelchairBoarding == 1) {
                                iswheelchair = '<span style="background-color: #32a0ff;padding: 3px;color: white;border-radius: 5px;"><i aria-hidden="true" class="fa fa-wheelchair"></i></span>';
                            } else {
                                iswheelchair = '';
                            }
                            siri_content += '<div class="row" style="font-size: 20px;text-align:center;background-color:d9d9d9;"><div class="col-md-1 timeline" style=""></div><div class="col-md-3" style="text-align:left;font-size: 14px;">' + el.StopPointName + '</div><div class="col-md-2" style="">' + time_format(el.ExpectedArrivalTime) + '</div><div class="col-md-3" style="">' + iswheelchair + '</div><div class="col-md-3" style=""></div></div>';
                        });
                    } else {
                        $("#journey-content").html("<center><h1 style='margin-top:20px;'>No Data Available</h1></center>");
                        $(".timelinelast").hide();
                        $("#journey_destination_time").hide();
                        $("#journey-departure").hide();
                        $("#journey_destination").html("");
                    }
                    $("#journey-content").html(siri_content);
                }
                //console.log(data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall);
                clearInterval(interval_journey);
                interval_journey = setInterval(function () {
                    ajax_screen(vehicle_id);
                }, 1000);
            }
        });
    }

    function setDataMockupScreen() {
        if (tableMockupScreen !== '') {
            $('#dataTableMockupScreen tbody').html('');
            $("#dataTableMockupScreen").dataTable().fnDestroy();
        }
        $.ajax({
            type: "GET",
            url: ipaddressGtfs_static + '/api/gtfs/vehicle-positions',
            cache: false,
            success: function (result) {
                var data = result['data'];
                var valTripID;
                for (var i = 0; i < data.length; i++) {
                    if (online_bus.indexOf(data[i].vehicleLabel) === -1) {
                        online_bus.push(data[i].vehicleLabel);
                        if (data[i].tripId !== null) {
                            trip_bus.push(data[i].vehicleLabel);
                            valTripID = data[i].tripId;
                        } else {
                            valTripID = '';
                        }
                        var html = '<tr>';
                        html += '<td>' + data[i].vehicleLabel + '</td>';
                        html += '<td>' + valTripID + '</td>';
                        html += '<td>' + data[i].location.latitude + ',' + data[i].location.longitude + '</td>';
                        html += '<td>' + timeConverter(data[i].timestamp) + '</td>';
                        html += '<td><a id="' + data[i].vehicleLabel + '" href="#modal-screen" class="tripbtn mb-xs mt-xs mr-xs modal-sizes btn btn-sm btn-primary">Trip</a></td>';
                        $('#dataTableMockupScreen tbody').append(html);
                        $('.modal-sizes').magnificPopup({
                            type: 'inline',
                            fixedContentPos: false,
                            fixedBgPos: true,
                            overflowY: 'auto',
                            closeBtnInside: true,
                            preloader: false,
                            midClick: true,
                            removalDelay: 300,
                            mainClass: 'my-mfp-zoom-in',
                            modal: true,
                            callbacks: {
                                close: function(){
                                    clearInterval(interval_time);
                                    clearInterval(interval_journey);
                                }
                            }
                        });
                    }
                }
                tableMockupScreen = $('#dataTableMockupScreen').dataTable({
                    "order": [
                        [1, "desc"]
                    ],
                    "columnDefs": [{
                        "targets": 1,
                    }],
                });
                $("#online_bus").text(online_bus.length);
                $("#trip_bus").text(trip_bus.length);
            }
        });
    }

    function timeConverter(UNIX_timestamp) {
        var a = new Date(UNIX_timestamp * 1000);
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var year = a.getFullYear();
        var month = a.getMonth() + 1;
        if (month < 10) {
            month = '0' + month;
        }
        var date = a.getDate();
        if (date < 10) {
            date = '0' + date;
        }
        var hour = a.getHours();
        if (hour < 10) {
            hour = '0' + hour;
        }
        var min = a.getMinutes();
        if (min < 10) {
            min = '0' + min;
        }
        var sec = a.getSeconds();
        if (sec < 10) {
            sec = '0' + sec;
        }
        var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec;
        return time;
    }

    setDataMockupScreen();

    $(document).on('click', '#refreshMockup', function () {
        setDataMockupScreen();
    });

    $(document).on('click', '.tripbtn', function () {
        vehicle_id = $(this).attr('id');
        $("#vehicle-no").html("<center><h1>Vehicle No: " + vehicle_id + "</h1></center>");
        $("#current_time").html(date_AMPM_AMERICA());
        
        clearInterval(interval_time);
        interval_time = setInterval(function () {
            $("#current_time").html(date_AMPM_AMERICA());
        }, 1000);

        ajax_screen(vehicle_id);
    });




});