            var vehicle_id;

            function convertTZ(date, tzString) {
                return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: tzString}));   
            }

            function time_format(dateStr,isheader){
                const date = new Date(dateStr);
                var server_date = new Date(date.toISOString().slice(0, -1));
                var now_date = convertTZ( new Date(), gtfs_timezone);
                var differenceTravel = server_date.getTime() - now_date.getTime();
                var seconds = Math.floor((differenceTravel) / (1000));
                // console.log("expected time arrival " + dateStr);
                // console.log("converted to chicago " + server_date);
                // console.log("local time to chicago " + now_date);
                // console.log("different time in second " + seconds);
                var minutes = Math.floor(seconds / 60);
                if(minutes < 0){
                    return 0;
                }
                if(isheader == true){
                    return minutes;
                }
                return new Date(seconds * 1000).toISOString().substr(14, 5);
            }

            function date_AMPM_AMERICA() {
                var new_date = convertTZ( new Date(), gtfs_timezone);
                var strTime = new_date.toLocaleString([], { hour: '2-digit', minute: '2-digit' });
                return strTime;
              }

            $(document).ready(function(){
                
                $(window).on('shown.bs.modal', function() { 
                    alert('shown');
                });

                $("#vehicle-no").html("<center><h1>Vehicle No: "+vehicle_id+"</h1></center>");
                $("#current_time").html(date_AMPM_AMERICA());
                
                setInterval(function() {
                    $("#current_time").html(date_AMPM_AMERICA());
                }, 1000);

                function send(){
                    $.ajax({
                        url: 'http://192.168.87.62:4002/api/gtfs/vehiclemonitoring?agency_id='+ gtfs_agency +'&format=json&vehicle_id='+vehicle_id,
                        crossDomain: true,
                        success:function(data)
                        {
                            if(typeof data.data.Gtfs  == "undefined"){
                                console.log("No data 1");
                                $("#journey-content").html("<center><h1 style='margin-top:20px;'>No Data Available</h1></center>");
                                $(".timelinelast").hide();
                                $("#journey_destination_time").hide();
                                $("#journey-departure").hide();
                                $("#journey_destination").html("");
                                
                            }else{

                                var gtfs_data = data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall;
                                
                                $(".timelinelast").show();
                                $("#journey_destination_time").show();
                                $("#journey-departure").show();
                                $("#journey_destination").html("");

                                var siri_content = "";
                                var siri_content_size = 5;
                                if (typeof gtfs_data !== "undefined" && gtfs_data.length > 0)
                                {
                                    gtfs_data.reverse();
                                    var journey_size = (data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall.length - 1);                                    
                                    $("#journey_destination").html(data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.DestinationName);
                                    $("#journey_destination_time").html(time_format(data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall[0].ExpectedArrivalTime,true));
                                        
                                    $("#journey-departure").html('<div class="row" style="font-size: 20px;text-align:center;background-color:d9d9d9;"><div class="col-1 bullet big" style="">    <svg aria-hidden="true" viewBox="0 0 32 32" focusable="false"><path d="M16 4c6.6 0 12 5.4 12 12s-5.4 12-12 12S4 22.6 4 16 9.4 4 16 4zm0-4C7.2 0 0 7.2 0 16s7.2 16 16 16 16-7.2 16-16S24.8 0 16 0z"></path><circle cx="16" cy="16" r="6"></circle></svg></div><div class="col-3" style="text-align:left;font-size: 14px;">'+data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.MonitoredCall.StopPointName+'</div><div class="col-2" style="">'+time_format(data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.MonitoredCall.ExpectedArrivalTime)+'</div><div class="col-3" style=""></div><div class="col-3" style="">1A/BK</div></div>');
                                    
                                    //gtfs_data.pop();
                                   gtfs_data = gtfs_data.slice(-3);
                                    gtfs_data.forEach(el => {
                                        var onward_date = new Date(el.AimedArrivalTime);
                                        siri_content += '<div class="row" style="font-size: 20px;text-align:center;background-color:d9d9d9;"><div class="col-1 timeline" style=""></div><div class="col-3" style="text-align:left;font-size: 14px;">'+el.StopPointName+'</div><div class="col-2" style="">'+time_format(el.ExpectedArrivalTime)+'</div><div class="col-3" style=""></div><div class="col-3" style="">1A/BK</div></div>';
                                    });
                                
                                 }else{
                                    $("#journey-content").html("<center><h1 style='margin-top:20px;'>No Data Available</h1></center>");
                                    $(".timelinelast").hide();
                                    $("#journey_destination_time").hide();
                                    $("#journey-departure").hide();
                                    $("#journey_destination").html("");
                                 }
                            $("#journey-content").html(siri_content);
                           }
                            //console.log(data.data.Gtfs.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity[0].MonitoredVehicleJourney.OnwardCalls.OnwardCall);
                            setTimeout(function(){
                                send();
                            }, 5000);
                        }
                    });
                }
                send();
            });