$(document).ready(function(){   
    $('.form_datetime').datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        autoclose: true,
        todayBtn: true,
        defaultDate: getNowDate(),
        fontAwesome: 'font-awesome'
    });

    $('input[name="optionsDestination"]').click(function(e) {
        var values = $(this).val();
        if(values=='1'){
            $('#byBus').css('display', 'block');
            $('#byRoute').css('display', 'none');
        }else{
            $('#byBus').css('display', 'none');
            $('#byRoute').css('display', 'block');
        }
    });

    function getDataRoute(){
        $.ajax({
            type: "GET",
            url: '',
            cache: false,
            success: function(result){
                console.log(result);
            }
        });
    }

    function getDataBus(){
        $.ajax({
            type: "GET",
            url: '',
            cache: false,
            success: function(result){
                console.log(result);
            }
        });
    }

    $('#dataTableAnnounceAlert').dataTable();

    $('#dataTableAnnounceAlertList').dataTable();
});