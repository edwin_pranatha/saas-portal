        $(document).ready(function(){
            var map = L.map('map', { zoomControl: false }).setView([52.37857546345924,  4.795741960682932], 11);
                L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '',
                    maxZoom: 18,
                }).addTo(map);

            $('.form_datetime').datetimepicker({
                format: "yyyy-mm-dd hh:ii",
                autoclose: true,
                todayBtn: true,
                defaultDate: getNowDate(),
                fontAwesome: 'font-awesome'
            });
        });