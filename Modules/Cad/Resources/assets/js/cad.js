var markerArr = new Array();
var markerStopArr = new Array();
var nomorlist = 0;
var status = false;
var statusClickMarker = false;
var myInterval;
var LeafIcon = L.Icon.extend({
    options: {
        iconSize:     [32, 37],
        iconAnchor:   [16, 16],
        popupAnchor:  [0, -16]
    }
});

var LeafIconStop = L.Icon.extend({
    options: {
        iconSize:     [26, 37],
        iconAnchor:   [16, 16],
        popupAnchor:  [0, -16]
    }
});

var busIcon = new LeafIcon({iconUrl: '/assets/Cad/assets/images/bus.png'});

var stopIcon = new LeafIconStop({iconUrl: '/assets/Cad/assets/images/stopicon.png'});

var mapOptions = {
    zoomControl: true,
    contextmenu: true,
    fullscreenControl: {
        pseudoFullscreen: true
    },
}

var map = L.map('map', mapOptions).setView([gtfs_map_lat, gtfs_map_long], 11);
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '',
    maxZoom: 18,
}).addTo(map);

map.on('zoomend',function(e) {
    /*console.log(e.target.getCenter());
    console.log(e.target.getZoom());*/
    //$('.fa-caret-down').click();
});

map.on('dragstart',function(e){
    $('.fa-caret-down').click();
});

map.on('dragend',function(e){
    $('.fa-caret-up').click();
});

map.on('fullscreenchange', function () {
    if (map.isFullscreen()) {
        console.log('Full');
        $('#controlbox').css({ 'z-index': '100000', 'left': '-220px', 'top': '-115px' });
        $('#controlboxRight').css({ 'z-index': '100000', 'right': '30px', 'top': '-115px' });
    } else {
        console.log('Non Full');
        $('#controlbox').css({ 'z-index': '', 'left': '', 'top': '' });
        $('#controlboxRight').css({ 'z-index': '', 'left': '', 'top': '' });
    }
});

var onMarkerClick = function(e) {
    
}

function clickMarketBus() {
    $('.bus_list').removeClass('markBus');
    var value = this.options.bus_id;
    var agencyID = this.options.agency_id;
    var dat = {
        "BMS":[{
            "id": value,
        }]
    }
    findMarker(dat, value, agencyID);
    showTripData(value, agencyID);
    var data = value;
    var nomorMark = $('#vehicle-list tr:contains('+data+')')[0].rowIndex;
    $('#vehicle-list tr:eq(' + nomorMark + ')').addClass('markBus');
}

var markers = {};

function setMarkers(data) {
    data.BMS.forEach(function (obj) {
        if (!markers.hasOwnProperty(obj.id)) {
            markers[obj.id] = new L.Marker([obj.lat, obj.long], {bus_id: obj.id, agency_id: obj.agencyid, route_id: obj.routeid, trip_id: obj.tripid, stop_id: obj.stopid, icon: busIcon})
            /*markers[obj.id].bindPopup(obj.id, {
                autoPan: false,
            });*/
            markers[obj.id].on('click', clickMarketBus);
            markers[obj.id].addTo(map);
            markers[obj.id].previousLatLngs = [];
            if(statusClickMarker===true){
                $('[src="/assets/Cad/assets/images/bus.png"]').addClass('hideMarkerMap');
            }
        } else {
            markers[obj.id].on('click', onMarkerClick);
            markers[obj.id].previousLatLngs.push(markers[obj.id].getLatLng());
            markers[obj.id].slideTo([obj.lat, obj.long], {
                duration: 2500
            });

            //markers[obj.id].getPopup().setContent("Trigger!");
            //markers[obj.id].getPopup().update();
            //markers[obj.id].openPopup();

            //markers[obj.id].setLatLng([obj.lat, obj.long]);
        }
    });
}

function findMarker(data, value, agencyID) {
    data.BMS.forEach(function (obj) {
        if (!markers.hasOwnProperty(obj.id)) {
            markers[obj.id] = new L.Marker([obj.lat, obj.long], {bus_id: obj.id,icon: busIcon})
            markers[obj.id].bindPopup(obj.id, {
                autoPan: false,
            });
            markers[obj.id].addTo(map);
            markers[obj.id].previousLatLngs = [];
        } else {
            $('[src="/assets/Cad/assets/images/bus.png"]').addClass('hideMarkerMap');
            $('[src="/assets/Cad/assets/images/bus.png"]').removeClass("showMarkerMap");
            $('.showAllBus').css('display', 'block');
            markers[obj.id]._icon.classList.add("showMarkerMap");
            statusClickMarker = true;
            /*var popup = L.popup({
                    closeButton: true,
                    autoClose: true,
                    className: "custom-popup"
                })
                .setLatLng(markers[obj.id].getLatLng())
                .setContent('<section class="panel"><div class="panel-body" style="background: red;color: #FFF;font-weight: bold;">VIA <span id="idbus-popup"></span> <span id="title-popup"></span></div></section><div class="overTableView"><div class="overTableViewInfo"></div><table class="vehicle-stops"><thead><tr><td class="vehicle-stop"><b>Stop</b></td><td><b>Estimated Arrival Time</b></td></tr></thead><tbody></tbody></table></div>')
                .openOn(map);*/
            map.setView(markers[obj.id].getLatLng(), 13);
            var models = markers[obj.id].options;
            $('.fa-caret-up').click();
            if($(".leaflet-popup-close-button")[0]!==undefined){
                $(".leaflet-popup-close-button")[0].click();
            }
            $('#vehicleIDBus').val(value);
            $('#controlboxRight').css('display', 'block');
            clearInterval(myInterval);
            myInterval = setInterval(showStopData, 1000, value, agencyID);
            showDetourData(agencyID, models.route_id, models.trip_id, models.stop_id, 1);
        }
    });
}

var nomor = 0;
var polyline;

function clickMarkDistance(){
    var stopid = this.options.stop_id;
    var vehicleid = this.options.vehicle_id;
    var agencyid = this.options.agency_id;
    var array = {};
    array['lat'] = this._latlng.lat;
    array['lng'] = this._latlng.lng;
    var popup = L.popup({
        closeButton: true,
        autoClose: true,
        className: "custom-popup"
    })
    .setLatLng(array)
    .setContent('<div style="width: 135px;"><p>Stop Predictions [<span id="stopRef"></span>]:</p> <p style="text-align: center;margin: -12px 0 !important;font-size: 16px;font-weight: bold;"><b id="stopNameTitle"></b></p><p style="text-align: center;">Arrival: <span id="predictStop"></span> <span data-id="2305"></span><br>- Vehicle: <span id="vehicleidStop"></span></p></div>')
    .openOn(map);
    setStopData(agencyid, stopid, vehicleid);
    //showDetourData(null, null, null, stopid, 2);
}

function setStopData(agencyid, stopid, vehicleid) {
    $.ajax({
        type: "GET",
        url: ipaddressGtfs_dynamic + '/api/gtfs/stopmonitoring?agency_id=' + agencyid + '&format=json&stop_id=' + stopid + '&vehicle_id=' + vehicleid,
        cache: false,
        success: function(result){
            if(result['ServiceDelivery']!==undefined){
                var data = result['ServiceDelivery']['StopMonitoringDelivery'];
                if(data['MonitoredStopVisitCancellation']!==undefined){
                    var monitoredStopVisitCancel = data['MonitoredStopVisitCancellation'];
                    $('#vehicleidStop').text(vehicleid);
                    $('#predictStop').text('Cancelled');
                    $('#stopNameTitle').text('-');
                    $('#stopRef').text('-');
                }else{
                    var monitoredStopVisit = data['MonitoredStopVisit'][0]['MonitoredVehicleJourney']['MonitoredCall'];
                    var stopref = monitoredStopVisit['StopPointRef'];
                    var stopname = monitoredStopVisit['StopPointName'];
                    var stoparrivaltime = monitoredStopVisit['ExpectedArrivalTime'];
                    $('#stopRef').text(stopref);
                    $('#stopNameTitle').text(stopname);
                    $('#vehicleidStop').text(vehicleid);
                    $('#predictStop').text(sumTimeStop(stoparrivaltime));
                }
            }else{
                $('#vehicleidStop').text(vehicleid);
                $('#predictStop').text('0 min');
                $('#stopNameTitle').text('The bus has passed');
                $('#stopRef').text('-');
            }
        }
    });
}

function showTripData(idvalues, agencyID) {
    if(nomor > 0){
        map.removeLayer(polyline);
    }
    if(markerArr.length > 0){
        for(var i = 0; i < markerArr.length; i++) {
            map.removeLayer(markerArr[i]);
        }
        markerArr = new Array();
        markerStopArr = new Array();
    }
    $.ajax({
        type: "GET",
        url: ipaddressGtfs_static + '/api/gtfs/shape-journey-stop?agency_id=' + agencyID + '&vehicle_id=' + idvalues,
        cache: false,
        success: function(result){
            var data = result['data'][0];
            if(data!==undefined){
                var shapes = data['shapes'];
                var polylinePoints = [];
                for (var i = 0; i < shapes.length; i++) {
                    polylinePoints.push([shapes[i].latitude, shapes[i].longitude]);
                }
                var stops = data['stops'];
                for (var i = 0; i < stops.length; i++) {
                    var newMarker = new L.marker([stops[i].latitude, stops[i].longitude], {agency_id: agencyID,vehicle_id: idvalues,stop_id: stops[i].stopId,icon: stopIcon});
                    markerArr.push(newMarker);
                    markerStopArr[stops[i].stopId] = stops[i].latitude + ',' + stops[i].longitude;
                    map.addLayer(markerArr[i]);
                    newMarker.on('click', clickMarkDistance);
                }
                polyline = L.polyline(polylinePoints, {
                    color: 'green',
                    weight: 10,
                    opacity: 1.0
                }).addTo(map);
                nomor++;
            }
        }
    });
}

function showStopData(idvalues, agencyID) {
    $.ajax({
        type: "GET",
        url: ipaddressGtfs_static + '/api/gtfs/vehiclemonitoring?agency_id=' + agencyID + '&vehicle_id=' + idvalues + '&format=json',
        cache: false,
        success: function(result){
            var message = result['message'];
            if(message==='No data available.'){
                $('#idbus-popup').text(idvalues);
                $('.vehicle-stops').css('display','none');
                $('.overTableViewInfo').html('<p>No Data Stop</p>');
                clearInterval(myInterval);
            }else{
                $('.vehicle-stops').css('display','block');
                $('.overTableViewInfo').html('');
                var data = result['data']['Gtfs']['ServiceDelivery']['VehicleMonitoringDelivery'][0]['VehicleActivity'][0]['MonitoredVehicleJourney'];
                var destinationName = data['DestinationName'];
                $('#title-popup').text('-> ' + destinationName);
                $('#idbus-popup').text(idvalues);
                var monitored = data['MonitoredCall'];
                var onward = data['OnwardCalls']['OnwardCall'];
                $(".vehicle-stops > tbody").html('');
                if(monitored.StopScheduleRelationship==='SKIPPED'){
                    var monitoredTime = "SKIPPED";
                }else{
                    var monitoredTime = sumTimeStop(monitored.ExpectedArrivalTime);
                }
                $(".vehicle-stops > tbody").append("<tr><td><a href='javascript:void(0)' class='stopPoint' id='" + monitored.StopPointRef + "-" + agencyID + "-" + idvalues + "'>" + monitored.StopPointName + "</a></td><td>" + monitoredTime + "</td></tr>");
                if(onward !== undefined){
                    for (var i = 0; i < onward.length; i++) {
                        if(onward[i].StopScheduleRelationship==='SKIPPED'){
                            var onwardTime = "SKIPPED";
                        }else{
                            var onwardTime = sumTimeStop(onward[i].ExpectedArrivalTime);
                        }
                        $(".vehicle-stops > tbody").append("<tr><td><a href='javascript:void(0)' class='stopPoint' id='" + onward[i].StopPointRef + "-" + agencyID + "-" + idvalues + "'>" + onward[i].StopPointName + "</a></td><td>" + onwardTime + "</td></tr>");
                    }
                }
                nomorlist = $('.vehicle-stops').find('tr').length;
                status = false;
            }
        }
    });
}

function showDetourData(agencyID, routeID, tripID, stopID, status) {
    if(status===1){
        var urlDetour = ipaddressGtfs_static + '/api/gtfs/service-alert?route_id=' + routeID + '&trip_id=' + tripID + '&stop_id=' + stopID;
    }else{
        var urlDetour = ipaddressGtfs_static + '/api/gtfs/service-alert?stop_id=' + stopID;
    }
    $.ajax({
        type: "GET",
        url: urlDetour,
        cache: false,
        success: function(result){
            var data = result['data'];
            //$(".vehicle-detour > tbody").html('');
            $(".vehicle-detour").html('');
            if(data.length > 0){
                $('.vehicle-detour').css('display','block');
                $('.overTableViewInfoDetour').html('');
                var detour = [];
                for (var i = 0; i < data.length; i++) {
                    //$(".vehicle-detour > tbody").append("<tr><td>" + data[i].headerText + "</td><td>" + data[i].cause + "</td><td>" + data[i].descriptionText + "</td></tr>");
                    if(detour.includes(data[i].headerText)===false){
                        detour.push(data[i].headerText);
                        var html  = '<div class="panel panel-accordion">';
                            html += '<div class="panel-heading">';
                            html += '<h4 class="panel-title">';
                            html += '<a class="accordion-toggle" data-toggle="collapse" data-parent="#collapseDetour' + i + '" href="#collapseDetour' + i + '">';
                            html += data[i].headerText;
                            html += '</a>';
                            html += '</h4>';
                            html += '</div>';
                            html += '<div id="collapseDetour' + i + '" class="accordion-body collapse in">';
                            html += '<div class="panel-body">';
                            html += data[i].descriptionText + '<br><br><p><b>' + data[i].cause + '</b>, ' + data[i].start.replace('T', ' ').replace('Z', '') + '-' + data[i].end.replace('T', ' ').replace('Z', '') + '</p>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        $(".vehicle-detour").append(html);
                    }
                }
            }else{
                $('.vehicle-detour').css('display','none');
                $('.overTableViewInfoDetour').html('<p>No Data Detour</p>');
            }
        }
    });
}

function sumTimeStop(val) {
    var date1 = new Date(val);
    var date2 = new Date().toLocaleString('en-US', { timeZone: gtfs_timezone });
        date2 = new Date(date2);
    var dateStr =
        date2.getFullYear() + "-" + ("00" + (date2.getMonth() + 1)).slice(-2) + "-" + ("00" + date2.getDate()).slice(-2) + " " + ("00" + date2.getHours()).slice(-2) + ":" + ("00" + date2.getMinutes()).slice(-2) + ":" + ("00" + date2.getSeconds()).slice(-2);
        date2 = dateStr.replace(' ','T');
        date2 = date2 + 'Z';
        date2 = new Date(date2);
        
    var diffInSeconds = Math.abs(date1 - date2) / 1000;
    var days = Math.floor(diffInSeconds / 60 / 60 / 24);
    var hours = Math.floor(diffInSeconds / 60 / 60 % 24);
    var minutes = Math.floor(diffInSeconds / 60 % 60);
    var seconds = Math.floor(diffInSeconds % 60);
    var milliseconds = Math.round((diffInSeconds - Math.floor(diffInSeconds)) * 1000);
    
    if(date1.getTime() > date2.getTime()){
        return ('0' + hours).slice(-2) + ':' + ('0' + minutes).slice(-2) + ':' + ('0' + seconds).slice(-2);
    }else{
        return 'PASSED';//'00:00:00'
    }
}

var online_bus = [];

function showDataMarker(){
    var vehicleidbus = $('#vehicleIDBus').val();
    var url;
    if(vehicleidbus!==null && vehicleidbus!==''){
        url = ipaddressGtfs_static + '/api/gtfs/vehicle-positions?vehicle_id=' + vehicleidbus;
    }else{
        url = ipaddressGtfs_static + '/api/gtfs/vehicle-positions';
    }
    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        success: function(result){
            var data = result['data'];
            for (var i = 0; i < data.length; i++) {
                if (online_bus.indexOf(data[i].vehicleLabel) === -1){
                    online_bus.push(data[i].vehicleLabel);
                    if(data[i].tripId!==null||data[i].agencyId){
                        var addhtml = ' <i class="fa fa-bus" style="float: right;"></i>';
                    }else{
                        var addhtml = '';
                    }
                    $("#myTable").append("<tr class='bus_list' id='" + data[i].agencyId + "'><td>" + data[i].vehicleLabel + "" + addhtml + "</td></tr>");
                } 
                var updatedJson = {
                    "BMS":[{
                        "id": data[i].vehicleLabel,
                        "agencyid": data[i].agencyId,
                        "routeid": data[i].routeId,
                        "tripid": data[i].tripId,
                        "stopid": data[i].stopId,
                        "lat": data[i].location.latitude,
                        "long": data[i].location.longitude
                    }]
                }
                setMarkers(updatedJson);
            }
        }
    });
    $("#online_bus").html("Total Online: "+online_bus.length);
}

setInterval(showDataMarker, 1000);

$(document).on('click','.detailNomorList',function(){
    for (var i = 0; i < nomorlist; i++) {
        $('.vehicle-stops').find('tr').eq(i).css('display', 'revert');
        $('.detailNomorList').css('display', 'none');
        $('.detailNomorListHide').css('display', 'block');
    }
});

$(document).on('click','.detailNomorListHide',function(){
    for (var i = 0; i < nomorlist; i++) {
        if(i > 5){
            $('.vehicle-stops').find('tr').eq(i).css('display', 'none');
        }
        $('.detailNomorList').css('display', 'block');
        $('.detailNomorListHide').css('display', 'none');
    }
});

$(document).on('click','.showAllBus',function(){
    $('[src="/assets/Cad/assets/images/bus.png"]').removeClass('hideMarkerMap');
    $('[src="/assets/Cad/assets/images/bus.png"]').removeClass("showMarkerMap");
    $('.showAllBus').css('display', 'none');
    $('.bus_list').removeClass('markBus');
    map.closePopup();
    $('.leaflet-popup-close-button').click();
    if(polyline !== undefined){
        map.removeLayer(polyline);
    }
    if(markerArr.length > 0){
        for(var i = 0; i < markerArr.length; i++) {
            map.removeLayer(markerArr[i]);
        }
    }
    markerArr = new Array();
    statusClickMarker = false;
    $('#vehicleIDBus').val('');
    clearInterval(myInterval);
    $('#controlboxRight').css('display', 'none');
});

$(document).on('click','.stopPoint',function(){
    var id = $(this).attr('id');
    var split = id.split('-');
    var stopid = split[0];
    var vehicleid = split[2];
    var agencyid = split[1];
    $.ajax({
        type: "GET",
        url: ipaddressGtfs_dynamic + '/api/gtfs/stopmonitoring?agency_id=' + agencyid + '&format=json&stop_id=' + stopid + '&vehicle_id=' + vehicleid,
        cache: false,
        success: function(result){
            if(result['ServiceDelivery']!==undefined){
                var data = result['ServiceDelivery']['StopMonitoringDelivery']['MonitoredStopVisit'][0]['MonitoredVehicleJourney']['MonitoredCall'];
                var stopref = data['StopPointRef'];
                var stopname = data['StopPointName'];
                var stoparrivaltime = data['ExpectedArrivalTime'];
                var array = {};
                var latlng = result['ServiceDelivery']['StopMonitoringDelivery']['MonitoredStopVisit'][0]['MonitoredVehicleJourney']['VehicleLocation'];
                var splitLatLng = markerStopArr[stopid].split(',');
                array['lat'] = splitLatLng[0];
                array['lng'] = splitLatLng[1];
                var popup = L.popup({
                    closeButton: true,
                    autoClose: true,
                    className: "custom-popup"
                })
                .setLatLng(array)
                .setContent('<div style="width: 135px;"><p>Stop Predictions [<span id="stopRef"></span>]:</p> <p style="text-align: center;margin: -12px 0 !important;font-size: 16px;font-weight: bold;"><b id="stopNameTitle"></b></p><p style="text-align: center;">Arrival: <span id="predictStop"></span> <span data-id="2305"></span><br>- Vehicle: <span id="vehicleidStop"></span></p></div>')
                .openOn(map);
                $('#stopRef').text(stopref);
                $('#stopNameTitle').text(stopname);
                $('#vehicleidStop').text(vehicleid);
                $('#predictStop').text(sumTimeStop(stoparrivaltime));
            }else{
                $('#vehicleidStop').text(vehicleid);
                $('#predictStop').text('0 min');
                $('#stopNameTitle').text('The bus has passed');
                $('#stopRef').text('-');
            }
        }
    });
});

$(document).on('click','.closeControlDetour',function(){
    $('#controlboxRight').css('display', 'none');
});

$(document).ready(function(){
    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $(document).on('click','.bus_list',function(){
        $('.bus_list').removeClass('markBus');
        $(this).addClass('markBus');
        var value = $(this).html().replace("<td>","").replace("</td>","").replace(' <i class="fa fa-bus" style="float: right;"></i>',"");
        var agencyID = $(this).attr('id');
        var dat = {
            "BMS":[{
                "id": value,
            }]
        }
        findMarker(dat, value, agencyID);
        showTripData(value, agencyID);
    });
});