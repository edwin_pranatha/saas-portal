Instruction :
1. Clone the repository
2. Run following command to install the dependencies
``composer install ``
3. Adjust the environment variables in .env file
4. Run to update the cache
``php artisan optimize:clear``
5. Run command to building master tenant
``php artisan migrate:fresh --seed``
6. Open url in browser depending on your "TENANCY_MASTER_HOSTNAME" value, if you plant to run locally adjust to localhost here
7. Run following command to enable the modules
<br>``php artisan tenant:module-install Fleet master``
<br>``php artisan tenant:module-install Gtfs master``
<br>``php artisan tenant:module-install Cad master``
<br>``php artisan tenant:module-install Infovision master``
<br>``php artisan tenant:module-enable Fleet master``
<br>``php artisan tenant:module-enable Gtfs master``
<br>``php artisan tenant:module-enable Cad master``
<br>``php artisan tenant:module-enable Infovision master``

<br>

Command :
<br>
    ``php artisan tenant:module-list`` to list all the modules
<br>
    ``php artisan tenant:module-install <ModuleName> <TenantName>`` to install specific module for specific tenant
<br>
    ``php artisan tenant:module-remove <ModuleName> <TenantName>`` to remove specific module for specific tenant
<br>
<br>

Development :<br />
    Laravel Mix currently only need to be run on root directory<br />
    ``
    npm run watch --module=module_name
    ``<br />
    Example : ``
    npm run watch --module=Core
    ``<br /><br />
    Run this to compile mix for theme  
    Note : npm run without argument will also running mix as theme<br />
    ``
    npm run watch --theme=default
    ``<br />

    